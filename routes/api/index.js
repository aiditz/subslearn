var express = require('express');
var router = express.Router();

router.use('/subtitles', require('./subtitles'));
router.use('/storage', require('./storage'));

module.exports = router;
