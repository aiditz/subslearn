const fs = require('fs-extra');
const path = require('path');

const SUBTITLES_PATH = path.resolve(__dirname, '../../../subs');

class SubtitlesService {

  constructor() {
    this.files = fs.readdirSync(SUBTITLES_PATH);
  }

  async loadSubs(filename) {
    return (await fs.readFile(SUBTITLES_PATH + '/' + filename)).toString();
  }

}

module.exports = new SubtitlesService();