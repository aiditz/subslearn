const subtitlesService = require('./subtitles.service');

module.exports = {
  async getFileNames(req, res, next) {
    const data = subtitlesService.files;

    res.json(data);
  },

  async getFile(req, res, next) {
    try {
      const data = await subtitlesService.loadSubs(req.query.fileName);

      res.json(data);
    } catch (err) {
      next(err);
    }
  }
};