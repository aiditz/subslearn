var express = require('express');
var router = express.Router();
const controller = require('./subtitles.controller');

router.get('/getFileNames', controller.getFileNames);
router.get('/getFile', controller.getFile);

module.exports = router;
