const fs = require('fs-extra');
const path = require('path');

const STORAGE_FILENAME = path.resolve(__dirname, '../../../storage/key-value.txt');

class StorageService {

  constructor() {
    this.content = {};

    let data;

    try {
      data = fs.readFileSync(STORAGE_FILENAME).toString();
    } catch (err) {
      return;
    }

    data.split('\n').forEach(line => {
      line = line.trim();

      if (!line) {
        return;
      }

      const data = line.split('=');

      this.content[data[0]] = data[1] || '';
    });
  }

  getValue(key) {
    return this.content[key] || '';
  }

  setValue(key, value) {
    this.content[key] = value;
    this.save();
  }

  save() {
    const arr = [];

    for (let key of Object.keys(this.content)) {
      arr.push(key + '=' + this.content[key]);
    }

    const str = arr.join('\n');

    fs.writeFile(STORAGE_FILENAME, str)
      .catch(err => console.error('Unable write storage file:', err.message));
  }

}

module.exports = new StorageService();