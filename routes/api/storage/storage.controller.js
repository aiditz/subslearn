const storageService = require('./storage.service');

module.exports = {
  async getValue(req, res, next) {
    res.json(storageService.getValue(req.query.key));
  },

  async setValue(req, res, next) {
    storageService.setValue(req.body.key, req.body.value);

    res.json(true);
  }
};