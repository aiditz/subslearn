var express = require('express');
var router = express.Router();
const controller = require('./storage.controller');

router.post('/set', controller.setValue);
router.get('/get', controller.getValue);

module.exports = router;
