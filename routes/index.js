var express = require('express');
var router = express.Router();

const subtitlesService = require('./api/subtitles/subtitles.service');

router.get('/', function(req, res, next) {
  res.render('index', { subsList: subtitlesService.files });
});

router.use('/api', require('./api'));

module.exports = router;
