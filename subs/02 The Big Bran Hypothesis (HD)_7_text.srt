1
00:00:02,127 --> 00:00:04,088
Here we go.
Pad thai, no peanuts.

2
00:00:04,208 --> 00:00:05,544
But does it have peanut oil?

3
00:00:05,767 --> 00:00:06,917
I'm not sure.

4
00:00:07,042 --> 00:00:09,631
Everyone keep an eye on Howard
in case he starts to swell up.

5
00:00:11,134 --> 00:00:13,682
Since it's not bee season,
you can have my epinephrine.

6
00:00:14,884 --> 00:00:17,501
- Are there any chopsticks.
- No need. This is Thai food.

7
00:00:17,709 --> 00:00:18,827
Here we go.

8
00:00:19,073 --> 00:00:21,802
Thailand has had the fork since
the latter half of the 19th Century.

9
00:00:21,992 --> 00:00:24,220
Interestingly  they don't put the fork
in their mouth--

10
00:00:24,396 --> 00:00:27,355
they use it to put the food on a spoon
which then goes into their mouth.

11
00:00:28,889 --> 00:00:30,589
Ask him for a napkin.
I dare you.

12
00:00:32,749 --> 00:00:33,749
I'll get it.

13
00:00:34,351 --> 00:00:36,042
Do I look puffy? I feel puffy.

14
00:00:37,952 --> 00:00:39,402
- Hey, Leonard.
- Oh, hi, Penny.

15
00:00:39,522 --> 00:00:41,941
- Am I interrupting?
- No. You're not swelling, Howard.

16
00:00:42,570 --> 00:00:44,684
Look at my fingers.
They're like Vienna sausages.

17
00:00:45,397 --> 00:00:48,224
- Sounds like you have company.
- They're not going anywhere.

18
00:00:49,663 --> 00:00:52,234
So, you're coming home from work.
That's great. How was work?

19
00:00:52,440 --> 00:00:53,990
You know, it's a Cheesecake Factory.

20
00:00:54,166 --> 00:00:56,103
People order cheesecake
and I bring it to them.

21
00:00:56,560 --> 00:01:00,999
So you kind of act like
a carbohydrate delivery system.

22
00:01:03,115 --> 00:01:05,379
Yeah. Call it whatever you want,
I get minimum wage.

23
00:01:08,043 --> 00:01:09,806
I was wondering if you could help me out

24
00:01:09,959 --> 00:01:11,259
- with something.
- Yes.

25
00:01:12,497 --> 00:01:13,918
Great. I'm having some furniture

26
00:01:14,094 --> 00:01:16,008
delivered tomorrow
and I may not be here, so...

27
00:01:18,424 --> 00:01:19,424
Hello.

28
00:01:23,956 --> 00:01:25,726
- I'm sorry?
- Haven't you ever been told

29
00:01:25,855 --> 00:01:27,614
how beautiful you are
in flawless Russian?

30
00:01:28,397 --> 00:01:29,419
No, I haven't.

31
00:01:29,599 --> 00:01:30,599
Get used to it.

32
00:01:31,757 --> 00:01:33,322
Yeah. I probably won't.

33
00:01:34,283 --> 00:01:35,446
- Hey, Sheldon.
- Hi.

34
00:01:35,683 --> 00:01:36,693
Hey, Raj.

35
00:01:39,059 --> 00:01:40,394
Still not talking to me, huh?

36
00:01:40,764 --> 00:01:42,684
Don't take it personally,
it's his pathology.

37
00:01:42,838 --> 00:01:44,068
He can't talk to women.

38
00:01:44,222 --> 00:01:46,388
It can't talk to atractive woman,
or in your case,

39
00:01:46,517 --> 00:01:48,393
a cheesecake- scented goddess.

40
00:01:49,068 --> 00:01:51,363
So there's gonna be
some furniture delivered?

41
00:01:51,517 --> 00:01:53,581
If it gets here and I'm not here,
could you sign

42
00:01:53,774 --> 00:01:55,414
and have them put it in my apartment?

43
00:01:55,617 --> 00:01:56,640
No problem.

44
00:01:56,813 --> 00:01:58,513
Great. Here's my spare key.
Thank you.

45
00:02:01,120 --> 00:02:02,129
Penny, wait.

46
00:02:05,550 --> 00:02:08,017
If you don't have any other plans,
do you want to join us

47
00:02:08,177 --> 00:02:10,322
for Thai food
and a Superman movie marathon?

48
00:02:10,729 --> 00:02:13,315
A marathon?
How many Superman movies are there?

49
00:02:13,980 --> 00:02:15,366
You're kidding, right?

50
00:02:16,874 --> 00:02:19,288
I do like the one where Lois Lane
falls from the helicopter

51
00:02:19,429 --> 00:02:22,000
and Superman swooshes down
and catches her. Which one was that?

52
00:02:22,186 --> 00:02:23,231
One.

53
00:02:24,333 --> 00:02:27,334
You realize that scene
was rife with scientific inaccuracy.

54
00:02:27,974 --> 00:02:29,553
Yes, I know, men can't fly.

55
00:02:29,800 --> 00:02:31,254
No.
Let's assume that they can.

56
00:02:33,071 --> 00:02:34,197
Lois Lane is falling,

57
00:02:34,363 --> 00:02:36,987
accelerating at an initial rate
of 32 feet per second per second.

58
00:02:37,200 --> 00:02:40,186
Superman swoops down to save her
by reaching out two arms of steel.

59
00:02:40,394 --> 00:02:42,035
Miss Lane, who is now traveling

60
00:02:42,257 --> 00:02:43,829
at approximately 120 miles an hour,

61
00:02:44,041 --> 00:02:46,615
hits them and is immediately
sliced into three equal pieces.

62
00:02:49,320 --> 00:02:52,462
Unless Superman matches
her speed and decelerates.

63
00:02:52,821 --> 00:02:55,916
In what space, sir?
She's two feet above the ground.

64
00:02:56,722 --> 00:02:59,246
Frankly, if he really loved her,
he'd let her hit the pavement.

65
00:02:59,399 --> 00:03:00,812
It'd be a more merciful death.

66
00:03:01,702 --> 00:03:05,745
Excuse me, your entire argument
is predicated on the assumption

67
00:03:05,956 --> 00:03:08,048
that Superman's flight
is a feat of strength.

68
00:03:08,176 --> 00:03:10,361
Are you listening  to yourself?
It is well established

69
00:03:10,514 --> 00:03:12,040
that his flight is a feat of strength.

70
00:03:12,164 --> 00:03:14,444
It is an extension of his
ability to leap tall buildings,

71
00:03:14,571 --> 00:03:17,014
an ability he derives from
exposure to Earth's yellow sun.

72
00:03:17,155 --> 00:03:18,841
And how does he fly at night?

73
00:03:19,193 --> 00:03:21,295
A combination
of the moon's solar reflection

74
00:03:21,436 --> 00:03:23,996
and the energy-storage capacity
of Kryptonian skin cells.

75
00:03:25,780 --> 00:03:27,236
I'm just gonna go wash up.

76
00:03:27,734 --> 00:03:30,716
I have 2,600 comic books in there.
I challenge you

77
00:03:30,881 --> 00:03:33,086
to find a single reference
to Kryptonian skin cells.

78
00:03:33,297 --> 00:03:34,578
Challenge accepted.

79
00:03:38,568 --> 00:03:39,695
We're locked out.

80
00:03:40,305 --> 00:03:41,993
Also, the pretty girl left.

81
00:03:44,359 --> 00:03:46,993
The Big Bang Theory
o/~ Our whole universe
was in a hot, dense state o/~

82
00:03:47,113 --> 00:03:50,513
Season 1 Episode 01
o/~ Then nearly 14 billion years
ago expansion started... Wait! o/~

83
00:03:50,731 --> 00:03:52,116
The Big Bran Hypothesis
o/~ The Earth began to cool o/~

84
00:03:52,292 --> 00:03:54,984
o/~ The autotrophs began to drool,
Neanderthals developed tools o/~

85
00:03:55,140 --> 00:03:57,261
o/~ We built the Wall o/~
o/~ We built the pyramid so/~

86
00:03:57,511 --> 00:03:59,715
o/~ Math, Science, History,
unraveling the mystery o/~

87
00:04:00,001 --> 00:04:02,244
o/~ That all started with a big bang o/~

88
00:04:08,947 --> 00:04:11,691
Her apartment's on the fourth floor
but the elevator's broken, so...

89
00:04:11,906 --> 00:04:14,231
You're just gonna be done?
Okay. Cool. Thanks

90
00:04:15,841 --> 00:04:17,365
We'll just bring it up ourselves.

91
00:04:17,577 --> 00:04:18,889
I hardly think so.

92
00:04:20,310 --> 00:04:21,320
Why not?

93
00:04:21,790 --> 00:04:23,431
Well, we don't have a dolly,

94
00:04:23,668 --> 00:04:27,062
or lifting belts
or any measurable upper-body strength.

95
00:04:28,211 --> 00:04:30,249
We don't need strength--
we're physicists.

96
00:04:30,732 --> 00:04:33,022
We are the intellectual
descendants of Archimedes.

97
00:04:33,186 --> 00:04:35,405
Give me a fulcrum and a lever
and I can move the Earth.

98
00:04:35,570 --> 00:04:37,411
It's just a matter of...
I don't have this.

99
00:04:39,472 --> 00:04:40,972
Archimedes would be so proud.

100
00:04:48,003 --> 00:04:49,306
Do you have any ideas?

101
00:04:49,950 --> 00:04:52,636
Yes, but they all involve
a green lantern and a power ring.

102
00:04:54,725 --> 00:04:57,026
Easy... easy.

103
00:05:02,490 --> 00:05:05,039
Now we've got an inclined plane.
The force required to lift

104
00:05:05,196 --> 00:05:07,274
is reduced by the sine
of the angle of the stairs,

105
00:05:07,431 --> 00:05:08,977
call it 30 degrees, so, about half.

106
00:05:09,627 --> 00:05:10,749
Exactly half.

107
00:05:12,986 --> 00:05:13,986
Exactly half.

108
00:05:15,690 --> 00:05:16,690
Let's push.

109
00:05:21,069 --> 00:05:22,643
See, it's moving, this is easy.

110
00:05:23,313 --> 00:05:24,523
It's all in the math.

111
00:05:25,643 --> 00:05:27,901
- What's your formula for the corner?
- What?

112
00:05:34,456 --> 00:05:35,478
Okay, no problem.

113
00:05:35,701 --> 00:05:37,497
Just come up here,
help me pull and turn.

114
00:05:42,553 --> 00:05:44,917
Ah, gravity,
thou art a heartless bitch.

115
00:05:47,363 --> 00:05:50,839
You do understand that our efforts here
will in no way increase the odds

116
00:05:51,003 --> 00:05:52,940
of you having sexual congress
with this woman.

117
00:05:54,359 --> 00:05:56,709
Men do things for women
without expecting sex.

118
00:05:57,154 --> 00:05:58,948
Those would be men who just had sex.

119
00:06:00,751 --> 00:06:02,629
I'm doing this
to be a good neighbor.

120
00:06:03,669 --> 00:06:06,057
In any case, there's no way
it could lower the odds.

121
00:06:09,087 --> 00:06:10,109
Almost there.

122
00:06:11,086 --> 00:06:12,086
Almost there.

123
00:06:13,154 --> 00:06:14,669
- Almost there.
- No, we're not.

124
00:06:14,833 --> 00:06:16,566
- I'm sorry.
- No, we're not!

125
00:06:18,354 --> 00:06:19,822
Watch your fingers.

126
00:06:20,044 --> 00:06:21,558
Oh, God, my fingers!

127
00:06:23,972 --> 00:06:25,673
- You okay?
- No, her...

128
00:06:26,184 --> 00:06:28,544
Great Caesar's ghost,
look at this place.

129
00:06:31,102 --> 00:06:32,364
So Penny's a little messy.

130
00:06:32,696 --> 00:06:34,082
A little messy?

131
00:06:34,457 --> 00:06:37,307
The Mandelbrot set of complex
numbers is a little messy.

132
00:06:37,446 --> 00:06:38,538
This is chaos.

133
00:06:39,275 --> 00:06:40,275
Excuse me.

134
00:06:40,787 --> 00:06:42,415
Explain to me
an organizational system

135
00:06:42,579 --> 00:06:44,633
where a tray of flatware
on a couch is valid.

136
00:06:45,926 --> 00:06:47,423
I'm just inferring this is a couch

137
00:06:47,588 --> 00:06:49,456
because the evidence suggests
the coffee table

138
00:06:49,620 --> 00:06:50,994
is having a tiny garage sale.

139
00:06:52,751 --> 00:06:54,718
Did it ever occur to you
that not everyone has

140
00:06:54,895 --> 00:06:57,078
the compulsive need to sort,
organize and label

141
00:06:57,281 --> 00:06:58,619
the entire world around them?

142
00:06:59,620 --> 00:07:00,620
No.

143
00:07:02,175 --> 00:07:03,175
Well, they don't.

144
00:07:03,487 --> 00:07:05,130
Hard as it may be for you to believe,

145
00:07:05,306 --> 00:07:08,659
most people don't sort their breakfast
cereal numerically by fiber content.

146
00:07:10,255 --> 00:07:12,791
Excuse me, but I think we've both
found that helpful at times.

147
00:07:16,347 --> 00:07:18,289
- Come on, we should go.
- Hang on.

148
00:07:20,152 --> 00:07:22,312
- What are you doing?
- I'm straightening up.

149
00:07:23,090 --> 00:07:24,599
This is not your home.

150
00:07:24,798 --> 00:07:28,285
This is not anyone's home.
This is a swirling vortex of entropy.

151
00:07:30,357 --> 00:07:33,627
When the transvestite lived here,
you didn't care how he kept the place.

152
00:07:33,824 --> 00:07:35,307
Because it was immaculate.

153
00:07:35,427 --> 00:07:37,568
I mean, you opened
that man's closet,

154
00:07:37,721 --> 00:07:39,353
it was left to right evening gowns,

155
00:07:39,494 --> 00:07:41,678
cocktail dresses,
then his police uniforms.

156
00:07:43,287 --> 00:07:44,772
What were you doing in his closet?

157
00:07:44,946 --> 00:07:46,994
I helped him run
some cable for a web cam.

158
00:07:49,095 --> 00:07:51,349
This just arrived,
we just brought this up... just now.

159
00:07:51,691 --> 00:07:53,734
Great.
Was it hard getting it up the stairs?

160
00:07:53,970 --> 00:07:55,198
- No.
- "No"?

161
00:07:55,398 --> 00:07:56,443
No.

162
00:07:56,904 --> 00:07:57,914
No.

163
00:07:59,024 --> 00:08:00,595
Well, we'll get out of your hair.

164
00:08:00,755 --> 00:08:02,174
Okay, great.
Thank you again.

165
00:08:07,251 --> 00:08:09,939
I just want you to know
that you don't have to live like this.

166
00:08:11,247 --> 00:08:12,247
I'm here for you.

167
00:08:14,602 --> 00:08:16,002
What's he talking about?

168
00:08:17,485 --> 00:08:19,005
- It's a joke.
- I don't get it.

169
00:08:19,204 --> 00:08:20,620
Yeah, he didn't tell it right.

170
00:09:17,159 --> 00:09:18,450
Penny's sleeping.

171
00:09:20,950 --> 00:09:22,061
Are you insane?

172
00:09:22,183 --> 00:09:24,050
You can't just break
into a woman's apartment

173
00:09:24,204 --> 00:09:25,805
in the middle
of the night and clean.

174
00:09:26,018 --> 00:09:28,468
I had no choice.
I couldn't sleep knowing

175
00:09:28,617 --> 00:09:30,614
that just outside my bedroom
was our living room,

176
00:09:30,752 --> 00:09:32,854
and just outside our living room
was that hallway,

177
00:09:32,995 --> 00:09:35,824
and immediately adjacent
to the hallway was... this.

178
00:09:38,134 --> 00:09:39,897
Do you realize
that if Penny wakes up,

179
00:09:40,191 --> 00:09:42,875
there is no reasonable explanation
as to why we're here.

180
00:09:43,472 --> 00:09:45,609
I just gave you
a reasonable explanation.

181
00:09:46,604 --> 00:09:48,737
No, no, you gave me
an explanation.

182
00:09:49,105 --> 00:09:52,185
Its reasonableness will be determined
by a jury of your peers.

183
00:09:53,098 --> 00:09:55,494
Don't be ridiculous.
I have no peers.

184
00:09:56,258 --> 00:09:57,914
We have to get out of here.

185
00:10:01,167 --> 00:10:03,724
You might want to speak
in a lower register.

186
00:10:04,192 --> 00:10:05,202
What?

187
00:10:05,965 --> 00:10:07,667
Evolution has made women sensitive

188
00:10:07,796 --> 00:10:09,486
to high-pitched noises
while they sleep

189
00:10:09,662 --> 00:10:11,659
so that they'll be
roused by a crying baby.

190
00:10:11,863 --> 00:10:13,352
If you want to avoid waking her,

191
00:10:13,647 --> 00:10:15,242
speak in a lower register.

192
00:10:17,131 --> 00:10:18,482
That's ridiculous!

193
00:10:21,715 --> 00:10:22,748
No.

194
00:10:23,608 --> 00:10:25,155
"That's ridiculous."

195
00:10:28,566 --> 00:10:29,566
Fine.

196
00:10:31,617 --> 00:10:34,353
I accept your premise.
Now, please, let's go.

197
00:10:34,681 --> 00:10:36,690
I'm not leaving until I'm done.

198
00:10:41,193 --> 00:10:44,047
If you have time to lean,
you have time to clean.

199
00:10:48,254 --> 00:10:49,604
Oh, what the hell.

200
00:10:57,469 --> 00:10:58,469
Morning.

201
00:10:59,194 --> 00:11:00,204
Morning.

202
00:11:00,874 --> 00:11:03,027
I have to say
I slept splendidly.

203
00:11:04,272 --> 00:11:06,783
Granted, not long,
but just deeply and well.

204
00:11:07,556 --> 00:11:08,796
I'm not surprised.

205
00:11:08,984 --> 00:11:10,622
A well-known folk cure for insomnia

206
00:11:10,810 --> 00:11:13,452
is to break in your neighbor's
apartment and clean.

207
00:11:14,588 --> 00:11:15,588
Sarcasm?

208
00:11:16,391 --> 00:11:17,391
You think?

209
00:11:18,334 --> 00:11:20,732
Granted, my methods may have
been somewhat unorthodox,

210
00:11:20,994 --> 00:11:23,683
but I think the end result
will be a measurable enhancement

211
00:11:23,803 --> 00:11:26,201
- to Penny's quality of life
- You've convinced me.

212
00:11:26,323 --> 00:11:29,002
Maybe tonight we should sneak in
and shampoo her carpet.

213
00:11:29,780 --> 00:11:32,346
- You don't think that crosses a line?
- Yes.

214
00:11:32,666 --> 00:11:33,902
For God's sake,

215
00:11:34,072 --> 00:11:37,834
do I have to hold up a sarcasm sign
every time I open my mouth?

216
00:11:38,322 --> 00:11:39,972
You have a sarcasm sign?

217
00:11:42,937 --> 00:11:44,924
No, I do not have
a sarcasm sign.

218
00:11:45,972 --> 00:11:48,161
Do you want some cereal?
I feel so good today,

219
00:11:48,315 --> 00:11:50,527
I'm gonna choose from
the low-fiber end of the shelf.

220
00:11:50,697 --> 00:11:52,097
Hello, Honey Puffs.

221
00:11:53,104 --> 00:11:54,377
Son of a bitch!

222
00:11:56,406 --> 00:11:57,406
Penny's up.

223
00:11:58,413 --> 00:12:00,801
You sick geeky bastards!

224
00:12:02,322 --> 00:12:03,980
How did she know it was us?

225
00:12:05,349 --> 00:12:07,676
I may have left a suggested
organizational schematic

226
00:12:07,850 --> 00:12:09,085
for her bedroom closet.

227
00:12:10,757 --> 00:12:12,064
God, this is gonna be bad.

228
00:12:12,316 --> 00:12:14,534
Good-bye, Honey Puffs.
Hello, Big Bran.

229
00:12:15,891 --> 00:12:18,303
You came into my apartment last
night while I was sleeping?!

230
00:12:18,475 --> 00:12:19,709
Yes, but only to clean.

231
00:12:19,883 --> 00:12:22,502
Really more to organize.
You're not actually dirty, per se.

232
00:12:23,553 --> 00:12:25,003
Give me back my key.

233
00:12:26,592 --> 00:12:27,700
I'm very, very sorry.

234
00:12:27,874 --> 00:12:29,787
Do you understand
how creepy this is?

235
00:12:30,063 --> 00:12:32,147
Oh, yes, we discussed it
at length last night.

236
00:12:32,572 --> 00:12:34,768
In my apartment,
while I was sleeping?!

237
00:12:35,045 --> 00:12:38,045
And snoring. And that's probably
just a sinus infection.

238
00:12:38,331 --> 00:12:39,711
But it could be sleep apnea.

239
00:12:39,885 --> 00:12:41,828
You might want to see
an otolaryngologist.

240
00:12:44,534 --> 00:12:45,767
A throat doctor.

241
00:12:47,514 --> 00:12:50,408
And what kind of doctor
removes shoes from asses?

242
00:12:51,540 --> 00:12:52,880
Depending on the depth,

243
00:12:53,009 --> 00:12:55,691
that's either
a... proctologist

244
00:12:55,884 --> 00:12:57,354
or a general surgeon.

245
00:13:08,672 --> 00:13:10,128
I think what you're feeling

246
00:13:10,283 --> 00:13:12,221
is perfectly valid
and maybe a little bit later

247
00:13:12,375 --> 00:13:15,460
when you're feeling a little less...
for lack of a better word-- violated,

248
00:13:15,580 --> 00:13:17,218
maybe we can talk
about this some more.

249
00:13:17,338 --> 00:13:19,656
- Stay away from me.
- Sure, that's another way to go.

250
00:13:20,832 --> 00:13:21,874
Penny, Penny!

251
00:13:22,241 --> 00:13:23,759
Just to clarify,

252
00:13:23,894 --> 00:13:26,374
because there will be
a discussion when you leave,

253
00:13:27,847 --> 00:13:29,730
is your objection
solely to our presence

254
00:13:29,904 --> 00:13:31,660
in the apartment
while you were sleeping,

255
00:13:31,836 --> 00:13:36,156
or do you also object to the imposition
of a new organizational paradigm.

256
00:13:42,576 --> 00:13:44,676
Well, that was
a little non-responsive.

257
00:13:45,476 --> 00:13:48,340
You are going to march yourself
over there right now and apologize.

258
00:13:53,439 --> 00:13:54,439
What's funny?

259
00:13:54,835 --> 00:13:56,467
That wasn't sarcasm?

260
00:13:59,252 --> 00:14:01,837
Boy, you are all over
the place this morning.

261
00:14:05,898 --> 00:14:08,617
I have a master's and two Ph.D.s,
I should not have to do this.

262
00:14:09,890 --> 00:14:10,969
What?!

263
00:14:11,583 --> 00:14:13,858
I am truly sorry for what
happened last night.

264
00:14:13,983 --> 00:14:15,546
I take full responsibility.

265
00:14:16,158 --> 00:14:18,525
And I hope that it won't color
your opinion of Leonard,

266
00:14:18,776 --> 00:14:22,240
who is not only a wonderful guy,
but also, I hear,

267
00:14:22,530 --> 00:14:24,175
a gentle and
thorough lover.

268
00:14:36,817 --> 00:14:38,267
I did what I could.

269
00:14:43,045 --> 00:14:44,068
Hey, Raj.

270
00:14:49,540 --> 00:14:50,540
Hey, listen.

271
00:14:51,123 --> 00:14:53,419
I don't know if you heard about
what happened last night.

272
00:14:53,612 --> 00:14:54,995
but I'm really upset about it.

273
00:14:55,155 --> 00:14:57,325
I mean, they just...
they let themselves into my place

274
00:14:57,516 --> 00:14:59,710
and then they cleaned it.
Can you even believe that?

275
00:14:59,864 --> 00:15:01,041
How weird is that?

276
00:15:01,598 --> 00:15:03,137
<i>She's standing very close to me.</i>

277
00:15:04,514 --> 00:15:06,212
<i>Oh, my, she does smell good.</i>

278
00:15:06,604 --> 00:15:08,418
<i>What is that, vanilla?</i>

279
00:15:09,117 --> 00:15:10,911
You know,
where I come from,

280
00:15:11,155 --> 00:15:14,046
if someone comes into your house
at night, you shoot. Okay?

281
00:15:14,200 --> 00:15:15,532
And you don't shoot to wound.

282
00:15:15,682 --> 00:15:17,549
I mean, all right,
my sister shot her husband,

283
00:15:17,742 --> 00:15:19,423
but it was an accident,
they were drunk.

284
00:15:20,436 --> 00:15:21,844
Wait, what was I saying?

285
00:15:22,425 --> 00:15:24,179
<i>She's so chatty.</i>

286
00:15:25,357 --> 00:15:26,707
<i>Maybe my parents are right.</i>|

287
00:15:27,040 --> 00:15:29,046
<i>Maybe I'd be better off
with an Indian girl.</i>

288
00:15:29,236 --> 00:15:30,876
<i>We'd have the same
cultural background</i>

289
00:15:31,069 --> 00:15:34,423
<i>and my wife could sing to my children
the same lullabies my mother sang to me.</i>

290
00:15:34,763 --> 00:15:36,518
It's obvious that they meant well,
but...

291
00:15:36,711 --> 00:15:39,420
I'm having a really rough time.
I broke up with my boyfriend

292
00:15:39,555 --> 00:15:41,099
and...

293
00:15:45,884 --> 00:15:49,289
Just because most of the men I've known
in my life happen to be jerks,

294
00:15:49,524 --> 00:15:52,264
doesn't mean I should just
assume Leonard and Sheldon are.

295
00:15:52,523 --> 00:15:53,523
Right?

296
00:15:53,773 --> 00:15:56,487
<i>She asked me a question.
I should probably nod.</i>

297
00:15:59,021 --> 00:16:00,461
That's exactly what I thought.

298
00:16:00,660 --> 00:16:02,861
Thank you for listening.
You're a doll.

299
00:16:05,388 --> 00:16:06,681
Turn your pelvis.

300
00:16:20,700 --> 00:16:22,924
Grab a napkin, homie.
You just got served.

301
00:16:26,188 --> 00:16:27,611
It's fine.You win.

302
00:16:29,395 --> 00:16:30,568
What's his problem?

303
00:16:31,052 --> 00:16:33,252
His imaginary girlfriend
broke up with him.

304
00:16:35,252 --> 00:16:36,252
Been there.

305
00:16:40,114 --> 00:16:41,114
Sorry I'm late,

306
00:16:41,357 --> 00:16:43,540
but I was in the hallway,
chatting up Penny.

307
00:16:44,321 --> 00:16:47,539
Really? You, Rajesh
Koothrappali, spoke to Penny?

308
00:16:48,021 --> 00:16:50,571
Actually, I was less
the chatter than the chat-ee.

309
00:16:51,384 --> 00:16:53,399
What did she say?
Is she still mad at me?

310
00:16:53,604 --> 00:16:55,072
Well, she was upset at first,

311
00:16:55,229 --> 00:16:57,775
but probably because
her sister shot somebody.

312
00:17:00,119 --> 00:17:01,894
But then there was
something about you,

313
00:17:02,087 --> 00:17:03,437
and then she hugged me.

314
00:17:04,184 --> 00:17:06,383
She hugged you?
How'd she hug you?

315
00:17:15,377 --> 00:17:16,827
Is that her perfume I smell?

316
00:17:19,083 --> 00:17:20,680
Intoxicating, isn't it?

317
00:17:37,560 --> 00:17:38,560
Hi.

318
00:17:40,943 --> 00:17:41,943
What's going on?

319
00:17:44,378 --> 00:17:45,593
Here's the thing.

320
00:17:52,245 --> 00:17:54,706
"Just as Oppenheimer came
to regret his contributions

321
00:17:54,879 --> 00:17:56,372
"to the first atomic bomb,

322
00:17:56,609 --> 00:17:58,842
"so too I regret
my participation in what was,

323
00:17:59,047 --> 00:18:01,062
"at the very least,
an error in judgment.

324
00:18:02,708 --> 00:18:04,753
"The hallmark of the
great human experiment

325
00:18:04,979 --> 00:18:06,919
"is the willingness to
recognize one's mistakes.

326
00:18:07,083 --> 00:18:09,415
"Some mistakes, such as
Madam Curie's discovery of radium,

327
00:18:09,550 --> 00:18:11,438
"turned out to have great
scientific potential,

328
00:18:11,578 --> 00:18:14,875
"even though she would later die a slow,
painful death "from radiation poisoning.

329
00:18:17,595 --> 00:18:19,874
Another example, from the
field of Ebola research..."

330
00:18:25,905 --> 00:18:26,905
We're okay.

331
00:18:45,780 --> 00:18:47,082
- Six two inch dowels.
- Check.

332
00:18:49,070 --> 00:18:51,376
- One package Phillips head screws.
- Check.

333
00:18:51,531 --> 00:18:53,498
You guys, seriously,
I grew up on a farm, okay?

334
00:18:53,672 --> 00:18:55,756
I rebuilt a tractor engine
when I was, like, 12.

335
00:18:55,927 --> 00:18:58,275
I think I can put together a cheap,
Swedish media center.

336
00:18:58,395 --> 00:19:01,357
No, please. We insist.
It's the least we can do, considering.

337
00:19:01,512 --> 00:19:02,843
Considering what?

338
00:19:03,005 --> 00:19:04,856
How great this place looks?

339
00:19:06,265 --> 00:19:08,306
- Oh, boy. I was afraid of this.
- What?

340
00:19:08,651 --> 00:19:11,301
These instructions are
a pictographic representation

341
00:19:11,421 --> 00:19:14,332
of the least imaginative way
to assemble these components.

342
00:19:14,467 --> 00:19:16,803
This, right here is why Sweden
has no space program.

343
00:19:18,428 --> 00:19:20,189
Well, it looked
pretty good in the store.

344
00:19:20,403 --> 00:19:23,301
It is an inefficient design.
For example, she has a flat screen TV,

345
00:19:23,455 --> 00:19:25,424
which means all the space
behind it is wasted.

346
00:19:25,646 --> 00:19:28,179
- We could put her stereo back there.
- And control it how?

347
00:19:28,370 --> 00:19:29,710
Run an infrared repeater.

348
00:19:29,845 --> 00:19:32,089
Photo cell here, emitter here,
easy-peasy.

349
00:19:33,532 --> 00:19:34,863
How are you gonna cool it?

350
00:19:34,998 --> 00:19:37,132
- Hey, guys, I got this.
- Hang on, Penny.

351
00:19:37,409 --> 00:19:39,733
How about fans?
Here and here.

352
00:19:39,999 --> 00:19:41,773
Also inefficient,
and might be loud.

353
00:19:41,912 --> 00:19:44,332
How about liquid coolant?
Maybe a little aquarium pump here,

354
00:19:44,525 --> 00:19:46,006
run some quarter-inch PVC...

355
00:19:46,285 --> 00:19:48,026
Guys, this is actually really simple.

356
00:19:49,030 --> 00:19:50,480
Hold on, honey.
Men at work.

357
00:19:53,089 --> 00:19:54,285
The PVC comes down here.

358
00:19:54,420 --> 00:19:56,696
Maybe a little corrugated
sheet metal as a radiator here.

359
00:19:56,832 --> 00:20:00,026
Yeah? Show me where we put a drip tray,
a sluice, and an overflow reservoir.

360
00:20:00,207 --> 00:20:03,179
If water's involved, we're gonna have
to ground the crap out of the thing.

361
00:20:03,582 --> 00:20:06,032
It's hot in here. I think
I'll just take off all my clothes.

362
00:20:11,758 --> 00:20:12,760
Oh, I've got it.

363
00:20:14,444 --> 00:20:15,616
What about if we replace

364
00:20:15,809 --> 00:20:19,423
panels A, B and F and crossbar H
with aircraft-grade aluminum?

365
00:20:19,648 --> 00:20:21,519
Right. Then the entire thing
is one heat sink.

366
00:20:21,674 --> 00:20:23,535
You and Sheldon
go to the junkyard and pick up

367
00:20:23,689 --> 00:20:25,040
6 square meters of scrap aluminum?

368
00:20:25,182 --> 00:20:27,049
Raj and I
will get the oxyacetylene torch.

369
00:20:27,203 --> 00:20:28,774
- Meet back here in an hour?
- Done.

370
00:20:33,067 --> 00:20:34,899
Okay, this place does look pretty good.

371
00:20:36,457 --> 00:20:34,899
Sync: swsub.com

