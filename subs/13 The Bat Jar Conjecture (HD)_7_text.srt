1
00:00:02,255 --> 00:00:04,238
More details
about the new <i>Star Trek</i> film.

2
00:00:04,697 --> 00:00:08,087
There's going to be a scene
depicting Spock's birth.

3
00:00:08,735 --> 00:00:11,662
I'd be more interested in a scene
depicting Spock's conception.

4
00:00:13,031 --> 00:00:13,969
Oh, please.

5
00:00:14,089 --> 00:00:17,464
For Vulcans, mating...
or if you will, <i>Pon farr</i>...

6
00:00:18,679 --> 00:00:22,192
- it's an extremely private matter.
- Still, I'd like to know the details.

7
00:00:22,312 --> 00:00:24,237
His mother was human.
His father is Vulcan.

8
00:00:24,357 --> 00:00:27,022
- They couldn't just conceive.
- Maybe they had to go to a clinic.

9
00:00:27,142 --> 00:00:29,134
Can you imagine Spock's Dad
in a little room

10
00:00:29,254 --> 00:00:31,789
with a copy of
<i>Pointy Ears and Shapely Rears</i>.

11
00:00:33,454 --> 00:00:36,019
How come on <i>Star Trek</i>
everybody's private parts are the same?

12
00:00:36,902 --> 00:00:40,723
No alien lady ever told Captain Kirk,
"Hey, get your thing out of my nose."

13
00:00:43,701 --> 00:00:44,927
Hi. Can you help me?

14
00:00:45,047 --> 00:00:47,309
I was writing an e-mail
and the "A" key got stuck.

15
00:00:47,429 --> 00:00:49,150
Now it's just going "Aaa..."

16
00:00:50,277 --> 00:00:52,453
- What'd you spill on it?
- Nothing.

17
00:00:53,541 --> 00:00:54,495
Diet Coke.

18
00:00:56,345 --> 00:00:57,392
And yogurt.

19
00:00:58,102 --> 00:00:59,459
And a little nail polish.

20
00:00:59,621 --> 00:01:01,086
I'll take a look at it.

21
00:01:01,516 --> 00:01:04,320
Gentlemen,
switching to local nerd news...

22
00:01:04,647 --> 00:01:07,347
Fishman, Chen, Chaudury and McNair
aren't fielding a team

23
00:01:07,467 --> 00:01:09,344
in the university Physics Bowl
this year.

24
00:01:09,504 --> 00:01:10,845
You're kidding. Why not?

25
00:01:11,006 --> 00:01:13,442
They formed a barbershop quartet
and got a gig

26
00:01:13,562 --> 00:01:15,335
playing Knott's Berry Farm.

27
00:01:16,985 --> 00:01:18,424
So, in your world,

28
00:01:18,967 --> 00:01:20,652
you're like the cool guys.

29
00:01:21,916 --> 00:01:23,075
Recognize.

30
00:01:25,312 --> 00:01:27,511
This is our year.
With those guys out,

31
00:01:27,631 --> 00:01:29,739
the entire Physics Bowl
will kneel before Zod.

32
00:01:30,393 --> 00:01:31,237
Zod?

33
00:01:31,444 --> 00:01:33,076
Kryptonian villain. Long story.

34
00:01:33,236 --> 00:01:34,277
Good story.

35
00:01:37,908 --> 00:01:40,166
- Count me out.
- What? Why?

36
00:01:40,535 --> 00:01:43,545
You want me to use my intelligence
in a tawdry competition?

37
00:01:44,185 --> 00:01:46,578
Would you ask Picasso
to play Pictionary?

38
00:01:47,656 --> 00:01:50,086
Would you ask Noah Webster
to play Boggle?

39
00:01:51,048 --> 00:01:53,749
Would you ask Jacques Cousteau
to play Go Fish?

40
00:01:55,217 --> 00:01:58,230
Come on, you need a four-person team.
We're four people.

41
00:01:58,350 --> 00:02:01,478
By that reasoning we should also
play Bridge, hold up a chuppah

42
00:02:01,598 --> 00:02:04,028
and enter the Olympic
bobsled competition.

43
00:02:04,844 --> 00:02:06,443
Tickets to that, please.

44
00:02:07,521 --> 00:02:11,364
Sheldon, what, do I need to quote
Spock's dying words to you?

45
00:02:11,525 --> 00:02:12,658
No, don't.

46
00:02:13,607 --> 00:02:15,244
"The needs of the many...

47
00:02:15,404 --> 00:02:17,411
"... outweigh the needs of the few.

48
00:02:17,573 --> 00:02:18,635
"Or the one."

49
00:02:19,690 --> 00:02:21,127
Damn it, I'll do it.

50
00:02:22,812 --> 00:02:25,242
The Big Bang Theory
Season 1 Episode 13 - The Bat Jar Conjecture

51
00:02:25,831 --> 00:02:28,326
Transcript: swsub.com
Sync: SerJo

52
00:02:48,466 --> 00:02:50,229
First order of Physics Bowl business.

53
00:02:51,052 --> 00:02:54,441
We need a truly kick-ass team name.
Suggestions?

54
00:02:54,807 --> 00:02:57,218
How about the Perpetual Motion Squad?

55
00:02:58,424 --> 00:03:02,283
It's beyond the laws of physics,
plus a little heads-up for the ladies.

56
00:03:03,051 --> 00:03:03,909
The ladies?

57
00:03:04,070 --> 00:03:07,145
"Perpetual Motion Squad...
we can go all night."

58
00:03:08,073 --> 00:03:09,867
- I like it.
- I don't.

59
00:03:10,378 --> 00:03:12,864
Teams are traditionally
named after fierce creatures,

60
00:03:12,984 --> 00:03:15,003
thus intimidating one's opponent.

61
00:03:15,164 --> 00:03:17,874
- Then we could be the Bengal tigers.
- Poor choice.

62
00:03:17,994 --> 00:03:20,044
Gram for gram,
no animal exceeds

63
00:03:20,164 --> 00:03:22,781
the relative fighting strength
of the army ant.

64
00:03:23,671 --> 00:03:24,662
Maybe so,

65
00:03:24,782 --> 00:03:27,892
but you can't incinerate a Bengal tiger
with a magnifying glass.

66
00:03:31,479 --> 00:03:33,467
Let's put it to a vote.
All those in favor...

67
00:03:33,587 --> 00:03:34,606
Point of order.

68
00:03:34,767 --> 00:03:37,275
I move that any vote on team names
must be unanimous.

69
00:03:37,395 --> 00:03:40,041
No man should be forced to emblazon
his chest with a Bengal tiger

70
00:03:40,161 --> 00:03:42,933
when common sense dictates
it should be an army ant.

71
00:03:43,776 --> 00:03:46,937
Will the gentleman from the great
State of Denial yield for a question?

72
00:03:47,760 --> 00:03:48,713
I will yield.

73
00:03:48,833 --> 00:03:51,716
After we go through the exercise
of an annoying series of votes,

74
00:03:51,836 --> 00:03:54,867
all of which the gentleman will lose,
does he then intend to threaten to quit

75
00:03:54,987 --> 00:03:56,819
- if he does not get his way?
- He does.

76
00:03:56,939 --> 00:03:59,305
I move we are the Army Ants.
All those in favor?

77
00:04:01,440 --> 00:04:04,929
Good afternoon, and welcome
to today's Physics Bowl practice round.

78
00:04:05,090 --> 00:04:06,778
I'm Penny,
and I'll be your host

79
00:04:06,898 --> 00:04:09,115
because apparently
I didn't have anything else to do

80
00:04:09,235 --> 00:04:12,506
on a Saturday afternoon,
and isn't that just a little sad?

81
00:04:14,556 --> 00:04:15,688
Gentlemen, ready?

82
00:04:15,849 --> 00:04:17,233
- Yes.
- Fire away.

83
00:04:17,353 --> 00:04:19,378
It's none of my business,
but isn't a guy

84
00:04:19,498 --> 00:04:22,105
who can't speak in front of women
gonna hold you back a little?

85
00:04:22,225 --> 00:04:24,797
He'll be okay once the women
are mixed into the crowd.

86
00:04:24,917 --> 00:04:28,177
He only has a problem
when they're one-on-one and smell nice.

87
00:04:28,996 --> 00:04:30,871
Thanks, Raj. It's vanilla oil.

88
00:04:31,258 --> 00:04:34,208
I was actually the one who noticed.
Okay, let's just start.

89
00:04:34,803 --> 00:04:37,653
Okay, the first question
is on the topic of optics.

90
00:04:37,773 --> 00:04:39,922
"What is the shortest
light pulse ever produced?"

91
00:04:40,874 --> 00:04:42,441
- Dr. Cooper.
- And of course,

92
00:04:42,561 --> 00:04:44,260
the answer is 130 attoseconds.

93
00:04:44,527 --> 00:04:46,470
- That is correct.
- I knew that, too.

94
00:04:47,288 --> 00:04:50,206
Good for you, sweetie.
Okay, next question:

95
00:04:50,326 --> 00:04:52,120
"What is the quantum mechanical effect

96
00:04:52,240 --> 00:04:54,353
"used to encode data
on hard-disk drives?"

97
00:04:56,072 --> 00:04:58,523
And of course the answer is
giant magnetoresistance.

98
00:04:58,759 --> 00:05:00,541
- Right.
- Hey, I buzzed in.

99
00:05:00,994 --> 00:05:03,162
And I answered.
It's called teamwork.

100
00:05:05,149 --> 00:05:07,630
Don't you think I should answer
the engineering questions?

101
00:05:07,750 --> 00:05:08,938
I am an engineer.

102
00:05:09,242 --> 00:05:11,764
By that logic I should answer
all the anthropology questions

103
00:05:11,884 --> 00:05:13,315
because I'm a mammal.

104
00:05:16,260 --> 00:05:17,543
Just ask another one.

105
00:05:19,070 --> 00:05:21,268
"What artificial satellite
has seen glimpses

106
00:05:21,388 --> 00:05:23,340
of Einstein's predicted
frame dragging?"

107
00:05:24,384 --> 00:05:26,600
And of course,
it's Gravity Probe B.

108
00:05:27,199 --> 00:05:29,762
Sheldon, you have to let
somebody else answer.

109
00:05:30,089 --> 00:05:30,798
Why?

110
00:05:31,051 --> 00:05:32,223
Because it's polite.

111
00:05:32,807 --> 00:05:34,825
What do manners
have to do with it?

112
00:05:34,985 --> 00:05:36,137
This is war.

113
00:05:37,085 --> 00:05:39,656
Were the Romans polite
when they salted the ground of Carthage

114
00:05:39,816 --> 00:05:41,808
to make sure
nothing would ever grow again?

115
00:05:42,721 --> 00:05:45,372
Leonard, you said
I only had to ask questions.

116
00:05:47,482 --> 00:05:49,929
The objective of the competion
is to give correct answers.

117
00:05:50,089 --> 00:05:52,074
If I know them,
why shouldn't I give them?

118
00:05:52,234 --> 00:05:54,707
Some of us might have
the correct answers, too.

119
00:05:54,957 --> 00:05:56,982
Oh, please.
You don't even have a PhD.

120
00:05:57,142 --> 00:05:59,377
- All right, that's it!
- Howard, sit down.

121
00:06:01,700 --> 00:06:04,780
- Maybe we should take a little break.
- Good idea. I need my wrist brace.

122
00:06:04,940 --> 00:06:08,085
All this button-pushing
is aggravating my old Nintendo injury.

123
00:06:09,588 --> 00:06:10,428
I agree.

124
00:06:10,791 --> 00:06:12,108
What did he say?

125
00:06:12,496 --> 00:06:13,812
He compared Sheldon

126
00:06:13,932 --> 00:06:17,051
to a disposable feminine
cleansing product

127
00:06:17,211 --> 00:06:19,309
one might use
on a summer's eve.

128
00:06:21,900 --> 00:06:23,674
Yeah, and the bag it came in.

129
00:06:28,284 --> 00:06:30,448
Leonard, excellent.
I want to show you something.

130
00:06:30,794 --> 00:06:32,465
Can it wait?
I need to talk to you.

131
00:06:32,654 --> 00:06:35,963
Just look. I've designed
the perfect uniforms for our team.

132
00:06:36,123 --> 00:06:38,473
The colors are based
on <i>Star Trek: The Original Series</i>.

133
00:06:38,709 --> 00:06:42,027
The three of you will wear Support Red,
and I will wear Command Gold.

134
00:06:44,037 --> 00:06:45,331
Why do they say "AA"?

135
00:06:46,092 --> 00:06:47,090
Army Ants.

136
00:06:48,595 --> 00:06:52,254
Isn't that confusing? "AA" might mean
something else to certain people.

137
00:06:53,301 --> 00:06:56,420
Why would a Physics Bowl team
be called Anodized Aluminum?

138
00:06:57,632 --> 00:06:58,794
No, I meant...

139
00:06:59,920 --> 00:07:00,920
Never mind.

140
00:07:02,020 --> 00:07:03,020
Check it out,

141
00:07:03,623 --> 00:07:06,580
I got you a <i>Batman</i> cookie jar.

142
00:07:06,981 --> 00:07:09,723
Oh, neat!
What's the occasion?

143
00:07:09,941 --> 00:07:12,337
Well, you're a friend,
and you like <i>Batman</i>

144
00:07:12,497 --> 00:07:14,693
and cookies,
and you're off the team.

145
00:07:18,402 --> 00:07:19,311
What?

146
00:07:19,754 --> 00:07:21,906
Howard, Raj and I
just had a team meeting.

147
00:07:22,123 --> 00:07:24,599
- No, you didn't.
- Yes, we did. I just came from there.

148
00:07:24,719 --> 00:07:26,800
Okay, I don't know
where you just came from,

149
00:07:26,960 --> 00:07:29,966
but it could'nt have been a team meeting
because I wasn't there.

150
00:07:30,132 --> 00:07:31,935
Ergo, the team did not meet.

151
00:07:32,804 --> 00:07:34,796
Okay, let me try it this way:

152
00:07:35,561 --> 00:07:38,457
I was at a coffee klatch
with a couple of friends,

153
00:07:38,617 --> 00:07:41,864
and one thing led to another,
and it turns out you're off the team.

154
00:07:43,133 --> 00:07:43,942
Why?

155
00:07:44,142 --> 00:07:46,225
Because you're taking
all the fun out of it.

156
00:07:46,628 --> 00:07:50,385
I'm sorry, is the winner of the Physics
Bowl the team that has the most fun?

157
00:07:50,614 --> 00:07:52,275
Okay, let me try it this way:

158
00:07:52,435 --> 00:07:55,648
You're annoying and no one
wants to play with you anymore.

159
00:07:57,941 --> 00:07:58,893
I see.

160
00:08:01,163 --> 00:08:03,325
At this point I should inform you

161
00:08:03,762 --> 00:08:06,002
that I intend to form my own team

162
00:08:06,162 --> 00:08:09,503
and destroy the molecular bonds
that bind your very matter together

163
00:08:09,663 --> 00:08:12,644
and reduce the resulting
particulate chaos to tears.

164
00:08:15,475 --> 00:08:17,089
Thanks for the heads-up.

165
00:08:17,879 --> 00:08:18,848
You're welcome.

166
00:08:20,636 --> 00:08:22,019
- One more thing.
- Yes?

167
00:08:22,229 --> 00:08:23,458
It's on, bitch.

168
00:08:32,915 --> 00:08:35,731
- So who'd he get to be on his team?
- He won't say.

169
00:08:36,222 --> 00:08:39,250
He just smiles and eats
macaroons out of his bat jar.

170
00:08:40,667 --> 00:08:42,287
He's using psychological warfare.

171
00:08:42,733 --> 00:08:46,238
We must reply in kind.
I say we wait until he looks at us,

172
00:08:46,398 --> 00:08:49,936
then laugh like, "Yes, you are
a smart and strong competitor,

173
00:08:50,096 --> 00:08:51,885
"but we are also smart and strong,

174
00:08:52,045 --> 00:08:54,736
and we have a reasonable
chance of defeating you."

175
00:08:56,748 --> 00:08:58,504
How exactly would that laugh go?

176
00:09:03,798 --> 00:09:06,696
That sounds more like,
"We are a tall, thin woman

177
00:09:06,856 --> 00:09:09,395
who wants to make a
coat out of your Dalmatians."

178
00:09:11,937 --> 00:09:15,111
Let's remember that Sheldon
is still our friend and my roommate.

179
00:09:15,364 --> 00:09:16,164
So?

180
00:09:17,069 --> 00:09:18,941
So nothing.
Let's destroy him.

181
00:09:20,328 --> 00:09:21,328
Gentlemen.

182
00:09:29,601 --> 00:09:31,871
We're going to need
a strong fourth for our team.

183
00:09:32,337 --> 00:09:35,833
You know who is apparently very smart
is the girl who played <i>TV's Blossom</i>.

184
00:09:37,796 --> 00:09:39,844
She got a PhD. in neuroscience
or something.

185
00:09:40,024 --> 00:09:43,851
Raj, we're not getting <i>TV's Blossom</i>
to join our Physics Bowl team.

186
00:09:46,025 --> 00:09:48,137
How about the girl
from <i>The Wonder Years</i>?

187
00:09:49,885 --> 00:09:53,518
Gentlemen, I believe I've found
the solution to all our problems.

188
00:09:55,938 --> 00:09:57,634
We can't ask Leslie Winkle.

189
00:09:57,926 --> 00:10:00,611
Why? Because you slept together,
and when she was done with you

190
00:10:00,771 --> 00:10:03,259
she discarded you
like last night's chutney?

191
00:10:07,344 --> 00:10:08,204
Yes.

192
00:10:09,368 --> 00:10:11,424
Sometimes you've got
to take one for the team.

193
00:10:11,816 --> 00:10:13,143
Sack up, dude.

194
00:10:14,956 --> 00:10:15,760
Fine.

195
00:10:17,443 --> 00:10:19,814
Here I go, taking one for the team...

196
00:10:20,155 --> 00:10:21,258
in the sack.

197
00:10:24,492 --> 00:10:26,683
- Hey, Leslie.
- Hi, guys.

198
00:10:27,026 --> 00:10:29,227
So, Leslie,
I have a question for you,

199
00:10:29,389 --> 00:10:33,360
and it might be a little awkward,
you know, given that I...

200
00:10:33,738 --> 00:10:34,897
Hit that thing.

201
00:10:38,482 --> 00:10:40,696
Leonard, there's no reason
to feel uncomfortable

202
00:10:40,816 --> 00:10:42,738
just because we've seen
each other's faces

203
00:10:42,858 --> 00:10:45,912
and naked bodies contorted
in the sweet agony of coitus.

204
00:10:47,391 --> 00:10:48,382
There's not?

205
00:10:48,502 --> 00:10:51,074
Gee, 'cause it sure sounds
like there should be.

206
00:10:51,965 --> 00:10:54,581
Rest assured that any aspects
of our sexual relationship

207
00:10:54,701 --> 00:10:58,040
regarding your preferences,
your idiosyncrasies, your performance

208
00:10:58,160 --> 00:11:01,068
are still protected by the inherent
confidentiality of the bedroom.

209
00:11:01,188 --> 00:11:03,031
That's all very comforting,
but if it's okay,

210
00:11:03,151 --> 00:11:05,141
I'd like to get on
to my question now.

211
00:11:05,742 --> 00:11:06,550
Proceed.

212
00:11:06,985 --> 00:11:10,424
We are entering the Physics Bowl,
and we need a fourth for our team.

213
00:11:10,544 --> 00:11:14,065
No, thanks. I'm really busy with my
like-sign dilepton supersymmetry search.

214
00:11:14,225 --> 00:11:16,444
Dilepton, shmylepton.
We need you.

215
00:11:17,913 --> 00:11:18,773
Sorry.

216
00:11:20,044 --> 00:11:21,464
Well, we tried.

217
00:11:21,820 --> 00:11:25,369
We'll just have to face Sheldon
<i>mano y mano y mano a mano</i>.

218
00:11:26,280 --> 00:11:28,537
Wait, you're going up
against Sheldon Cooper?

219
00:11:29,740 --> 00:11:32,641
That arrogant, misogynistic,
East Texas doorknob

220
00:11:32,761 --> 00:11:35,456
that told me I should abandon
my work with high-energy particles

221
00:11:35,576 --> 00:11:37,837
for laundry and childbearing?

222
00:11:40,874 --> 00:11:41,921
She's in.

223
00:11:44,827 --> 00:11:46,323
So, how do you feel?

224
00:11:46,757 --> 00:11:49,809
Nice and loose? Come to play?
Got your game face on?

225
00:11:51,262 --> 00:11:52,435
Are you ready?

226
00:11:53,305 --> 00:11:55,939
Yeah. You know, you don't have
to stay for the whole thing.

227
00:11:56,101 --> 00:11:58,860
Oh, no. I want to.
Sounds really interesting.

228
00:12:05,811 --> 00:12:06,690
Gentlemen.

229
00:12:15,426 --> 00:12:17,014
I'm just gonna sit down.

230
00:12:20,958 --> 00:12:22,591
So, is that your team?

231
00:12:22,932 --> 00:12:24,494
Actually, I don't need a team.

232
00:12:24,614 --> 00:12:26,511
I could easily defeat you
single-handedly,

233
00:12:26,705 --> 00:12:28,471
but the rules require four.

234
00:12:28,632 --> 00:12:31,629
So, may I introduce
the third-floor janitor,

235
00:12:31,749 --> 00:12:34,627
the lady from the lunch room,
and my Spanish is not good...

236
00:12:34,747 --> 00:12:36,561
either her son or her butcher.

237
00:12:37,645 --> 00:12:39,178
And what about your team?

238
00:12:39,298 --> 00:12:42,244
What rat have you recruited
to the S.S. Sinking Ship?

239
00:12:42,364 --> 00:12:43,458
Hello, Sheldon.

240
00:12:46,991 --> 00:12:48,034
Leslie Winkle.

241
00:12:48,430 --> 00:12:50,801
Yeah, Leslie Winkle.
The answer to the question,

242
00:12:50,921 --> 00:12:53,622
"Who made Sheldon Cooper cry
like a little girl?"

243
00:12:55,404 --> 00:12:58,960
Well, I'm polymerized tree sap,
and you're an inorganic adhesive.

244
00:12:59,080 --> 00:13:01,855
So whatever verbal projectile
you launch in my direction

245
00:13:01,975 --> 00:13:04,578
is reflected off of me,
returns on its original trajectory

246
00:13:04,698 --> 00:13:05,923
and adheres to you.

247
00:13:07,041 --> 00:13:08,274
Oh, ouch!

248
00:13:09,548 --> 00:13:12,183
Okay, if everyone could
please take your seats.

249
00:13:20,119 --> 00:13:21,540
Here's your T-shirt.

250
00:13:25,140 --> 00:13:26,093
PMS?

251
00:13:26,766 --> 00:13:28,392
It's a couple days early...

252
00:13:30,562 --> 00:13:33,328
It stands for "Perpetual Motion Squad."

253
00:13:33,714 --> 00:13:35,386
Of course.
What was I thinking?

254
00:13:37,371 --> 00:13:38,829
Good afternoon, everyone,

255
00:13:38,949 --> 00:13:41,867
and welcome to this year's Physics Bowl!

256
00:13:43,954 --> 00:13:47,425
Today's preliminary match
features two great teams.

257
00:13:47,966 --> 00:13:50,221
AA versus...

258
00:13:51,382 --> 00:13:52,322
PMS.

259
00:13:52,883 --> 00:13:54,445
All night long, y'all!

260
00:13:59,506 --> 00:14:03,441
Okay, well, let's jump right in.
First question for ten points:

261
00:14:03,787 --> 00:14:06,555
"What is the iso-spin singlet partner

262
00:14:06,675 --> 00:14:08,321
"of the pi-zero meson?"

263
00:14:09,400 --> 00:14:10,199
PMS?

264
00:14:10,360 --> 00:14:11,898
- The eta meson.
- Correct.

265
00:14:14,573 --> 00:14:16,395
- Formal protest.
- On what grounds?

266
00:14:16,515 --> 00:14:19,083
The Velcro on my wrist brace
caught on my shirt.

267
00:14:19,618 --> 00:14:22,572
Denied.
All right, for ten points,

268
00:14:22,789 --> 00:14:25,142
"What is the lightest element on Earth

269
00:14:25,262 --> 00:14:27,008
"with no stable isotope?"

270
00:14:27,349 --> 00:14:28,154
AA?

271
00:14:28,274 --> 00:14:30,176
And of course,
the answer is technetium.

272
00:14:30,338 --> 00:14:31,136
Terrific.

273
00:14:32,423 --> 00:14:33,472
Next question:

274
00:14:33,633 --> 00:14:36,184
"What is the force between
two uncharged plates

275
00:14:36,304 --> 00:14:38,518
"due to quantum vacuum fluctuations?"

276
00:14:38,994 --> 00:14:39,811
PMS?

277
00:14:39,973 --> 00:14:42,456
Sheldon can suck on...
the Casimir effect.

278
00:14:42,576 --> 00:14:43,454
Correct.

279
00:14:48,728 --> 00:14:52,167
How does a quantum computer
factor large numbers?

280
00:14:52,758 --> 00:14:53,576
PMS?

281
00:14:53,736 --> 00:14:55,450
- Shor's algorithm.
- Correct!

282
00:14:59,706 --> 00:15:02,584
4.1855 times ten
to the seventh ergs per calorie.

283
00:15:03,164 --> 00:15:04,809
Prevost's theory of exchanges.

284
00:15:05,406 --> 00:15:07,493
Lambda equals one
over pi r squared n.

285
00:15:07,988 --> 00:15:09,674
760 degrees Celsius...

286
00:15:09,836 --> 00:15:12,698
the approximate temperature
of the young lady in the front row.

287
00:15:13,923 --> 00:15:15,890
Mr. Wolowitz,
this is your second warning.

288
00:15:17,652 --> 00:15:18,792
A sigma particle.

289
00:15:19,190 --> 00:15:22,521
Yes, assuming the hypothetical planet
has a mass greater than the Earth.

290
00:15:22,683 --> 00:15:23,481
Correct!

291
00:15:28,971 --> 00:15:32,864
Ladies and gentlemen,
I hold in my hand the final question.

292
00:15:33,901 --> 00:15:37,034
The score now stands.
AA: 1,150,

293
00:15:37,154 --> 00:15:40,289
PMS: 1,175.

294
00:15:40,450 --> 00:15:43,941
So, for 100 points and the match,

295
00:15:44,061 --> 00:15:48,011
please turn your attention
to the formula on the screens.

296
00:15:48,624 --> 00:15:50,341
Solve the equation.

297
00:15:52,137 --> 00:15:53,277
Holy crap.

298
00:15:55,299 --> 00:15:56,556
What the hell is that?

299
00:15:56,906 --> 00:16:00,392
Looks like something
they found on the ship at Roswell.

300
00:16:00,553 --> 00:16:02,687
Come on. Think.
Leslie?

301
00:16:02,848 --> 00:16:05,731
It's not gonna work if you rush me.
You have to let me get there.

302
00:16:05,892 --> 00:16:08,233
You are never gonna
let that go, are you?

303
00:16:08,395 --> 00:16:09,610
Ten seconds.

304
00:16:13,763 --> 00:16:14,657
PMS?

305
00:16:16,725 --> 00:16:18,052
Sorry, I panicked.

306
00:16:19,655 --> 00:16:20,871
Then guess.

307
00:16:23,613 --> 00:16:24,548
Eight.

308
00:16:28,977 --> 00:16:30,416
.4

309
00:16:31,834 --> 00:16:33,743
I'm sorry, that's incorrect.

310
00:16:34,124 --> 00:16:34,928
AA,

311
00:16:35,642 --> 00:16:38,014
if you can answer correctly,
the match is yours.

312
00:16:41,119 --> 00:16:42,390
He doesn't have it.

313
00:16:43,006 --> 00:16:44,109
He's got squat.

314
00:16:53,621 --> 00:16:55,359
AA, I need your answer.

315
00:16:57,330 --> 00:17:00,163
The answer is minus eight pi alpha.

316
00:17:00,283 --> 00:17:02,120
Hang on a second.
That's not our answer.

317
00:17:02,282 --> 00:17:04,499
- What are you doing?
- Answering question.

318
00:17:04,619 --> 00:17:06,038
Winning Physics Bowl.

319
00:17:08,080 --> 00:17:10,038
How do you know anything
about physics?

320
00:17:10,158 --> 00:17:11,590
Here I am janitor.

321
00:17:11,710 --> 00:17:14,633
In former Soviet Union, I am physicist.

322
00:17:15,078 --> 00:17:16,927
Leningrad Politechnika.

323
00:17:17,418 --> 00:17:18,970
Go Polar Bears.

324
00:17:22,111 --> 00:17:23,749
That's a delightful little story,

325
00:17:23,869 --> 00:17:26,373
but our arrangement was that
you sit here and not say anything...

326
00:17:26,493 --> 00:17:29,446
- I answer the questions.
- You didn't answer question.

327
00:17:29,566 --> 00:17:32,588
Look, now, maybe you have
democracy now in your beloved Russia,

328
00:17:32,708 --> 00:17:35,568
but on this Physics Bowl team,
I rule with an iron fist.

329
00:17:37,474 --> 00:17:39,421
AA, I need your official answer.

330
00:17:39,611 --> 00:17:41,701
- It's not what he said.
- Then what is it?

331
00:17:41,914 --> 00:17:43,871
- I want a different question.
- You can't.

332
00:17:43,991 --> 00:17:45,414
- Formal protest.
- Denied.

333
00:17:45,575 --> 00:17:47,040
Informal protest.

334
00:17:47,789 --> 00:17:48,626
Denied.

335
00:17:49,578 --> 00:17:52,671
- I need your official answer.
- No. I decline to provide one.

336
00:17:52,833 --> 00:17:56,174
That's too bad because the answer
your teammate gave was correct.

337
00:17:57,660 --> 00:17:59,230
That's your opinion.

338
00:18:02,212 --> 00:18:04,809
- The winner of the match is...
- Hang on.

339
00:18:06,275 --> 00:18:09,944
Is proving that you are single-handedly
smarter than everyone else so important

340
00:18:10,064 --> 00:18:13,441
that you would rather lose by yourself
than win as part of a team?

341
00:18:15,126 --> 00:18:16,903
I don't understand the question.

342
00:18:18,365 --> 00:18:20,824
- Go ahead.
- The winner is PMS.

343
00:18:58,649 --> 00:19:00,400
Sorry, somebody's sitting there.

344
00:19:01,317 --> 00:19:02,117
Who?

345
00:19:02,443 --> 00:19:04,499
My Physics Bowl trophy.

346
00:19:07,150 --> 00:19:10,416
That trophy is meaningless.
I forfeited, therefore you did not win.

347
00:19:10,576 --> 00:19:12,746
- I know someone who would disagree.
- Who?

348
00:19:12,866 --> 00:19:15,170
My Physics Bowl trophy.

349
00:19:16,426 --> 00:19:17,924
Leonard is so smart.

350
00:19:18,710 --> 00:19:21,385
- Sheldon who?
- All right, that is very immature.

351
00:19:21,546 --> 00:19:23,975
You're right. I'm sorry.
I'm not!

352
00:19:26,456 --> 00:19:28,887
- Okay, new contest.
- What are you doing?

353
00:19:29,186 --> 00:19:31,341
I am settling once and for all

354
00:19:31,461 --> 00:19:34,176
who is the smartest around here, okay?

355
00:19:34,296 --> 00:19:35,604
- Ready?
- Absolutely.

356
00:19:35,724 --> 00:19:36,776
Bring it on.

357
00:19:37,408 --> 00:19:40,025
"Marsha, Jan and Cindy
were the three daughters

358
00:19:40,145 --> 00:19:41,573
"in what TV family?"

359
00:19:46,052 --> 00:19:47,119
<i>The Brady Bunch.</i>

360
00:19:50,033 --> 00:19:53,877
"Sammy Hagar replaced David Lee Roth
as the lead singer in what group?"

361
00:19:58,776 --> 00:19:59,916
<i>The Brady Bunch?</i>

362
00:20:02,757 --> 00:20:03,720
<i>Van Halen.</i>

363
00:20:05,256 --> 00:20:07,891
"Madonna was married
to this Ridgemont High alum."

364
00:20:10,237 --> 00:20:12,401
Oh, my God! Sean Penn!

365
00:20:12,597 --> 00:20:14,534
How do you know these things?

366
00:20:15,373 --> 00:20:18,233
I go outside,
and I talk to people.

367
00:20:20,096 --> 00:20:22,627
Okay, here.
"What actor holds the record

368
00:20:22,747 --> 00:20:25,283
"for being named <i>People Magazine</i>'s
Sexiest Man Alive?"

369
00:20:25,742 --> 00:20:27,285
- William Shatner.
- Wait.

370
00:20:30,090 --> 00:20:31,679
I don't think it's Shatner.

371
00:20:32,034 --> 00:20:33,941
Then it's got to be Patrick Stewart.

372
00:20:38,044 --> 00:20:39,296
Formal protest.

373
00:20:41,457 --> 00:20:43,718
"Singer who sang,
<i>Oops, I Did It Again</i>?"

374
00:20:51,939 --> 00:20:54,687
"Tweetie Bird tought
he taw a what?"

375
00:20:58,761 --> 00:20:59,751
Romulan.

376
00:21:04,200 --> 00:21:05,134
Yes.

377
00:21:05,254 --> 00:21:05,134
He tought he taw a Romulan.

