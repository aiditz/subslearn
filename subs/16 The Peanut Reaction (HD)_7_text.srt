1
00:00:02,917 --> 00:00:04,900
- Take him down!
- He's got you, Sheldon.

2
00:00:05,020 --> 00:00:07,399
- Look at this move!
- He's completely schooling you.

3
00:00:07,519 --> 00:00:10,688
Hey, guys, some of the other waitresses
wanted me to ask you something.

4
00:00:10,808 --> 00:00:12,137
It's called Tresling.

5
00:00:12,297 --> 00:00:14,451
It combines the physical strength
of arm wrestling

6
00:00:14,571 --> 00:00:17,768
with the mental agility of Tetris
into the ultimate sport.

7
00:00:17,928 --> 00:00:20,048
That's terrific,
but they wanted me to ask you

8
00:00:20,168 --> 00:00:21,730
to cut it the hell out.

9
00:00:21,891 --> 00:00:23,667
Great. Come here, guys. Come on.

10
00:00:23,979 --> 00:00:27,861
<i>Happy birthday to you</i>

11
00:00:28,021 --> 00:00:31,129
We might as well stop. It's a stalemate.
You're beating me in Tetris,

12
00:00:31,249 --> 00:00:34,076
but you've got the upper body
strength of a Keebler elf.

13
00:00:34,236 --> 00:00:36,537
Keebler elf?
I've got your Keebler elf right here.

14
00:00:41,576 --> 00:00:43,192
Okay, it's a stalemate.

15
00:00:45,285 --> 00:00:47,381
So, Leonard, will we be
seeing you on Saturday

16
00:00:47,542 --> 00:00:50,151
- for your free birthday cheesecake?
- He can't eat cheesecake.

17
00:00:50,271 --> 00:00:52,886
- He's lactose intolerant.
- He can have carrot cake.

18
00:00:53,047 --> 00:00:55,931
- What about the cream cheese frosting?
- He can scrape it off.

19
00:00:57,396 --> 00:01:00,102
Forget about the cake.
How do you know my birthday's Saturday?

20
00:01:00,263 --> 00:01:02,851
I did your horoscope, remember?
I was gonna do everybody's,

21
00:01:02,971 --> 00:01:05,399
until Sheldon went on
one of his typical psychotic rants.

22
00:01:06,408 --> 00:01:09,771
For the record, "that psychotic rant"
was a concise summation

23
00:01:09,891 --> 00:01:13,699
of the research of Bertram Forer,
who, in 1948, proved conclusively

24
00:01:13,860 --> 00:01:15,718
through meticulously
designed experiments

25
00:01:15,838 --> 00:01:18,203
that astrology is nothing
but pseudo-scientific hokum.

26
00:01:18,364 --> 00:01:20,163
Blah, blah, blah. Typical Taurus.

27
00:01:21,616 --> 00:01:23,207
Are we gonna see you Saturday?

28
00:01:23,369 --> 00:01:25,001
- I don't think so.
- Why not?

29
00:01:25,163 --> 00:01:27,713
- I don't celebrate my birthday.
- Shut up. Yeah, you do.

30
00:01:27,873 --> 00:01:30,007
It's no big deal;
it's just the way I was raised.

31
00:01:30,322 --> 00:01:32,634
My parents focused
on celebrating achievements,

32
00:01:32,794 --> 00:01:35,929
and being expelled from a birth canal
was not considered one of them.

33
00:01:37,202 --> 00:01:38,440
That's so silly.

34
00:01:38,560 --> 00:01:40,718
It's actually based
on very sound theories.

35
00:01:40,838 --> 00:01:43,600
- His mother published a paper on it.
- What was it called?

36
00:01:43,720 --> 00:01:45,898
"I Hate My Son
and That's Why He Can't Have Cake"?

37
00:01:47,110 --> 00:01:48,609
It was obviously effective.

38
00:01:48,770 --> 00:01:50,913
Leonard grew up to be
an experimental physicist.

39
00:01:51,033 --> 00:01:52,957
Perhaps if she'd also denied him
Christmas,

40
00:01:53,077 --> 00:01:54,595
he'd be a little better at it.

41
00:01:55,770 --> 00:01:57,534
- Thank you.
- You're welcome.

42
00:01:57,695 --> 00:01:58,989
Well, I love birthdays.

43
00:01:59,109 --> 00:02:01,827
Waking up to Mom's special
French toast breakfast,

44
00:02:01,947 --> 00:02:03,557
wearing the birthday king crown,

45
00:02:03,677 --> 00:02:05,500
playing laser tag with all my friends...

46
00:02:05,661 --> 00:02:07,711
Yeah, see?
That's what kids should have.

47
00:02:07,872 --> 00:02:09,648
Actually, that was last year.

48
00:02:13,369 --> 00:02:16,553
So you really never had
a birthday party?

49
00:02:16,713 --> 00:02:19,807
No, but it was okay.
When I was little, I'd think

50
00:02:19,927 --> 00:02:22,865
maybe my parents would change their mind
and surprise me with a party.

51
00:02:22,985 --> 00:02:25,312
Like this one birthday
I came home from my cello lesson

52
00:02:25,473 --> 00:02:27,676
and I saw a lot of strange
cars parked out front.

53
00:02:27,796 --> 00:02:30,102
When I got to the door,
I could hear people whispering

54
00:02:30,222 --> 00:02:33,153
and I could smell German chocolate cake,
which is my favorite.

55
00:02:34,269 --> 00:02:35,030
And?

56
00:02:35,191 --> 00:02:37,221
It turns out my grandfather had died.

57
00:02:38,669 --> 00:02:40,827
Oh, my God. That's terrible.

58
00:02:40,987 --> 00:02:43,205
Well, it was kind
of like a birthday party.

59
00:02:44,114 --> 00:02:47,237
I got to see all my cousins,
and there was cake, so...

60
00:02:48,551 --> 00:02:50,378
It's the saddest thing I've ever heard.

61
00:02:50,539 --> 00:02:51,380
You think?

62
00:02:51,541 --> 00:02:53,632
Go ahead, tell her about
your senior prom.

63
00:02:56,348 --> 00:02:58,805
The Big Bang Theory
Season 1 Episode 16 - The Peanut Reaction

64
00:02:59,490 --> 00:03:03,147
Transcript: swsub.com
Synch: SerJo

65
00:03:20,476 --> 00:03:22,356
Make sure they remember, no peanuts.

66
00:03:22,476 --> 00:03:25,984
Howard, every Thai restaurant in town
knows you can't eat peanuts.

67
00:03:27,088 --> 00:03:30,186
When they see me coming,
they go, "Ah, no-peanut boy!"

68
00:03:41,547 --> 00:03:44,882
- Hello, Penny. Leonard just left.
- I know. I want to talk to you.

69
00:03:45,410 --> 00:03:46,718
What would we talk about?

70
00:03:47,376 --> 00:03:49,495
We have no overlapping
areas of interest I'm aware of.

71
00:03:49,615 --> 00:03:51,498
As you know, I don't care for chitchat.

72
00:03:51,869 --> 00:03:53,657
- Can you just let me in?
- All right,

73
00:03:53,777 --> 00:03:55,831
but I don't see this
as a promising endeavor.

74
00:03:57,693 --> 00:04:00,440
Here's the deal. We're gonna throw
Leonard a kick-ass surprise party

75
00:04:00,601 --> 00:04:03,014
- for his birthday on Saturday.
- I hardly think so.

76
00:04:03,134 --> 00:04:05,236
Leonard made it very clear
he doesn't want a party.

77
00:04:05,398 --> 00:04:06,987
Did someone say... "party"?

78
00:04:09,199 --> 00:04:12,311
He just doesn't know he wants
one 'cause he's never had one.

79
00:04:12,431 --> 00:04:14,469
I suppose that's possible,
but for the record,

80
00:04:14,589 --> 00:04:17,249
I've never had a threesome
and yet I still know I want one.

81
00:04:19,702 --> 00:04:22,234
Howard, here's the difference...
The possibility exists

82
00:04:22,354 --> 00:04:25,591
that Leonard could have a birthday party
before hell freezes over.

83
00:04:27,796 --> 00:04:30,512
Fine. If I do have a threesome,
you can't be part of it.

84
00:04:32,340 --> 00:04:34,015
I'm just kidding. Yes, you can.

85
00:04:35,260 --> 00:04:36,835
Can you bring a friend?

86
00:04:39,010 --> 00:04:41,442
I think a birthday party's
a terrible idea.

87
00:04:41,562 --> 00:04:44,457
I envy Leonard for growing up
without that anguish.

88
00:04:44,577 --> 00:04:45,449
Anguish?!

89
00:04:45,569 --> 00:04:47,611
Year after year, I had to endure

90
00:04:47,773 --> 00:04:49,920
wearing comical hats while being forced

91
00:04:50,040 --> 00:04:52,387
into the crowded, sweaty
hell of bouncy castles.

92
00:04:52,507 --> 00:04:54,304
Not to mention being blindfolded

93
00:04:54,424 --> 00:04:56,566
and spun toward a grotesque
tailless donkey

94
00:04:56,686 --> 00:04:59,015
as the other children mocked
my disorientation.

95
00:05:00,494 --> 00:05:01,959
Okay, sweetie, I understand

96
00:05:02,121 --> 00:05:04,670
you have scars
that no nonprofessional can heal,

97
00:05:05,106 --> 00:05:07,757
but, nevertheless, we're going
to throw Leonard a birthday party.

98
00:05:07,917 --> 00:05:10,327
Have I pointed out that I'm extremely
uncomfortable with dancing,

99
00:05:10,447 --> 00:05:13,346
loud music and most other forms
of alcohol-induced frivolity?

100
00:05:14,994 --> 00:05:16,798
- Nevertheless...
- In addition, I...

101
00:05:16,918 --> 00:05:17,975
Here's the deal:

102
00:05:18,136 --> 00:05:21,369
You either help me throw a birthday
party or I will go into your bedroom

103
00:05:21,489 --> 00:05:24,046
and unbag all of your most valuable
mint-condition comic books.

104
00:05:24,166 --> 00:05:25,985
And on one of them,
you won't know which,

105
00:05:26,105 --> 00:05:28,028
I'll draw a tiny happy face in ink.

106
00:05:30,531 --> 00:05:33,491
You can't do that. If you make a mark
in a mint comic, it's no longer mint.

107
00:05:34,659 --> 00:05:36,745
Do you understand
the concept of blackmail?

108
00:05:36,905 --> 00:05:38,356
Well, of course, I...

109
00:05:41,259 --> 00:05:44,376
I have an idea. Let's throw Leonard
a kick-ass birthday party.

110
00:06:04,290 --> 00:06:05,807
That's not the secret knock.

111
00:06:07,562 --> 00:06:09,204
This is the secret knock.

112
00:06:12,998 --> 00:06:14,449
What difference does it make?

113
00:06:14,609 --> 00:06:17,738
The whole point of a secret knock
is to establish a non-verbal signal

114
00:06:17,858 --> 00:06:20,630
to verify the identity
of one's co-conspirators.

115
00:06:21,059 --> 00:06:23,922
- Is that Raj and Howard?
- Possibly, but unverified.

116
00:06:24,419 --> 00:06:26,016
Will you just let us in.

117
00:06:26,136 --> 00:06:28,851
Luckily for you,
this is not a nuclear reactor.

118
00:06:29,426 --> 00:06:32,550
- So what'd you get the birthday boy?
- Well, Raj got him an awesome,

119
00:06:32,710 --> 00:06:36,140
limited edition Dark Knight sculpture
based on Alex Ross's definitive Batman.

120
00:06:36,260 --> 00:06:38,199
And I got him this amazing
autographed copy

121
00:06:38,319 --> 00:06:40,058
of <i>The Feynman Lectures on Physics</i>.

122
00:06:40,579 --> 00:06:41,415
Nice.

123
00:06:41,915 --> 00:06:43,330
I got him a sweater.

124
00:06:44,484 --> 00:06:47,404
Okay, well, he might like that.
I've seen him get chilly.

125
00:06:48,994 --> 00:06:52,112
- Sheldon, I didn't see your present.
- That's because I didn't bring one.

126
00:06:52,272 --> 00:06:53,586
- Why not?
- Don't ask.

127
00:06:53,706 --> 00:06:56,616
The entire institution
of gift-giving makes no sense.

128
00:06:56,777 --> 00:06:58,587
- Too late.
- Let's say I go out

129
00:06:58,707 --> 00:07:00,305
and I spend $50 on you.

130
00:07:00,425 --> 00:07:03,248
It's a laborious activity
because I have to imagine what you need,

131
00:07:03,408 --> 00:07:04,999
whereas you know what you need.

132
00:07:05,159 --> 00:07:08,294
Now, I could simplify things...
just give you the $50 directly,

133
00:07:08,454 --> 00:07:10,046
and then you could give me $50

134
00:07:10,206 --> 00:07:12,634
on my birthday, and so on,
until one of us dies,

135
00:07:12,754 --> 00:07:15,168
leaving the other one old
and $50 richer.

136
00:07:17,630 --> 00:07:19,538
And I ask you, is it worth it?

137
00:07:20,836 --> 00:07:22,216
Told you not to ask.

138
00:07:23,004 --> 00:07:26,229
Sheldon, you're his friend.
Friends give each other presents.

139
00:07:26,716 --> 00:07:28,815
I accept your premise;
I reject your conclusion.

140
00:07:31,090 --> 00:07:34,570
Try telling him
it's a non-optional social convention.

141
00:07:35,111 --> 00:07:36,630
- What?
- Just do it.

142
00:07:39,527 --> 00:07:42,787
It's a non-optional...
social convention.

143
00:07:44,042 --> 00:07:45,404
Oh. Fair enough.

144
00:07:49,120 --> 00:07:50,686
He came with a manual.

145
00:07:52,019 --> 00:07:54,924
Question. How am I going to get Leonard
a present before the party?

146
00:07:55,256 --> 00:07:58,015
I don't drive and the only things
available within walking distance

147
00:07:58,135 --> 00:07:59,978
are a Thai restaurant and a gas station.

148
00:08:00,098 --> 00:08:02,132
I suppose I could wrap up
an order of mee krob

149
00:08:02,252 --> 00:08:03,969
and a couple of lottery scratchers.

150
00:08:04,674 --> 00:08:05,945
Okay let's do this...

151
00:08:06,065 --> 00:08:09,125
I will drive Sheldon to get a present.
Howard, you need to get rid of Leonard

152
00:08:09,245 --> 00:08:10,898
- for about 2 hours.
- No problem.

153
00:08:11,110 --> 00:08:14,362
And then, Raj, you bring the stuff
across the hall and start setting up.

154
00:08:15,416 --> 00:08:16,778
What if guests show up?

155
00:08:17,302 --> 00:08:18,574
Entertain them.

156
00:08:19,307 --> 00:08:20,819
What if they're women?

157
00:08:22,262 --> 00:08:24,496
Stare at them and make them
feel uncomfortable.

158
00:08:37,675 --> 00:08:39,585
- How's it goin'?
- Fine.

159
00:08:40,616 --> 00:08:43,890
So listen, the NuArt
is showing the revised,

160
00:08:44,051 --> 00:08:45,975
definitive cut of <i>Blade Runner</i>.

161
00:08:46,136 --> 00:08:47,018
Seen it.

162
00:08:47,179 --> 00:08:49,891
No, you've seen
the 25th Anniversary Final Cut.

163
00:08:50,745 --> 00:08:53,691
This one has eight seconds
of previously unseen footage.

164
00:08:53,852 --> 00:08:56,528
They say it completely changes
the tone of the film.

165
00:08:57,306 --> 00:08:58,112
Pass.

166
00:08:58,959 --> 00:09:02,928
Come on. Afterwards, there's a Q & A
with Harrison Ford's body double.

167
00:09:03,779 --> 00:09:06,311
Look, I'm in the Halo
battle of my life here.

168
00:09:06,431 --> 00:09:08,843
There's this kid in Copenhagen...
he has no immune system,

169
00:09:08,963 --> 00:09:12,001
so all he does is sit in his bubble
and play Halo 24-seven.

170
00:09:13,044 --> 00:09:16,776
- Can't you play him some other time?
- Not if you believe his doctors.

171
00:09:20,651 --> 00:09:22,499
Oh, my God, do you smell gas?

172
00:09:24,006 --> 00:09:25,442
- No.
- Yeah, no.

173
00:09:30,719 --> 00:09:32,230
They have DVDs over there.

174
00:09:32,391 --> 00:09:35,115
Yes, but they have
DVD burners over here.

175
00:09:36,427 --> 00:09:38,421
Leonard needs a DVD burner.

176
00:09:39,476 --> 00:09:43,032
A gift shouldn't be something someone
needs, it should be something fun.

177
00:09:43,193 --> 00:09:45,330
Something
they wouldn't buy for themself.

178
00:09:45,536 --> 00:09:47,248
You mean like a sweater?

179
00:09:50,342 --> 00:09:51,833
It's a fun sweater.

180
00:09:51,993 --> 00:09:54,127
It's got a bold geometric print.

181
00:09:54,614 --> 00:09:56,837
Is it the geometry that makes it fun?

182
00:09:57,917 --> 00:10:00,582
The point is one of the ways
we show we care about people

183
00:10:00,702 --> 00:10:03,595
is by putting thought and imagination
into the gifts we give them.

184
00:10:04,279 --> 00:10:07,223
Okay, I see. So, not a DVD burner.

185
00:10:07,932 --> 00:10:08,707
Exactly.

186
00:10:08,827 --> 00:10:11,475
Something he wouldn't buy for himself,
something fun,

187
00:10:11,595 --> 00:10:14,802
Something like...
An 802-11n wireless router!

188
00:10:19,271 --> 00:10:21,149
Here you go, Copenhagen boy.

189
00:10:21,269 --> 00:10:24,052
How about a taste
of Hans Christian hand grenade?

190
00:10:27,270 --> 00:10:28,738
That could not feel good.

191
00:10:32,127 --> 00:10:34,328
Come on, come... Oh, you clever...

192
00:10:38,872 --> 00:10:39,855
Come on.

193
00:10:41,181 --> 00:10:42,206
Take that!

194
00:10:49,563 --> 00:10:51,768
- What's the matter?
- This granola bar has peanuts in it!

195
00:10:51,928 --> 00:10:55,433
- Oh, my God. Why did you eat it?
- I don't know. It was just there!

196
00:10:55,765 --> 00:10:58,576
If I had a gun there,
would you have shot yourself?

197
00:10:59,306 --> 00:11:01,986
Don't yell at me!
I've got to go to the emergency room!

198
00:11:02,146 --> 00:11:04,341
- Now?!
- No, after my tongue has swollen

199
00:11:04,461 --> 00:11:05,990
to the size of a brisket!

200
00:11:06,894 --> 00:11:09,243
All right, just let me get my keys.

201
00:11:09,751 --> 00:11:11,491
Oh, God, oh, God, oh, God!

202
00:11:12,993 --> 00:11:15,166
The laundry is out of the hamper.

203
00:11:18,839 --> 00:11:21,235
Okay, Sheldon,
what was it supposed to be?

204
00:11:22,340 --> 00:11:24,213
Fine. It's out of the washer!

205
00:11:25,119 --> 00:11:27,271
I'll call you when it's in the dryer.

206
00:11:28,167 --> 00:11:29,574
All right, let's go.

207
00:11:41,208 --> 00:11:43,262
- What do you think?
- That one.

208
00:11:43,423 --> 00:11:45,725
Because of the two
additional Ethernet ports?

209
00:11:45,845 --> 00:11:47,010
- Sure.
- No need,

210
00:11:47,130 --> 00:11:49,227
he's already got
a six-port Ethernet switch.

211
00:11:49,347 --> 00:11:50,938
- Okay, then this one.
- Why?

212
00:11:51,260 --> 00:11:53,804
I don't know.
The man on the box looks so happy.

213
00:11:54,601 --> 00:11:57,777
Penny, if I'm going to buy Leonard
a gift, I'm going to do it right.

214
00:11:58,113 --> 00:12:01,031
I refuse to let him experience
the same childhood trauma I did.

215
00:12:01,902 --> 00:12:04,434
I know I'm gonna regret this,
but: What trauma?

216
00:12:06,228 --> 00:12:07,495
On my 12th birthday,

217
00:12:07,656 --> 00:12:09,919
I really wanted a titanium centrifuge

218
00:12:10,039 --> 00:12:12,626
so, you know,
I could separate radioactive isotopes.

219
00:12:13,164 --> 00:12:14,103
Of course.

220
00:12:14,367 --> 00:12:17,674
Instead of a titanium centrifuge,
my parents bought me...

221
00:12:18,624 --> 00:12:19,708
This is hard.

222
00:12:20,873 --> 00:12:21,901
They got me

223
00:12:22,472 --> 00:12:23,965
a motorized dirt bike.

224
00:12:28,100 --> 00:12:30,895
What 12-year-old boy wants
a motorized dirt bike?

225
00:12:32,543 --> 00:12:33,597
All of them?

226
00:12:34,116 --> 00:12:35,314
- Really?
- Yeah.

227
00:12:38,966 --> 00:12:40,331
So we're getting this one?

228
00:12:40,451 --> 00:12:42,547
- Yeah, I suppose.
- All right, let's go.

229
00:12:43,182 --> 00:12:45,659
Excuse me.
Do you know anything about this stuff?

230
00:12:46,077 --> 00:12:48,036
I know everything about this stuff.

231
00:12:48,197 --> 00:12:50,830
Okay, I have my own
wholesale flower business

232
00:12:50,991 --> 00:12:53,303
and I want to hook up
my computer in the front entrance

233
00:12:53,423 --> 00:12:55,370
with the one in my
refrigerated warehouse.

234
00:12:55,490 --> 00:12:57,931
Buy this one:
it's the one we're getting. Happy guy.

235
00:12:58,051 --> 00:12:59,500
No, she doesn't want that.

236
00:12:59,620 --> 00:13:03,541
She needs a point-to-point
peer network with a range extender.

237
00:13:03,867 --> 00:13:04,827
Thank you.

238
00:13:04,947 --> 00:13:06,699
Which hard drive do I want...

239
00:13:06,819 --> 00:13:08,515
firewire or USB?

240
00:13:08,675 --> 00:13:10,684
It depends on what buss
you have available.

241
00:13:14,622 --> 00:13:16,793
I drive a Chevy Cavalier.

242
00:13:18,473 --> 00:13:19,902
Oh, dear Lord.

243
00:13:20,063 --> 00:13:21,653
- We have to go.
- Not now.

244
00:13:21,813 --> 00:13:24,782
This poor man needs me.
You, hold on. I'll be right with you.

245
00:13:25,406 --> 00:13:28,682
What computer do you have?
And please don't say "a white one."

246
00:13:34,785 --> 00:13:37,210
- Excuse me.
- Fill this out. Have a seat.

247
00:13:37,372 --> 00:13:39,563
Listen. We're throwing
my friend a surprise party

248
00:13:39,683 --> 00:13:42,121
I'm supposed to keep him out
of his apartment for 2 hours.

249
00:13:42,494 --> 00:13:44,050
Fill this out and have a seat.

250
00:13:44,261 --> 00:13:46,128
No, the only way I could
get him to leave

251
00:13:46,248 --> 00:13:48,888
was to tell him I ate a peanut
because I'm allergic to peanuts.

252
00:13:49,050 --> 00:13:51,848
Well, in that case,
fill this out and have a seat.

253
00:13:53,085 --> 00:13:55,826
All I need from you is to take me
in the back and give me a Band-Aid

254
00:13:55,946 --> 00:13:58,474
so I can pretend I had a shot of
epinephrine, and you tell my friend

255
00:13:58,594 --> 00:14:00,655
you need to keep me under observation
for 1 hour 1/2.

256
00:14:00,775 --> 00:14:02,179
- Is that all you need?
- Yes.

257
00:14:02,299 --> 00:14:03,695
Get out of my ER.

258
00:14:04,656 --> 00:14:07,501
- No, you don't understand.
- I understand, but unfortunately,

259
00:14:07,621 --> 00:14:10,244
this hospital is not equipped
to treat stupid.

260
00:14:11,995 --> 00:14:14,380
Okay, I get it.
I know how the world works.

261
00:14:14,646 --> 00:14:16,856
How about if I were to introduce to you

262
00:14:17,297 --> 00:14:19,433
to the man who freed your people?

263
00:14:24,232 --> 00:14:26,713
Unless my people were freed
by Benjamin Franklin

264
00:14:26,833 --> 00:14:29,345
and his five twin brothers,
you are wasting your time.

265
00:14:32,004 --> 00:14:33,976
Sorry, I couldn't find a parking spot.

266
00:14:34,096 --> 00:14:36,220
- How are you doing?
- Bad, very bad.

267
00:14:36,358 --> 00:14:38,636
Really? You don't look like
you're swelling up at all.

268
00:14:38,756 --> 00:14:41,191
We should just pick up some Benadryl
at the drugstore and go home.

269
00:14:41,351 --> 00:14:43,433
- We can't go home.
- Why not?

270
00:14:43,940 --> 00:14:45,174
Because...

271
00:14:45,888 --> 00:14:47,005
because...

272
00:14:47,955 --> 00:14:49,575
Brisket! Brisket!

273
00:14:51,132 --> 00:14:53,563
- Water! Need water!
- I'll be right back.

274
00:14:58,478 --> 00:14:59,751
I've got a problem.

275
00:15:00,101 --> 00:15:02,663
Well, so do I.
You gotta stall Leonard a little longer.

276
00:15:02,873 --> 00:15:04,601
- I don't think I can.
- You have to.

277
00:15:04,721 --> 00:15:06,976
We all have to be there
at the same time to yell surprise.

278
00:15:07,096 --> 00:15:09,887
You have to understand something.
We're in a hospital right now.

279
00:15:10,048 --> 00:15:13,078
- Why? Is Leonard okay?
- Leonard's fine. I'm fine.

280
00:15:13,198 --> 00:15:14,850
Thanks for asking, by the way.

281
00:15:15,327 --> 00:15:18,455
Okay, I don't need your attitude.
Just hold him there a little longer.

282
00:15:18,575 --> 00:15:21,565
I've done my best, but he wants to go
home and I don't know how to stop him.

283
00:15:22,569 --> 00:15:25,031
Okay, how about this?
You keep him there a little longer,

284
00:15:25,151 --> 00:15:28,179
and when you get to the party, I'll
point out which of my friends are easy?

285
00:15:38,001 --> 00:15:39,794
Don't toy with me, woman.

286
00:15:41,551 --> 00:15:43,837
I got a hot former fat girl
with no self-esteem,

287
00:15:43,997 --> 00:15:46,135
a girl who punishes her father
by sleeping around

288
00:15:46,255 --> 00:15:50,122
a alcoholic who's 2 tequila shots away
from letting you wear her like a hat.

289
00:15:51,988 --> 00:15:53,347
Thy will be done.

290
00:16:03,889 --> 00:16:06,030
I'm doing this for you, little buddy.

291
00:16:12,243 --> 00:16:14,374
Okay, we don't have that in stock...

292
00:16:15,904 --> 00:16:18,302
but I can special-order it for you.

293
00:16:19,011 --> 00:16:19,783
Him.

294
00:16:20,770 --> 00:16:23,043
Excuse me, sir.
You don't work here.

295
00:16:24,100 --> 00:16:26,380
Yes, well, apparently,
neither does anyone else.

296
00:16:27,923 --> 00:16:29,883
- Sheldon, we have to go.
- Why?

297
00:16:30,044 --> 00:16:32,592
For one thing, we're late for
Leonard's birthday party,

298
00:16:32,712 --> 00:16:34,721
and for another,
I told him to call security.

299
00:16:36,251 --> 00:16:37,415
Good luck.

300
00:16:39,636 --> 00:16:42,233
By the way, a six-year-old
could hack your computer system.

301
00:16:42,353 --> 00:16:43,462
Keep walking.

302
00:16:43,582 --> 00:16:45,844
"1234" is not a secure password.

303
00:16:48,747 --> 00:16:51,651
Excuse me, my friend is having
an allergic reaction to peanuts.

304
00:16:51,964 --> 00:16:53,824
- No, he's not.
- Yes, he is.

305
00:16:53,984 --> 00:16:56,374
Look, sir, we are very
busy here, and I...

306
00:16:56,759 --> 00:16:58,411
Holy crap!

307
00:16:59,828 --> 00:17:01,114
Please help me!

308
00:17:02,299 --> 00:17:03,917
Code four! I need a gurney!

309
00:17:05,444 --> 00:17:07,458
- Right away! Right away!
- Thank you.

310
00:17:11,742 --> 00:17:14,428
Say what you will about the
healthcare system in this country,

311
00:17:14,548 --> 00:17:17,451
but when they're afraid of lawsuits,
they sure test everything.

312
00:17:18,572 --> 00:17:21,370
I really don't think
the colonoscopy was necessary.

313
00:17:24,005 --> 00:17:25,855
You know, before you got all swollen up,

314
00:17:25,986 --> 00:17:28,671
I actually thought you were trying
to keep me out of the apartment

315
00:17:28,791 --> 00:17:30,822
so you could throw me a surprise party.

316
00:17:31,555 --> 00:17:34,239
Right, it's your birthday.
I had no idea it was your birthday.

317
00:17:34,400 --> 00:17:36,890
I completely forgot.
What a lousy way to spend a birthday.

318
00:17:37,010 --> 00:17:38,264
It's all over now.

319
00:17:42,347 --> 00:17:44,015
There is a party, isn't there?

320
00:17:44,950 --> 00:17:45,829
Maybe.

321
00:17:49,249 --> 00:17:52,069
- Are you mad?
- How could I be mad?

322
00:17:52,732 --> 00:17:55,968
You actually risked your life
because you care about me.

323
00:17:57,680 --> 00:17:59,389
Yeah, that's why I did it.

324
00:18:02,447 --> 00:18:03,435
All right.

325
00:18:04,215 --> 00:18:05,228
Here we go.

326
00:18:06,311 --> 00:18:07,907
My first birthday party.

327
00:18:18,359 --> 00:18:19,254
Dude,

328
00:18:19,943 --> 00:18:21,495
everybody left an hour ago.

329
00:18:24,396 --> 00:18:25,832
Surprise!

330
00:18:31,367 --> 00:18:34,418
Okay, Leonard.
I am at your birthday party.

331
00:18:34,735 --> 00:18:38,045
I don't know where you are,
dude, but it's really kick-ass!

332
00:18:38,817 --> 00:18:41,753
Everyone is very, very drunk and...

333
00:18:43,822 --> 00:18:46,685
And look,
there's a girl taking her shirt off!

334
00:18:48,420 --> 00:18:50,035
That's my friend Carol.

335
00:18:50,378 --> 00:18:52,935
Remind me,
I gotta introduce her to Howard.

336
00:18:53,581 --> 00:18:56,917
Oh, sweet Krishna,
shake that rupee maker!

337
00:18:59,149 --> 00:19:01,481
I'm so sorry you didn't get your party.

338
00:19:01,884 --> 00:19:02,932
It's okay.

339
00:19:05,347 --> 00:19:06,881
Happy birthday, anyways.

340
00:19:15,409 --> 00:19:06,881
When's your birthday?

