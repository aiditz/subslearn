1
00:00:08,511 --> 00:00:10,366
Okay, if no one else will say it,
I will.

2
00:00:11,088 --> 00:00:12,863
We really suck at paintball.

3
00:00:14,522 --> 00:00:16,316
That was absolutely humiliating.

4
00:00:16,490 --> 00:00:19,114
Come on, some battles you win,
some battles you lose.

5
00:00:19,480 --> 00:00:24,058
Yes, but you don't have to lose
to Kyle Burnsteen's Bar Mitzva party.

6
00:00:25,527 --> 00:00:26,935
I think we have to acknowlegde,

7
00:00:27,070 --> 00:00:30,340
those was some fairly savage
preadolescent Jews.

8
00:00:32,871 --> 00:00:35,302
No, we were annihilated
by our own incompetence

9
00:00:35,484 --> 00:00:37,780
and the inability
of some people to follow

10
00:00:38,108 --> 00:00:40,093
- the chain of command.
- Sheldon, let it go!

11
00:00:40,274 --> 00:00:43,727
No, I want to talk about the fact
that Wolowitz shot me in the back.

12
00:00:45,233 --> 00:00:48,590
I shot you for a good reason.
You were leading us into disaster.

13
00:00:49,692 --> 00:00:51,839
I was giving clear,
concise orders.

14
00:00:52,310 --> 00:00:55,706
You hid behind a tree yelling,
"Get the kid in the yarmulke!

15
00:00:56,649 --> 00:00:57,786
Oh, hey, guys.

16
00:00:58,721 --> 00:00:59,800
Morning, ma'am.

17
00:01:00,899 --> 00:01:02,809
So, how was paintball?
Did you have fun?

18
00:01:03,270 --> 00:01:06,260
Sure, if you consider being fragged
by your own troops fun.

19
00:01:07,765 --> 00:01:10,820
You clear space on your calendar--
there will be an inquiry.

20
00:01:12,419 --> 00:01:13,808
I'm having a party on Saturday,

21
00:01:13,963 --> 00:01:15,776
so if you are around,
you should come on by.

22
00:01:15,985 --> 00:01:17,066
A party?
Yeah.

23
00:01:17,284 --> 00:01:19,250
A... "boy-girl" party?

24
00:01:21,581 --> 00:01:22,932
Well, there will be boys

25
00:01:23,125 --> 00:01:25,228
and there will be girls
and it is a party, so...

26
00:01:25,567 --> 00:01:28,481
It'll be a bunch of my friends.
We'll have some beer, a little dancing.

27
00:01:28,799 --> 00:01:30,873
- Dancing?
- Yeah, I don't know, Penny...

28
00:01:31,021 --> 00:01:33,696
- The thing is, we're not...
- No, we're really more of a...

29
00:01:35,559 --> 00:01:37,279
But thanks.
Thanks for thinking of us.

30
00:01:37,600 --> 00:01:39,278
Are you sure?
Come on, it's Halloween.

31
00:01:39,515 --> 00:01:40,717
A Halloween party?

32
00:01:41,007 --> 00:01:42,319
As in... costumes?

33
00:01:42,581 --> 00:01:43,816
Well, yeah.

34
00:01:52,773 --> 00:01:53,969
Is there a theme?

35
00:01:54,592 --> 00:01:55,846
Yeah, Halloween.

36
00:01:56,791 --> 00:01:59,453
Yes, but are the costumes
random, or genre-specific?

37
00:02:00,302 --> 00:02:02,405
As usual, I'm not following.

38
00:02:03,089 --> 00:02:06,784
He's asking if we can come as anyone
from science fiction, fantasy...

39
00:02:07,051 --> 00:02:08,325
- Sure.
- Comic books?

40
00:02:08,629 --> 00:02:09,557
Fine.
Anime?

41
00:02:09,769 --> 00:02:10,923
Of course.
TV, film,

42
00:02:11,100 --> 00:02:13,199
D- and-D, manga, Greek gods,
Roman gods, Norse gods--

43
00:02:13,509 --> 00:02:15,032
Anything you want!
Okay?

44
00:02:15,325 --> 00:02:17,139
Any costume you want.

45
00:02:18,521 --> 00:02:19,521
Bye.

46
00:02:21,815 --> 00:02:23,918
Gentlemen,
to the sewing machines.

47
00:02:43,923 --> 00:02:48,141
The Big Bang Theory
Season 1 Episode 06 - The Middle Earth Paradigm
Synchro: Sixe, SerJo

48
00:02:51,676 --> 00:02:52,735
I'll get it.

49
00:03:08,152 --> 00:03:09,366
Oh, no.

50
00:03:13,459 --> 00:03:16,025
Make way
for the fastest man alive!

51
00:03:18,394 --> 00:03:21,064
See, this is why I wanted
to have a costume meeting.

52
00:03:21,353 --> 00:03:23,563
We all have other costumes:
We can change.

53
00:03:24,207 --> 00:03:26,750
Or we could walk right
behind each other all night

54
00:03:26,989 --> 00:03:29,304
and look like one person going
really fast.

55
00:03:36,882 --> 00:03:38,192
No, no, no.

56
00:03:38,329 --> 00:03:41,293
It's a boy-girl party,
this flash runs solo.

57
00:03:42,925 --> 00:03:46,548
How about this? Nobody gets
to be The Flash. We all change. Agreed?

58
00:03:48,144 --> 00:03:49,184
Agreed.

59
00:03:51,453 --> 00:03:52,453
I call Frodo!

60
00:03:52,801 --> 00:03:53,801
Damn!

61
00:04:04,677 --> 00:04:08,081
Sorry I'm late, but my hammer
got stuck in the door on the bus.

62
00:04:08,867 --> 00:04:09,947
You went with Thor?

63
00:04:10,149 --> 00:04:13,043
What, just because I'm Indian
I can't be a Norse god?

64
00:04:13,893 --> 00:04:16,467
"No, no,
Raj has to be an Indian god."

65
00:04:17,002 --> 00:04:18,002
That's racism.

66
00:04:18,440 --> 00:04:19,655
I mean look at Wolowitz.

67
00:04:19,848 --> 00:04:22,010
He's not English,
but he's dressed like Peter Pan.

68
00:04:23,761 --> 00:04:25,368
Sheldon is neither
sound nor light,

69
00:04:25,673 --> 00:04:27,378
but he's obviously
the Doppler effect.

70
00:04:32,098 --> 00:04:33,217
I'm not Peter Pan.

71
00:04:33,469 --> 00:04:34,683
I'm Robin Hood.

72
00:04:35,186 --> 00:04:37,073
Really?
Because I sawPeter Pan,

73
00:04:37,225 --> 00:04:39,251
and you're dressed
exactly like Cathy Rigby.

74
00:04:40,880 --> 00:04:43,639
She was a little bigger than you,
but it's basically the same look, man.

75
00:04:45,651 --> 00:04:48,547
There's something I want to talk
to you about before we go to the party.

76
00:04:48,701 --> 00:04:50,187
I don't care if anybody gets it.

77
00:04:50,438 --> 00:04:52,386
I'm going as the Doppler effect.

78
00:04:53,416 --> 00:04:55,905
- No, it's not that.
- If I have to, I can demonstrate.

79
00:05:00,732 --> 00:05:01,732
Terrific.

80
00:05:03,688 --> 00:05:06,655
This party is my first chance
for Penny to see me

81
00:05:06,948 --> 00:05:09,202
in the context of her social group,

82
00:05:09,588 --> 00:05:11,999
and I need you not
to embarrass me tonight.

83
00:05:13,450 --> 00:05:15,317
What exactly do you mean
by embarrass you?

84
00:05:16,385 --> 00:05:18,303
For example, tonight,

85
00:05:18,477 --> 00:05:21,274
no one needs to know
that my middle name is Leakey.

86
00:05:21,795 --> 00:05:23,776
But there's nothing
embarrassing about that.

87
00:05:23,969 --> 00:05:26,993
Your father worked with Louis Leakey,
a great anthropologist.

88
00:05:27,186 --> 00:05:29,054
It had nothing to do
with your bed-wetting.

89
00:05:31,375 --> 00:05:34,342
All I'm saying is that this party
is the perfect opportunity

90
00:05:34,535 --> 00:05:37,408
for Penny to see me
as a member of her peer group,

91
00:05:37,642 --> 00:05:40,721
a potential close friend,
and perhaps more,

92
00:05:42,348 --> 00:05:44,226
and I don't want to look like a dork.

93
00:05:55,132 --> 00:05:56,344
Just a heads up, fellas.

94
00:05:56,499 --> 00:05:59,026
If anyone gets lucky, I've got
a dozen condoms in my quiver.

95
00:06:02,189 --> 00:06:03,193
Hey, guys.

96
00:06:03,405 --> 00:06:04,591
Hey. Sorry we're late.

97
00:06:05,094 --> 00:06:06,907
Late?
It's 7:05.

98
00:06:07,198 --> 00:06:08,780
You said the party starts at 7:00.

99
00:06:09,632 --> 00:06:11,523
Yeah, I mean,
when you start a party at 7:00,

100
00:06:11,716 --> 00:06:13,928
no one shows up at,
you know, 7:00.

101
00:06:15,718 --> 00:06:16,855
It's 7:05.

102
00:06:18,472 --> 00:06:19,859
Yes. Yes, it is.

103
00:06:20,084 --> 00:06:22,206
Okay. Well, um, come on in.

104
00:06:30,393 --> 00:06:32,681
So, what, are all the girls
in the bathroom?

105
00:06:33,290 --> 00:06:35,455
Probably,
but in their own homes.

106
00:06:37,293 --> 00:06:39,743
So, what time does
the costume parade start?

107
00:06:40,602 --> 00:06:41,700
The parade?

108
00:06:42,023 --> 00:06:44,898
Yeah, so the judges can give out
the prizes for best costume.

109
00:06:45,067 --> 00:06:47,087
You know, most frightening,
most authentic,

110
00:06:47,473 --> 00:06:50,222
most accurate visualization
of a scientific principal.

111
00:06:52,434 --> 00:06:54,041
Oh, Sheldon, I'm sorry,

112
00:06:54,253 --> 00:06:57,745
but there aren't going to be any parades
or judges or prizes.

113
00:06:59,339 --> 00:07:01,292
This party is just going to suck.

114
00:07:02,297 --> 00:07:03,961
No!
Come on, it's going to be fun,

115
00:07:04,125 --> 00:07:06,093
and you all look great.
I mean, look at you, Thor,

116
00:07:06,361 --> 00:07:07,268
and, oh,

117
00:07:07,518 --> 00:07:09,472
Peter Pan.
That's so cute.

118
00:07:10,359 --> 00:07:13,143
- Actually, he's Robin Hood.
- I'm Peter Pan.

119
00:07:14,314 --> 00:07:17,227
And I got a handful of pixie dust
with your name on it.

120
00:07:19,324 --> 00:07:20,324
No, you don't.

121
00:07:22,994 --> 00:07:24,460
What's Sheldon supposed to be?

122
00:07:25,064 --> 00:07:26,743
He's the Doppler effect.

123
00:07:27,007 --> 00:07:29,605
Yes. It's the apparent change
in the frequency of a wave

124
00:07:29,755 --> 00:07:33,618
caused by relative motion between
the source of the wave and the observer.

125
00:07:35,562 --> 00:07:38,597
Oh, sure, I see it now.
The Doppler effect.

126
00:07:39,231 --> 00:07:40,543
All right, I got to shower.

127
00:07:40,678 --> 00:07:42,733
You guys...
make yourselves comfortable.

128
00:07:46,150 --> 00:07:47,770
See?
People get it.

129
00:08:02,154 --> 00:08:04,709
By Odin's beard,
this is good Chex mix.

130
00:08:05,906 --> 00:08:07,293
No, thanks. Peanuts.

131
00:08:07,487 --> 00:08:09,899
I can't afford to swell up
in these tights.

132
00:08:10,528 --> 00:08:11,780
I'm confused.

133
00:08:11,975 --> 00:08:14,656
If there's no costume parade,
what are we doing here?

134
00:08:15,577 --> 00:08:18,401
We're socializing,
meeting new people.

135
00:08:18,890 --> 00:08:20,200
Telepathically?

136
00:08:21,679 --> 00:08:23,666
Oh, hey, when
did you get here?

137
00:08:24,976 --> 00:08:28,040
Penny is wearing the worst
Catwoman costume I've ever seen.

138
00:08:28,322 --> 00:08:30,059
And that includes Halle Berry's.

139
00:08:30,337 --> 00:08:33,128
She's not Catwoman.
She's just a generic cat.

140
00:08:33,336 --> 00:08:35,221
And that's the kind of sloppy costuming

141
00:08:35,433 --> 00:08:38,103
which results from a lack
of rules and competion.

142
00:08:40,849 --> 00:08:42,437
Hey, guys, check out the sexy nurse.

143
00:08:43,383 --> 00:08:47,357
I believe it's time for me
to turn my head and cough.

144
00:08:49,455 --> 00:08:50,534
What is your move?

145
00:08:50,688 --> 00:08:52,671
I'm going to use
the mirror technique.

146
00:08:53,001 --> 00:08:54,969
She brushes her hair back,
I brush my hair back...

147
00:08:55,123 --> 00:08:57,149
She shrugs, I shrug.
Subconsciously she's thinking,

148
00:08:57,303 --> 00:08:58,866
"We're in sync.
We belong together."

149
00:08:59,674 --> 00:09:01,488
Where do you get this stuff?

150
00:09:02,073 --> 00:09:04,819
You know, psychology journals,
Internet research,

151
00:09:05,028 --> 00:09:08,416
and there's this great show on VH-1
about how to pick up girls.

152
00:09:11,480 --> 00:09:13,391
If only I had his confidence.

153
00:09:13,641 --> 00:09:15,754
I have such difficulty
speaking to women,

154
00:09:16,046 --> 00:09:17,377
or around women...

155
00:09:17,649 --> 00:09:19,832
or at times even effeminate men.

156
00:09:21,908 --> 00:09:23,451
If that's a working stethoscope,

157
00:09:23,760 --> 00:09:26,211
maybe you'd like to hear
my heart skip a beat?

158
00:09:27,266 --> 00:09:28,306
No, thanks.

159
00:09:28,625 --> 00:09:31,655
No, seriously, you can.
I have transient idiopathic arrhythmia.

160
00:09:38,836 --> 00:09:41,390
I want to get to know Penny's friends,
I just...

161
00:09:41,679 --> 00:09:44,110
I don't know
how to talk to these people.

162
00:09:44,337 --> 00:09:46,622
Well, I actually might
be able to help.

163
00:09:47,503 --> 00:09:48,507
How so?

164
00:09:48,706 --> 00:09:50,828
Like Jane Goodall
observing the apes,

165
00:09:51,204 --> 00:09:54,704
I initially saw their interactions
as confusing and unstructured.

166
00:09:55,065 --> 00:09:56,252
But patterns emerge.

167
00:09:56,398 --> 00:09:59,331
They have their own
language, if you will.

168
00:10:00,561 --> 00:10:01,601
Go on.

169
00:10:02,637 --> 00:10:05,622
It seems that the newcomer
approaches the existing group

170
00:10:05,776 --> 00:10:08,123
with the greeting,
"How wasted am I?"

171
00:10:08,451 --> 00:10:11,345
Which is met with an
approving chorus of "Dude."

172
00:10:12,919 --> 00:10:13,978
Then what happens?

173
00:10:14,337 --> 00:10:15,727
That's as far as I've gotten.

174
00:10:17,336 --> 00:10:18,685
This is ridiculous.

175
00:10:18,843 --> 00:10:20,682
- I'm jumping in.
- Good luck.

176
00:10:21,088 --> 00:10:22,360
No, you're coming with me.

177
00:10:22,620 --> 00:10:24,183
Oh, I hardly think so.

178
00:10:24,741 --> 00:10:25,686
Come on.

179
00:10:26,326 --> 00:10:28,043
Aren't you afraid
I'll embarrass you?

180
00:10:28,197 --> 00:10:30,319
Yes; but I need a wing man.

181
00:10:30,754 --> 00:10:33,702
All right, but if we're
going to use flight metaphors,

182
00:10:33,875 --> 00:10:37,515
I'm much more suited to being the guy
from the FAA analyzing wreckage.

183
00:10:43,682 --> 00:10:45,167
So what are you
supposed to be?

184
00:10:45,631 --> 00:10:48,292
Me?
I'll give you a hint.

185
00:10:52,238 --> 00:10:53,434
A choo-choo train?

186
00:10:53,945 --> 00:10:54,945
Close!

187
00:10:57,953 --> 00:10:59,731
A brain damaged choo-choo train?

188
00:11:02,454 --> 00:11:04,806
How wasted am I?

189
00:11:16,519 --> 00:11:17,656
I still don't get it.

190
00:11:18,004 --> 00:11:19,509
I'm the Doppler effect.

191
00:11:19,979 --> 00:11:21,909
If that's some sort
of learning disability,

192
00:11:22,064 --> 00:11:23,472
I think it's very insensitive.

193
00:11:26,031 --> 00:11:28,301
Why don't you just tell people
you're a zebra?

194
00:11:29,338 --> 00:11:32,227
Why don't you just tell people
you're one of the seven dwarves?

195
00:11:32,516 --> 00:11:33,538
Because I'm Frodo.

196
00:11:33,798 --> 00:11:35,766
Yes, well,
I'm the Doppler effect.

197
00:11:37,796 --> 00:11:39,473
- Oh, no.
- What?

198
00:11:40,207 --> 00:11:41,751
That's Penny's ex-boyfriend.

199
00:11:42,154 --> 00:11:43,768
What do you suppose
he's doing here?

200
00:11:44,056 --> 00:11:46,749
Besides disrupting the
local gravity field.

201
00:11:48,266 --> 00:11:50,790
If he were any bigger,
he'd have moons orbiting him.

202
00:11:51,215 --> 00:11:52,274
Oh, snap.

203
00:11:54,122 --> 00:11:56,055
So, I guess we'll be leaving now.

204
00:11:56,376 --> 00:11:57,744
Why should we leave?

205
00:11:58,226 --> 00:12:01,713
For all we know he crashed the party
and Penny doesn't even want him here.

206
00:12:03,634 --> 00:12:05,391
You have a backup hypothesis?

207
00:12:06,186 --> 00:12:07,847
Maybe they want to be friends.

208
00:12:08,600 --> 00:12:10,202
Or maybe she wants
to be friends

209
00:12:10,452 --> 00:12:12,285
and he wants
something more.

210
00:12:12,670 --> 00:12:14,580
Then he and I are on equal ground.

211
00:12:14,839 --> 00:12:17,232
Yes, but you're much
closer to it than he is.

212
00:12:19,264 --> 00:12:21,642
Look, if this was 1,500 years ago,

213
00:12:21,818 --> 00:12:23,502
by virtue of his size and strength,

214
00:12:23,687 --> 00:12:26,040
Kurt would be entitled to his
choice of female partners.

215
00:12:26,233 --> 00:12:29,508
And male partners, animal partners,
large primordial eggplants--

216
00:12:29,662 --> 00:12:32,016
pretty much whatever
tickled his fancy.

217
00:12:32,687 --> 00:12:35,463
Yes, but our society has
undergone a paradigm shift.

218
00:12:35,594 --> 00:12:37,044
In the Information Age,

219
00:12:37,511 --> 00:12:39,556
you and I are the alpha males.

220
00:12:39,994 --> 00:12:42,277
- We shouldn't have to back down.
- True.

221
00:12:42,648 --> 00:12:45,213
Why don't you text him that
and see if he backs down?

222
00:12:46,510 --> 00:12:47,510
5No.

223
00:12:49,114 --> 00:12:51,693
I'm going to assert
my dominance face-to-face.

224
00:12:52,413 --> 00:12:54,653
Face-to-face? Are you going
to wait for him to sit down,

225
00:12:54,826 --> 00:12:56,890
or are you going to stand
on the coffee table?

226
00:12:58,871 --> 00:13:00,375
Hello, Penny.
Hello, Kurt.

227
00:13:00,728 --> 00:13:02,408
Hey, guys, are you
having a good time?

228
00:13:02,812 --> 00:13:04,336
Given the reaction to my costume,

229
00:13:04,491 --> 00:13:07,384
this party is a scathing indictment
of the American education system.

230
00:13:08,838 --> 00:13:10,266
What, you're a zebra, right?

231
00:13:11,545 --> 00:13:13,187
Yet another child left behind.

232
00:13:14,579 --> 00:13:16,142
What are you supposed to be, an elf?

233
00:13:16,848 --> 00:13:18,043
No, I'm a hobbit.

234
00:13:18,342 --> 00:13:19,480
What's the difference?

235
00:13:19,903 --> 00:13:22,867
A hobbit is a mortal halfling
inhabitant of Middle Earth,

236
00:13:23,060 --> 00:13:25,950
whereas an elf is an immortal,
tall warrior.

237
00:13:26,466 --> 00:13:28,910
So why the hell would you
want to be a hobbit?

238
00:13:29,616 --> 00:13:31,534
Because he's neither
tall nor immortal

239
00:13:31,688 --> 00:13:33,501
and none of us could be The Flash.

240
00:13:34,778 --> 00:13:37,560
Well, whatever.
Why don't you go hop off on a quest?

241
00:13:37,734 --> 00:13:39,026
I'm talking to Penny here.

242
00:13:39,173 --> 00:13:41,088
I think we're all
talking to Penny here.

243
00:13:41,300 --> 00:13:42,400
I'm not. No offense.

244
00:13:43,777 --> 00:13:45,282
Okay, maybe you didn't hear me.

245
00:13:45,488 --> 00:13:47,576
- Go away.
- All right, Kurt, be nice.

246
00:13:48,056 --> 00:13:49,482
Oh, I am being nice.

247
00:13:50,561 --> 00:13:51,815
Right, little buddy?

248
00:13:54,895 --> 00:13:58,464
I understand your impulse
to try to physically intimidate me.

249
00:13:58,759 --> 00:14:01,252
I mean, you can't compete
with me on an intellectual level

250
00:14:01,407 --> 00:14:03,760
so you're driven
to animalistic puffery.

251
00:14:04,281 --> 00:14:06,152
You calling me a puffy animal?

252
00:14:06,785 --> 00:14:08,671
Of course not.
No, he's not.

253
00:14:08,844 --> 00:14:10,137
You're not, right, Leonard?

254
00:14:11,138 --> 00:14:13,203
No, I said "animalistic."

255
00:14:13,691 --> 00:14:15,446
Of course
we're all animals, but,

256
00:14:15,739 --> 00:14:18,698
some of us have climbed a little higher
on the evolutionary tree.

257
00:14:18,910 --> 00:14:20,820
If he understands that,
you're in trouble.

258
00:14:21,213 --> 00:14:22,853
So, what, I'm unevolved?

259
00:14:23,204 --> 00:14:24,342
You're in trouble.

260
00:14:25,429 --> 00:14:28,670
You use a lot of big words
for such a little dwarf.

261
00:14:28,873 --> 00:14:30,268
Okay, Kurt, please.

262
00:14:30,847 --> 00:14:32,680
It's okay.
I can handle this.

263
00:14:33,529 --> 00:14:34,724
I am not a dwarf,

264
00:14:35,181 --> 00:14:36,356
I'm a hobbit.

265
00:14:36,889 --> 00:14:37,873
A hobbit.

266
00:14:38,184 --> 00:14:40,596
Are misfiring neurons
in your hippocampus preventing

267
00:14:40,770 --> 00:14:43,046
the conversion from short-term
to long-term memory?

268
00:14:45,990 --> 00:14:47,833
Okay, now you're starting
to make me mad.

269
00:14:49,249 --> 00:14:52,126
A homo habilis discovering his
opposable thumbs says what?

270
00:14:53,960 --> 00:14:54,960
What?

271
00:14:59,834 --> 00:15:01,184
I think I've made my point.

272
00:15:01,458 --> 00:15:04,410
Yeah? How about I make a point
out of your pointy little head?

273
00:15:04,822 --> 00:15:07,253
Let me remind you,
while my moral support is absolute,

274
00:15:07,422 --> 00:15:10,298
in a physical confrontation,
I will be less than useless.

275
00:15:10,580 --> 00:15:12,493
There's not going
to be a confrontation.

276
00:15:12,821 --> 00:15:16,313
In fact, I doubt if he can
even spell "confrontation."

277
00:15:18,945 --> 00:15:20,429
C- O-N...

278
00:15:21,856 --> 00:15:23,052
frontation!

279
00:15:24,108 --> 00:15:25,902
Kurt, put him down this instant!

280
00:15:26,121 --> 00:15:27,119
He started it!

281
00:15:27,319 --> 00:15:29,154
I don't care. I'm finishing it.
Put him down!

282
00:15:30,328 --> 00:15:31,328
Fine.

283
00:15:33,434 --> 00:15:35,349
You're one lucky little leprechaun.

284
00:15:37,384 --> 00:15:38,501
He's a hobbit!

285
00:15:39,239 --> 00:15:40,453
I got your back.

286
00:15:41,993 --> 00:15:42,993
Are you okay?

287
00:15:43,296 --> 00:15:44,415
Yeah, I'm fine.

288
00:15:47,912 --> 00:15:49,725
It's a good party,
thanks for having us.

289
00:15:49,877 --> 00:15:51,483
It's just getting a little late, so...

290
00:15:53,482 --> 00:15:55,103
All right, thank you for coming.

291
00:15:58,512 --> 00:15:59,591
Happy Halloween.

292
00:16:03,470 --> 00:16:04,686
If it's any consolation,

293
00:16:04,833 --> 00:16:07,831
I thought that homo habilis line
really put him in his place.

294
00:16:17,417 --> 00:16:18,417
What's that?

295
00:16:18,805 --> 00:16:19,818
Tea.

296
00:16:20,753 --> 00:16:21,968
When people are upset,

297
00:16:22,123 --> 00:16:24,997
the cultural convention is
to bring them hot beverages.

298
00:16:31,793 --> 00:16:32,793
There, there.

299
00:16:36,119 --> 00:16:37,392
You want to talk about it?

300
00:16:37,701 --> 00:16:38,820
- No.
- Good.

301
00:16:40,456 --> 00:16:42,507
"There, there"
was really all I had.

302
00:16:44,168 --> 00:16:46,546
- Good night, Sheldon.
- Good night, Leonard.

303
00:16:56,232 --> 00:16:58,025
I just wanted to make sure
you were okay.

304
00:16:58,219 --> 00:16:59,319
I'm fine.

305
00:17:01,392 --> 00:17:03,731
I am so sorry about
what happened.

306
00:17:03,991 --> 00:17:05,436
It's not your fault.

307
00:17:05,695 --> 00:17:06,695
Yes, it is.

308
00:17:06,849 --> 00:17:09,960
That's why I broke up with him.
He always does stuff like that.

309
00:17:11,577 --> 00:17:13,005
So why was he at your party?

310
00:17:16,983 --> 00:17:18,570
I ran into him last week

311
00:17:18,920 --> 00:17:22,212
and he was just all apologetic
about how he's changed.

312
00:17:22,532 --> 00:17:24,692
And he was just
going on and on and...

313
00:17:24,920 --> 00:17:28,174
I believed him and I'm an idiot
because I always believe guys like that.

314
00:17:29,634 --> 00:17:31,988
I can't go back to my
party because he's there.

315
00:17:32,222 --> 00:17:33,955
You don't want to hear this
and I'm upset

316
00:17:34,110 --> 00:17:36,116
and I'm really drunk
and I just want to...

317
00:17:45,527 --> 00:17:46,664
There, there.

318
00:17:50,818 --> 00:17:52,342
God, what is wrong with me?

319
00:17:52,525 --> 00:17:54,260
Nothing, you're perfect.

320
00:17:54,674 --> 00:17:56,783
- I'm not perfect.
- Yes, you are.

321
00:17:59,832 --> 00:18:01,629
You really think so, don't you?

322
00:18:19,522 --> 00:18:21,556
How much have you
had to drink tonight?

323
00:18:24,289 --> 00:18:26,257
Just... a lot.

324
00:18:28,232 --> 00:18:30,417
Are you sure that your being drunk

325
00:18:30,632 --> 00:18:32,253
and your being angry with Kurt

326
00:18:32,508 --> 00:18:34,881
doesn't have something to do
with what's going on here?

327
00:18:36,543 --> 00:18:37,757
It might.

328
00:18:41,056 --> 00:18:42,521
Boy, you're really smart.

329
00:18:42,972 --> 00:18:44,631
Yeah, I'm a frickin' genius.

330
00:18:50,112 --> 00:18:51,751
Leonard, you are so great.

331
00:18:52,220 --> 00:18:53,978
Why can't all guys be like you?

332
00:18:55,223 --> 00:18:58,717
Because if all guys were like me,
the human race couldn't survive.

333
00:19:03,503 --> 00:19:04,699
I should probably go.

334
00:19:05,119 --> 00:19:06,119
Probably.

335
00:19:15,597 --> 00:19:16,600
Thank you.

336
00:19:27,655 --> 00:19:29,725
That's right,
you saw what you saw.

337
00:19:32,012 --> 00:19:33,941
That's how we roll in the Shire.

338
00:19:42,657 --> 00:19:43,657
Coming.

339
00:19:49,010 --> 00:19:50,788
Hey, have you seen Koothrappali?

340
00:19:51,712 --> 00:19:53,042
He's not here.

341
00:19:53,420 --> 00:19:55,565
Maybe the Avengers summoned him.

342
00:19:57,223 --> 00:19:59,851
He's not the Marvel Comics Thor,
he's the original Norse god.

343
00:20:00,282 --> 00:20:02,157
Thank you for the clarification.

344
00:20:02,778 --> 00:20:05,549
- I'm supposed to give him a ride home.
- I'm sure he'll be fine.

345
00:20:05,737 --> 00:20:07,163
He has his hammer.

346
00:20:15,201 --> 00:20:18,257
I have to say,
you are an amazing man.

347
00:20:19,457 --> 00:20:21,948
You're gentle,
and passionate.

348
00:20:22,970 --> 00:20:24,165
And, my God,

349
00:20:24,618 --> 00:20:24,165
you are such a good listener.

