1
00:00:10,835 --> 00:00:14,625
Отлично, еще чуть-чуть. И...

2
00:00:14,635 --> 00:00:18,305
Вот мы и пришли, господа,
Врата Элзбоб.

3
00:00:18,315 --> 00:00:19,845
Боже мой.

4
00:00:19,855 --> 00:00:23,235
Без паники. Мы шли к этому
все последние 97 часов.

5
00:00:23,245 --> 00:00:25,585
Не горячитесь. Там
толпы вооруженных гоблинов

6
00:00:25,595 --> 00:00:28,895
по ту сторону ворот,
охраняющих Меч Азерота.

7
00:00:28,905 --> 00:00:32,305
Воины, обнажите своё оружие.
Маги, поднимите посохи.

8
00:00:32,315 --> 00:00:33,455
Начинаем.

9
00:00:33,465 --> 00:00:35,375
Раж, взорви ворота.

10
00:00:36,145 --> 00:00:37,375
Взрываю ворота.

11
00:00:37,975 --> 00:00:40,665
Контрол, шифт...

12
00:00:40,975 --> 00:00:42,105
b!

13
00:00:43,935 --> 00:00:46,145
О боже, как много гоблинов!

14
00:00:47,145 --> 00:00:49,395
Не стойте на месте,
бей и беги, бей и беги!

15
00:00:49,405 --> 00:00:50,545
Оставайтесь в строе!

16
00:00:50,555 --> 00:00:51,955
Леонард, у тебя один на хвосте.

17
00:00:51,965 --> 00:00:54,345
Все в порядке, у меня
цепкий хвост, я его им прибью.

18
00:00:54,355 --> 00:00:55,695
Я снял его, Леонард.

19
00:00:55,705 --> 00:00:59,465
Сегодня я орошу луга
гоблинской кровью.

20
00:00:59,475 --> 00:01:01,485
Раж, нет, это ловушка! Они окружают нас!

21
00:01:01,945 --> 00:01:03,415
Он завалил меня!

22
00:01:03,425 --> 00:01:06,025
Шелдон, он завалил Ража.
Используй заклинание сна!

23
00:01:06,375 --> 00:01:07,995
Шелдон... шелдон?

24
00:01:08,005 --> 00:01:11,465
У меня Меч Азерота!

25
00:01:12,125 --> 00:01:13,735
Забудь про меч, Шелдон,
помоги Ражу.

26
00:01:13,745 --> 00:01:15,365
Нет больше никакого Шелдона.

27
00:01:15,375 --> 00:01:17,585
Я — Мастер Клинка!

28
00:01:17,595 --> 00:01:18,795
Леонард, аккуратнее!

29
00:01:18,805 --> 00:01:20,205
Блин, чел, мы тут умираем!

30
00:01:20,215 --> 00:01:21,845
Пока, слабачки!

31
00:01:22,905 --> 00:01:24,495
Телепортнулся, сволочь!

32
00:01:26,145 --> 00:01:28,765
Он продает
Меч Азерота на ebay.

33
00:01:30,095 --> 00:01:32,525
Ты предал нас из-за денег?
Кто ты такой?

34
00:01:32,535 --> 00:01:36,245
Я ночной эльф, разбойник. Почему никто
не читает описания персонажей?

35
00:01:36,675 --> 00:01:39,205
Стоп, стоп, стоп! Кто-то
только что купил его.

36
00:01:39,215 --> 00:01:42,925
Я — Мастер Клинка!

37
00:02:09,175 --> 00:02:12,955
Ох, я весь вспотел. Кто хочет
пойти в Second Life поплавать?

38
00:02:12,965 --> 00:02:14,695
Я только что построил виртуальный бассейн.

39
00:02:15,525 --> 00:02:18,785
Нет. Не хочу сейчас видеть
тебя и твой аватар.

40
00:02:22,895 --> 00:02:24,495
Кажется, ваша соседка пришла.

41
00:02:26,395 --> 00:02:28,015
Извините меня...

42
00:02:28,025 --> 00:02:30,075
Не забудь письмо, которое
ты случайно специально взял,

43
00:02:30,085 --> 00:02:31,515
чтобы найти повод с ней заговорить.

44
00:02:31,525 --> 00:02:33,535
Ах, точно, точно, точно.

45
00:02:34,195 --> 00:02:37,315
Крадем бумажную почту,
старинный способ, мне нравится.

46
00:02:38,425 --> 00:02:42,265
Пенни, почтально снова ошибся...
он... ой, простите.

47
00:02:42,275 --> 00:02:44,665
Эм... Привет, Леонард. Это Даг.

48
00:02:44,675 --> 00:02:46,095
Даг, это мой сосед Леонард.

49
00:02:46,105 --> 00:02:47,685
Как житуха, братуха?

50
00:02:49,475 --> 00:02:51,575
Не очень...

51
00:02:52,865 --> 00:02:54,545
братуха.

52
00:02:55,005 --> 00:02:56,365
Все в порядке?

53
00:02:56,375 --> 00:02:58,785
Ага, я просто... Я получил
твою почту опять. Вот.

54
00:02:59,245 --> 00:03:01,435
Спасибо. Нужно будет поговорить с этим почтальоном.

55
00:03:01,445 --> 00:03:04,015
Не стоит, это не очень хорошая идея.

56
00:03:04,755 --> 00:03:09,455
Знаешь, работники гос служб
имеют предписание, знаешь, огрызаться, так что...

57
00:03:10,525 --> 00:03:12,595
Ладно, еще раз спасибо.

58
00:03:12,605 --> 00:03:14,355
Не за что. Пока.

59
00:03:15,115 --> 00:03:17,755
А, и пока... братуха.

60
00:03:25,375 --> 00:03:27,855
О чем задумался?

61
00:03:28,905 --> 00:03:30,175
Что случилось?

62
00:03:30,185 --> 00:03:32,795
Я в порядке. Пенни в порядке.

63
00:03:32,805 --> 00:03:35,455
Парень, целующий ее, тоже очень даже в порядке.

64
00:03:35,915 --> 00:03:39,025
Целует, как целует?
В щечку? В губы? По-французски?

65
00:03:40,945 --> 00:03:43,145
Да что с тобой такое?

66
00:03:43,855 --> 00:03:46,335
Я — романтик.

67
00:03:47,515 --> 00:03:51,695
Пожалуйста, только не говори, что твоя безнадежная
одержимость переходит в бессмысленную ревность.

68
00:03:51,705 --> 00:03:54,815
Я не ревную. Я просто
немного волнуюсь за нее.

69
00:03:54,825 --> 00:03:56,855
Мне не понравился этот парень.

70
00:03:56,865 --> 00:03:58,765
Потому что он выглядит лучше тебя?

71
00:03:59,965 --> 00:04:01,745
Ага.

72
00:04:02,315 --> 00:04:04,245
Он просто мечта.

73
00:04:04,795 --> 00:04:07,705
Ну, по крайней мере ты можешь достать
черный ящик из обломков того,

74
00:04:07,715 --> 00:04:10,335
что было когда-то твоей фантазией
о ней и проанализировать данные,

75
00:04:10,345 --> 00:04:14,425
чтобы не разбиться об
гору гика в следующий раз.

76
00:04:15,855 --> 00:04:20,025
Я не согласен. Любовь это
не спринт,
это марафон

77
00:04:20,035 --> 00:04:25,035
Безжалостная погоня, которая заканчивается
только когда она падет в твои объятья...

78
00:04:25,045 --> 00:04:27,595
или брызнет в тебя из газового баллончика.

79
00:04:28,865 --> 00:04:30,475
С Пенни покончено.

80
00:04:30,485 --> 00:04:33,535
Буду более разумным и побегу за
кем-нибудь со своей собственной скоростью.

81
00:04:33,545 --> 00:04:34,735
За кем например?

82
00:04:34,745 --> 00:04:37,085
Ну незнаю... Оливия Гайгер.

83
00:04:37,095 --> 00:04:40,315
Та продавщица из кафе, с косым глазом?

84
00:04:42,515 --> 00:04:43,455
Ага.

85
00:04:43,465 --> 00:04:45,415
По-моему, у тебя нет шансов.

86
00:04:46,565 --> 00:04:49,875
Я обратил внимание, что Лесли Винкл
недавно начала брить ноги.

87
00:04:49,885 --> 00:04:51,555
При этом, если учесть, что скоро зима,

88
00:04:51,565 --> 00:04:54,705
можно сделать вывод что она сообщает
о своей сексуальной доступности.

89
00:04:55,225 --> 00:04:57,365
Незнаю, ребята, вы
работаете в одной лаборатории.

90
00:04:57,375 --> 00:04:58,325
И?

91
00:04:58,335 --> 00:05:01,445
Здесь не все так просто.
Поверь мне, я знаю.

92
00:05:01,455 --> 00:05:06,935
Что касается сексуальных домогательств,
то тут я эксперт-самоучка.

93
00:05:09,355 --> 00:05:12,645
Слушай, Говард, если бы я пригласил Лесли Винкл
куда-нибудь, это было бы не более, чем ужин.

94
00:05:12,655 --> 00:05:16,815
Я не собираюсь подходить к ней в лаборатории
и просить станцевать стриптиз для меня.

95
00:05:16,825 --> 00:05:18,905
А, ну тогда все должно быть хорошо.

96
00:05:22,815 --> 00:05:23,885
Привет, Лесли.

97
00:05:23,895 --> 00:05:25,025
Привет, Леонард.

98
00:05:25,035 --> 00:05:26,445
Лесли, я хотел бы
провести эксперимент...

99
00:05:26,455 --> 00:05:28,125
- Одень очки.
- Ладно.

100
00:05:30,575 --> 00:05:32,365
Лесли, я хотел бы
провести эксперимент...

101
00:05:32,375 --> 00:05:33,565
Погоди.

102
00:05:33,575 --> 00:05:37,365
Я пытаюсь узнать сколько времени потребуется
500-киловаттному ионно-водороному лазеру,

103
00:05:37,375 --> 00:05:39,525
чтобы разогреть лапшу.

104
00:05:40,485 --> 00:05:43,615
Я пробовал это. Около двух секунд.
2,6 для куриного супа.

105
00:05:50,125 --> 00:05:55,455
Ну так вот, я думал о некотором биологически-социальном
исследовании применительно к нейро-химическим процессам.

106
00:05:57,015 --> 00:05:58,935
Ты что, приглашаешь меня на свидание?

107
00:06:00,435 --> 00:06:05,205
Я собирался охарактеризовать это как модификацию
парадигмы нашего сотрудничества-дробь-дружбы

108
00:06:05,215 --> 00:06:07,095
с добавлением свидания как компонента,

109
00:06:07,105 --> 00:06:10,705
но без нужды цепляться к терминологии.

110
00:06:11,275 --> 00:06:13,055
И какого рода эксперимент ты хочешь провести?

111
00:06:13,065 --> 00:06:16,085
Здесь есть общепринятая схема.

112
00:06:16,095 --> 00:06:17,715
Я бы встретился с тобой, сводил в ресторан,

113
00:06:17,725 --> 00:06:19,445
потом мы посмотрели бы вместе фильм,

114
00:06:19,455 --> 00:06:23,985
возможно романтическую комедию с
Хью Грантом или Сандрой Баллок.

115
00:06:24,795 --> 00:06:25,855
Интересно...

116
00:06:25,865 --> 00:06:28,685
Согласен ли ты, что основной способ определить

117
00:06:28,695 --> 00:06:31,075
насколько удачно прошло свидание будет основан на

118
00:06:31,085 --> 00:06:33,615
биохимической реакции во время прощального поцелуя?

119
00:06:33,625 --> 00:06:35,745
Сердечный ритм, феромоны, и т.д. Да.

120
00:06:36,595 --> 00:06:40,215
Тогда почему бы не взять как условие, что
свидание прошло хорошо, и перейти к сути?

121
00:06:40,635 --> 00:06:41,685
То есть поцеловать тебя сейчас?

122
00:06:41,695 --> 00:06:42,655
Да.

123
00:06:42,665 --> 00:06:44,365
Можешь определить параметры поцелуя?

124
00:06:44,375 --> 00:06:46,215
Обычный, но романтический. Мятные конфетки?

125
00:06:46,225 --> 00:06:47,725
О, спасибо.

126
00:06:54,905 --> 00:06:55,985
Должен ли я сделать отсчет от трех?

127
00:06:55,995 --> 00:06:58,605
Нет, думаю он должен быть неожиданным.

128
00:07:05,775 --> 00:07:06,725
Что скажешь?

129
00:07:06,735 --> 00:07:10,075
Ты предложила эксперимент, я думаю ты
должна первой предоставить полученные данные.

130
00:07:10,725 --> 00:07:12,405
Довольно честно.

131
00:07:12,415 --> 00:07:14,785
С одной стороны, это был хороший поцелуй.

132
00:07:14,795 --> 00:07:17,295
Подходящая техника, никаких инородных слюней.

133
00:07:18,875 --> 00:07:21,045
С другой стороны, нет никакого возбуждения.

134
00:07:22,075 --> 00:07:23,655
- Нет?
- Нет.

135
00:07:27,315 --> 00:07:29,575
Ладно, спасибо за уделенное время.

136
00:07:29,585 --> 00:07:31,205
Тебе спасибо.

137
00:07:35,275 --> 00:07:37,225
Вообще никакого?

138
00:07:41,105 --> 00:07:46,725
Шелдон, если бы ты был роботом, и
я знал бы об этом, а ты нет...

139
00:07:48,725 --> 00:07:50,805
Хотел бы ты чтоб я тебе сказал?

140
00:07:52,105 --> 00:07:53,875
Возможны варианты.

141
00:07:53,885 --> 00:07:57,405
После того как я узнаю, что я робот...

142
00:07:57,415 --> 00:08:00,445
смогу ли я это пережить?

143
00:08:00,875 --> 00:08:04,425
Наверно...  правда история научной
фантастики не на твоей стороне.

144
00:08:05,445 --> 00:08:07,685
Хорошо, а вот такой вопрос...

145
00:08:07,695 --> 00:08:09,915
Когда я узнаю, что я робот...

146
00:08:09,925 --> 00:08:12,765
буду ли я подчиняться трем законам
робототехники Азимова?

147
00:08:14,255 --> 00:08:17,635
Возможно, ты подчиняешься им прямо сейчас

148
00:08:20,935 --> 00:08:22,075
Это правда.

149
00:08:22,085 --> 00:08:23,375
Ты когда-нибудь причинял вред человеку,

150
00:08:23,385 --> 00:08:25,635
или своим бездействием
позволял человеку получить травму?

151
00:08:25,645 --> 00:08:26,405
Конечно нет.

152
00:08:26,415 --> 00:08:29,135
Ты когда-нибудь причинял себе вред если
был вынужден причинить себе вред в случае

153
00:08:29,145 --> 00:08:31,805
если человек находился в опасности?

154
00:08:31,815 --> 00:08:32,805
Ну.. нет.

155
00:08:32,815 --> 00:08:34,815
Тут пахнет роботом.

156
00:08:38,185 --> 00:08:39,325
Эй, что делаете?

157
00:08:39,335 --> 00:08:41,785
Интернет упал полчаса назад.

158
00:08:43,885 --> 00:08:46,355
И еще Шелдон возможно робот.

159
00:08:47,975 --> 00:08:50,115
Ну, как прошло с Лесли?

160
00:08:50,795 --> 00:08:54,675
Мы попробовали целоваться,
но земля не двинулась. *
<i>[поговорка, "ничего не произошло"]</i>

161
00:08:55,255 --> 00:08:58,955
Точнее, не более чем на 383 мили,
как она и должна была.

162
00:09:01,445 --> 00:09:03,455
О, я знаю, что значит этот его взгляд.

163
00:09:03,465 --> 00:09:05,675
Будет недели две хандры и апатии,

164
00:09:05,685 --> 00:09:07,295
и нудных эмо-песен,

165
00:09:07,305 --> 00:09:10,555
и просьб сходить в зоомагазин посмотреть на кошечек.

166
00:09:12,685 --> 00:09:14,275
Не знаю, справлюсь ли я.

167
00:09:15,215 --> 00:09:17,115
Ты можешь отключить себе питание.

168
00:09:19,445 --> 00:09:22,535
Ну, как обычно, Воловиц знает что делать.

169
00:09:23,825 --> 00:09:27,265
Я как раз знаю место, где
полно подходящих женщин

170
00:09:27,275 --> 00:09:29,435
и Леонард может сделать свой выбор.

171
00:09:38,975 --> 00:09:41,045
Помните латинские движения.

172
00:09:41,055 --> 00:09:44,325
Плечи не двигаются, и.. мы покачиваемся.

173
00:09:49,205 --> 00:09:51,395
Раз, два, три...

174
00:09:52,215 --> 00:09:54,535
пять, шесть, семь.

175
00:09:54,965 --> 00:09:57,395
Думаю миссис Тишмен
положила на тебя глаз.

176
00:09:58,415 --> 00:10:00,465
Я уже был тут. Ты сюда приглашен.

177
00:10:27,142 --> 00:10:30,182
О-о, да-а. Просто прекрасная песня.

178
00:10:31,102 --> 00:10:34,162
Да, если подбираешь музыку на диск
для двойного самоубийства.

179
00:10:34,962 --> 00:10:38,202
О, я надеюсь эту когтеточку
ты купил для себя.

180
00:10:38,702 --> 00:10:41,762
Знаю о чем ты думаешь.
Я не забыл про твою астму.

181
00:10:41,772 --> 00:10:43,952
В Сан-Диего есть один кошачий гететик,

182
00:10:43,962 --> 00:10:47,032
он вывел породу маленьких миленьких
гипоаллергенных кошечек.

183
00:10:47,042 --> 00:10:48,022
Леонард, послушай...

184
00:10:48,032 --> 00:10:49,162
Я думал на счет имени.

185
00:10:49,172 --> 00:10:53,222
Никак не могу выбрать: Эйнштейн,
Ньютон или Сержант Пушистые Ботинки.

186
00:10:54,252 --> 00:10:56,182
Леонард, ты правда считаешь,
что можешь удовлетворить свои потребности

187
00:10:56,192 --> 00:10:58,972
в отношениях с помощью
генетически модифицированной кошки?

188
00:10:59,602 --> 00:11:00,792
Может быть.

189
00:11:00,802 --> 00:11:03,392
Если это симпатичная, милая, игривая кошка.

190
00:11:03,402 --> 00:11:05,762
Ну хватит, Леонард...

191
00:11:07,852 --> 00:11:10,012
Очевидно, это все из-за Пенни.

192
00:11:12,212 --> 00:11:13,532
Это не важно.

193
00:11:13,542 --> 00:11:16,432
Женщина не заинтересована мной.
Женщина отвергла меня.

194
00:11:16,442 --> 00:11:23,362
Ладно, слушай, я думаю у тебя столько же
шансов иметь сексуальную связь с Пенни,

195
00:11:23,372 --> 00:11:27,522
как у телескопа в попытках установить, что
центром каждой черной дыры является

196
00:11:27,532 --> 00:11:30,482
маленький человечек с фонариком,
пытающийся найти прерыватель.

197
00:11:32,972 --> 00:11:34,922
Тем не менее,

198
00:11:34,932 --> 00:11:39,782
я обязан обратить твое внимание на то,
что она не отвергала тебя.

199
00:11:40,572 --> 00:11:42,532
Ты не приглашал ее на свидание.

200
00:11:43,862 --> 00:11:45,482
Ты прав.

201
00:11:45,492 --> 00:11:47,502
Я не приглашал ее.
Я должен пригласить ее.

202
00:11:47,512 --> 00:11:49,732
Нет, нет, это не то, что я хотел сказать.

203
00:11:50,432 --> 00:11:52,782
Я хотел сказать, не покупай кошку.

204
00:11:53,372 --> 00:11:55,392
Нет, но ты прав.

205
00:11:55,402 --> 00:11:57,902
Я должен пойти и пригласить ее.

206
00:11:58,312 --> 00:12:00,972
Ох, похоже, у нас точно будет кошка.

207
00:12:03,262 --> 00:12:05,172
misc

208
00:12:08,382 --> 00:12:09,282
О, привет, Леонард.

209
00:12:09,292 --> 00:12:11,862
Добрый день, Пенни. Итак.. привет.. да..

210
00:12:11,872 --> 00:12:13,772
Э...

211
00:12:14,222 --> 00:12:16,192
Хотел спросить какие у тебя планы на обед?

212
00:12:16,202 --> 00:12:17,902
Ты имеешь ввиду обед сегодня вечером?

213
00:12:19,092 --> 00:12:21,452
Здесь есть некоторая
неопределнность в слове "обед".

214
00:12:21,462 --> 00:12:24,962
Технически, оно обозначает наибольшую
трапезу, употребляемую в любое время дня.

215
00:12:24,972 --> 00:12:27,552
Поэтому для ясности, под обедом я понимаю ужин.

216
00:12:27,562 --> 00:12:29,462
- Ужин?
- Или обед.

217
00:12:30,842 --> 00:12:33,502
Может быть в 18:30 если ты можешь,
или в любое другое время.

218
00:12:33,512 --> 00:12:35,102
18:30, отлично.

219
00:12:35,112 --> 00:12:36,712
Правда?

220
00:12:38,342 --> 00:12:39,112
Здорово.

221
00:12:39,122 --> 00:12:40,782
Да, мне нравится проводить с вами время, ребята.

222
00:12:40,792 --> 00:12:41,872
Ребята?

223
00:12:41,882 --> 00:12:44,922
Да, знаешь, Шелдон, Говард, Раж. Все придут?

224
00:12:45,922 --> 00:12:49,812
Они... все могут там оказаться.

225
00:12:51,532 --> 00:12:54,072
Или некоторое количество из них
сможет там оказаться.

226
00:12:54,082 --> 00:12:56,692
Математически выражаясь, здесь
слишком много неизвестных.

227
00:12:56,702 --> 00:12:58,932
Например, Шелдон перекусил сендвичами,

228
00:12:58,942 --> 00:13:01,392
иногда он наедается ими, иногда нет,

229
00:13:01,402 --> 00:13:04,492
это не вина сендвичей, просто меню часто меняется.

230
00:13:06,512 --> 00:13:08,532
Ладно, не важно. Думаю будет весело.

231
00:13:09,012 --> 00:13:11,452
Отлично. Я назвал время?

232
00:13:11,462 --> 00:13:12,312
18:30.

233
00:13:12,322 --> 00:13:14,112
- И тебя все устраивает?
- Все отлично.

234
00:13:14,122 --> 00:13:16,822
- Потому что оно не высечено на камне.
- Нет, 18:30 подходит.

235
00:13:16,832 --> 00:13:19,042
Я возьму долото.

236
00:13:19,912 --> 00:13:21,452
Зачем?

237
00:13:23,462 --> 00:13:27,012
Чтобы... высечь... Увидимся в 18:30.

238
00:13:38,212 --> 00:13:40,002
Как я выгляжу?

239
00:13:44,492 --> 00:13:46,432
Что конкретно ты имеешь в виду?

240
00:13:48,062 --> 00:13:50,222
Мне кажется я немного вспотел?

241
00:13:51,252 --> 00:13:52,312
Нет.

242
00:13:52,322 --> 00:13:55,952
Темные пятна в виде полумесяца
у тебя подмышками это отлично скрывают.

243
00:13:57,912 --> 00:13:58,992
Во сколько у тебя свидание?

244
00:13:59,002 --> 00:13:59,972
18:30.

245
00:13:59,982 --> 00:14:02,802
Превосходно. Значит у тебя
еще есть два часа и 15 минут

246
00:14:02,812 --> 00:14:05,562
чтобы этот плотный молекулярный газ рассеялся.

247
00:14:06,692 --> 00:14:07,812
Что, все так плохо?

248
00:14:07,822 --> 00:14:09,992
Нет, если ты играешь в регби.

249
00:14:11,622 --> 00:14:13,862
Кстати, на всякий случай,
ты не присоединился к нам,

250
00:14:13,872 --> 00:14:16,932
потому что ты объелся сэндвичами в забегаловке.

251
00:14:17,502 --> 00:14:19,322
Зачем мне к вас присоединяться?

252
00:14:19,632 --> 00:14:21,242
Незачем.

253
00:14:23,522 --> 00:14:26,052
Эх, знаешь, может это
не такая уж хорошая идея.

254
00:14:26,062 --> 00:14:30,492
Нет, нет, здесь всегда есть возможность,
что алкоголь и ее нетрезвый рассудок

255
00:14:30,502 --> 00:14:33,162
приведет вас к романтическому вечеру.

256
00:14:34,962 --> 00:14:37,782
Ты прав. Алкоголь, нетрезвый рассудок...
Все может пройти хорошо.

257
00:14:38,542 --> 00:14:40,132
Конечно, тут есть и другая вероятность,

258
00:14:40,142 --> 00:14:42,862
что после этого свидания,
вы двое будете месяцев шесть

259
00:14:42,872 --> 00:14:44,612
неловко себя чувствовать,
случайно встретившись в холле,

260
00:14:44,622 --> 00:14:47,272
пока один из вас не сорвется и уедет в другой город.

261
00:14:48,722 --> 00:14:50,692
Ты мог бы остановиться на хорошем варианте.

262
00:14:52,302 --> 00:14:54,562
Если бы я мог, я бы остановился.

263
00:14:54,572 --> 00:14:56,642
Слушай, я же весьма милый парень.

264
00:14:56,982 --> 00:15:00,232
Нет причин, по которой мы не можем
сходить в ресторан, поужинать вдвоем,

265
00:15:00,242 --> 00:15:04,742
может, прогуляться после этого,
поговорить, найти что-то общее.

266
00:15:04,752 --> 00:15:08,372
Ты любишь поэзию? Я люблю поэзию!

267
00:15:08,382 --> 00:15:11,142
Потом небольшая пауза, мы оба понимаем, что происходит.

268
00:15:11,152 --> 00:15:12,192
Я наклоняюсь, мы целуемся.

269
00:15:12,202 --> 00:15:15,132
Сначала получается неуверенно, но
потом я чувствую, что она тоже целует меня.

270
00:15:15,142 --> 00:15:17,412
Она прикусывает мою нижнюю губу! Она хочет меня!

271
00:15:17,422 --> 00:15:19,472
Все идет просто отлично!
У нас будет секс!

272
00:15:19,482 --> 00:15:21,642
О боже! о боже! о боже!

273
00:15:23,532 --> 00:15:25,902
Это ты сейчас секс показываешь?

274
00:15:26,962 --> 00:15:28,212
Это у меня приступ паники.

275
00:15:28,222 --> 00:15:31,002
А, ясно. Ну.. тогда.. успокойся!

276
00:15:31,882 --> 00:15:35,482
Если бы я мог остановиться, это не был бы приступ...
Именно поэтому говорят "ПРИСТУП паники".

277
00:15:35,492 --> 00:15:37,872
Ладно, ладно. Просто... просто сядь.

278
00:15:38,212 --> 00:15:40,362
Да, сядь. И закрой глаза.

279
00:15:40,372 --> 00:15:41,522
- Зачем?
- Просто закрой.

280
00:15:41,532 --> 00:15:42,232
Ладно.

281
00:15:42,242 --> 00:15:45,522
Попытайся увеличить активность своих альфа-волн.

282
00:15:45,532 --> 00:15:46,402
Что?!

283
00:15:46,412 --> 00:15:48,572
Это медитативная техника.

284
00:15:48,582 --> 00:15:50,412
Расслабление через управление мозговыми волнами.

285
00:15:50,422 --> 00:15:53,212
Я читал статью в журнале про нейрологию.

286
00:15:53,222 --> 00:15:56,022
Она слегка плохо обоснована, но я
думаю основные принципы должны быть верны.

287
00:15:56,032 --> 00:15:58,722
Должно быть у меня где-то сохранился тот номер.

288
00:15:58,732 --> 00:16:00,492
Кого я обманываю? Я не смогу через это пройти.

289
00:16:00,502 --> 00:16:02,072
Ты должен позвонить ей и отменить встречу.

290
00:16:02,082 --> 00:16:03,942
- Я?
- Да.

291
00:16:03,952 --> 00:16:05,202
- Что мне ей сказать?
- Я не знаю.

292
00:16:05,212 --> 00:16:06,942
- Скажи, что я заболел.
- Хорошо.

293
00:16:06,952 --> 00:16:09,612
Не так заболел, чтобы ей хотелось бы
прийти и поухаживать за мной,

294
00:16:09,622 --> 00:16:13,202
но ничего критичного, чтобы она не расхотела
пойти со мной поужинать в другой раз,

295
00:16:13,212 --> 00:16:14,282
если я захочу попробовать еще раз.

296
00:16:14,292 --> 00:16:15,432
Понял.

297
00:16:15,442 --> 00:16:17,212
То есть, я предполагаю, ничего венерического?

298
00:16:21,602 --> 00:16:25,192
Я просто скажу, что тебе назначили
колоноскопию, и ты еще не пришел в норму.

299
00:16:26,452 --> 00:16:27,452
Дай мне трубку.

300
00:16:27,462 --> 00:16:28,742
Я думал ты хотел отказаться.

301
00:16:28,752 --> 00:16:31,432
Я не могу, потому что если я не приду,
она будет ждать тебя.

302
00:16:32,272 --> 00:16:33,812
С чего бы ей ждать меня?

303
00:16:35,132 --> 00:16:38,282
Хватит мне все эти вопросы задавать!
Мне нужно снова принять душ.

304
00:16:42,412 --> 00:16:44,162
Остальные должны сейчас подойти?

305
00:16:44,172 --> 00:16:46,222
Ах, да... нет.

306
00:16:47,142 --> 00:16:50,462
Так получилось, что Раж и Говард
заняты работой, а Шелдон...

307
00:16:50,932 --> 00:16:54,412
ему сделали колоноскопию, и он
еще не пришел в норму.

308
00:16:56,222 --> 00:16:58,282
Моему дяде недавно делали колоноскопию.

309
00:16:58,292 --> 00:17:01,782
Ты шутишь. Получается, у нас есть что-то общее.

310
00:17:02,872 --> 00:17:04,552
То есть?

311
00:17:04,562 --> 00:17:07,582
У нас обоих есть знакомые люди, кто...

312
00:17:07,592 --> 00:17:11,032
имеет проблемы с кишечником.

313
00:17:17,102 --> 00:17:19,732
Ну, что нового в мире физики?

314
00:17:20,332 --> 00:17:22,112
Ничего.

315
00:17:22,882 --> 00:17:24,422
Правда? Ничего?

316
00:17:24,432 --> 00:17:30,102
Ну, за исключением теории струн, ничего
особенного не происходило с 1930-го года.

317
00:17:30,112 --> 00:17:32,022
и теорию струн нельзя доказать.

318
00:17:32,032 --> 00:17:33,792
Максимум можно сказать, "Смотрите-ка все,

319
00:17:33,802 --> 00:17:37,562
моя идея логична и последовательна"

320
00:17:38,562 --> 00:17:39,992
Ах.

321
00:17:40,002 --> 00:17:42,162
Ну, я думаю все получится.

322
00:17:46,442 --> 00:17:48,642
А что нового на кондитерской фабрике?

323
00:17:48,652 --> 00:17:51,182
Э.. да ничего особенного.

324
00:17:51,192 --> 00:17:54,792
Шоколадки с лимонами неплохо продаются.

325
00:17:55,792 --> 00:17:58,772
Хорошо. Хорошо.

326
00:18:02,112 --> 00:18:04,652
Как у тебя дела с.. э-э...

327
00:18:04,662 --> 00:18:06,472
коридорным другом?

328
00:18:06,482 --> 00:18:09,012
С Дагом? А, ну я не знаю,

329
00:18:09,022 --> 00:18:11,132
он такой милый и забавный, но...

330
00:18:11,142 --> 00:18:12,142
Хотите заказать чего-нибудь выпить?

331
00:18:12,152 --> 00:18:13,962
Нет!

332
00:18:14,672 --> 00:18:16,632
- На чем ты остановилась, "но..."
- Я бы хотела выпить.

333
00:18:16,642 --> 00:18:19,532
Просто скажи, что там после "но",
и я позову ее обратно.

334
00:18:20,702 --> 00:18:23,762
Ладно, ну, в общем, это из-за меня.

335
00:18:23,772 --> 00:18:26,202
Я все еще переживаю из-за разрыва с Куртом,

336
00:18:26,212 --> 00:18:28,962
и все это с Дагом, просто ради секса, чтобы забыться.

337
00:18:29,432 --> 00:18:32,792
Ой, только не надо о сексе, чтобы забыться.

338
00:18:34,032 --> 00:18:35,652
Это просто... мой характер.

339
00:18:35,662 --> 00:18:38,052
Я расстаюсь, потом нахожу другого милого парня,

340
00:18:38,062 --> 00:18:41,982
а потом следуют 36 часов бессмысленного...

341
00:18:41,992 --> 00:18:43,962
..сам знаешь чего.

342
00:18:45,152 --> 00:18:47,072
Не уверен, что знаю.

343
00:18:48,102 --> 00:18:51,342
Эти 36 часов проходят непрерывно,

344
00:18:51,352 --> 00:18:53,932
или они растянуты на всё, скажем...

345
00:18:53,942 --> 00:18:56,412
на одно жаркое лето?

346
00:18:56,422 --> 00:18:58,192
Нет, обычно это происходит за выходные,

347
00:18:58,202 --> 00:19:00,892
и поверь, я не чувсвую себя потом хорошо.

348
00:19:01,362 --> 00:19:03,472
Мозоли от трения, да?

349
00:19:04,802 --> 00:19:05,852
В эмоциональном плане.

350
00:19:05,862 --> 00:19:08,332
Ну да, конечно, эмоциональные трения.

351
00:19:15,962 --> 00:19:18,062
Эй, хочешь фокус покажу?

352
00:19:19,392 --> 00:19:23,722
Я могу поместить эту оливку в этот стакан, не трогая ее.

353
00:19:24,132 --> 00:19:26,632
- Как?
- Физика.

354
00:19:32,042 --> 00:19:34,212
Ух ты, центробежная сила.

355
00:19:34,222 --> 00:19:36,552
Вообще-то, это центростремительная сила,

356
00:19:36,562 --> 00:19:40,722
являющаяся обратной реакцией воздействия оливки на стакан.

357
00:19:41,992 --> 00:19:43,472
Прости.

358
00:19:43,482 --> 00:19:46,502
Если бы ты находилась на месте оливки,

359
00:19:46,512 --> 00:19:49,772
ты была бы в неинерциальной системе отсчета, и...

360
00:19:51,712 --> 00:19:52,652
Ты в порядке?

361
00:19:52,662 --> 00:19:54,362
Да, я в порядке.

362
00:19:55,522 --> 00:19:57,202
Ты пролила кетчуп?

363
00:19:57,212 --> 00:19:59,402
- Нет.
- Я не в порядке.

364
00:20:01,195 --> 00:20:03,135
Уверен, что не нужно сходить в медпункт?

365
00:20:03,145 --> 00:20:06,105
Нет, нет, уже все нормально. Не кровоточит.

366
00:20:06,115 --> 00:20:09,025
Я знаю, но тебя вырвало,
разве это не признак сотрясения?

367
00:20:09,035 --> 00:20:11,995
Да. Но меня укачивает в машинах, так что...

368
00:20:12,665 --> 00:20:14,075
Ладно.

369
00:20:14,085 --> 00:20:15,735
Извини за машину, кстати.

370
00:20:15,745 --> 00:20:18,425
Да ничего, ты же высунулся из окна.

371
00:20:19,935 --> 00:20:21,365
Бедный парень на мотоцикле...

372
00:20:24,505 --> 00:20:25,985
Я хорошо провел время.

373
00:20:25,995 --> 00:20:27,805
Да, я тоже.

374
00:20:28,735 --> 00:20:30,235
Ну, что ж, спокойной ночи.

375
00:20:30,705 --> 00:20:32,185
Спокойной ночи.

376
00:20:33,125 --> 00:20:34,715
- Леонард?
- А?

377
00:20:35,505 --> 00:20:37,925
Это должно было быть свидание?

378
00:20:38,515 --> 00:20:40,205
Это?

379
00:20:40,805 --> 00:20:43,045
Нет.

380
00:20:43,725 --> 00:20:45,075
Нет, конечно нет.

381
00:20:45,085 --> 00:20:47,745
Это просто мы с тобой выбрались
поужинать с ребятами, которые

382
00:20:47,755 --> 00:20:51,105
не смогли появится из-за работы и колоноскопии.

383
00:20:53,345 --> 00:20:55,175
Ладно, я на всякий случай спросила.

384
00:20:55,805 --> 00:20:59,565
Когда я приглашаю девушку на свидание...

385
00:21:01,245 --> 00:21:03,695
она знает, что на идет свидание.

386
00:21:05,145 --> 00:21:07,515
С большой буквы "С".

387
00:21:07,525 --> 00:21:09,605
Жирным шрифтом, подчеркнуто.

388
00:21:09,615 --> 00:21:12,365
<i><u>СВИ-ДА-НИ-Е</i></u>.

389
00:21:13,095 --> 00:21:15,975
А вообще, возможно у меня и есть
сотрясение, пойду прилягу.

390
00:21:15,985 --> 00:21:17,825
Спокойной ночи.

391
00:21:21,215 --> 00:21:23,265
Ну, как прошло свидание?

392
00:21:23,275 --> 00:21:25,265
Потрясно!

393
00:21:28,755 --> 00:21:25,265
Ликер и нетрезвый рассудок победил.

