1
00:00:10,171 --> 00:00:12,197
Damn you, walletnook.com!

2
00:00:14,053 --> 00:00:14,979
Problem?

3
00:00:15,133 --> 00:00:17,400
The online description
was completely misleading.

4
00:00:17,584 --> 00:00:20,165
They said:
"8 slots plus removable ID".

5
00:00:20,474 --> 00:00:23,349
To any rational person,
that would mean room for nine cards.

6
00:00:23,503 --> 00:00:27,439
But they don't tell you the removable ID
takes up one slot. It's a nightmare.

7
00:00:29,003 --> 00:00:33,404
Do you really need the Honorary Justice
League of American membership card?

8
00:00:34,752 --> 00:00:37,744
It's been in every wallet I've owned
since I was five.

9
00:00:38,486 --> 00:00:39,460
Why?

10
00:00:39,875 --> 00:00:42,576
It says,
"Keep this on your person at all times."

11
00:00:43,564 --> 00:00:45,721
It's right here
under Batman's signature.

12
00:00:47,872 --> 00:00:51,076
...and this is Leonard
and Sheldon's apartment.

13
00:00:51,674 --> 00:00:53,993
Guess whose parents
just got broadband.

14
00:00:54,334 --> 00:00:56,341
Leonard, may I present,
live from New Delhi,

15
00:00:56,534 --> 00:00:58,193
Dr. and Mrs. V. M. Koothrappali.

16
00:00:58,465 --> 00:01:00,490
<i>Hi!
Tilt up the camera up!</i>

17
00:01:00,688 --> 00:01:02,259
<i>I'm looking at his crotch.</i>

18
00:01:02,469 --> 00:01:03,588
Sorry, Papa.

19
00:01:03,793 --> 00:01:05,197
<i>Oh, that's much better. Hi.</i>

20
00:01:06,494 --> 00:01:08,481
And over here is Sheldon.

21
00:01:10,085 --> 00:01:11,288
He lives with Leonard.

22
00:01:11,715 --> 00:01:14,334
<i>That's nice.
Like Haroon and Tanvir.</i>

23
00:01:14,557 --> 00:01:16,015
Not like Haroon and Tanvir.

24
00:01:16,307 --> 00:01:17,569
<i>Such sweet young men.</i>

25
00:01:17,836 --> 00:01:21,038
<i>They just adopted the cutest
little Punjabi baby.</i>

26
00:01:21,439 --> 00:01:23,581
No, we're not like
Haroon and Tanvir.

27
00:01:24,621 --> 00:01:26,564
<i>So, are you boys academics
like our son?</i>

28
00:01:26,807 --> 00:01:27,660
Yes.

29
00:01:27,894 --> 00:01:30,473
<i>And your parents are comfortable
with your limited earning potential?</i>

30
00:01:30,648 --> 00:01:31,902
- Oh, yes.
- Not at all.

31
00:01:32,319 --> 00:01:33,755
Papa, please. Don't start.

32
00:01:33,937 --> 00:01:36,144
<i>It was just a question.
He's so sensitive.</i>

33
00:01:36,295 --> 00:01:38,707
That's my life. That's my friends.
Good to see you. Say good-bye.

34
00:01:38,919 --> 00:01:40,887
- Bye.
- <i>Wait! Wait!</i>

35
00:01:43,053 --> 00:01:44,886
<i>Before you go,
we have good news.</i>

36
00:01:45,079 --> 00:01:46,950
<i>Put the computer down
and gather your friends.</i>

37
00:01:52,086 --> 00:01:53,743
- What is it, Papa?
- <i>Friends!</i>

38
00:01:57,557 --> 00:01:59,197
Is it just me,
or does web chatting

39
00:01:59,371 --> 00:02:01,416
with your clothes on
seem a little pointless?

40
00:02:03,607 --> 00:02:05,054
<i>Do you rember Lalita Gupta?</i>

41
00:02:05,443 --> 00:02:09,170
The little fat girl that used to kick me
in the samosas and call me untouchable?

42
00:02:09,363 --> 00:02:12,315
<i>Yes. Well, now she's
a dental student at USC,</i>

43
00:02:12,702 --> 00:02:14,681
<i>so we gave her
your contact information.</i>

44
00:02:15,329 --> 00:02:16,555
Why did you do that?

45
00:02:16,744 --> 00:02:19,228
<i>You're 26 years old, Rajesh.
We want grandchildren.</i>

46
00:02:19,441 --> 00:02:22,865
- But, Papa, I'm not supposed...
- <i>Lalita's parents approved the match.</i>

47
00:02:23,492 --> 00:02:26,271
<i>If you decide on a spring wedding,
we can avoid monsoon season.</i>

48
00:02:26,638 --> 00:02:27,969
A spring wedding?!

49
00:02:28,354 --> 00:02:30,308
<i>It's up to you, dear.
We don't want to meddle.</i>

50
00:02:30,467 --> 00:02:32,319
If you don't want to meddle,
then why are you?

51
00:02:32,531 --> 00:02:35,097
If I may, your parents probably
don't consider this meddling.

52
00:02:35,270 --> 00:02:37,188
While arranged marriages
are no longer the norm,

53
00:02:37,356 --> 00:02:38,784
Indian parents continue to have

54
00:02:38,938 --> 00:02:41,369
a greater-than-average involvement
in their children's love lives.

55
00:02:41,684 --> 00:02:43,940
Why are you telling me
about my own culture?

56
00:02:45,039 --> 00:02:46,274
You seemed confused.

57
00:02:48,190 --> 00:02:50,390
Sorry, but with all due respect,
I really don't want to...

58
00:02:50,572 --> 00:02:52,139
<i>I'm sorry, darling.
We have to go.</i>

59
00:02:52,395 --> 00:02:53,764
Doogie Howser <i>is on.</i>

60
00:02:54,977 --> 00:02:56,983
<i>Grandma!
It's </i>Doogie <i>Time!</i>

61
00:02:57,601 --> 00:02:58,951
<i>- Bye-bye!
- Bye-bye!</i>

62
00:03:02,487 --> 00:03:03,810
I don't believe it.

63
00:03:04,025 --> 00:03:05,025
Neither do I.

64
00:03:05,218 --> 00:03:07,814
Doogie Howser's been off
the air for like 20 years.

65
00:03:08,931 --> 00:03:10,243
Actually,
I read somewhere

66
00:03:10,417 --> 00:03:12,531
that it's one of the most
popular programs in India.

67
00:03:12,685 --> 00:03:14,609
It might speak to a cultural aspiration

68
00:03:14,729 --> 00:03:16,835
to have one's children
enter the medical profession.

69
00:03:16,955 --> 00:03:18,012
I bet you're right.

70
00:03:18,225 --> 00:03:20,560
- I bet they loveScrubs.
- What's not to love?

71
00:03:20,773 --> 00:03:21,929
Excuse me! Hello?

72
00:03:22,576 --> 00:03:25,952
My parents are trying to marry me to
a total stranger. What am I going to do?

73
00:03:26,500 --> 00:03:28,024
I suggest you go through with it.

74
00:03:28,214 --> 00:03:29,104
What?!

75
00:03:29,323 --> 00:03:33,527
Romantic love as the basis for marriage
has only existed since the 19th century.

76
00:03:33,858 --> 00:03:36,076
Up until then,
arranged marriages were the norm,

77
00:03:36,249 --> 00:03:37,767
and it served society quite well.

78
00:03:37,999 --> 00:03:39,889
It's the entire premise
of <i>Fiddler on the Roof</i>.

79
00:03:40,044 --> 00:03:42,648
I'm not a big fan of musicals,
but I love that show.

80
00:03:42,977 --> 00:03:45,519
Me too.
Of course, it speaks to me culturally.

81
00:03:45,827 --> 00:03:48,246
Understandable, but there's
a universality to that story

82
00:03:48,439 --> 00:03:49,751
which transcends ethnicity.

83
00:03:50,151 --> 00:03:52,246
Let's not forget it's got
some really catchy tunes.

84
00:03:54,339 --> 00:03:56,007
- I know what I'm going to do.
- What?

85
00:03:56,250 --> 00:03:57,465
Find new friends.

86
00:04:03,683 --> 00:04:05,207
So who wants to rent <i>Fiddler</i>?

87
00:04:05,545 --> 00:04:07,127
No need, we have the special edition.

88
00:04:09,689 --> 00:04:11,899
Maybe we are like Haroon and Tanvir.

89
00:04:32,955 --> 00:04:34,383
The Big Bang Theory
Season 1 Episode 08 - The Grasshopper Experiment
Subtitles: swsub.com, Synchro: SerJo

90
00:04:37,266 --> 00:04:39,033
This is Dr. Sheldon Cooper.

91
00:04:39,344 --> 00:04:41,959
Yeah, I need to cancel
my membership to the planetarium.

92
00:04:45,206 --> 00:04:46,383
Well, I'm sorry, too,

93
00:04:46,592 --> 00:04:48,887
but there's just no room
for you in my wallet.

94
00:04:51,316 --> 00:04:52,306
I understand,

95
00:04:52,462 --> 00:04:55,009
but it was between you
and the Museum of Natural History,

96
00:04:55,200 --> 00:04:57,245
and frankly,
you don't have dinosaurs.

97
00:04:59,439 --> 00:05:00,770
I'll miss you, too. Bye-bye.

98
00:05:02,669 --> 00:05:05,872
Okay, I know you're texting about me
and I'd really like you to stop.

99
00:05:08,597 --> 00:05:10,989
Oh, dear,
I am rightly and truly screwed.

100
00:05:11,896 --> 00:05:13,777
Hey, I thought you were
finding new friends.

101
00:05:13,989 --> 00:05:15,475
I've got some feelers out.

102
00:05:16,349 --> 00:05:18,156
In the meantime, listen to this.

103
00:05:18,466 --> 00:05:20,723
Hi, Rajesh.
This is Lalita Gupta.

104
00:05:21,067 --> 00:05:24,155
Your mother gave my mother
your phone number to give to me.

105
00:05:24,347 --> 00:05:25,601
So, I'm calling you

106
00:05:26,006 --> 00:05:28,224
and... call me back. Bye.

107
00:05:31,016 --> 00:05:32,984
Can you believe how pushy she is?

108
00:05:35,333 --> 00:05:36,682
So don't call her.

109
00:05:36,986 --> 00:05:39,361
If I don't call her, I won't hear
the end of it from my parents.

110
00:05:39,533 --> 00:05:40,696
- So call her.
- How?

111
00:05:40,870 --> 00:05:42,317
You know I can't talk to women.

112
00:05:43,254 --> 00:05:44,464
I'm done.
Anybody else?

113
00:05:45,569 --> 00:05:46,900
- Give me the phone.
- Why?

114
00:05:47,228 --> 00:05:48,323
Just give it to me.

115
00:05:49,496 --> 00:05:51,583
- What are you doing?
- Don't worry. You'll thank me.

116
00:05:52,832 --> 00:05:55,041
Hello, Lalita?
Raj Koothrappali here.

117
00:05:58,449 --> 00:06:00,610
Yes, it is good
to talk to you, too.

118
00:06:03,208 --> 00:06:04,906
So what are you wearing?

119
00:06:08,149 --> 00:06:09,403
Oh, not important.

120
00:06:09,697 --> 00:06:12,224
So, anyhow,
when would you like to meet?

121
00:06:13,637 --> 00:06:15,162
Friday works for me!

122
00:06:16,815 --> 00:06:18,920
I'll call you with a time and place.

123
00:06:19,416 --> 00:06:21,992
But in the meantime,
keep it real, babe.

124
00:06:28,683 --> 00:06:30,084
You may now thank me.

125
00:06:31,636 --> 00:06:34,031
For what? Making me sound
like a <i>Simpsons</i> character?

126
00:06:35,207 --> 00:06:36,726
Next time make your own date.

127
00:06:36,924 --> 00:06:39,144
- I didn't want this one!
- Look on the bright side,

128
00:06:39,317 --> 00:06:41,496
she might turn out to be
a nice, beautiful girl.

129
00:06:41,767 --> 00:06:44,160
Great, then we'll get married,
I won't be able to talk to her

130
00:06:44,391 --> 00:06:47,015
and we'll spend the rest
of our lives in total silence.

131
00:06:47,603 --> 00:06:49,205
It worked for my parents.

132
00:06:51,045 --> 00:06:51,987
Hi, guys.

133
00:06:52,353 --> 00:06:53,727
I need some guinea pigs.

134
00:06:54,152 --> 00:06:57,900
There's a lab animal supply company
in Reseda you could try.

135
00:06:58,267 --> 00:07:00,453
But if your research is going
to have human applications,

136
00:07:00,627 --> 00:07:02,074
may I suggest white mice instead?

137
00:07:02,228 --> 00:07:04,794
Their brain chemistry
is far closer to ours.

138
00:07:06,593 --> 00:07:08,051
I swear to God, one day,

139
00:07:08,263 --> 00:07:10,501
I'm going to get the hang
of talking to you.

140
00:07:11,168 --> 00:07:13,260
His mom's been saying that for years.

141
00:07:15,978 --> 00:07:16,926
What's up?

142
00:07:17,254 --> 00:07:19,743
I finally convinced the restaurant
to give me a bartending shift,

143
00:07:19,909 --> 00:07:21,549
so I need to practice mixing drinks.

144
00:07:21,791 --> 00:07:24,082
Great.
The key to acquiring proficiency

145
00:07:24,275 --> 00:07:25,857
in any task is repetition.

146
00:07:26,186 --> 00:07:27,730
With certain obvious exceptions.

147
00:07:29,201 --> 00:07:30,737
Suicide, for example.

148
00:07:33,104 --> 00:07:34,560
So, Leonard, how about it?

149
00:07:36,578 --> 00:07:40,217
We'd love to help you, but Raj
is going through some stuff right now,

150
00:07:40,504 --> 00:07:42,104
besides, he doesn't drink, so...

151
00:07:44,722 --> 00:07:45,722
Really?

152
00:07:48,459 --> 00:07:50,482
Raj is going through
some stuff right now

153
00:07:50,694 --> 00:07:52,584
and he'd like to take up drinking.

154
00:07:55,687 --> 00:07:58,138
Here you go, Leonard.
One Tequila Sunrise.

155
00:07:58,351 --> 00:07:59,351
Thank you.

156
00:07:59,834 --> 00:08:01,393
This drink is a wonderful example

157
00:08:01,661 --> 00:08:03,747
of how liquids with different
specific gravities

158
00:08:03,940 --> 00:08:05,811
interact in a cylindrical container.

159
00:08:07,913 --> 00:08:08,913
Thank you.

160
00:08:10,261 --> 00:08:11,477
Okay, Raj, what'll it be?

161
00:08:14,385 --> 00:08:15,575
Whatever you recommend.

162
00:08:15,942 --> 00:08:18,271
How about a Grasshopper?
I make a mean Grasshopper.

163
00:08:19,239 --> 00:08:20,377
Okay? Good. Coming up.

164
00:08:20,634 --> 00:08:22,267
Sheldon, what are you going to have?

165
00:08:22,439 --> 00:08:24,082
I'll have a Diet Coke.

166
00:08:25,680 --> 00:08:28,955
Can you please order a cocktail?
I need to practice mixing drinks.

167
00:08:29,213 --> 00:08:31,760
Fine.
I'll have a Virgin Cuba Libre.

168
00:08:34,454 --> 00:08:36,690
That's, rum and Coke
without the rum.

169
00:08:37,052 --> 00:08:38,036
Yes.

170
00:08:39,760 --> 00:08:40,760
So... Coke.

171
00:08:41,109 --> 00:08:42,015
Yes.

172
00:08:43,480 --> 00:08:45,023
And would you make it diet?

173
00:08:50,282 --> 00:08:51,594
There's a can in the fridge.

174
00:08:52,428 --> 00:08:56,023
A Cuba Libre traditionally comes
in a tall glass with a lime wedge.

175
00:08:56,589 --> 00:08:58,016
Then swim to Cuba.

176
00:09:00,695 --> 00:09:02,773
Bartenders are supposed
to have people skills.

177
00:09:06,102 --> 00:09:07,395
Raj, here you go.

178
00:09:07,917 --> 00:09:09,191
All right.
Who's next?

179
00:09:09,470 --> 00:09:11,650
I'd like to try
a Slippery Nipple.

180
00:09:16,670 --> 00:09:17,904
Okay, you're cut off.

181
00:09:20,683 --> 00:09:21,840
Anybody need a refill?

182
00:09:22,510 --> 00:09:23,860
Where did my life go, Penny?

183
00:09:26,852 --> 00:09:29,233
One day I'm a carefree bachelor,
and the next I'm married

184
00:09:29,426 --> 00:09:31,403
and driving a minivan
to peewee cricket matches

185
00:09:31,577 --> 00:09:33,217
in suburban New Delhi.

186
00:09:35,692 --> 00:09:36,908
Are you talking to me?

187
00:09:37,424 --> 00:09:39,015
Is the another Penny here?

188
00:09:40,778 --> 00:09:43,175
I had such plans.
I had dreams.

189
00:09:43,548 --> 00:09:46,823
I was going to be the Indira Gandhi
of particle astrophysics.

190
00:09:47,133 --> 00:09:48,930
But with a penis, of course.

191
00:09:51,354 --> 00:09:53,632
- Amazing.
- Ever since I was a little boy,

192
00:09:53,861 --> 00:09:55,925
my father wanted me to be
a gynecologist like him.

193
00:09:57,131 --> 00:10:00,642
How can I be a gynecologist?
I can barely look a woman in the eye!

194
00:10:04,464 --> 00:10:05,400
You know what?

195
00:10:05,584 --> 00:10:07,959
I'm not going to let my parents
control my future any longer.

196
00:10:08,148 --> 00:10:09,575
It's time for a showdown.

197
00:10:09,824 --> 00:10:12,034
Somebody give me a computer
with a webcam!

198
00:10:13,776 --> 00:10:15,995
Sweetie, I think
that's the Grasshopper talking.

199
00:10:16,323 --> 00:10:17,829
And it's about to tell my parents

200
00:10:18,002 --> 00:10:21,282
that I'm not riding an elephant
down the aisle with Lalita Gupta.

201
00:10:21,592 --> 00:10:22,883
Okay, calm down.

202
00:10:23,645 --> 00:10:25,497
No one can make you get married.

203
00:10:26,114 --> 00:10:28,258
Why don't you just meet this girl
and see what happens?

204
00:10:28,442 --> 00:10:30,978
Haven't you been listening to me?
I cannot talk to women.

205
00:10:32,221 --> 00:10:34,285
No, no, let's see
how long it takes him.

206
00:10:35,818 --> 00:10:36,937
Raj, honey,

207
00:10:37,383 --> 00:10:40,698
you say you can't talk to women,
but you've been talking to me.

208
00:10:40,943 --> 00:10:42,372
And now we'll never know.

209
00:10:44,091 --> 00:10:45,091
You're right.

210
00:10:46,687 --> 00:10:48,057
I am talking to you.

211
00:10:48,887 --> 00:10:50,411
Hello, Penny, how are you?

212
00:10:52,179 --> 00:10:54,385
- I'm fine.
- Okay, now I just need

213
00:10:54,636 --> 00:10:57,520
to make sure I have a Lalita
before I meet the Grasshopper.

214
00:11:00,823 --> 00:11:03,199
It's a sweet, green miracle.

215
00:11:03,980 --> 00:11:07,370
If you're going to drink on this date,
just promise me you won't overdo it.

216
00:11:07,826 --> 00:11:08,826
Overdo what?

217
00:11:09,285 --> 00:11:10,285
Happiness?

218
00:11:11,585 --> 00:11:12,585
Freedom?

219
00:11:13,076 --> 00:11:14,897
This warm glow inside of me

220
00:11:15,090 --> 00:11:17,954
that promises everything's
going to be all hunky-dunky?

221
00:11:19,002 --> 00:11:21,533
Yeah, that.
Why don't you bring her to my restaurant

222
00:11:21,706 --> 00:11:23,887
while I'm tending the bar
so I can keep an eye on you.

223
00:11:26,412 --> 00:11:27,716
What's the plan here?

224
00:11:27,867 --> 00:11:30,012
Let's say he meets her,
he likes her, they get married.

225
00:11:30,147 --> 00:11:32,731
What's he going to do,
stay drunk for the rest of his life?

226
00:11:32,925 --> 00:11:34,390
Worked for my parents.

227
00:11:45,703 --> 00:11:49,118
I can't believe I'm sitting here
next to little Lalita Gupta.

228
00:11:50,455 --> 00:11:51,613
Well, you are.

229
00:11:52,333 --> 00:11:53,528
Little Lalita.

230
00:11:54,435 --> 00:11:55,968
That's kind of fun to say.

231
00:11:56,229 --> 00:11:58,429
Little Lalita, little
Lalita, little Lalita.

232
00:11:59,586 --> 00:12:01,265
- You should try it.
- Oh, it's okay.

233
00:12:04,748 --> 00:12:06,349
You have lost so much weight.

234
00:12:07,730 --> 00:12:11,048
That must have been difficult for you
because you were so, so fat.

235
00:12:12,947 --> 00:12:13,950
Do you remember?

236
00:12:14,711 --> 00:12:15,785
Yes, I do.

237
00:12:16,071 --> 00:12:18,814
Of course you do.
Who could forget being that fat?

238
00:12:20,818 --> 00:12:21,925
I've been trying.

239
00:12:23,342 --> 00:12:24,943
So you're a dental student.

240
00:12:27,184 --> 00:12:30,417
Are you aware that dentists
have an extremely high suicide rate?

241
00:12:31,627 --> 00:12:33,712
Not as high as, say,
air traffic controllers,

242
00:12:33,832 --> 00:12:36,192
but there are far more dentists
than air traffic controllers,

243
00:12:36,353 --> 00:12:38,167
so in pure numbers,
you're still winning.

244
00:12:39,032 --> 00:12:40,032
Yay, me.

245
00:12:41,837 --> 00:12:44,323
Do you have a drink that will
make him less obnoxious?

246
00:12:44,770 --> 00:12:46,437
Drinks do not work that way.

247
00:12:47,908 --> 00:12:49,501
I'd say he's doing fine.
Look at her.

248
00:12:49,731 --> 00:12:53,107
The last girl my mom set me up with
had a mustache and a vestigial tail.

249
00:12:55,868 --> 00:12:57,412
- Sorry I'm late.
- What happened?

250
00:12:57,779 --> 00:12:59,826
Nothing.
I just really didn't want to come.

251
00:13:01,976 --> 00:13:04,484
Virgin diet Cuba Libre, please.

252
00:13:06,193 --> 00:13:08,566
In a tall glass
with a lime wedge.

253
00:13:09,787 --> 00:13:11,523
Oh, I'll wedge it right in there.

254
00:13:14,044 --> 00:13:15,307
So how's Koothrappali...

255
00:13:15,723 --> 00:13:17,016
Oh, my Lord.

256
00:13:17,304 --> 00:13:19,480
- What?
- That's Princess Panchali.

257
00:13:20,427 --> 00:13:22,018
I'm pretty sure her name's Lalita.

258
00:13:22,233 --> 00:13:25,734
No, no, Princess Panchali,
fromThe Monkey and the Princess.

259
00:13:26,781 --> 00:13:30,139
Oh, yeah. I tried to watch that online,
but they wanted my credit card.

260
00:13:32,389 --> 00:13:33,652
It's a children's story.

261
00:13:33,899 --> 00:13:35,134
Oh, no, it isn't.

262
00:13:38,749 --> 00:13:41,796
When I was a little boy and got sick,
which was most of the time,

263
00:13:41,981 --> 00:13:43,396
my mother would read it to me.

264
00:13:43,581 --> 00:13:46,571
It's about an Indian princess
who befriends a monkey

265
00:13:46,861 --> 00:13:49,882
who was mocked by all the other monkeys
because he was different.

266
00:13:50,120 --> 00:13:52,541
For some reason,
I related to it quite strongly.

267
00:13:53,575 --> 00:13:54,925
I know the reason.

268
00:13:56,123 --> 00:13:57,558
We all know the reason.

269
00:13:59,449 --> 00:14:00,934
Sheldon, what are you getting at?

270
00:14:01,155 --> 00:14:03,171
That woman looks
exactly like the pictures

271
00:14:03,325 --> 00:14:04,936
of Princess Panchali in the book.

272
00:14:05,413 --> 00:14:08,870
How often does one see a beloved
fictional character come to life?

273
00:14:09,351 --> 00:14:11,024
Every year at Comic-Con.

274
00:14:12,459 --> 00:14:13,674
Every day at Disneyland.

275
00:14:13,828 --> 00:14:16,242
You can hire Snow White
to come to your house.

276
00:14:16,540 --> 00:14:18,778
Of course, they prefer it
if you have a kid.

277
00:14:20,010 --> 00:14:21,802
Hey, guys.
This is Lalita Gupta.

278
00:14:22,014 --> 00:14:23,211
Lalita, this is Leonard

279
00:14:23,414 --> 00:14:25,363
and Sheldon
and Howard and Penny.

280
00:14:25,726 --> 00:14:27,694
Isn't it great?
She isn't fat anymore.

281
00:14:29,134 --> 00:14:31,572
"Forgive me, Your Highness,
for I am but a monkey,

282
00:14:31,758 --> 00:14:33,302
"and it is in my nature to climb.

283
00:14:33,489 --> 00:14:34,937
"I did not mean to gaze upon you

284
00:14:35,110 --> 00:14:36,649
as you comb your hair."

285
00:14:38,384 --> 00:14:39,465
I'm sorry?

286
00:14:39,862 --> 00:14:42,871
You are the living embodiment
of the beautiful Princess Panchali.

287
00:14:43,502 --> 00:14:44,832
Oh, no kidding.

288
00:14:45,491 --> 00:14:46,494
Who is that?

289
00:14:46,861 --> 00:14:49,195
A beloved character
from an Indian folktale.

290
00:14:51,372 --> 00:14:53,745
Us Indian or
"Come to our casino" Indian?

291
00:14:56,108 --> 00:14:56,995
You Indian.

292
00:14:58,240 --> 00:14:59,994
The resemblance is remarkable.

293
00:15:00,385 --> 00:15:02,257
I can practically smell
the lotus blossoms

294
00:15:02,450 --> 00:15:03,859
woven into your ebony hair.

295
00:15:04,097 --> 00:15:05,292
Well, thanks.

296
00:15:06,093 --> 00:15:08,056
I imagine you smell very nice, too.

297
00:15:08,750 --> 00:15:12,006
I shower twice a day and wash
my hands as often as I can.

298
00:15:12,957 --> 00:15:14,634
Really? So do I.

299
00:15:14,877 --> 00:15:16,903
But you're a dentist.
He's nuts.

300
00:15:18,952 --> 00:15:20,457
Don't be insulting, Rajesh.

301
00:15:21,155 --> 00:15:22,061
So, Sheldon,

302
00:15:22,620 --> 00:15:25,009
tell me more about this princess
you say I look like.

303
00:15:25,434 --> 00:15:29,142
It was said that the gods
fashioned her eyes out of the stars

304
00:15:29,489 --> 00:15:33,437
and that roses were ashamed to bloom
in the presence of her ruby lips.

305
00:15:34,337 --> 00:15:36,431
- Oh, my.
- Back off, Sheldon.

306
00:15:37,023 --> 00:15:37,872
What?

307
00:15:38,056 --> 00:15:40,923
If you do not stop hitting on my lady,
you'll feel the full extent of my wrath.

308
00:15:42,812 --> 00:15:44,027
I'm not hitting on her.

309
00:15:44,452 --> 00:15:45,790
And I am not your lady.

310
00:15:45,973 --> 00:15:47,608
And you have no wrath.

311
00:15:49,954 --> 00:15:52,056
You are my lady.
Our parents said so.

312
00:15:52,269 --> 00:15:54,681
We are, for all intents
and purposes, 100% hooked up.

313
00:15:55,203 --> 00:15:56,728
Let's get something straight here.

314
00:15:56,921 --> 00:15:59,712
The only reason I came tonight
was to get my parents off my case.

315
00:15:59,918 --> 00:16:02,567
I certainly don't need to be getting
this Old World crap from you.

316
00:16:02,746 --> 00:16:04,279
That's exactly the kind of spirit

317
00:16:04,471 --> 00:16:06,981
with which Princess Panchali
led the monkeys to freedom.

318
00:16:07,197 --> 00:16:09,463
- Screw Princess Panchali.
- You can't talk to me like that.

319
00:16:09,617 --> 00:16:11,160
But you're not Princess Panchali.

320
00:16:11,352 --> 00:16:13,612
Luckily for you--
she could have you beheaded.

321
00:16:14,541 --> 00:16:15,775
Sheldon, are you hungry?

322
00:16:15,943 --> 00:16:17,932
- I could eat.
- Let's go.

323
00:16:22,222 --> 00:16:23,322
What just happened?

324
00:16:23,564 --> 00:16:25,213
Beats the hell out of me.

325
00:16:25,380 --> 00:16:28,674
I'll tell you what happened. I just
learned how to pick up Indian chicks.

326
00:16:31,823 --> 00:16:33,974
<i>What are we supposed to say
to Lalita's parents?</i>

327
00:16:34,297 --> 00:16:36,929
<i>I play golf with her father.
I won't be able to look at him.</i>

328
00:16:37,095 --> 00:16:39,614
Maybe you should keep your eye
on the ball, Papa.

329
00:16:40,199 --> 00:16:41,490
<i>Oh, now you're a funny man.</i>

330
00:16:41,610 --> 00:16:43,509
<i>This is not funny, Mr. Funny Man.</i>

331
00:16:44,484 --> 00:16:45,820
Dr. and Mrs. Koothrappali,

332
00:16:46,032 --> 00:16:48,521
in all fairness,
it wasn't entirely Raj's fault.

333
00:16:48,900 --> 00:16:50,366
<i>This is a family matter, Sheldon.</i>

334
00:16:50,520 --> 00:16:51,504
I'm Leonard.

335
00:16:52,401 --> 00:16:54,291
Sorry.
You all look alike to us.

336
00:16:56,227 --> 00:16:58,351
But he's right, Papa.
Listen to him.

337
00:16:59,068 --> 00:17:01,338
You! You are the one
who ruined everything.

338
00:17:01,681 --> 00:17:03,103
<i>Who is it?
We can't see.</i>

339
00:17:03,382 --> 00:17:04,674
<i>Turn us.
Turn us!</i>

340
00:17:08,054 --> 00:17:10,813
Go ahead, tell my parents
why they won't have any grandchildren.

341
00:17:11,343 --> 00:17:13,870
How would I know?
Do you have a low sperm count?

342
00:17:15,952 --> 00:17:18,008
This has nothing to do
with my sperm count.

343
00:17:18,247 --> 00:17:20,926
<i>You are wearing the boxers
that we sent you, aren't you?</i>

344
00:17:21,119 --> 00:17:23,090
- Yes, Mumi.
- <i>Because you know what happens</i>

345
00:17:23,263 --> 00:17:25,540
<i>to the samosas
when you wear tighty-whities.</i>

346
00:17:26,865 --> 00:17:28,982
Can we please stop talking
about my testicles?

347
00:17:29,550 --> 00:17:30,997
Sheldon, tell them what you did.

348
00:17:31,279 --> 00:17:32,272
What did I do?

349
00:17:32,823 --> 00:17:34,103
You left with his date.

350
00:17:34,634 --> 00:17:36,468
Friends don't do that to each other.

351
00:17:38,741 --> 00:17:39,936
All right, noted.

352
00:17:42,199 --> 00:17:43,199
Sorry.

353
00:17:43,453 --> 00:17:45,142
Sorry?
That's all you can say is sorry?

354
00:17:45,382 --> 00:17:47,624
Take it.
It's more than I've ever gotten.

355
00:17:47,856 --> 00:17:48,845
May I point out,

356
00:17:49,003 --> 00:17:52,002
she wouldn't have asked me to go with
her if you hadn't been drunk and boring.

357
00:17:52,221 --> 00:17:54,540
- <i>Drunk?</i>
- And boring-- her words.

358
00:17:55,666 --> 00:17:58,376
<i>I knew it. He moves to America
and he becomes an alcoholic.</i>

359
00:17:58,593 --> 00:17:59,964
I'm not an alcoholic.

360
00:18:00,253 --> 00:18:01,603
<i>Then why were you drunk?</i>

361
00:18:02,181 --> 00:18:05,007
- It was just this one time, I swear.
- <i>Are you in denial?</i>

362
00:18:05,176 --> 00:18:07,198
<i>Do we have to come over
and do an intervention?</i>

363
00:18:07,366 --> 00:18:09,705
<i>Don't embarrass him
in front of his friends.</i>

364
00:18:10,115 --> 00:18:11,951
<i>All right.
Carry us outside.</i>

365
00:18:12,162 --> 00:18:13,951
<i>We want to talk to you in private.</i>

366
00:18:14,699 --> 00:18:16,378
- But, Papa, please...
- <i>Now, Rajesh!</i>

367
00:18:20,930 --> 00:18:21,989
I have to go.

368
00:18:24,461 --> 00:18:25,561
Now, listen to me...

369
00:18:25,774 --> 00:18:27,583
At least wait till I get into the hall.

370
00:18:31,317 --> 00:18:32,527
Okay, well, good night.

371
00:18:33,188 --> 00:18:34,134
Hold on.

372
00:18:35,334 --> 00:18:37,001
What happened
with you and Lalita?

373
00:18:37,872 --> 00:18:39,863
We ate,
she lectured me

374
00:18:40,073 --> 00:18:42,079
on the link between gum disease
and heart attacks--

375
00:18:42,272 --> 00:18:44,433
nothing I didn't already know--
and I came home.

376
00:18:45,200 --> 00:18:46,802
So you're not going
to see her again?

377
00:18:47,572 --> 00:18:49,057
Why would I see her again?

378
00:18:49,418 --> 00:18:50,981
I already have a dentist.

379
00:18:56,655 --> 00:19:00,093
I wonder who's going to tell his parents
they're not having grandchildren?

380
00:19:21,342 --> 00:19:23,117
I don't believe it.
What's gotten into him?

381
00:19:23,634 --> 00:19:26,088
Oh, maybe a couple
virgin Cuba Libres

382
00:19:26,301 --> 00:19:28,596
that turned out
to be kind of slutty.

383
00:19:30,091 --> 00:19:31,113
You didn't.

384
00:19:31,353 --> 00:19:31,113
You do your experiments.
I do mine.

