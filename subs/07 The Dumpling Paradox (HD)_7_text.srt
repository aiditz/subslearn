1
00:00:02,013 --> 00:00:04,174
Watch this, it's really cool.

2
00:00:04,679 --> 00:00:06,158
Call Leonard Hofstadter.

3
00:00:06,992 --> 00:00:09,857
<i>Did you say:
"Call Hellen Boxlightner?</i>

4
00:00:13,811 --> 00:00:16,194
Call Leonard Hofstadter.

5
00:00:16,916 --> 00:00:19,883
<i>Did you say:
"Call Temple Badsaidor?"</i>

6
00:00:22,328 --> 00:00:23,582
Let me try!

7
00:00:24,836 --> 00:00:25,877
Call...

8
00:00:29,562 --> 00:00:31,622
<i>Calling "Rajesh Koothrappali."</i>

9
00:00:38,548 --> 00:00:40,169
That's very impressive.

10
00:00:41,222 --> 00:00:42,631
And a little racist.

11
00:00:43,908 --> 00:00:46,358
If we're all through playing
"mock the flawed technology,"

12
00:00:46,532 --> 00:00:47,905
can we get on with <i>Halo</i> night?

13
00:00:48,021 --> 00:00:50,282
We were supposed to start at
8:00. It is now 8:06.

14
00:00:50,491 --> 00:00:52,150
So we'll start now.

15
00:00:52,304 --> 00:00:54,765
First we have to decide
if those lost six minutes will be coming

16
00:00:54,885 --> 00:00:57,217
out of game time, bathroom time
or the pizza break.

17
00:00:57,389 --> 00:00:59,062
We could split it
two, two and two.

18
00:00:59,351 --> 00:01:02,631
If we're having anchovies on the pizza,
we can't take it out of bathroom time.

19
00:01:03,440 --> 00:01:05,290
Oh, what fresh hell is this?

20
00:01:06,588 --> 00:01:08,795
- Hey, Penny. Come on in.
- Hey, guys.

21
00:01:08,991 --> 00:01:10,264
See a Penny, pick her up,

22
00:01:10,488 --> 00:01:12,755
and all the day
you'll have good luck.

23
00:01:15,714 --> 00:01:16,814
No, you won't.

24
00:01:17,634 --> 00:01:19,139
Can I hide out here for a while?

25
00:01:19,312 --> 00:01:20,661
Sure. What's going on?

26
00:01:20,913 --> 00:01:23,451
There's this girl I know
from back in Nebraska, Christy.

27
00:01:23,605 --> 00:01:26,007
She called me up,
and she's like, "Hey, how's California?"

28
00:01:26,132 --> 00:01:28,409
And I'm like, "Awesome,"
'cause, you know, it's not Nebraska.

29
00:01:28,602 --> 00:01:29,849
And the next thing I know

30
00:01:30,060 --> 00:01:32,112
she's invited herself
out here to stay with me.

31
00:01:32,272 --> 00:01:33,447
8:08.

32
00:01:35,849 --> 00:01:38,457
Anyway, she got here today
and she's just been in my apartment

33
00:01:38,577 --> 00:01:40,777
yakity yakking about every guy
she slept with in Omaha,

34
00:01:40,912 --> 00:01:42,560
which is basically
every guy in Omaha,

35
00:01:42,680 --> 00:01:44,277
and washing the sluttiest

36
00:01:44,476 --> 00:01:47,221
collection of underwear
you have ever seen in my bathroom sink.

37
00:01:47,341 --> 00:01:50,096
Is she doing it one thong at a time,
or does she throw it all in...

38
00:01:50,832 --> 00:01:53,379
...like some sort
of erotic bouillabaisse?

39
00:01:55,355 --> 00:01:57,657
- He really needs to dial it down.
- I know.

40
00:01:59,460 --> 00:02:02,134
So if you don't like this Christy,
why are you letting her stay?

41
00:02:02,304 --> 00:02:03,760
She was engaged to my cousin

42
00:02:03,903 --> 00:02:06,665
while she was sleeping with my brother,
so she's kind of family.

43
00:02:08,626 --> 00:02:10,439
I apologize for my
earlier outburst.

44
00:02:10,656 --> 00:02:12,356
Who needs <i>Halo</i>,
when we can be regaled

45
00:02:12,511 --> 00:02:15,154
with the delightfully folksy
tale of the Whore of Omaha?

46
00:02:17,371 --> 00:02:18,741
I don't think she's a whore.

47
00:02:19,059 --> 00:02:20,810
No, yeah, she's definitely a whore.

48
00:02:20,949 --> 00:02:23,322
I mean, she has
absolutely no standards.

49
00:02:23,518 --> 00:02:25,292
This one time she was at...

50
00:02:26,044 --> 00:02:27,044
Where's Howard?

51
00:02:27,244 --> 00:02:30,080
Bonjour, mademoiselle.
I understand you're new in town.

52
00:02:31,912 --> 00:02:33,049
Oh, good grief.

53
00:02:51,704 --> 00:02:56,289
The Big Bang Theory
Season 1 Episode 07 - The Dumpling Paradox
Subtitles: swsub.com, Synchro: SerJo

54
00:03:03,787 --> 00:03:07,125
I cannot believe Christy
let Howard into my apartment.

55
00:03:08,478 --> 00:03:10,421
And I cannot believe people
pay for horoscopes,

56
00:03:10,541 --> 00:03:11,848
but on a more serious note,

57
00:03:11,998 --> 00:03:13,850
it's 8:13 and we're
still not playing <i>Halo</i>.

58
00:03:15,663 --> 00:03:18,150
Fine. We'll just play one-on-one
until he gets back.

59
00:03:18,351 --> 00:03:22,345
One-on-one? We don't play one-on-one.
We play teams, not one-on-one.

60
00:03:22,552 --> 00:03:23,651
One-on-one.

61
00:03:24,693 --> 00:03:28,678
The only way we can play teams
at this point is if we cut Raj in half.

62
00:03:29,022 --> 00:03:30,597
Sure, cut the foreigner in half.

63
00:03:30,751 --> 00:03:33,141
There's a billion more
where he came from.

64
00:03:33,793 --> 00:03:35,394
If you guys need a fourth, I'll play.

65
00:03:35,600 --> 00:03:37,372
- Great idea.
- No.

66
00:03:39,112 --> 00:03:40,962
The wheel was a great idea,

67
00:03:42,203 --> 00:03:43,612
relativity was a great idea.

68
00:03:43,747 --> 00:03:46,176
This is a notion,
and a rather sucky one at that.

69
00:03:47,244 --> 00:03:48,175
Why?

70
00:03:48,295 --> 00:03:49,142
Why?

71
00:03:49,354 --> 00:03:50,659
Oh, Penny, Penny, Penny.

72
00:03:50,910 --> 00:03:52,183
What, what, what?

73
00:03:53,085 --> 00:03:56,023
This is a complex battle simulation
with a steep learning curve.

74
00:03:56,351 --> 00:03:58,551
There are myriad weapons,
vehicles and strategies

75
00:03:58,686 --> 00:04:01,560
to master, not to mention
an extremely intricate backstory.

76
00:04:02,711 --> 00:04:05,261
Oh cool.
Whose head did I just blow off?

77
00:04:06,258 --> 00:04:07,258
Mine.

78
00:04:08,506 --> 00:04:10,974
Okay, I got this.
Lock and load, boys.

79
00:04:12,381 --> 00:04:14,140
It's the only way we can play teams.

80
00:04:14,358 --> 00:04:16,380
Yes, but whoever's her
partner will be hamstrung

81
00:04:16,515 --> 00:04:18,753
by her lack of experience,
and not to mention the fact...

82
00:04:19,023 --> 00:04:20,837
There goes your head again.

83
00:04:22,416 --> 00:04:25,413
It's not good sportsmanship
to shoot somebody who's just re-spawned.

84
00:04:25,533 --> 00:04:27,748
You need to to give them a chance to...
Now, come on!

85
00:04:28,498 --> 00:04:30,489
Raj, Raj!
She's got me cornered. Cover me!

86
00:04:30,757 --> 00:04:32,234
Cover this, suckers!

87
00:04:34,320 --> 00:04:37,517
- Penny, you are on fire!
- Yes, and so is Sheldon.

88
00:04:39,492 --> 00:04:42,281
Okay, that's it.
I don't know how, but she is cheating.

89
00:04:42,401 --> 00:04:45,775
No one can be that attractive
and this skilled at a video game.

90
00:04:46,191 --> 00:04:48,331
Wait, Sheldon, come back.
You forgot something.

91
00:04:48,701 --> 00:04:49,704
What?

92
00:04:50,012 --> 00:04:51,462
This plasma grenade.

93
00:04:54,470 --> 00:04:56,017
Look, it's raining you.

94
00:04:57,800 --> 00:05:00,790
You laugh now-- you just wait
until you need tech support.

95
00:05:02,782 --> 00:05:05,070
Gosh, he's kind of
a sore loser, isn't he?

96
00:05:05,248 --> 00:05:07,833
To be fair, he's also
a rather unpleasant winner.

97
00:05:08,994 --> 00:05:10,549
Well, it's been fun.

98
00:05:11,196 --> 00:05:12,599
We make such a good team.

99
00:05:12,767 --> 00:05:15,488
Maybe we could enter a couple
of <i>Halo</i> tournaments sometime.

100
00:05:16,199 --> 00:05:18,149
Or we could just have a life.

101
00:05:19,752 --> 00:05:21,902
I guess for you that's an option.

102
00:05:22,609 --> 00:05:23,999
- Good night.
- Good night.

103
00:05:24,142 --> 00:05:25,704
As usual, nice talking to you, Raj.

104
00:05:27,752 --> 00:05:29,632
What do you suppose
she meant by that?

105
00:05:30,847 --> 00:05:32,259
She's an enigma, Raj.

106
00:05:33,284 --> 00:05:35,867
And another thing-- there's
a certain ethic to the game, Penny,

107
00:05:35,987 --> 00:05:37,881
- a well- established...
- She's gone.

108
00:05:39,106 --> 00:05:40,726
She could have said good-bye.

109
00:05:42,042 --> 00:05:44,005
Okay, I have a problem.

110
00:05:44,450 --> 00:05:47,441
It's called carpal tunnel syndrome,
and quite frankly you deserve it.

111
00:05:49,456 --> 00:05:50,594
What's wrong?

112
00:05:51,231 --> 00:05:52,601
Howard and Christy are...

113
00:05:52,853 --> 00:05:55,206
kind of hooking up
in my bedroom.

114
00:05:57,344 --> 00:05:58,347
Are you sure?

115
00:05:58,519 --> 00:05:59,890
I grew up on a farm, okay?

116
00:06:00,010 --> 00:06:01,821
From what I heard,
they're either having sex

117
00:06:01,956 --> 00:06:03,791
or Howard's caught
in a milking machine.

118
00:06:06,712 --> 00:06:08,295
Do you mind if I stay here tonight?

119
00:06:08,626 --> 00:06:10,894
No, take the couch, or my bed.

120
00:06:11,079 --> 00:06:13,531
I just got new pillows.
Hypo-allergenic.

121
00:06:14,722 --> 00:06:15,918
The couch is good.

122
00:06:16,181 --> 00:06:18,284
Hold that thought.
Leonard, a moment.

123
00:06:22,547 --> 00:06:24,380
Let me guess.
You have a problem with this.

124
00:06:24,534 --> 00:06:27,064
- Where do I begin?
- It's up to you. Crazy person's choice.

125
00:06:28,531 --> 00:06:30,894
First, we don't have houseguests.

126
00:06:31,094 --> 00:06:33,852
Frankly, if I could afford
the rent, I'd ask you to leave.

127
00:06:34,857 --> 00:06:37,472
Your friendship means
a lot to me as well. What else?

128
00:06:37,973 --> 00:06:38,973
Well...

129
00:06:39,299 --> 00:06:42,037
our earthquake supplies.
We have a two-man, two-day kit.

130
00:06:42,227 --> 00:06:43,112
So?

131
00:06:43,250 --> 00:06:46,450
So if there's an earthquake
and the three of us are trapped here,

132
00:06:46,662 --> 00:06:49,283
we could be out of food
by tomorrow afternoon.

133
00:06:50,280 --> 00:06:52,489
I'm sorry, are you suggesting
that if we let Penny stay

134
00:06:52,624 --> 00:06:54,466
we might succumb to cannibalism?

135
00:06:54,851 --> 00:06:57,436
No one ever thinks it'll
happen until it does.

136
00:06:59,407 --> 00:07:01,714
If you promise not to
chew the flesh off our bones

137
00:07:01,869 --> 00:07:03,682
while we sleep, you can stay.

138
00:07:05,125 --> 00:07:08,125
- What?
- He's engaging in reductio ad absurdum.

139
00:07:09,810 --> 00:07:11,956
It's the logical fallacy
of extending someone's argument

140
00:07:12,076 --> 00:07:14,318
to ridiculous proportions
and then criticizing the result.

141
00:07:14,472 --> 00:07:16,176
And I do not appreciate it.

142
00:07:17,634 --> 00:07:19,698
I'll get you a blanket and a pillow.

143
00:07:20,364 --> 00:07:22,786
Okay, since I'm obviously
being ignored here,

144
00:07:23,486 --> 00:07:25,087
let's go over the morning schedule.

145
00:07:25,242 --> 00:07:27,207
I use the bathroom
from 7:00 to 7:20.

146
00:07:27,546 --> 00:07:29,803
Plan your ablutions and bodily
functions accordingly.

147
00:07:31,641 --> 00:07:33,631
How am I supposed to plan
my bodily functions?

148
00:07:33,898 --> 00:07:36,050
I suggest no liquids after 11:00 p.m.

149
00:07:37,262 --> 00:07:39,362
- Here you go.
- Thanks, Leonard.

150
00:07:41,314 --> 00:07:42,314
Wrong.

151
00:07:45,873 --> 00:07:46,895
I'm listening.

152
00:07:47,200 --> 00:07:48,854
Your head goes on the other end.

153
00:07:49,362 --> 00:07:51,936
- Why?
- It's culturally universal. A bed,

154
00:07:52,148 --> 00:07:53,316
even a temporary bed,

155
00:07:53,470 --> 00:07:56,057
is always oriented with the headboard
away from the door.

156
00:07:56,288 --> 00:08:00,432
It serves the ancient imperative
of protecting oneself against marauders.

157
00:08:04,076 --> 00:08:05,097
I'll risk it.

158
00:08:06,152 --> 00:08:07,529
Anything else should I know?

159
00:08:07,715 --> 00:08:09,427
Yes, if you use my toothbrush,

160
00:08:09,638 --> 00:08:11,201
I'll jump out that window.

161
00:08:13,098 --> 00:08:15,664
Please don't come to my funeral.
Have a good night.

162
00:08:18,463 --> 00:08:19,529
Sorry about that.

163
00:08:19,732 --> 00:08:21,410
- That's okay.
- FYI--

164
00:08:21,626 --> 00:08:25,050
his toothbrush is the red one in
the Plexiglas case under the UV light.

165
00:08:25,730 --> 00:08:26,730
Got it.

166
00:08:28,019 --> 00:08:29,621
- Sleep tight.
- Thanks

167
00:08:33,435 --> 00:08:35,499
Funny expression,
"sleep tight."

168
00:08:38,090 --> 00:08:40,176
It refers to the early
construction of beds

169
00:08:40,350 --> 00:08:42,680
which featured a mattress
suspended on interlocking ropes,

170
00:08:42,815 --> 00:08:44,416
which would occasionally...

171
00:08:44,745 --> 00:08:45,745
Sleep tight.

172
00:10:01,532 --> 00:10:02,709
What are you doing?

173
00:10:06,260 --> 00:10:08,226
Every Saturday
since we've lived in this apartment

174
00:10:08,420 --> 00:10:10,761
I have awakened at 6:15,
poured myself a bowl of cereal,

175
00:10:10,935 --> 00:10:12,651
added a quarter cup
of two percent milk,

176
00:10:12,860 --> 00:10:15,566
sat on this end of this couch,
turned on BBC America,

177
00:10:15,778 --> 00:10:17,302
and watched <i>Doctor Who</i>.

178
00:10:18,400 --> 00:10:19,867
Penny's still sleeping.

179
00:10:21,056 --> 00:10:23,139
Every Saturday
since we've lived in this apartment

180
00:10:23,293 --> 00:10:25,464
I have awakened at 6:15,
poured myself a bowl

181
00:10:25,593 --> 00:10:28,002
I know.
You have a TV in your room.

182
00:10:28,128 --> 00:10:30,103
Why don't you just have
breakfast in bed?

183
00:10:30,291 --> 00:10:33,769
Because I am neither an invalid,
nor a woman celebrating Mother's Day.

184
00:10:36,026 --> 00:10:37,129
What time is it?

185
00:10:37,669 --> 00:10:38,807
Almost 6:30.

186
00:10:39,098 --> 00:10:40,449
I slept all day?

187
00:10:41,747 --> 00:10:43,541
No, it's 6:30 in the morning.

188
00:10:44,495 --> 00:10:46,357
What the hell is your problem?

189
00:10:47,694 --> 00:10:50,383
Okay, this cereal has lost
all its molecular integrity.

190
00:10:50,604 --> 00:10:52,398
I now have a bowl
of shredded wheat paste.

191
00:10:54,506 --> 00:10:55,874
Hola, nerd-migos.

192
00:10:59,685 --> 00:11:01,633
Why do you people hate sleep?

193
00:11:02,779 --> 00:11:04,299
Are you wearing my robe?

194
00:11:06,490 --> 00:11:08,091
Yeah.
Sorry, I'll have it cleaned.

195
00:11:08,227 --> 00:11:09,774
That's okay, keep it.

196
00:11:11,434 --> 00:11:13,581
- Where's Christy?
- In the shower.

197
00:11:13,991 --> 00:11:16,125
By the way, where did you
get that loofah mitt?

198
00:11:16,318 --> 00:11:18,897
Yours reaches places
that mine just won't.

199
00:11:23,448 --> 00:11:24,898
You used my loofah?

200
00:11:25,603 --> 00:11:28,001
More precisely,
weused your loofah.

201
00:11:28,405 --> 00:11:30,333
I exfoliated her brains out.

202
00:11:33,033 --> 00:11:34,610
You can keep that, too.

203
00:11:35,477 --> 00:11:39,418
Then we'll probably need to talk
about your stuffed bear collection.

204
00:11:42,026 --> 00:11:43,182
In here, milady!

205
00:11:45,783 --> 00:11:47,843
Here's my little engine that could.

206
00:11:52,653 --> 00:11:55,350
There's one beloved
children's book I'll never read again.

207
00:11:56,153 --> 00:11:57,889
Hi. Christy.

208
00:11:59,171 --> 00:12:00,618
- I'm Sheldon.
- Right.

209
00:12:00,844 --> 00:12:02,186
You're Howard's entourage.

210
00:12:05,006 --> 00:12:06,588
So, Christy, what are your plans?

211
00:12:07,164 --> 00:12:10,135
Howard said he'd take
me shopping in Beverly Hills.

212
00:12:10,491 --> 00:12:13,281
Yeah, no, I meant plans to find
someplace to live.

213
00:12:13,417 --> 00:12:15,505
Other than with me.
Not that I don't love having you,

214
00:12:15,640 --> 00:12:17,165
but it's...
It's a little crowded.

215
00:12:17,319 --> 00:12:19,285
Penny, you're always
welcome to stay with us.

216
00:12:19,405 --> 00:12:22,151
Oh, terrific.
Now we're running a cute little B & B.

217
00:12:23,563 --> 00:12:25,598
Let me offer a little
outside-the-box thinking here.

218
00:12:25,718 --> 00:12:27,299
Why doesn't Christy stay with me?

219
00:12:27,498 --> 00:12:29,620
- You live with your mother.
- I do not.

220
00:12:29,793 --> 00:12:31,298
My mother lives with me.

221
00:12:32,557 --> 00:12:34,795
Then it's all settled.
Christy will stay with Howard,

222
00:12:34,949 --> 00:12:36,518
Penny can go back to her apartment,

223
00:12:36,653 --> 00:12:38,716
and I'll watch the last 24 minutes
of <i>Doctor Who</i>.

224
00:12:38,947 --> 00:12:41,947
Although at this point,
it's more like <i>Doctor Why Bother</i>?

225
00:12:43,350 --> 00:12:46,655
- Sheldon, you just can't dictate...
- No more talking! Everybody go!

226
00:12:47,858 --> 00:12:51,483
So what do you say?
Want to repair to Casa Wolowitz?

227
00:12:51,616 --> 00:12:53,931
What is that, like a Mexican deli?

228
00:12:54,394 --> 00:12:56,372
I'm sorry,
I should have mentioned this earlier.

229
00:12:56,545 --> 00:12:58,108
My last name is Wolowitz.

230
00:12:59,918 --> 00:13:01,056
That's so cool.

231
00:13:01,492 --> 00:13:02,590
My first Jew.

232
00:13:06,659 --> 00:13:09,937
I imagine there aren't very many
Kosher Cornhuskers.

233
00:13:12,359 --> 00:13:14,037
But you're still taking me shopping?

234
00:13:14,183 --> 00:13:15,224
Anything you want.

235
00:13:15,365 --> 00:13:17,324
Okay, I'll go pack my stuff.

236
00:13:20,231 --> 00:13:23,691
When they perfect human cloning,
I'm going to order 12 of those.

237
00:13:26,015 --> 00:13:27,703
Can't you see she's using you?

238
00:13:27,877 --> 00:13:30,988
Who cares? Last night,
she pulled off her blouse and I wept.

239
00:13:31,706 --> 00:13:34,238
I know her. Okay?
She'll have sex with anyone,

240
00:13:34,393 --> 00:13:36,410
as long as
they keep buying her things.

241
00:13:36,947 --> 00:13:38,425
- Really?
- Yeah.

242
00:13:42,557 --> 00:13:46,669
If you'll excuse me,
I have some Bar Mitzvah bonds to cash.

243
00:13:54,956 --> 00:13:57,806
I'm sorry, we cannot do this
without Wolowitz.

244
00:13:58,398 --> 00:14:00,964
We can't order Chinese
food without Wolowitz?

245
00:14:01,369 --> 00:14:03,970
Let me walk you through it.
Our standard order is:

246
00:14:04,227 --> 00:14:06,553
the steamed dumpling appetizer,
General Tso's chicken,

247
00:14:06,708 --> 00:14:08,536
beef with broccoli,
shrimp with lobster sauce

248
00:14:08,710 --> 00:14:10,148
and vegetable lo mein.

249
00:14:10,391 --> 00:14:11,949
Do you see the problem?

250
00:14:13,820 --> 00:14:15,170
I see a problem.

251
00:14:15,952 --> 00:14:19,349
Our entire order is predicated
on four dumplings and four entrees

252
00:14:19,600 --> 00:14:21,086
divided amongst
four people.

253
00:14:24,269 --> 00:14:26,742
- So, we'll just order three entrees.
- Fine.

254
00:14:26,934 --> 00:14:29,421
What do you want to eliminate?
And who gets the extra dumpling?

255
00:14:29,692 --> 00:14:31,062
We could cut it into thirds.

256
00:14:31,207 --> 00:14:33,538
Then it's no longer a dumpling.
Once you cut it open it is,

257
00:14:33,692 --> 00:14:35,469
at best, a very small open-faced
sandwich.

258
00:14:37,339 --> 00:14:38,458
Hi, fellas.

259
00:14:38,932 --> 00:14:42,462
Where's your annoying little friend
who thinks he speaks Mandarin?

260
00:14:43,346 --> 00:14:45,659
He's putting his needs ahead of
the collective good.

261
00:14:45,854 --> 00:14:48,111
Where he comes from,
that's punishable by death.

262
00:14:49,004 --> 00:14:50,736
I come from Sacramento.

263
00:14:55,312 --> 00:14:57,923
Can we get an order of dumplings
but with three instead of four?

264
00:14:58,110 --> 00:14:59,110
No substitutions.

265
00:15:00,015 --> 00:15:02,234
This isn't a substitution.
It's a reduction.

266
00:15:02,572 --> 00:15:04,122
Okay.
No reductions.

267
00:15:05,675 --> 00:15:08,207
Fine. Bring us three orders
of dumplings.

268
00:15:08,327 --> 00:15:10,575
- That's 12. We'll each have four.
- That works.

269
00:15:10,797 --> 00:15:13,321
If we fill up on dumplings,
we need to eliminate another entree.

270
00:15:13,586 --> 00:15:14,703
No eliminations.

271
00:15:16,541 --> 00:15:18,661
If we have extra,
we'll just take the leftovers home.

272
00:15:18,781 --> 00:15:22,265
And divide it how? I'm telling you,
we cannot do this without Wolowitz.

273
00:15:22,427 --> 00:15:24,091
Wolowitz is with his new girlfriend.

274
00:15:24,255 --> 00:15:26,741
If you had let me invite Penny,
then you would have had your fourth.

275
00:15:26,886 --> 00:15:28,496
Have you seen Penny
eat Chinese food?

276
00:15:28,649 --> 00:15:30,973
She uses a fork and she
double-dips her egg rolls.

277
00:15:31,798 --> 00:15:33,448
- We don't order egg rolls.
- Exactly,

278
00:15:33,621 --> 00:15:35,091
but we'd have to if she was here!

279
00:15:35,306 --> 00:15:36,695
Can we please
make a decision?

280
00:15:36,844 --> 00:15:38,717
Not only are there children
starving in India,

281
00:15:38,852 --> 00:15:40,492
there's an Indian starving right here.

282
00:15:42,803 --> 00:15:45,637
There's an idea. Why don't we
just go out for Indian food?

283
00:15:47,340 --> 00:15:48,515
You're nice boys.

284
00:15:48,751 --> 00:15:50,160
Tell you what I'm going to do.

285
00:15:50,333 --> 00:15:52,234
I'm going to bring you
the four dumplings.

286
00:15:52,354 --> 00:15:55,159
When I'm walking over to the
table, maybe I get bumped,

287
00:15:55,351 --> 00:15:58,161
one of the dumplings fall to the floor.
No one has to know.

288
00:16:00,111 --> 00:16:01,111
I'll know.

289
00:16:07,241 --> 00:16:09,606
- How about soup?
- Yeah, we can always divide soup.

290
00:16:09,819 --> 00:16:11,295
What about the wontons?

291
00:16:18,150 --> 00:16:19,326
Hey, guys, what's up?

292
00:16:19,539 --> 00:16:20,774
It's <i>Halo</i> night.

293
00:16:25,490 --> 00:16:26,490
So?

294
00:16:27,592 --> 00:16:30,580
With Wolowitz spending all of his time
with your friend Christy...

295
00:16:30,760 --> 00:16:34,368
She's not my friend. Friends do not get
their friends' Care Bears all sweaty.

296
00:16:35,700 --> 00:16:39,276
Right. Anyway, uh, with Wolowitz
occupied elsewhere,

297
00:16:39,450 --> 00:16:41,283
we had something
we wanted to ask you.

298
00:16:43,066 --> 00:16:44,066
Yes.

299
00:16:48,285 --> 00:16:50,100
We would very much appreciate it

300
00:16:50,220 --> 00:16:52,570
if you would be the fourth
member of our <i>Halo</i> team.

301
00:16:53,629 --> 00:16:57,237
I don't think I need to tell
you what an honor this is.

302
00:16:59,634 --> 00:17:02,761
That's so sweet, but I'm going out
dancing with a girlfriend.

303
00:17:03,193 --> 00:17:05,343
You can't go out;
it's <i>Halo</i> night.

304
00:17:06,282 --> 00:17:09,583
- Well, for Penny, it's dancing night.
- You go dancing every Wednesday?

305
00:17:09,823 --> 00:17:11,887
- No.
- Then that's not "dancing night."

306
00:17:13,738 --> 00:17:15,448
Why don't I play
with you guys tomorrow?

307
00:17:15,615 --> 00:17:18,711
Tonight is <i>Halo</i> night.
It's like talking to a wall.

308
00:17:20,027 --> 00:17:22,573
All right, now, Sheldon,
you and I are about to have a problem.

309
00:17:25,603 --> 00:17:27,622
Sheldon, remember,
we role-played this.

310
00:17:27,794 --> 00:17:30,321
Yes, but you didn't portray her
as completely irrational.

311
00:17:31,119 --> 00:17:33,147
All right, fellas,
I gotta go.

312
00:17:33,289 --> 00:17:34,426
But good luck.

313
00:17:37,932 --> 00:17:41,404
Maybe we should've asked if we could go
dancing with her and her girlfriend.

314
00:17:41,648 --> 00:17:43,704
Assuming we could dance--
which we can't--

315
00:17:43,878 --> 00:17:46,010
there are three of us
and two of them.

316
00:17:46,165 --> 00:17:46,994
So?

317
00:17:47,149 --> 00:17:49,660
It's the Chinese restaurant
all over again.

318
00:17:50,510 --> 00:17:52,578
I assure you that
cutting a dumpling in thirds

319
00:17:52,752 --> 00:17:54,575
is child's play compared with three men

320
00:17:54,710 --> 00:17:57,035
each attempting to dance
with 67% of a woman.

321
00:17:59,223 --> 00:18:02,492
For God's sakes, Sheldon,
you're driving me crazy!

322
00:18:02,769 --> 00:18:05,094
Your anger is not with me, sir,
but with basic mathematics.

323
00:18:05,248 --> 00:18:06,868
I'm pretty sure my anger is with you.

324
00:18:07,022 --> 00:18:09,690
What's happening to us?
We're falling apart.

325
00:18:11,667 --> 00:18:13,914
- Who are you calling?
- The only man who can restore

326
00:18:14,068 --> 00:18:16,443
any semblance of
balance to our universe.

327
00:18:16,953 --> 00:18:18,528
<i>Hi, this is Howard Wolowitz.</i>

328
00:18:18,648 --> 00:18:20,235
<i>And this is Christy Vanderbelt.</i>

329
00:18:20,355 --> 00:18:23,583
<i>We can't get to the phone
right now because we're having sex!</i>

330
00:18:26,064 --> 00:18:28,292
<i>You're not going to put that
on your message, are you?</i>

331
00:18:28,453 --> 00:18:30,964
<i>Nah, I'm just kidding.
I'll re-record it.</i>

332
00:18:34,370 --> 00:18:37,346
Sheldon, think this through.
You're going to ask Howard to choose

333
00:18:37,501 --> 00:18:38,726
between sex and <i>Halo</i>.

334
00:18:38,957 --> 00:18:42,513
No, I'm going to ask him to choose
between sex and <i>Halo Three</i>.

335
00:18:43,079 --> 00:18:45,105
As far as I know, sex
has not been upgraded

336
00:18:45,259 --> 00:18:48,327
to include Hi-Def graphics
and enhanced weapon systems.

337
00:18:48,592 --> 00:18:52,226
You're right, all sex has is nudity,
orgasms and human contact.

338
00:18:52,859 --> 00:18:53,859
My point.

339
00:18:55,885 --> 00:18:58,875
You can take the damn plastic
off the couch once in a while!

340
00:18:59,016 --> 00:19:01,090
Why, so you and Howard
can hump on it?!

341
00:19:02,974 --> 00:19:05,227
Ladies, ladies,
I'm sure there's a middle ground.

342
00:19:05,436 --> 00:19:06,652
Shut up, Howard!

343
00:19:08,146 --> 00:19:11,082
You guys talk. I'm gonna take
my scooter out for a little spin.

344
00:19:11,275 --> 00:19:13,623
You happy? You drove
your own son out of the house.

345
00:19:13,816 --> 00:19:15,205
Why don't you stop...

346
00:19:15,359 --> 00:19:16,678
What are you doing here?

347
00:19:16,912 --> 00:19:18,282
It's <i>Halo</i> night.

348
00:19:18,936 --> 00:19:20,421
He's not a man, he's a putz!

349
00:19:20,600 --> 00:19:22,984
And don't you take that tone with me,
you gold digger!

350
00:19:23,104 --> 00:19:24,786
- What'd you call me?
- You heard me!

351
00:19:24,981 --> 00:19:27,964
And I'll tell you something else,
you're barking up the wrong tree.

352
00:19:28,176 --> 00:19:31,160
'Cause as long as you're around,
Howard is out of the will!

353
00:19:31,391 --> 00:19:34,728
You know what?
I got better offers. I'm outta here.

354
00:19:34,893 --> 00:19:37,767
That's right.
Go back to Babylon, you whore!

355
00:19:41,833 --> 00:19:43,183
So, <i>Halo</i> night, huh?

356
00:19:45,838 --> 00:19:47,903
I thought she was the whore of Omaha.

357
00:19:55,080 --> 00:19:58,282
Sheldon, you got him in your sights!
Fire! He's charging his plasma rifle!

358
00:19:58,627 --> 00:20:00,183
I can't shoot now. I'm cloaking!

359
00:20:00,414 --> 00:20:02,462
- Raj! Kill Sheldon!
- I can't see him!

360
00:20:02,887 --> 00:20:04,963
That's why they call it
cloaking, dead man!

361
00:20:05,450 --> 00:20:07,800
- Start throwing grenades!
- I'm all out!

362
00:20:08,808 --> 00:20:10,914
Hey, guys.
My friends and I got tired of dancing,

363
00:20:11,088 --> 00:20:13,271
so we came over to
have sex with you.

364
00:20:13,714 --> 00:20:15,096
Raj, hop in the tank!

365
00:20:15,216 --> 00:20:17,873
- We said no tanks!
- There are no rules in hell!

366
00:20:18,901 --> 00:20:21,438
Son of a bitch. Med pack!
I need a med pack!

367
00:20:23,384 --> 00:20:24,384
Told ya.

368
00:20:25,424 --> 00:20:27,198
There's a sniper.
Use your rocket launcher!

369
00:20:27,429 --> 00:20:29,810
All I've got is a Needler
and I'm all out of ammo!

370
00:20:29,976 --> 00:20:31,789
And now you're out of life.

371
00:20:34,094 --> 00:20:35,232
Why'd you hit pause?

372
00:20:35,737 --> 00:20:37,920
I thought I heard... something.

373
00:20:38,278 --> 00:20:39,281
What?

374
00:20:39,531 --> 00:20:39,281
No, never mind.
Sorry. Go.

