1
00:00:02,116 --> 00:00:02,866
Так...

2
00:00:04,734 --> 00:00:06,462
X10 подключен.
X10 — коммуникационный протокол, использующийся в системах домашней автоматизации. Благодаря нему возможно управление бытовыми приборами с помощью компьютера.

3
00:00:06,849 --> 00:00:12,807
Джентельмены, сейчас через нашего местного интернет-провайдера
с помощью этого лэптопа я собираюсь отправить сигнал,
Лэптоп — переносной ПК с плоским ЖК. Промежуточный класс между портативными и блокнотными ПК.

4
00:00:12,927 --> 00:00:16,960
который пронесётся по оптоволоконной линии
со скоростью света до Сан-Франциско,

5
00:00:17,080 --> 00:00:20,133
отражаясь от спутника португальской
геостационарной орбиты в Лиссабоне,

6
00:00:20,253 --> 00:00:23,073
где пакет данных будет остановлен из-за погружения
трансантлантических кабелей

7
00:00:23,193 --> 00:00:24,879
замыкающихся в шотландском Галифаксе.

8
00:00:24,971 --> 00:00:28,284
И доставлен через континент посредством
радиорелейной линии обратно к нашему провайдеру.

9
00:00:28,404 --> 00:00:31,611
И в итоге включается эта...

10
00:00:32,596 --> 00:00:33,456
Лампа.

11
00:00:36,435 --> 00:00:38,482
Посмотри на меня, посмотри! У меня мурашки по коже пробежали.

12
00:00:38,602 --> 00:00:41,387
— Вы готовы заценить стерео?
— Да, включай.

13
00:01:03,146 --> 00:01:05,648
— Привет, ребята.
— Привет.

14
00:01:06,018 --> 00:01:07,353
У вас немного громко.

15
00:01:07,473 --> 00:01:09,382
Без проблем...
Сделаю потише.

16
00:01:10,022 --> 00:01:13,882
Сан-Франциско, Лиссабон, Галифакс...
...Вуаля!

17
00:01:15,073 --> 00:01:18,868
— Да, спасибо.
— Погоди, погоди. Разве ты не понимаешь, что мы сейчас сделали?

18
00:01:19,125 --> 00:01:21,257
Да, вы выключили стерео лэптопом.

19
00:01:21,581 --> 00:01:26,147
Нет, мы это сделали, отправив сигнал через весь мир по Интернету.

20
00:01:27,471 --> 00:01:29,853
Дак в РадиоШаке продаются универсальные пульты же.

21
00:01:29,882 --> 00:01:31,334
Совсем дешевые.

22
00:01:32,684 --> 00:01:34,360
Нет, ты не поняла.
Говард,

23
00:01:34,480 --> 00:01:37,618
— Открой общий доступ.
— Общий доступ открыт.

24
00:01:41,530 --> 00:01:44,662
— Да, потрясающе. Увидимся.
— Нет, погоди, погоди...

25
00:01:47,165 --> 00:01:48,047
Понимаешь?

26
00:01:49,680 --> 00:01:50,319
Нет.

27
00:01:51,201 --> 00:01:55,017
Кто-то в Сычуане — китайской провинции
использует его компьютер,

28
00:01:55,137 --> 00:01:56,649
чтобы включать и выключать у нас свет.

29
00:01:58,017 --> 00:01:59,627
Да, удобно.

30
00:02:00,995 --> 00:02:02,980
Но такой вопрос...
А зачем?

31
00:02:04,679 --> 00:02:06,553
Потому что это в наших силах.

32
00:02:07,922 --> 00:02:10,238
Они нашли наши радиоуправляемые машинки!

33
00:02:12,752 --> 00:02:16,061
— А что это сверху них?
— Беспроводные веб-камеры, скажи «привет»!

34
00:02:17,032 --> 00:02:19,789
Грузовой Монстр управляется из Техаса,

35
00:02:19,909 --> 00:02:23,478
А голубой Вайпер кем-то из пригорода Тель Авива.

36
00:02:26,032 --> 00:02:28,657
— Ты могла бы надеть и штаны.
— Что?

37
00:02:31,392 --> 00:02:34,569
Эй, остановите их! Нет!
Оставьте меня в покое!

38
00:02:38,054 --> 00:02:40,745
— Кто управляет красным Корветом?
— Наверное, это я.

39
00:02:47,047 --> 00:02:51,347
Теория Большого Взрыва
Сезон 1 Эпизод 09 — The Cooper-Hofstadter Polarization

40
00:03:05,860 --> 00:03:09,257
Знаете, в будущем, когда поместят в банки
наши мозги, освобожденные от остального тела.

41
00:03:09,377 --> 00:03:12,081
эти восемь часов нам покажутся зря потраченным временем.

42
00:03:14,000 --> 00:03:17,287
Я не хочу быть в банке.
Я хочу, чтобы мой мозг поместили в тело андроида.

43
00:03:18,632 --> 00:03:20,397
Робота-трансформера высотой 8 футов...

44
00:03:21,698 --> 00:03:22,801
Я тоже.

45
00:03:22,921 --> 00:03:26,331
Я бы согласился, если бы был уверен, что
будучи андроидом, я все еще буду евреем.

46
00:03:28,493 --> 00:03:29,574
Я обещал своей маме.

47
00:03:31,846 --> 00:03:35,375
Полагаю, ты мог бы сделать обрезание.

48
00:03:37,426 --> 00:03:40,635
Но раввин об этом должен был бы договориться с изготовителем твоего робота.

49
00:03:41,154 --> 00:03:43,933
Не говоря уже о том, что по субботам
ты будешь вынужден отключаться.

50
00:03:45,963 --> 00:03:48,281
Шелдон, почему это письмо находится в мусорной корзине?

51
00:03:48,401 --> 00:03:49,978
Всегда есть вероятность, что

52
00:03:50,079 --> 00:03:52,979
мусорная корзина самопроизвольно образовалась вокруг этого письма,

53
00:03:53,662 --> 00:03:56,441
но Бритва Оккамы скорее свидетельствует о том,
что кто-то просто его выкинул.
Бритва Оккама — принцип, отдающий предпочтение более простым теориям, чем сложным, при условии, что возможны и те, и другие.

54
00:03:57,522 --> 00:04:00,632
Это из института Экспериментальной Физики, они приглашают нас

55
00:04:00,691 --> 00:04:02,884
представить презентацию о свойствах сверхплотных веществ

56
00:04:02,913 --> 00:04:05,873
на местной конференции, посвященной конденсатам Бозе-Эйнштейна.

57
00:04:05,993 --> 00:04:08,132
Я знаю.
Я прочитал это перед тем как выкинуть.

58
00:04:09,831 --> 00:04:12,059
Хорошо, постараюсь раскрыть суть моего вопроса:

59
00:04:12,179 --> 00:04:13,471
Зачем ты выкинул это письмо?

60
00:04:13,591 --> 00:04:17,353
Потому что мне неинтересно находиться
в Роуз Рум Пасадена Марриот

61
00:04:17,473 --> 00:04:19,647
перед аудиторией незнакомцев, готовых критиковать все и вся,

62
00:04:19,767 --> 00:04:23,110
которые даже не поймут ,
что перед ними с докладом выступает гений.

63
00:04:24,169 --> 00:04:25,735
Которым и был бы я, если бы я там был.

64
00:04:27,049 --> 00:04:28,186
Я не знаю, Шелдон.

65
00:04:28,187 --> 00:04:31,561
Эти местные конференции, посвященные
конденсатам Бозе-Эйнштейна знамениты своими вечеринками.

66
00:04:32,399 --> 00:04:35,818
— Забудь о вечеринках!
— Забудь о вечеринках? Что ты за ботан!

67
00:04:37,870 --> 00:04:40,495
Получал ли я еще какие-нибудь награды, о которых мне неизвестно?

68
00:04:40,615 --> 00:04:43,429
Не доставляла ли экспресс-почта Нобелевскую премию на мое имя?

69
00:04:43,700 --> 00:04:46,693
Леонард, пожалуйста, не шути так.
В день, когда ты получишь Нобелевскую премию,

70
00:04:46,813 --> 00:04:50,893
я начну исследовать коэффициент трения кисточек у ковров-самолетов.

71
00:04:53,210 --> 00:04:55,747
Только упоминание о твоей маме недостает этому оскорблению.

72
00:04:57,533 --> 00:04:58,526
Я добавлю...
Эй, Леонард,

73
00:04:58,646 --> 00:05:01,950
— Методология исследований твоей мамы совсем некорректны...
— Заткнись, Говард.

74
00:05:03,243 --> 00:05:05,572
— Шелдон, мы должны выступить там.
— Нет, не должны.

75
00:05:05,627 --> 00:05:07,970
Мы должны чем-то питаться, выделять отходы,

76
00:05:08,039 --> 00:05:10,629
и получать немного кислорода, чтобы не дать умереть нашим клеткам раньше времени.

77
00:05:10,711 --> 00:05:12,136
Всё остальное необязательно.

78
00:05:13,181 --> 00:05:15,209
Хорошо, тогда позволь мне сказать.
Я выступлю там.

79
00:05:15,291 --> 00:05:17,539
Ты не можешь. Я главный автор.

80
00:05:17,731 --> 00:05:21,389
Да, ладно, единственная причина, по которой ты главный автор,
это то, что мы написали наши фамилии в алфавитном порядке.

81
00:05:21,413 --> 00:05:23,939
Чесно говоря, я сказал тебе написать фамилии в алфавитном порядке,

82
00:05:23,967 --> 00:05:26,361
чтобы обойтись без унижений, принимая в факт то,

83
00:05:26,391 --> 00:05:29,488
что идея нашей работы была моей.
Но я все же пригласил тебя поучаствовать.

84
00:05:30,365 --> 00:05:31,365
Не стоит благодарностей.

85
00:05:31,886 --> 00:05:35,339
Прошу прощения, но я придумал эксперимент, который подтвердил гипотезу.

86
00:05:35,462 --> 00:05:37,216
Но это не значит, что ты ее доказал.

87
00:05:37,504 --> 00:05:40,889
И ты думаешь, что главное научное сообщество станет тебя слушать?

88
00:05:40,944 --> 00:05:42,903
Может быть, они и не станут, но они будут обязаны.

89
00:05:43,986 --> 00:05:45,616
Хорошо, мне все равно, что ты скажешь.

90
00:05:45,671 --> 00:05:47,987
Я пойду на конференцию и представлю нашу работу.

91
00:05:48,042 --> 00:05:49,919
Я не позволю.

92
00:05:50,673 --> 00:05:51,906
Не позволишь?

93
00:05:51,961 --> 00:05:54,537
Если я не принимаю заслуг нашей работы, то никто другой и тем более не имеет на это права.

94
00:05:54,592 --> 00:05:56,826
— Так, ты согласен, что это наша работа?
— Нет.

95
00:05:56,894 --> 00:05:58,963
Еще раз. Я просто пригласил тебя поучаствовать.

96
00:05:59,772 --> 00:06:01,485
И еще раз. Не стоит благодарностей.

97
00:06:10,638 --> 00:06:13,680
Ну, как у вас дела с Шелдоном?
Вы до сих пор не разговариваете друг с другом?

98
00:06:13,804 --> 00:06:15,448
Он не только не разговаривает со мной,

99
00:06:15,503 --> 00:06:18,819
но еще так странно на меня смотрит, пытаясь взорвать мой мозг.

100
00:06:21,107 --> 00:06:24,287
Ну, ты знаешь, как в классическом фантастическом фильме «Сканнеры»?

101
00:06:24,985 --> 00:06:26,506
Там.... Бззз..

102
00:06:28,329 --> 00:06:29,535
А, неважно.

103
00:06:29,727 --> 00:06:31,234
Как насчет этого?

104
00:06:31,700 --> 00:06:35,276
Он как будто говорит: «Да, я знаю свою физику, но я все еще умею веселиться.»

105
00:06:35,948 --> 00:06:39,086
Хм, а я и не знала, что до сих пор выпускают вильветовые костюмы.

106
00:06:39,154 --> 00:06:41,374
Уже нет, но этот я сохранил.

107
00:06:42,210 --> 00:06:45,348
Хорошо, посмотрим, что еще у тебя есть.

108
00:06:45,705 --> 00:06:47,609
Так, держи это.

109
00:06:47,774 --> 00:06:51,665
И это, и это, и эти...

110
00:06:51,761 --> 00:06:53,447
Я должен все это примерять?

111
00:06:53,474 --> 00:06:55,283
Нет, от этого тебе лучше избавиться.

112
00:06:56,434 --> 00:07:00,049
Я серьезно, даже не отдавай это на благотворительность,
этим ты никому не поможешь.

113
00:07:00,901 --> 00:07:02,450
А это что?

114
00:07:02,792 --> 00:07:05,273
О, это город Кэндор в бутылке.

115
00:07:08,137 --> 00:07:12,152
Понимаешь, Кэндор — столица планеты Криптон.

116
00:07:12,302 --> 00:07:14,978
Он был уменьшен Брайниаком, перед тем как взорвался Криптон.

117
00:07:15,015 --> 00:07:16,875
И после спасен Суперменом.

118
00:07:18,030 --> 00:07:20,058
А, мило.

119
00:07:21,223 --> 00:07:23,950
Он был был гораздо круче, если бы девушки не смотрели на него.

120
00:07:24,891 --> 00:07:28,116
Ладно, примерь эти брюки, а я пока поищу
рубашку и еще как-нибудь неспортивные вещи.

121
00:07:28,143 --> 00:07:29,157
Хорошо, я скоро вернусь.

122
00:07:29,198 --> 00:07:30,939
Куда ты уходишь? Просто надень их здесь.

123
00:07:30,980 --> 00:07:31,953
Здесь?

124
00:07:32,021 --> 00:07:33,761
А ты что, стесняешься?

125
00:07:33,926 --> 00:07:36,434
— Нет, не стесняюсь.
— Не беспокойся, я не смотрю.

126
00:07:36,598 --> 00:07:38,283
Я знаю, что ты не смотришь.
Зачем тебе смотреть.

127
00:07:38,332 --> 00:07:39,805
Тут не на что смотреть...
В смысле, не совсем ничего...

128
00:07:39,847 --> 00:07:42,298
Солнышко, просто надень эти штаны.

129
00:07:44,066 --> 00:07:46,752
Ну, знаешь, может быть вы с Шелдоном найдете компромисс

130
00:07:46,793 --> 00:07:50,233
по поводу вашей совместной презентации?
— Нет, нет..

131
00:07:50,370 --> 00:07:52,055
Ученые не могу искать компромисс.

132
00:07:52,083 --> 00:07:54,172
Наши мозги предназначены для синтезирования фактов

133
00:07:54,210 --> 00:07:56,097
и вывода умозаключений.

134
00:07:56,358 --> 00:07:59,263
Не говоря уже о том, что Шелдон просто чертов псих.

135
00:08:02,374 --> 00:08:03,949
Что это такое?

136
00:08:04,086 --> 00:08:06,046
А, аккуратнее.

137
00:08:06,498 --> 00:08:09,998
Это оригинальный скафандр из сериала Звёздный крейсер «Галактика».

138
00:08:10,458 --> 00:08:12,692
А, почему ты тогда не одел его на хэллуин?

139
00:08:13,062 --> 00:08:15,063
Потому что это не костюм, а скафандр.

140
00:08:18,379 --> 00:08:20,475
Ладно, посмотрим...

141
00:08:20,571 --> 00:08:22,805
Ух-ты. Пейсли-рубашка.

142
00:08:22,846 --> 00:08:25,518
Она хорошо сочетается с моим вильветовым костюмом.

143
00:08:25,861 --> 00:08:28,766
Если ты имеешь в виду, что и ее надо выкинуть, то я согласна.

144
00:08:30,164 --> 00:08:32,315
У тебя только один галстук?

145
00:08:33,356 --> 00:08:36,234
Технически, да. Но, если ты присмотришься..

146
00:08:37,084 --> 00:08:38,934
Он двусторонний!

147
00:08:41,099 --> 00:08:42,784
Он ведет себя как два галстука.

148
00:08:42,853 --> 00:08:45,758
Солнышко, я даже и не думала, что он ведет себя как один.

149
00:08:47,564 --> 00:08:49,226
— Так это вся твоя одежда?
— Ну, да.

150
00:08:49,252 --> 00:08:51,143
Вся, что осталась с восьмого класса.

151
00:08:51,266 --> 00:08:54,089
— Восьмого класса?
— Последняя стадия физического роста.

152
00:08:56,213 --> 00:08:59,283
Ладно, пусть будет вельветовый костюм.

153
00:08:59,324 --> 00:09:01,270
— Отлично.
— Да уж.

154
00:09:02,941 --> 00:09:04,788
Нет! Это убери.

155
00:09:10,697 --> 00:09:12,725
Привет, Шелдон.

156
00:09:12,986 --> 00:09:14,246
Привет, Пэнни.

157
00:09:15,754 --> 00:09:17,672
Получил что-нибудь хорошее?

158
00:09:19,317 --> 00:09:22,770
Только последний номер трехмесячного
издания  «Физики Элементарных Частиц».

159
00:09:22,838 --> 00:09:26,086
О, так странно, что твой пришел, а мой нет.

160
00:09:31,869 --> 00:09:33,445
Это была шутка.

161
00:09:37,706 --> 00:09:39,597
Да.

162
00:09:39,803 --> 00:09:42,256
Если что, не ходи к другим официанткам.
Я здесь всю неделю.

163
00:09:43,270 --> 00:09:45,407
Пэнни, просто чтобы предостеречь тебя от дальнейших неловкостей,

164
00:09:45,476 --> 00:09:48,929
я скажу, что мне будет гораздо удобнее, если
подниматься по ступенькам мы будем в тишине.

165
00:09:49,176 --> 00:09:51,711
А, да, мне тоже. Рот на замок, ключ в огород.

166
00:09:54,451 --> 00:09:55,644
Положи его в свой карман.

167
00:10:00,042 --> 00:10:02,550
— Так, вы с Леонардом...
— О, боже мой...

168
00:10:03,251 --> 00:10:04,851
Небольшое недопонимание, да?

169
00:10:05,378 --> 00:10:06,700
Небольшое недопонимание...?

170
00:10:06,940 --> 00:10:10,065
У Галилео и Папы тоже было небольшое недопонимание.

171
00:10:13,729 --> 00:10:17,554
Ну, как бы то ни было, этим утром я беседовала с Леонардом,
и я думаю, что после вашей ссоры он себя нехорошо чувствует.

172
00:10:19,926 --> 00:10:21,273
А ты как?

173
00:10:22,638 --> 00:10:24,089
Я не понял вопроса.

174
00:10:25,539 --> 00:10:28,847
Я просто спрашиваю, разве это не тяжело
ссориться со своим лучшим другом?

175
00:10:30,286 --> 00:10:32,157
Я не думал об этом в таком ключе.

176
00:10:32,437 --> 00:10:35,327
Интересно, связано ли это с тем, что я испытываю психологические проявления

177
00:10:35,447 --> 00:10:37,990
Невольных эмоциональных расстройств.

178
00:10:39,386 --> 00:10:40,415
Подожди, ты о чем?

179
00:10:40,630 --> 00:10:42,378
Я не смог сходить в туалет сегодня утром.

180
00:10:46,909 --> 00:10:49,839
Ты должен просто поговорить с ним
Я уверена, вы сможете решить эту проблему.

181
00:10:50,218 --> 00:10:52,996
— Конечно, но мой план предпочтительнее.
— Какой же?

182
00:10:53,386 --> 00:10:54,806
Мощное слабительное.

183
00:10:57,025 --> 00:10:59,187
Ты определенно должен с ним поговорить.

184
00:10:59,360 --> 00:11:01,778
Слушай, я знаю, Леонард много для тебя значит как друг, и он сам сказал мне

185
00:11:01,986 --> 00:11:05,458
Что без твоей небольшой помощи, он никак бы не устроил весь этот эксперимент.

186
00:11:09,792 --> 00:11:11,509
Прошу прощения. Небольшой помощи?

187
00:11:11,754 --> 00:11:13,521
Да, я имею в виду, он пытался объяснить мне...

188
00:11:13,641 --> 00:11:15,300
— На самом деле я не поняла, но...
— Конечно, ты не поняла.

189
00:11:15,420 --> 00:11:16,970
Он сказал: «Небольшая помощь»?

190
00:11:18,518 --> 00:11:20,224
Ну, да, нет...
Нет...

191
00:11:20,408 --> 00:11:22,734
— Не с такими словами.
— А что именно тогда он сказал?

192
00:11:23,192 --> 00:11:26,153
Ну, ты знаешь... Так... Если честно...
Он вложил больше души в свои слова...

193
00:11:26,273 --> 00:11:28,694
— И что же он сказал?
— Что у тебя была просто удачная догадка.

194
00:11:30,547 --> 00:11:32,691
Шелдон, я тут думаю, вместо споров об этой...

195
00:11:32,811 --> 00:11:34,788
Никогда со мной больше не разговаривай.

196
00:11:37,360 --> 00:11:38,371
Что...?

197
00:11:54,033 --> 00:11:56,061
Ладно, я на конференцию.

198
00:11:56,428 --> 00:11:58,761
Веселой презентации моя «удачная догадка».

199
00:11:58,881 --> 00:12:00,595
Шел, я не имел это в виду.

200
00:12:00,715 --> 00:12:02,381
— Тогда почему ты сказал это?
— Я не знаю, я не...

201
00:12:02,501 --> 00:12:06,250
— Ты пытался произвести впечатление на Пэнни?
— Нет, нет, не совсем. Немного, да.

202
00:12:07,524 --> 00:12:09,297
Ну, и как это сработало для тебя?

203
00:12:09,585 --> 00:12:13,105
— Леонард, ты готов?
— Либидо: 1, правда: 0.

204
00:12:15,965 --> 00:12:17,238
Ладно, я спрошу тебя еще раз.

205
00:12:17,358 --> 00:12:19,888
Мы работали вместе, давай и представим нашу презентацию вместе.

206
00:12:20,008 --> 00:12:21,820
Говорю тебе в последний раз,

207
00:12:21,940 --> 00:12:24,770
Это потакание, это обесчестивание, и это задевает меня.

208
00:12:27,159 --> 00:12:28,164
Пошли.

209
00:12:28,402 --> 00:12:30,284
— Пока Шелдон.
— Пока Пенни.

210
00:12:41,503 --> 00:12:43,062
Когда-нибудь... бшшшшш!

211
00:12:46,623 --> 00:12:47,628
Ну вот.

212
00:12:47,927 --> 00:12:49,413
Ты права, эта сторона выглядит лучше.

213
00:12:49,670 --> 00:12:52,436
Нет, я не говорила «лучше», я сказала «менее позорно».

214
00:12:54,550 --> 00:12:57,425
Я сейчас проверил.
Там где-то 20-25 человек.

215
00:12:57,545 --> 00:12:59,041
— Да ну!
— Всего-то?

216
00:12:59,231 --> 00:13:02,570
«Всего-то?»
Для физике элементарных частит это Вудсток!

217
00:13:05,903 --> 00:13:07,950
— Ну и хорошо!
— Не знал, что будет такая толпа,

218
00:13:08,070 --> 00:13:09,072
Что-то я занервничал.

219
00:13:09,192 --> 00:13:11,368
Всё ок, просто начни доклад с шутки.

220
00:13:11,686 --> 00:13:13,587
A шутка... Хорошо.

221
00:13:15,001 --> 00:13:16,500
Может эта?...

222
00:13:16,620 --> 00:13:18,066
Жил-был фермер,

223
00:13:18,335 --> 00:13:22,117
были у него курочки, которые никак не хотели нести яиц.
И поэтому

224
00:13:22,429 --> 00:13:24,238
он позвал физика, чтобы тот ему помог.

225
00:13:24,459 --> 00:13:27,181
Физик что-то измерил, посчитал,

226
00:13:27,433 --> 00:13:30,771
и говорит: «У меня есть решение проблемы,

227
00:13:31,138 --> 00:13:35,349
но оно действительно только для сферических цыплят в вакууме!»

228
00:13:37,910 --> 00:13:38,915
Ы?

229
00:13:43,761 --> 00:13:45,850
Оу, простите, я просто слышала уже эту шутку.

230
00:13:46,557 --> 00:13:49,603
— Поехали. Хэй, крутой костюмчик!
— Классика, а? ;)

231
00:13:51,087 --> 00:13:53,488
Похоже мне пора покупать собственную тачку..

232
00:13:56,102 --> 00:13:57,692
Итак, в заключении,

233
00:13:57,812 --> 00:14:00,592
график показывает, что температура приближается к абсолютному нулю,

234
00:14:00,712 --> 00:14:04,211
момент инерции меняется и плотное тело становится сверхплотным,

235
00:14:04,331 --> 00:14:08,352
что явно становится фактом, неизвестным до этого.

236
00:14:11,254 --> 00:14:12,344
Спасибо!

237
00:14:17,510 --> 00:14:19,557
— Вопросы есть?
— Да.

238
00:14:19,790 --> 00:14:21,551
Что за нафег это было?

239
00:14:24,192 --> 00:14:25,282
Ещё вопросы?

240
00:14:25,886 --> 00:14:30,438
Я — доктор Шелдон Купер и я
главный автор этой особой статьи

241
00:14:32,126 --> 00:14:33,161
Спасибо.

242
00:14:35,173 --> 00:14:39,566
А вы, сэр, вы полностью пропустили
ту часть, где я шёл через парк

243
00:14:39,829 --> 00:14:42,956
и я увидел тех детишек
на карусели, которые заставили меня задуматься

244
00:14:43,076 --> 00:14:45,873
насчёт моментов инерции в газах типа гелия

245
00:14:45,993 --> 00:14:47,931
при температурах, стремящихся к нулю.

246
00:14:48,051 --> 00:14:50,853
Я не пропускал этого. Это всего
лишь анекдот, это не наука!

247
00:14:50,973 --> 00:14:53,967
О, ну да. Яблоко, упавшее
на голову Ньютону

248
00:14:54,087 --> 00:14:56,655
— это просто анекдот?
— Ты не Исаак Ньютон.

249
00:14:56,775 --> 00:15:00,092
Да, верно. Гравитация была бы для меня очевидна и без яблока.

250
00:15:00,884 --> 00:15:02,582
Не могу поверить, что ты так эгоистичен.

251
00:15:02,702 --> 00:15:05,061
Вы продолжаете недооценивать меня.

252
00:15:05,656 --> 00:15:09,530
Смотри, если ты недоволен моей презентацией,
так может быть нам следовало представлять её вместе!

253
00:15:09,543 --> 00:15:11,576
Как я неоднократно объяснял,
в отличие от тебя,

254
00:15:11,591 --> 00:15:14,039
мне не нужно признание низших умов.

255
00:15:14,069 --> 00:15:14,887
Без обид.

256
00:15:15,450 --> 00:15:16,659
Действительно? Тогда зачем ты пришёл?

257
00:15:16,674 --> 00:15:18,115
Потому что я знал что ты напортачишь.

258
00:15:18,137 --> 00:15:19,470
Я не напортачил!

259
00:15:19,483 --> 00:15:21,898
Ой, да ладно, допустим эта «сферо-цыплятная» шутка

260
00:15:21,920 --> 00:15:22,909
— была уморительной...
— Спасибо.

261
00:15:22,941 --> 00:15:24,847
Но всё после неё — полный провал

262
00:15:25,052 --> 00:15:27,145
Хватит с меня твоих важничаний..

263
00:15:27,245 --> 00:15:30,216
Может быть я не поступил в университет
в 11 лет, как ты

264
00:15:30,229 --> 00:15:33,714
может быть я защитил кандидатскую
в 24, а не в 16,

265
00:15:33,734 --> 00:15:35,441
но ты точно не единственный человек,
который умнее чем

266
00:15:35,454 --> 00:15:36,848
все остальные здесь!

267
00:15:38,228 --> 00:15:38,838
Без обид.

268
00:15:40,907 --> 00:15:42,667
И я явно не единственный человек,

269
00:15:42,692 --> 00:15:44,458
которого мучает неуверенность в себе,

270
00:15:44,477 --> 00:15:46,585
и чьё эго нуждается в постоянных одобрениях других людей!

271
00:15:46,726 --> 00:15:48,325
Так ты признаёшь что ты эгоцентрист?

272
00:15:48,559 --> 00:15:49,336
Да!

273
00:15:50,742 --> 00:15:53,730
Меня зовут доктор Леонард Хофстаддер
и я никогда не мог угодить родителям,

274
00:15:53,736 --> 00:15:56,109
и поэтому да, мне нужно одобрение от вас, незнакомцев.

275
00:15:56,221 --> 00:15:57,210
Но он хуже!

276
00:15:57,762 --> 00:15:59,119
Ну всё!

277
00:15:59,510 --> 00:16:00,063
Прекрати!

278
00:16:02,764 --> 00:16:05,364
Ты не сможешь взорвать мне голову усилием своего мозга!

279
00:16:05,577 --> 00:16:07,297
Тогда я соглашусь с тем, что у тебя аневризма!
Аневризма — выпячивание стенки артерии вследствие её истончения или растяжения

280
00:16:08,859 --> 00:16:09,483
— Прекрати это!
— Ты ударил меня!

281
00:16:09,588 --> 00:16:10,738
Он ударил меня!
Все видели!

282
00:16:10,904 --> 00:16:12,366
Ты пытался мне голову взорвать!

283
00:16:12,398 --> 00:16:13,303
Так это работало!

284
00:16:13,335 --> 00:16:15,689
Это не...Это не работало! Ты псих!

285
00:16:15,709 --> 00:16:16,665
Вот и посмотрим!

286
00:16:16,810 --> 00:16:18,158
Люди в первом ряду, пригнитесь,

287
00:16:18,162 --> 00:16:19,394
Это зона заплескивания!

288
00:16:20,550 --> 00:16:21,205
Прекрати!

289
00:16:21,692 --> 00:16:22,180
Хватит!

290
00:16:28,341 --> 00:16:30,581
А часто физические конференции проходят вот так?

291
00:16:31,028 --> 00:16:32,267
Чаще чем ты думаешь..

292
00:16:48,882 --> 00:16:50,667
Мог бы и машину мне вызвать..

293
00:16:52,599 --> 00:16:54,339
Тебе ещё повезло, что я тебя не переехал.

294
00:16:55,442 --> 00:16:57,328
Я действительно не понимаю, почему ты не рад.

295
00:16:57,341 --> 00:16:58,991
Ты просил меня придти — я пришёл.

296
00:16:59,019 --> 00:17:00,194
Тебе не угодишь.

297
00:17:01,199 --> 00:17:03,753
Ну да, ты прав, я — единственная проблема
и я единственный, кому нужна помощь.

298
00:17:04,222 --> 00:17:06,430
Хм.. Не особо смахивает на извинение,
но я принимаю его.

299
00:17:07,615 --> 00:17:08,828
Прошу прощения,

300
00:17:09,367 --> 00:17:11,640
но нет ничего такого из-за чего
ты бы хотел извиниться?

301
00:17:13,238 --> 00:17:13,854
Да.

302
00:17:14,994 --> 00:17:17,151
Прости, что хотел взорвать твою голову.

303
00:17:19,121 --> 00:17:20,225
Оно не стоило того.

304
00:17:21,669 --> 00:17:22,645
Не поверите!

305
00:17:22,671 --> 00:17:25,126
Кто-то заснял всё действо на свой мобильник
и выложил на YouTube!

306
00:17:26,731 --> 00:17:27,313
Что?

307
00:17:29,006 --> 00:17:31,254
— Кто это был?
— Наверное, это я.

308
00:17:33,050 --> 00:17:34,752
Эй, зацените, это нашумевшее видео!
http://youtube.com/watch?v=lMA_mMG1Scg =]

309
00:17:34,854 --> 00:17:36,780
<i>Он ударил меня!
Все видели!</i>

310
00:17:36,812 --> 00:17:37,833
<i>Ты пытался мне голову взорвать!</i>

311
00:17:37,872 --> 00:17:40,843
<i>— Так это работало!
— Это не работало! Ты псих!</i>

312
00:17:40,869 --> 00:17:41,884
<i>Вот и посмотрим!</i>

313
00:17:41,935 --> 00:17:43,216
<i>Люди в первом ряду, пригнитесь,</i>

314
00:17:43,229 --> 00:17:44,500
<i>Это зона заплескивания!</i>

315
00:17:46,091 --> 00:17:48,685
<i>Прекрати! Хватит,
хватит!</i>

316
00:17:52,186 --> 00:17:53,721
<i>Тебе нужен обморочный зажим!</i>

317
00:17:55,367 --> 00:17:57,809
<i>Тебе бы ногти подстричь!
Больно же!</i>

318
00:17:59,367 --> 00:18:02,498
Омг, неужели костюмчик и правда так паршиво выглядит?

319
00:18:03,625 --> 00:18:05,484
Забудь про свой костюм,
гляди на мои руки.

320
00:18:05,490 --> 00:18:07,115
Я как фламинго под риталином!
Риталин — аналог амфетамина (спиды).

321
00:18:10,240 --> 00:18:10,927
Говард,

322
00:18:11,155 --> 00:18:13,584
Не хотел бы ты объяснить мне
почему у тебя на Facebook

323
00:18:13,600 --> 00:18:15,873
есть фотография меня, спящей на твоём плече

324
00:18:15,889 --> 00:18:17,334
с подписью  «Я и моя девушка»?

325
00:18:19,680 --> 00:18:21,702
О-оу.. чую грядёт
<b>Р</b>азговор

326
00:18:32,099 --> 00:18:33,859
<i>где-то в Китае</i>
— Ты ударил меня!
— Ты пытался мне голову взорвать!

327
00:18:33,900 --> 00:18:36,898
— Ага! Так это работало!
— Это не работало! Ты псих!

328
00:18:36,937 --> 00:18:37,880
Вот и посмотрим!

329
00:18:37,919 --> 00:18:38,441
Лузеры..

330
00:18:39,518 --> 00:18:43,271
Ога. Огромные Американские Ботаны.

331
00:18:46,376 --> 00:18:48,919
Кто это делает?

332
00:18:50,150 --> 00:18:53,370
Кто-то из Пасадены, Калифорния, под ником...

333
00:18:54,409 --> 00:18:57,221
Wolowizard!

334
00:18:58,338 --> 00:19:01,426
Ы! Чумаа!

335
00:19:01,426 --> 00:19:01,426
Перевод: Gerynd, Irka, Yokky. Спс. =)

