1
00:00:10,835 --> 00:00:13,160
Alright, just a few more feet.

2
00:00:13,316 --> 00:00:15,946
And... here we are, gentlemen,

3
00:00:16,156 --> 00:00:17,934
the Gates of Elzebob.

4
00:00:18,523 --> 00:00:19,621
Good Lord.

5
00:00:19,978 --> 00:00:22,931
Don't panic. This is what the
last 97 hours have been about.

6
00:00:23,331 --> 00:00:25,523
Stay frosty. There's a horde
of armed goblins

7
00:00:25,688 --> 00:00:28,743
on the other side of that gate
guarding the Sword of Asaroth.

8
00:00:29,090 --> 00:00:32,109
Warriors, unsheathe your weapons.
Magic wielders, raise your wands.

9
00:00:32,455 --> 00:00:33,455
Lock and load.

10
00:00:33,636 --> 00:00:35,086
Raj, blow the gates.

11
00:00:35,972 --> 00:00:37,228
Blowing the gates.

12
00:00:38,064 --> 00:00:39,064
Control,

13
00:00:39,310 --> 00:00:41,645
shift... B!

14
00:00:44,294 --> 00:00:46,434
Oh, my God, so many goblins!

15
00:00:47,441 --> 00:00:49,260
Don't just stand there,
slash and move!

16
00:00:49,414 --> 00:00:50,455
Stay in formation!

17
00:00:50,580 --> 00:00:52,054
Leonard, you got one on your tail.

18
00:00:52,325 --> 00:00:54,408
My tail's prehensile--
I'll swat him off.

19
00:00:54,528 --> 00:00:55,702
I got him, Leonard.

20
00:00:55,822 --> 00:00:58,884
Tonight I spice my mead
with goblin blood.

21
00:00:59,537 --> 00:01:01,377
Raj, no, it's a trap!
They're flanking us!

22
00:01:01,632 --> 00:01:03,142
He's got me!

23
00:01:03,708 --> 00:01:05,740
Sheldon, he's got Raj.
Use your sleep spell!

24
00:01:08,077 --> 00:01:10,875
I've got the Sword of Asaroth!

25
00:01:12,133 --> 00:01:13,681
Forget the sword, help Raj!

26
00:01:13,898 --> 00:01:17,351
There is no more Sheldon.
I am the sword master!

27
00:01:17,722 --> 00:01:20,187
- Leonard, look out!
- Damn it, man, we're dying here!

28
00:01:20,349 --> 00:01:21,772
Good-bye, peasants!

29
00:01:22,887 --> 00:01:24,233
The bastard teleported!

30
00:01:26,263 --> 00:01:28,413
He's selling the Sword
of Asaroth on eBay.

31
00:01:30,122 --> 00:01:32,434
You betrayed us for money?
Who are you?

32
00:01:32,771 --> 00:01:33,993
I'm a rogue night elf.

33
00:01:34,128 --> 00:01:36,052
Don't you people read
character descriptions?

34
00:01:36,739 --> 00:01:39,123
Wait, wait!
Somebody just clicked "Buy it now."

35
00:01:39,334 --> 00:01:42,289
I am the sword master!

36
00:02:07,512 --> 00:02:08,827
* sweety.

37
00:02:09,175 --> 00:02:11,395
Anybody want to log on
to Second Life and go swimming?

38
00:02:11,530 --> 00:02:13,101
I just built a virtual pool.

39
00:02:14,030 --> 00:02:17,016
No. I can't look at you
or your avatar right now.

40
00:02:21,345 --> 00:02:23,045
Sounds like your neighbor's home.

41
00:02:24,901 --> 00:02:25,901
Excuse me.

42
00:02:26,336 --> 00:02:28,516
Don't forget the mail you took
accidentally on purpose

43
00:02:28,647 --> 00:02:29,975
as an excuse to talk to her.

44
00:02:30,156 --> 00:02:31,624
Oh, right, right, right.

45
00:02:32,681 --> 00:02:35,262
Stealing snail mail--
very old school. I like it.

46
00:02:36,948 --> 00:02:38,871
Penny,
the mailman, did it again...

47
00:02:39,654 --> 00:02:40,664
Sorry.

48
00:02:41,335 --> 00:02:42,705
Hi, Leonard.
This is Doug.

49
00:02:43,051 --> 00:02:44,556
Doug, this is my neighbor Leonard.

50
00:02:44,730 --> 00:02:45,752
What's up, bro?

51
00:02:47,951 --> 00:02:48,951
Not much...

52
00:02:51,316 --> 00:02:52,316
bro.

53
00:02:53,502 --> 00:02:54,549
Is everything okay?

54
00:02:54,954 --> 00:02:56,972
Yeah, no, I just...
I got your mail again. Here.

55
00:02:57,733 --> 00:02:59,683
Thank you.
I've got to talk to that mailman.

56
00:02:59,845 --> 00:03:02,102
Oh, no, that's probably not
such a good idea.

57
00:03:03,189 --> 00:03:05,407
Civil servants have
a documented propensity

58
00:03:05,547 --> 00:03:07,772
to, you know, snap, so...

59
00:03:09,053 --> 00:03:10,903
Okay. Well, thank you again.

60
00:03:11,065 --> 00:03:12,138
No problem. Bye.

61
00:03:13,529 --> 00:03:15,825
Oh, and bye... bro.

62
00:03:23,976 --> 00:03:25,626
Penny for your thoughts.

63
00:03:27,217 --> 00:03:28,297
What's the matter?

64
00:03:28,687 --> 00:03:29,687
I'm fine.

65
00:03:30,396 --> 00:03:33,050
Penny's fine.
The guy she's kissing is really fine.

66
00:03:34,313 --> 00:03:37,290
Kissing, what kind of kissing?
Cheeks? Lips? Chaste? French?

67
00:03:39,401 --> 00:03:40,998
What is wrong with you?

68
00:03:42,200 --> 00:03:43,510
I'm a romantic.

69
00:03:46,191 --> 00:03:48,198
Don't tell me
that your hopeless infatuation

70
00:03:48,333 --> 00:03:49,900
is devolving into pointless jealousy.

71
00:03:50,215 --> 00:03:52,915
I'm not jealous.
I'm just a little concerned for her.

72
00:03:53,326 --> 00:03:55,155
I didn't like the look of the guy.

73
00:03:55,317 --> 00:03:57,017
Because he looks better than you?

74
00:04:00,729 --> 00:04:02,256
He was kind of dreamy.

75
00:04:03,191 --> 00:04:05,179
At least now you can
retrieve the black box

76
00:04:05,323 --> 00:04:06,992
from the twisted,
smoldering wreckage

77
00:04:07,107 --> 00:04:08,752
that was once your fantasy of dating her

78
00:04:08,962 --> 00:04:10,930
and analyze the data
so that you don't crash

79
00:04:11,085 --> 00:04:12,577
into Geek Mountain again.

80
00:04:14,253 --> 00:04:15,257
I disagree.

81
00:04:15,411 --> 00:04:18,207
Love is not a sprint,
it's a marathon--

82
00:04:18,499 --> 00:04:22,444
a relentless pursuit that only ends
when she falls into your arms...

83
00:04:23,639 --> 00:04:25,517
or hits you with
the pepper spray.

84
00:04:27,227 --> 00:04:28,365
I'm done with Penny.

85
00:04:29,041 --> 00:04:31,817
I'm going to be more realistic
and go after someone my own speed.

86
00:04:32,074 --> 00:04:33,850
- Like who?
- I don't know...

87
00:04:34,427 --> 00:04:35,427
Olivia Geiger.

88
00:04:35,635 --> 00:04:38,785
The dietician at the cafeteria
with the limp and the lazy eye?

89
00:04:41,758 --> 00:04:43,370
I don't think you have a shot there.

90
00:04:45,074 --> 00:04:48,237
I have noticed that Leslie Winkle
recently started shaving her legs.

91
00:04:48,357 --> 00:04:49,789
Now given that winter is coming,

92
00:04:50,013 --> 00:04:52,588
one can only assume
she's signaling sexual availability.

93
00:04:53,680 --> 00:04:55,485
I don't know.
You guys work in the same lab.

94
00:04:56,025 --> 00:04:58,186
- So?
- There are pitfalls.

95
00:04:58,398 --> 00:04:59,690
Trust me, I know.

96
00:04:59,936 --> 00:05:02,612
When it comes to
sexual harassment law,

97
00:05:03,132 --> 00:05:05,158
I'm a bit of a self-taught expert.

98
00:05:07,903 --> 00:05:10,041
If I were to ask Leslie Winkle out,

99
00:05:10,176 --> 00:05:12,800
it would just be for dinner.
I'm not going to walk into the lab,

100
00:05:12,935 --> 00:05:15,028
ask her to strip naked
and dance for me.

101
00:05:15,268 --> 00:05:17,169
Oh, then you're probably okay.

102
00:05:21,322 --> 00:05:23,067
- Hello, Leslie.
- Hi, Leonard.

103
00:05:23,384 --> 00:05:24,928
I'd like to propose an experiment...

104
00:05:25,101 --> 00:05:26,722
- Goggles, Leonard.
- Right.

105
00:05:29,006 --> 00:05:30,804
I would like to propose an experiment.

106
00:05:30,896 --> 00:05:31,909
Hang on.

107
00:05:32,102 --> 00:05:34,237
I'm trying to see how long it
takes a 500-kilowatt

108
00:05:34,411 --> 00:05:36,780
oxygen iodine laser to heat up
my Cup a' Noodles.

109
00:05:39,006 --> 00:05:41,678
I've done it. About two seconds.
2.6 for minestrone.

110
00:05:48,534 --> 00:05:50,185
Anyway, I was thinking more

111
00:05:50,305 --> 00:05:53,516
of a bio-social exploration
with a neuro-chemical overlay.

112
00:05:55,514 --> 00:05:57,288
Wait, are you asking me out?

113
00:05:58,747 --> 00:06:01,335
I was going to characterize it
as the modification

114
00:06:01,509 --> 00:06:03,449
of our
colleague-slash-friendship paradigm

115
00:06:03,584 --> 00:06:05,391
with the addition
of a date-like component,

116
00:06:05,567 --> 00:06:08,500
but we don't need to
quibble over terminology.

117
00:06:09,958 --> 00:06:11,420
What sort of experiment?

118
00:06:11,665 --> 00:06:14,215
There's a generally accepted
pattern in this area.

119
00:06:14,335 --> 00:06:16,503
I would pick you up,
take you to a restaurant.

120
00:06:16,677 --> 00:06:19,393
Then we would see a movie,
probably a romantic comedy

121
00:06:19,528 --> 00:06:22,080
featuring the talents
of Hugh Grant or Sandra Bullock.

122
00:06:23,321 --> 00:06:24,879
Interesting.
And would you agree

123
00:06:24,999 --> 00:06:27,475
that the primary way we would
evaluate either the success

124
00:06:27,610 --> 00:06:29,018
or failure of the date would be

125
00:06:29,173 --> 00:06:31,853
based on the biochemical reaction
during the good night kiss?

126
00:06:32,039 --> 00:06:33,739
Heart rate,
pheromones, etc. Yes.

127
00:06:35,034 --> 00:06:37,109
Why don't we just stipulate
that the date goes well

128
00:06:37,254 --> 00:06:38,637
and move to the key variable?

129
00:06:39,167 --> 00:06:40,614
- You mean kiss you now?
- Yes.

130
00:06:40,983 --> 00:06:42,805
Can you define the
parameters of the kiss?

131
00:06:42,896 --> 00:06:44,535
Close-mouthed but romantic.
Mint?

132
00:06:44,801 --> 00:06:45,862
Thank you.

133
00:06:53,042 --> 00:06:54,334
I count down from three?

134
00:06:54,531 --> 00:06:56,496
No, I think it needs
to be spontaneous.

135
00:07:04,318 --> 00:07:06,438
- What do you think?
- You proposed the experiment.

136
00:07:06,611 --> 00:07:08,581
I think you should
present your findings first.

137
00:07:09,156 --> 00:07:10,156
Fair enough.

138
00:07:11,107 --> 00:07:13,126
On the plus side,
it was a good kiss.

139
00:07:13,319 --> 00:07:15,541
Reasonable technique,
no extraneous spittle.

140
00:07:17,341 --> 00:07:19,193
On the other hand, no arousal.

141
00:07:20,641 --> 00:07:21,945
- None?
- None.

142
00:07:26,928 --> 00:07:28,587
- Thank you for your time.
- Thank you.

143
00:07:33,746 --> 00:07:34,746
None at all?

144
00:07:39,516 --> 00:07:41,462
Sheldon,
if you were a robot,

145
00:07:41,910 --> 00:07:44,456
and I knew
and you didn't...

146
00:07:47,136 --> 00:07:48,836
...would you want me to tell you?

147
00:07:50,635 --> 00:07:51,635
That depends.

148
00:07:52,319 --> 00:07:53,226
When

149
00:07:53,477 --> 00:07:54,943
I learn that I'm a robot...

150
00:07:55,945 --> 00:07:57,795
will I be able to handle it?

151
00:07:59,150 --> 00:08:02,699
Maybe-- although the history
of science fiction is not on your side.

152
00:08:03,833 --> 00:08:05,656
Okay,
let me ask you this--

153
00:08:06,160 --> 00:08:08,112
when I learn that I'm a robot,

154
00:08:08,440 --> 00:08:11,429
would I be bound
by Asimov's Three Laws of Robotics?

155
00:08:12,924 --> 00:08:15,649
You might be bound
by them right now.

156
00:08:19,365 --> 00:08:21,493
That's true.
Have you ever harmed a human being

157
00:08:21,608 --> 00:08:23,923
or through inaction allowed
a human being to come to harm?

158
00:08:24,068 --> 00:08:26,007
- Of course not.
- Have you ever harmed yourself

159
00:08:26,161 --> 00:08:27,578
or allowed yourself to be harmed

160
00:08:27,694 --> 00:08:30,232
except in cases where a human being
would have been endangered?

161
00:08:30,378 --> 00:08:32,269
- Well, no.
- I smell robot.

162
00:08:36,350 --> 00:08:37,508
Hey, what's going on?

163
00:08:37,807 --> 00:08:39,757
Internet's been down
for half an hour.

164
00:08:42,618 --> 00:08:44,152
Also, Sheldon may be a robot.

165
00:08:46,473 --> 00:08:47,964
So how'd it go with Leslie?

166
00:08:49,403 --> 00:08:52,229
Oh, we tried kissing,
but the Earth didn't move.

167
00:08:53,724 --> 00:08:57,350
I mean, any more than the 383 miles
that it was going to move anyway.

168
00:08:59,905 --> 00:09:01,837
Oh, I've seen that look before.

169
00:09:02,090 --> 00:09:04,116
This is just going to be
two weeks of moping

170
00:09:04,307 --> 00:09:05,653
and tedious emo songs

171
00:09:05,865 --> 00:09:08,652
and calling me to come down
to pet stores to look at cats.

172
00:09:11,117 --> 00:09:12,597
I don't know if I can take it.

173
00:09:13,665 --> 00:09:15,215
You could power down.

174
00:09:18,200 --> 00:09:20,496
As usual, Wolowitz has the solution.

175
00:09:22,296 --> 00:09:23,644
I happen to know a place

176
00:09:23,818 --> 00:09:27,207
where there are plenty of eligible women
and Leonard could have his pick.

177
00:09:37,432 --> 00:09:39,082
Remember the Latin hips.

178
00:09:39,652 --> 00:09:42,300
Shoulders stay still, and...
we sway.

179
00:09:47,766 --> 00:09:49,116
One, two, three...

180
00:09:50,672 --> 00:09:52,122
...five, six, seven.

181
00:09:53,426 --> 00:09:55,576
I think Mrs. Tishman's
got her eye on you.

182
00:09:56,874 --> 00:09:58,723
I've been there.
You're in for a treat.

183
00:10:15,350 --> 00:10:16,350
Oh, good Lord.

184
00:10:27,142 --> 00:10:28,792
God, that's a good song.

185
00:10:29,853 --> 00:10:32,403
If you're compiling a mix CD
for a double suicide.

186
00:10:33,655 --> 00:10:36,453
Oh, I hope
that scratching post is for you.

187
00:10:37,615 --> 00:10:40,267
I know what you're thinking.
I've taken your asthma into account.

188
00:10:40,467 --> 00:10:42,630
There's a feline geneticist
in San Diego

189
00:10:42,785 --> 00:10:45,566
who's developed the cutest
little hypoallergenic calicos.

190
00:10:45,748 --> 00:10:47,788
- Listen to me...
- I've been thinking about names.

191
00:10:47,903 --> 00:10:50,064
I'm kind of torn between
Einstein, Newton

192
00:10:50,237 --> 00:10:51,952
and Sergeant Fuzzy Boots.

193
00:10:53,013 --> 00:10:54,991
Do you really think
you can satisfy your need

194
00:10:55,158 --> 00:10:57,203
for a relationship with
a genetically altered cat?

195
00:10:58,303 --> 00:10:59,303
Maybe.

196
00:10:59,500 --> 00:11:01,461
If it's a cute, little, cuddly cat.

197
00:11:02,304 --> 00:11:03,954
Oh, come on. Leonard...

198
00:11:06,595 --> 00:11:08,235
This is obviously about Penny.

199
00:11:10,672 --> 00:11:11,923
It doesn't matter.

200
00:11:12,205 --> 00:11:14,922
The woman's not interested in me.
The woman rejected me.

201
00:11:16,309 --> 00:11:17,351
Look,

202
00:11:18,339 --> 00:11:20,041
I think that you have
as much of a chance

203
00:11:20,215 --> 00:11:22,038
of having a sexual relationship
with Penny

204
00:11:22,192 --> 00:11:24,114
as the Hubble Telescope
does of discovering

205
00:11:24,268 --> 00:11:25,735
at the center of every black hole

206
00:11:25,870 --> 00:11:28,629
is a little man with a flashlight
searching for a circuit breaker.

207
00:11:31,672 --> 00:11:32,847
Nevertheless,

208
00:11:33,572 --> 00:11:36,096
I do feel obligated
to point out to you

209
00:11:36,774 --> 00:11:38,599
that she did not reject you.

210
00:11:39,301 --> 00:11:40,724
You did not ask her out.

211
00:11:42,583 --> 00:11:43,583
You're right.

212
00:11:44,398 --> 00:11:46,218
I didn't ask her out.
I should ask her out.

213
00:11:46,390 --> 00:11:48,249
No, no, that was not my point.

214
00:11:49,422 --> 00:11:51,235
My point was don't buy a cat.

215
00:11:52,154 --> 00:11:53,704
No, but you're right.

216
00:11:54,048 --> 00:11:56,198
I should march over there
and ask her out.

217
00:11:57,063 --> 00:11:59,207
Oh, goody, we're getting a cat.

218
00:12:08,009 --> 00:12:10,153
Good afternoon, Penny.
So, hi... hey.

219
00:12:12,858 --> 00:12:14,718
I was wondering if you
had plans for dinner.

220
00:12:14,969 --> 00:12:16,223
You mean dinner tonight?

221
00:12:17,651 --> 00:12:19,754
There is an inherent ambiguity
in the word "dinner."

222
00:12:20,196 --> 00:12:21,334
Technically it refers

223
00:12:21,488 --> 00:12:23,688
to the largest meal of the day
whenever it's consumed.

224
00:12:23,822 --> 00:12:25,963
So to clarify here,
by dinner I mean supper.

225
00:12:26,635 --> 00:12:28,063
- Supper?
- Or dinner.

226
00:12:29,593 --> 00:12:31,984
I was thinking 6:30 if you can go.
Or a different time.

227
00:12:32,300 --> 00:12:33,342
6:30's great.

228
00:12:33,754 --> 00:12:34,754
Really?

229
00:12:37,014 --> 00:12:39,180
- Great.
- I like hanging out with you guys.

230
00:12:39,513 --> 00:12:40,571
Us guys?

231
00:12:40,879 --> 00:12:43,328
Yeah, you know, Sheldon, Howard, Raj.
Who all's coming?

232
00:12:44,586 --> 00:12:45,609
They...

233
00:12:46,916 --> 00:12:48,286
might all be there.

234
00:12:50,486 --> 00:12:52,551
Or a subset
of them might be there.

235
00:12:52,877 --> 00:12:55,321
Algebraically speaking,
there are too many unknowns.

236
00:12:55,441 --> 00:12:57,518
For example,
Sheldon had Quiznos for lunch.

237
00:12:57,727 --> 00:13:00,095
Sometimes he finds that filling,
other times he doesn't.

238
00:13:00,373 --> 00:13:02,804
It's no fault of Quiznos--
they have a varied menu.

239
00:13:05,177 --> 00:13:06,933
Okay, whatever.
It sounds like fun.

240
00:13:07,658 --> 00:13:08,658
Great.

241
00:13:09,278 --> 00:13:10,691
- Did we say a time?
- 6:30.

242
00:13:10,888 --> 00:13:12,494
- And that's still good?
- It's fine.

243
00:13:12,687 --> 00:13:15,021
- 'Cause it's not carved in stone.
- No, 6:30's great.

244
00:13:15,467 --> 00:13:16,917
I'll get my chisel.

245
00:13:18,619 --> 00:13:19,680
Why?

246
00:13:22,064 --> 00:13:25,131
To... carve the...
I'll see you at 6:30.

247
00:13:36,902 --> 00:13:37,905
How do I look?

248
00:13:43,244 --> 00:13:44,806
Could you be more specific?

249
00:13:46,748 --> 00:13:48,759
Can you tell I'm perspiring a little?

250
00:13:49,977 --> 00:13:54,191
No. The dark crescent-shaped patterns
under your arms conceal it nicely.

251
00:13:56,615 --> 00:13:58,525
- What time is your date?
- 6:30.

252
00:13:58,660 --> 00:13:59,876
Perfect. That gives you

253
00:14:00,020 --> 00:14:02,422
two hours and 15 minutes
for that dense molecular cloud

254
00:14:02,639 --> 00:14:04,159
of Aramis to dissipate.

255
00:14:05,434 --> 00:14:06,434
Is it too much?

256
00:14:06,652 --> 00:14:08,330
Not if you're a rugby team.

257
00:14:10,366 --> 00:14:11,694
If it should ever come up,

258
00:14:11,829 --> 00:14:13,758
you didn't join us
because you stuffed yourself

259
00:14:13,913 --> 00:14:15,649
with a chicken carbonara sub
at Quiznos.

260
00:14:16,075 --> 00:14:17,194
Why would I join you?

261
00:14:18,310 --> 00:14:19,310
No reason.

262
00:14:22,314 --> 00:14:24,494
You know what,
maybe this isn't such a good idea.

263
00:14:24,849 --> 00:14:27,241
No, well, now there's
always the possibility

264
00:14:27,434 --> 00:14:29,518
that alcohol and poor judgment
on her part

265
00:14:29,672 --> 00:14:32,007
might lead to a nice
romantic evening.

266
00:14:33,769 --> 00:14:36,740
You're right. Alcohol, poor judgment--
it could go well.

267
00:14:37,312 --> 00:14:39,204
There is the other
possibility,  that...

268
00:14:39,359 --> 00:14:41,327
this date kicks off
a rather unpleasant 6 months

269
00:14:41,481 --> 00:14:43,448
of the two of you passing
awkwardly in the hall,

270
00:14:43,605 --> 00:14:46,034
until one of you breaks down and
moves to another zip code.

271
00:14:47,544 --> 00:14:49,898
You could've stopped
at "it could go well."

272
00:14:51,102 --> 00:14:52,749
If I could've, I would've.

273
00:14:53,407 --> 00:14:55,107
I mean, I'm a perfectly nice guy.

274
00:14:55,668 --> 00:14:58,677
There's no reason we couldn't go
and have a lovely dinner,

275
00:14:59,048 --> 00:15:00,998
maybe take a walk afterwards,

276
00:15:01,278 --> 00:15:03,438
talk about things we have in common.

277
00:15:03,612 --> 00:15:06,572
You love pottery?
I love pottery!

278
00:15:07,122 --> 00:15:08,482
There's a pause,

279
00:15:08,637 --> 00:15:10,643
we both know what's happening.
I lean in, we kiss.

280
00:15:10,797 --> 00:15:12,900
It's a little tentative at first,
but then I realize

281
00:15:13,103 --> 00:15:15,255
she's kissing me back.
She's biting my lower lip!

282
00:15:15,371 --> 00:15:17,265
She wants me!
This thing is going the distance!

283
00:15:17,413 --> 00:15:19,786
We're going to have sex!
Oh, God, oh, my God!

284
00:15:22,276 --> 00:15:23,926
Is the sex starting now?

285
00:15:25,522 --> 00:15:26,950
I'm having a panic attack.

286
00:15:28,185 --> 00:15:29,785
Well then... calm down.

287
00:15:30,407 --> 00:15:32,289
If I could,
I wouldn't be having a panic...

288
00:15:32,424 --> 00:15:34,222
That's why they call it a panic attack.

289
00:15:35,141 --> 00:15:36,492
Sit down.

290
00:15:36,933 --> 00:15:38,963
Yeah, sit down. Now close your eyes.

291
00:15:39,117 --> 00:15:40,465
- Why?
- Just do it.

292
00:15:40,908 --> 00:15:43,821
Now try to increase
your alpha wave activity.

293
00:15:44,362 --> 00:15:46,888
- What?!
- It's a biofeedback technique.

294
00:15:47,107 --> 00:15:49,097
It's relaxation through
brain wave manipulation.

295
00:15:49,350 --> 00:15:51,757
I read a paper about it
in Journal of American Neuroscience.

296
00:15:51,877 --> 00:15:54,789
It was a little sparsely sourced,
but I think the basic science is valid.

297
00:15:54,944 --> 00:15:56,436
I probably have it here somewhere.

298
00:15:57,287 --> 00:15:59,216
Who am I kidding?
I can't go through with this.

299
00:15:59,371 --> 00:16:00,798
You need to call her and cancel.

300
00:16:00,953 --> 00:16:02,072
- Me?
- Yes.

301
00:16:02,207 --> 00:16:04,670
- What should I tell her?
- I don't know. Tell her I'm sick.

302
00:16:05,692 --> 00:16:08,249
Not an illness that will make her
want to take care of me,

303
00:16:08,403 --> 00:16:10,588
but nothing so critical
that she'll feel uncomfortable

304
00:16:10,744 --> 00:16:12,801
going out with me
if I want to try this again.

305
00:16:12,936 --> 00:16:15,402
Got it.
So I'm assuming nothing venereal?

306
00:16:20,221 --> 00:16:22,559
I'll just tell her
that you had a routine colonoscopy

307
00:16:22,679 --> 00:16:24,069
and haven't quite bounced back.

308
00:16:25,074 --> 00:16:27,293
- Give me the phone.
- I thought you wanted to cancel.

309
00:16:27,470 --> 00:16:30,171
I can't, because if I don't show up
she'll still be expecting you.

310
00:16:30,985 --> 00:16:32,628
Why would she be expecting me?

311
00:16:33,921 --> 00:16:35,604
Stop asking me all these questions.

312
00:16:35,777 --> 00:16:37,641
I need to take another shower.

313
00:16:41,017 --> 00:16:42,830
So are the rest of the guys
meeting us here?

314
00:16:43,016 --> 00:16:44,442
Oh, yeah... no.

315
00:16:45,904 --> 00:16:49,123
It turns out that Raj
and Howard had to work, and Sheldon...

316
00:16:49,710 --> 00:16:52,797
had a colonoscopy and he hasn't
quite bounced back yet.

317
00:16:54,995 --> 00:16:56,760
My uncle just had a colonoscopy.

318
00:16:57,017 --> 00:16:58,039
You're kidding.

319
00:16:58,213 --> 00:17:00,500
Then that's something
we have in common.

320
00:17:01,640 --> 00:17:02,640
How?

321
00:17:03,445 --> 00:17:06,027
We both have people
in our lives who...

322
00:17:06,343 --> 00:17:09,284
want to nip intestinal polyps
in the bud.

323
00:17:15,777 --> 00:17:17,727
So what's new in
the world of physics?

324
00:17:19,019 --> 00:17:20,019
Nothing.

325
00:17:21,617 --> 00:17:22,812
Really? Nothing?

326
00:17:23,129 --> 00:17:25,840
Well, with the exception
of string theory,

327
00:17:26,014 --> 00:17:28,503
not much has happened
since the 1930s.

328
00:17:28,772 --> 00:17:30,365
And you can't prove string theory.

329
00:17:30,541 --> 00:17:31,782
At best you can say,

330
00:17:32,033 --> 00:17:35,574
"Hey, look, my idea
has an internal logical consistency."

331
00:17:38,689 --> 00:17:40,747
Well, I'm sure things will pick up.

332
00:17:45,064 --> 00:17:46,882
What's new at the Cheesecake Factory?

333
00:17:48,536 --> 00:17:49,674
Not much.

334
00:17:49,907 --> 00:17:53,080
We do have a chocolate key lime
that's moving pretty well.

335
00:17:54,516 --> 00:17:55,516
Good.

336
00:17:56,366 --> 00:17:57,366
Good.

337
00:18:00,821 --> 00:18:02,371
What about your...

338
00:18:03,425 --> 00:18:04,639
hallway friend?

339
00:18:05,151 --> 00:18:07,461
Doug?
Oh, yeah, I don't know.

340
00:18:07,615 --> 00:18:09,767
I mean, you know,
he's nice and funny, but...

341
00:18:09,998 --> 00:18:11,754
Can I get you started with some drinks?

342
00:18:13,318 --> 00:18:15,202
- You were saying? But...
- I'd like a drink.

343
00:18:15,462 --> 00:18:18,725
Just say the "but" thing about Doug
and then I'll get her back.

344
00:18:20,830 --> 00:18:22,253
I don't know, it's just me.

345
00:18:22,429 --> 00:18:24,679
I'm still getting over
this breakup with Kurt

346
00:18:24,834 --> 00:18:27,361
and this thing with Doug
would just be rebound sex.

347
00:18:28,961 --> 00:18:31,122
Don't get me started
on rebound sex.

348
00:18:32,763 --> 00:18:34,352
It's just... it's my pattern.

349
00:18:34,472 --> 00:18:36,741
I break up,
then I find some cute guy,

350
00:18:36,895 --> 00:18:39,920
and then it's just 36
meaningless hours of...

351
00:18:40,651 --> 00:18:41,692
you know.

352
00:18:43,892 --> 00:18:45,106
I'm not sure that I do.

353
00:18:46,895 --> 00:18:49,952
Is that one 36-hour experience,

354
00:18:50,098 --> 00:18:52,433
or is that 36 hours
spread out over, say...

355
00:18:52,606 --> 00:18:54,489
one glorious summer?

356
00:18:55,328 --> 00:18:56,781
No, it's usually over a weekend,

357
00:18:56,936 --> 00:18:59,444
and trust me, you do not feel
good after it.

358
00:19:00,066 --> 00:19:01,803
Well, chafing, right?

359
00:19:03,516 --> 00:19:04,516
Emotionally.

360
00:19:04,768 --> 00:19:06,883
Of course, yeah,
emotional chafing.

361
00:19:14,659 --> 00:19:16,145
Do you want to see something cool?

362
00:19:18,269 --> 00:19:20,644
I can make this olive
go into this glass

363
00:19:20,887 --> 00:19:22,218
without touching it.

364
00:19:22,903 --> 00:19:24,735
- How?
- Physics.

365
00:19:31,230 --> 00:19:32,639
Centrifugal force.

366
00:19:33,012 --> 00:19:35,096
Actually, it's centripetal force,

367
00:19:35,298 --> 00:19:37,783
which is an inward force
generated by the glass

368
00:19:37,956 --> 00:19:39,472
acting on the olive...

369
00:19:40,691 --> 00:19:41,791
Excuse me.

370
00:19:42,111 --> 00:19:44,947
If you were riding
on the olive,

371
00:19:45,300 --> 00:19:47,306
you'd be in a non-inertial
reference frame

372
00:19:47,576 --> 00:19:48,734
and would...

373
00:19:50,390 --> 00:19:52,367
- Are you okay?
- Yeah, I'm okay.

374
00:19:54,194 --> 00:19:55,664
Did you spill ketchup?

375
00:19:55,864 --> 00:19:57,272
- No.
- I'm not okay.

376
00:20:01,195 --> 00:20:03,278
You sure you don't want to go
to the emergency room?

377
00:20:03,413 --> 00:20:05,555
No, I'm okay.
It stopped bleeding.

378
00:20:06,110 --> 00:20:09,132
I know, but you did throw up.
Isn't that a sign of a concussion?

379
00:20:09,325 --> 00:20:11,347
Yes.
But I get car sick, too, so...

380
00:20:13,935 --> 00:20:15,595
Sorry about
your car, by the way.

381
00:20:16,433 --> 00:20:18,845
It's fine.
You got most of it out the window.

382
00:20:20,187 --> 00:20:21,588
The poor guy on the bike.

383
00:20:24,646 --> 00:20:25,912
I had a nice time.

384
00:20:26,204 --> 00:20:27,458
Yeah, me, too.

385
00:20:28,778 --> 00:20:29,955
Well, um, good night.

386
00:20:30,850 --> 00:20:31,911
Good night.

387
00:20:35,592 --> 00:20:37,562
Was this supposed to be a date?

388
00:20:38,600 --> 00:20:39,651
This?

389
00:20:40,904 --> 00:20:41,904
No.

390
00:20:44,551 --> 00:20:46,615
Of course not.
This was just you and me

391
00:20:46,789 --> 00:20:48,863
hanging out with a bunch of guys
who didn't show up

392
00:20:49,027 --> 00:20:51,154
'cause of work and a colonoscopy.

393
00:20:53,178 --> 00:20:54,509
Okay, I was just checking.

394
00:20:55,928 --> 00:20:57,803
When I take a girl on a date--

395
00:20:58,125 --> 00:20:59,125
and I do--

396
00:21:01,414 --> 00:21:03,324
she knows she's been dated.

397
00:21:05,238 --> 00:21:06,413
Capital D.

398
00:21:07,848 --> 00:21:10,993
Boldface. Underlined.
Da...ted.

399
00:21:13,140 --> 00:21:14,883
I think I might have
a little concussion.

400
00:21:15,003 --> 00:21:17,363
I'm gonna go lie down for a while.
Good night.

401
00:21:21,314 --> 00:21:22,919
So, how was your date?

402
00:21:23,569 --> 00:21:24,569
Awesome!

403
00:21:28,871 --> 00:21:24,569
Score 1 for liquor and poor jugement.

