1
00:00:02,042 --> 00:00:04,965
All right!
I'm moving my infantry division.

2
00:00:05,188 --> 00:00:08,300
Augmented by bataillon of orcs
from Lord of the Rings.

3
00:00:08,511 --> 00:00:10,108
We flank the Tennessee volunteers

4
00:00:10,296 --> 00:00:13,196
and the North, once again,
wins the battle of Gettysburg.

5
00:00:13,583 --> 00:00:14,722
Not so fast!

6
00:00:14,887 --> 00:00:17,035
Remember, the South still has
two infantry divisions,

7
00:00:17,270 --> 00:00:18,949
plus Superman and Godzilla.

8
00:00:19,541 --> 00:00:22,912
No, orcs are magic. Superman
is vulnerable to magic.

9
00:00:22,998 --> 00:00:27,130
Not to mention you already lost Godzilla
to the Illinois cavalry and Hulk.

10
00:00:27,743 --> 00:00:31,708
Why don't you just have Robert E. Lee
charge the line with Shiva and Ganesh?

11
00:00:31,933 --> 00:00:33,946
- You guys ready to order?
- Hang on, honey.

12
00:00:34,123 --> 00:00:35,344
Shiva and Ganesh,

13
00:00:35,591 --> 00:00:37,894
the Hindu gods,
against the entire Union army?

14
00:00:38,014 --> 00:00:39,014
And orcs.

15
00:00:39,927 --> 00:00:41,066
I'll be back.

16
00:00:41,444 --> 00:00:42,461
Excuse me.

17
00:00:42,581 --> 00:00:45,308
Ganesh is the Remover of Obstacles
and Shiva is the Destroyer.

18
00:00:45,484 --> 00:00:48,102
When the smoke clears,
Abraham Lincoln will be speaking Hindi

19
00:00:48,255 --> 00:00:49,623
and drinking mint juleps.

20
00:00:50,290 --> 00:00:53,543
My boss says you have to either order
or leave and never come back.

21
00:00:54,474 --> 00:00:57,375
What do you recommend for someone
who worked up a man-sized appetite

22
00:00:57,579 --> 00:00:59,669
from a morning of weight training
and cardio funk?

23
00:00:59,848 --> 00:01:00,848
A shower.

24
00:01:01,951 --> 00:01:04,027
I'll take the Heart Smart platter.

25
00:01:04,841 --> 00:01:06,598
All right, thank you,
and Sheldon?

26
00:01:06,939 --> 00:01:08,975
We don't eat here.
I don't know what's good.

27
00:01:09,225 --> 00:01:11,752
- It's all good.
- Statistically unlikely.

28
00:01:12,881 --> 00:01:14,862
Just get a hamburger.
You like hamburgers.

29
00:01:14,982 --> 00:01:16,909
I like hamburgers
where we usually have them.

30
00:01:17,029 --> 00:01:19,379
You can't make the assumption
that I'll like them here.

31
00:01:20,221 --> 00:01:21,243
I'm sorry.

32
00:01:21,499 --> 00:01:23,395
- Give him a hamburger.
- Which one?

33
00:01:23,544 --> 00:01:25,415
The Classic Burger,
the Ranch House Burger,

34
00:01:25,552 --> 00:01:27,366
the Barbecue Burger,
or the Kobe Burger?

35
00:01:27,545 --> 00:01:29,283
Can't we just go to Big Boy?

36
00:01:29,590 --> 00:01:32,140
They only have one burger...
the Big Boy.

37
00:01:32,651 --> 00:01:34,282
The Barbecue Burger's like the Big Boy.

38
00:01:34,437 --> 00:01:36,846
Excuse me, in a world
that already includes a Big Boy,

39
00:01:37,011 --> 00:01:39,113
why would I settle for
something like a Big Boy?

40
00:01:39,357 --> 00:01:41,247
Because you're not at Big Boy!

41
00:01:42,651 --> 00:01:45,245
- Fine, I'll have the Barbecue Burger.
- Make it two.

42
00:01:45,365 --> 00:01:47,597
Waitresses don't yell at you
at Big Boy.

43
00:01:48,715 --> 00:01:50,374
Hey, Leonard.
Hi, guys.

44
00:01:50,548 --> 00:01:51,616
Hi, Leslie.

45
00:01:51,736 --> 00:01:53,034
I didn't know you ate here.

46
00:01:53,170 --> 00:01:55,286
We don't.
This is a disturbing aberration.

47
00:01:55,633 --> 00:01:56,777
Leslie, this is Penny.

48
00:01:56,897 --> 00:01:58,797
She lives across the hall
from Sheldon and me.

49
00:01:58,930 --> 00:02:01,419
And walks in quiet beauty
like the night.

50
00:02:02,472 --> 00:02:04,364
Howard, I've asked you not to do that.

51
00:02:05,613 --> 00:02:07,909
Leslie and I do research together
at the university.

52
00:02:08,691 --> 00:02:10,273
Wow, a girl scientist.

53
00:02:10,609 --> 00:02:12,668
Yep, come for the breasts,
stay for the brains.

54
00:02:14,394 --> 00:02:15,346
Glad I ran into you.

55
00:02:15,440 --> 00:02:17,895
The Physics Department string quartet
needs a new cellist.

56
00:02:18,049 --> 00:02:19,405
What happened to Elliot Wong?

57
00:02:19,557 --> 00:02:21,554
He switched over
to high-energy radiation research,

58
00:02:21,708 --> 00:02:23,478
had a little mishap,
and now the other guys

59
00:02:23,613 --> 00:02:25,311
are uncomfortable sitting next to him.

60
00:02:25,881 --> 00:02:27,830
- You're in?
- Yeah, sure, why not?

61
00:02:28,200 --> 00:02:30,320
Great, we rehearse
on Tuesdays at your place.

62
00:02:30,440 --> 00:02:31,480
Why at my place?

63
00:02:31,653 --> 00:02:34,936
Department of Energy said
our regular space is kind of a hot zone.

64
00:02:35,749 --> 00:02:37,560
- Nice meeting you.
- Yeah, you, too.

65
00:02:38,593 --> 00:02:40,137
I didn't know you played the cello.

66
00:02:40,310 --> 00:02:42,545
Yeah, my parents felt
that naming me Leonard

67
00:02:42,665 --> 00:02:44,863
and putting me
in Advanced Placement classes

68
00:02:45,018 --> 00:02:46,948
wasn't getting me beaten up enough.

69
00:02:47,739 --> 00:02:50,576
If you're into music,
I happen to be a human beatbox.

70
00:02:50,850 --> 00:02:51,699
Really?

71
00:02:58,621 --> 00:03:00,570
I'm actually not that into music.

72
00:03:01,599 --> 00:03:02,854
Your friend's really cute.

73
00:03:03,047 --> 00:03:04,526
Anything going on with you two?

74
00:03:05,246 --> 00:03:06,246
Leslie?

75
00:03:06,400 --> 00:03:07,400
No, no.

76
00:03:07,875 --> 00:03:09,843
- What, are you kidding?
- He asked her out once.

77
00:03:09,997 --> 00:03:11,723
It was an embarrassing failure.

78
00:03:12,255 --> 00:03:13,798
- Thank you, Sheldon.
- I'm sorry.

79
00:03:13,981 --> 00:03:15,435
Was that supposed to be a secret?

80
00:03:16,764 --> 00:03:18,772
That's too bad.
You guys'd make a cute couple.

81
00:03:21,128 --> 00:03:22,128
Oh, dear.

82
00:03:22,476 --> 00:03:24,425
- What's the matter?
- She didn't take my order.

83
00:03:24,598 --> 00:03:27,572
How can she take your order
when you're too neurotic to talk to her?

84
00:03:28,444 --> 00:03:31,218
Nevertheless,
this will be reflected in her tip.

85
00:03:34,602 --> 00:03:36,898
What did Penny mean,
"You'd make a cute couple?"

86
00:03:37,587 --> 00:03:39,365
I assume she meant
the two of you together

87
00:03:39,519 --> 00:03:42,728
would constitute a couple
that others might consider cute.

88
00:03:44,367 --> 00:03:46,585
An alternate and somewhat
less likely interpretation

89
00:03:46,778 --> 00:03:48,782
is that you could manufacture one.

90
00:03:50,347 --> 00:03:53,041
As in, "Look, Leonard and Leslie
made Mr. and Mrs. Goldfarb.

91
00:03:53,135 --> 00:03:54,327
Aren't they adorable?"

92
00:03:55,652 --> 00:03:57,741
If Penny didn't know
that Leslie had turned me down,

93
00:03:57,896 --> 00:03:59,362
then it would unambiguously mean

94
00:03:59,497 --> 00:04:02,292
that she, Penny, thought
I should ask her, Leslie, out,

95
00:04:02,465 --> 00:04:05,437
indicating that she had no interest
in me asking her, Penny, out.

96
00:04:05,591 --> 00:04:07,983
But, because she did know
that I had asked Leslie out

97
00:04:08,207 --> 00:04:10,876
and that she, Leslie,
had turned me down, then she, Penny,

98
00:04:11,011 --> 00:04:12,883
could be offering consolation.

99
00:04:14,872 --> 00:04:17,766
"That's too bad,
you would have made a cute couple,"

100
00:04:17,951 --> 00:04:21,328
but while thinking: "Good,
Leonard remains available."

101
00:04:23,485 --> 00:04:25,337
You're a lucky man, Leonard.

102
00:04:27,287 --> 00:04:29,736
- How so?
- You're talking to one of the three men

103
00:04:29,890 --> 00:04:33,575
in the Western hemisphere capable
of following that train of thought.

104
00:04:34,046 --> 00:04:35,505
Well, what do you think?

105
00:04:35,659 --> 00:04:38,245
I said I could follow it.
I didn't say I care.

106
00:04:59,557 --> 00:05:01,640
Subtitles: swsub.com

107
00:05:09,306 --> 00:05:11,038
I admire your fingering.

108
00:05:12,187 --> 00:05:13,187
Thank you.

109
00:05:14,908 --> 00:05:17,624
Maybe sometime you can try that
on my instrument.

110
00:05:26,730 --> 00:05:28,293
- G'night, guys. Good job.
- Thanks.

111
00:05:28,428 --> 00:05:29,894
See you next week.

112
00:05:30,311 --> 00:05:33,057
- That was fun. Thanks for including me.
- You're welcome.

113
00:05:33,423 --> 00:05:36,394
If you're up for it, we could practice
that middle section again.

114
00:05:36,592 --> 00:05:37,595
Sure, why not?

115
00:05:44,358 --> 00:05:45,966
Just so we're clear,
you understand

116
00:05:46,069 --> 00:05:47,980
that me hanging back
to practice with you

117
00:05:48,115 --> 00:05:50,565
is a pretext for letting you know
that I'm sexually available.

118
00:05:58,011 --> 00:05:59,011
Really?

119
00:05:59,304 --> 00:06:00,643
Yeah, I'm good to go.

120
00:06:01,488 --> 00:06:03,312
I thought you weren't interested in me.

121
00:06:03,609 --> 00:06:05,251
That was before I saw you handling

122
00:06:05,424 --> 00:06:07,836
that beautiful piece
of wood between your legs.

123
00:06:09,970 --> 00:06:11,320
You mean my cello?

124
00:06:11,586 --> 00:06:14,133
No, I mean the obvious,
crude, double entendre.

125
00:06:14,328 --> 00:06:15,600
I'm seducing you.

126
00:06:17,543 --> 00:06:18,624
No kidding.

127
00:06:19,835 --> 00:06:21,955
What can I say?
I'm a passionate and impulsive woman.

128
00:06:24,091 --> 00:06:25,091
So how about it?

129
00:06:28,020 --> 00:06:29,081
Is it the waitress?

130
00:06:30,925 --> 00:06:31,948
What about her?

131
00:06:32,160 --> 00:06:34,515
I thought I saw your pupils
dilate when you looked at her.

132
00:06:34,688 --> 00:06:37,910
Which, unless you're a heroin addict,
points to sexual attraction.

133
00:06:39,448 --> 00:06:41,397
I did have a poppy seed bagel
for breakfast.

134
00:06:41,551 --> 00:06:44,461
Which could cause a positive
urine test for opiates,

135
00:06:44,591 --> 00:06:46,294
but certainly not dilate my pupils.

136
00:06:46,449 --> 00:06:48,995
So I guess there was no point
in bringing it up.

137
00:06:50,218 --> 00:06:51,903
You and the waitress then.

138
00:06:52,147 --> 00:06:53,147
No... no.

139
00:06:53,349 --> 00:06:55,823
There's nothing going on
between Penny and me.

140
00:06:56,410 --> 00:06:58,451
So you're open
to a sexual relationship?

141
00:06:59,923 --> 00:07:01,563
Yeah, yeah, I guess I am.

142
00:07:01,919 --> 00:07:04,367
- Good.
- Yeah, it is. It is good.

143
00:07:06,673 --> 00:07:08,320
Did you want to start now?

144
00:07:08,857 --> 00:07:10,711
Why don't we finish
the section first?

145
00:07:12,074 --> 00:07:14,050
A little musical foreplay.
Terrific.

146
00:07:35,560 --> 00:07:36,563
I'm good to go.

147
00:07:36,823 --> 00:07:37,826
Me, too.

148
00:07:49,800 --> 00:07:51,189
Hey, Sheldon.
What's going on?

149
00:07:51,517 --> 00:07:53,756
I need your opinion
on a matter of semiotics.

150
00:07:54,316 --> 00:07:55,348
I'm sorry?

151
00:07:55,867 --> 00:07:57,885
Semiotics.
The study of signs and symbols.

152
00:07:58,040 --> 00:08:00,711
It's a branch of philosophy
related to linguistics.

153
00:08:02,488 --> 00:08:06,111
Okay, sweetie, I know you think
you're explaing yourself,

154
00:08:06,284 --> 00:08:07,866
but you're really not.

155
00:08:10,357 --> 00:08:11,649
Just come with me.

156
00:08:16,431 --> 00:08:18,385
- Well?
- Well, what?

157
00:08:18,912 --> 00:08:20,262
What does it mean?

158
00:08:21,107 --> 00:08:23,100
Oh, come on,
you went to college.

159
00:08:23,278 --> 00:08:24,570
Yes, but I was 11.

160
00:08:27,473 --> 00:08:29,552
All right, look,
a tie on the doorknob usually means

161
00:08:29,698 --> 00:08:31,319
someone doesn't want to be disturbed

162
00:08:31,493 --> 00:08:34,567
because, they're...
you know, gettin' busy.

163
00:08:36,766 --> 00:08:38,657
So you're saying Leonard
has a girl in there?

164
00:08:38,888 --> 00:08:41,187
Well, either that
or he's lost his tie rack

165
00:08:41,322 --> 00:08:43,392
and gotten really into Bryan Adams.

166
00:08:44,753 --> 00:08:46,642
Oh, Leonard, you magnificent beast.

167
00:08:49,802 --> 00:08:51,942
We really shouldn't be standing here.

168
00:08:54,314 --> 00:08:55,864
This is very awkward.

169
00:08:56,366 --> 00:08:59,470
Oh, come on, Leonard's had
girls over before, right?

170
00:09:00,218 --> 00:09:03,834
Yes, but there's usually planning,
courtship, advance notice...

171
00:09:05,122 --> 00:09:08,983
Last time, I was able to book a cruise
to the Arctic to see a solar eclipse.

172
00:09:10,401 --> 00:09:13,687
You had to leave the state
because your roommate was having sex?

173
00:09:14,001 --> 00:09:16,913
I didn't have to.
The dates just happened to coincide.

174
00:09:19,879 --> 00:09:21,927
So, do you know who's in there?

175
00:09:23,051 --> 00:09:24,601
Well, there's Leonard.

176
00:09:28,196 --> 00:09:31,772
And he's either with Leslie Winkle
or a 1930s gangster.

177
00:09:35,816 --> 00:09:37,090
Good for him.

178
00:09:37,970 --> 00:09:39,589
Good for Leonard.

179
00:09:41,424 --> 00:09:42,496
Okay, g'night.

180
00:09:42,708 --> 00:09:44,158
No, no, wait, hold on.

181
00:09:44,369 --> 00:09:45,449
What's the matter?

182
00:09:46,051 --> 00:09:47,782
I don't know what the protocol is here.

183
00:09:49,695 --> 00:09:51,319
Do I stay? Do I leave?

184
00:09:51,841 --> 00:09:54,443
Do I wait to greet them
with a refreshing beverage?

185
00:09:57,273 --> 00:09:58,732
You're asking the wrong girl.

186
00:09:58,881 --> 00:10:00,800
I'm usually
on the other side of the tie.

187
00:10:21,849 --> 00:10:23,044
Hi, Leonard?

188
00:10:26,455 --> 00:10:28,781
It's me, Sheldon...

189
00:10:31,056 --> 00:10:32,437
In the living room.

190
00:10:32,958 --> 00:10:35,296
I just... I wanted you
to know I saw the tie.

191
00:10:35,569 --> 00:10:36,823
Message received.

192
00:10:40,778 --> 00:10:41,973
You're welcome.

193
00:10:43,873 --> 00:10:44,876
You carry on.

194
00:10:45,028 --> 00:10:46,706
Give my best to Leslie.

195
00:11:25,661 --> 00:11:26,856
Big Boy...

196
00:11:52,029 --> 00:11:53,649
Someone touched my board.

197
00:11:57,408 --> 00:11:58,855
Oh, God, my board!

198
00:12:05,208 --> 00:12:06,786
Hey, what's the matter?

199
00:12:06,999 --> 00:12:09,180
My equations, someone's tampered
with my equations.

200
00:12:09,533 --> 00:12:11,019
- Are you sure?
- Of course I am.

201
00:12:11,154 --> 00:12:13,276
Look at the beta function
of quantum chromodynamics.

202
00:12:13,430 --> 00:12:14,993
The sign's been changed.

203
00:12:15,895 --> 00:12:16,895
Oh, yeah.

204
00:12:17,302 --> 00:12:19,480
But doesn't that fix the problem
you've been having?

205
00:12:19,608 --> 00:12:21,993
Are you insane?
Are you out of your mind?

206
00:12:22,125 --> 00:12:22,896
Are you-- Look!

207
00:12:23,046 --> 00:12:25,361
That fixes the problem
I've been having.

208
00:12:26,763 --> 00:12:27,919
You're welcome.

209
00:12:29,822 --> 00:12:31,171
You did this?

210
00:12:32,512 --> 00:12:35,174
I noticed it when I got up
to get a glass of water. So I fixed it.

211
00:12:35,309 --> 00:12:38,283
Now you can show that quarks are
asymptotically free at high energies.

212
00:12:38,403 --> 00:12:39,482
Pretty cool, huh?

213
00:12:41,805 --> 00:12:42,826
"Cool"?

214
00:12:44,067 --> 00:12:46,963
Listen, I've got to get to the lab.
Thanks for a great night.

215
00:12:48,713 --> 00:12:50,179
Thank you.
I'll see you at work.

216
00:12:50,696 --> 00:12:52,335
Hold on. Hold on!

217
00:12:53,294 --> 00:12:55,396
- What?
- Who told you you could touch my board?

218
00:12:56,162 --> 00:12:57,016
No one.

219
00:12:57,184 --> 00:12:59,317
I don't come into your house
and touch your board.

220
00:12:59,757 --> 00:13:02,008
There are no incorrect equations
on my board.

221
00:13:05,546 --> 00:13:06,798
Oh, that is so...

222
00:13:07,367 --> 00:13:08,331
so...

223
00:13:10,564 --> 00:13:14,036
Sorry, I've got to run. If you come up
with an adjective, text me.

224
00:13:17,717 --> 00:13:18,721
Inconsiderate.

225
00:13:18,864 --> 00:13:21,357
That is the adjective,
"inconsiderate. "

226
00:13:23,412 --> 00:13:26,246
You can stare at your board all day.
She's still going to be right.

227
00:13:26,438 --> 00:13:28,262
I'm not staring, I'm hauling.

228
00:13:30,786 --> 00:13:31,786
So...

229
00:13:32,083 --> 00:13:33,549
how's it going?

230
00:13:36,941 --> 00:13:38,174
Pretty good.

231
00:13:39,440 --> 00:13:42,646
Just pretty good?
I'd think you were doing very good.

232
00:13:44,456 --> 00:13:46,888
Pretty, very...
there's really no objective scale

233
00:13:47,041 --> 00:13:50,091
for delineating variations of "good."
Why do you ask?

234
00:13:50,718 --> 00:13:52,114
Well, a little bird told me

235
00:13:52,249 --> 00:13:54,484
that you and Leslie
hooked up last night.

236
00:13:59,577 --> 00:14:02,033
So, is it serious?
Do you like her?

237
00:14:02,592 --> 00:14:03,632
I don't...

238
00:14:05,785 --> 00:14:07,719
That's really two different questions.

239
00:14:08,050 --> 00:14:09,169
I'm not...

240
00:14:09,426 --> 00:14:10,946
Sheldon, we have to go!

241
00:14:11,508 --> 00:14:14,672
You're wound awfully tight for a man
who's just had sexual intercourse.

242
00:14:17,137 --> 00:14:20,003
All right, I'll talk to you later,
but I am so happy for you.

243
00:14:22,311 --> 00:14:23,314
Thank you.

244
00:14:26,581 --> 00:14:28,931
What did she mean
she's happy for me?

245
00:14:29,640 --> 00:14:31,291
Is she happy that
I'm seeing someone?

246
00:14:31,465 --> 00:14:33,422
Or is she happy because
she thinks that I am?

247
00:14:33,562 --> 00:14:36,374
Because anyone who cared for someone
would want them to be happy.

248
00:14:36,509 --> 00:14:40,155
Even if the reason for their happiness
made the first person unhappy.

249
00:14:40,912 --> 00:14:42,978
Because the second person,
though happy,

250
00:14:43,131 --> 00:14:46,162
is now romantically unavailable
to the first person.

251
00:14:46,776 --> 00:14:50,590
Do you realize I may have to share
a Nobel Prize with your booty call?

252
00:14:54,218 --> 00:14:55,218
You know what?

253
00:14:56,084 --> 00:14:57,457
I'm being ridiculous.

254
00:14:58,487 --> 00:15:00,123
Who cares what Penny thinks?

255
00:15:00,426 --> 00:15:02,717
Leslie is a terrific girl.
She's attractive.

256
00:15:02,837 --> 00:15:05,335
We like each other.
She's extremely intelligent...

257
00:15:05,490 --> 00:15:08,265
- She's not that intelligent.
- She fixed your equation.

258
00:15:08,535 --> 00:15:10,472
- She got lucky.
- You don't believe in luck.

259
00:15:10,627 --> 00:15:13,213
I don't have to believe
in it for her to be lucky.

260
00:15:14,313 --> 00:15:17,042
Regardless, I have a chance
at a real relationship with Leslie.

261
00:15:17,162 --> 00:15:18,527
I'm not going to pass that up

262
00:15:18,693 --> 00:15:20,912
for some hypothetical future
of happiness with a woman

263
00:15:21,337 --> 00:15:23,449
who may or may not
want me to be happy,

264
00:15:23,663 --> 00:15:26,419
with a woman who is
currently making me happy.

265
00:15:29,176 --> 00:15:30,626
I still don't care.

266
00:15:41,047 --> 00:15:44,367
Careful, Leonard. Liquid nitrogen,
320 degrees below zero.

267
00:15:53,156 --> 00:15:55,600
Why are you smashing
a flash-frozen banana?

268
00:15:55,812 --> 00:15:58,821
Because I got a bowl of Cheerios
and I couldn't find a knife.

269
00:16:00,973 --> 00:16:01,973
So anyway...

270
00:16:03,478 --> 00:16:04,478
Hello.

271
00:16:07,366 --> 00:16:08,693
What are you doing?

272
00:16:09,424 --> 00:16:10,986
Just extending the intimacy.

273
00:16:11,494 --> 00:16:13,613
Do you want to slip over
to the radiation lab

274
00:16:13,840 --> 00:16:16,090
and share a decontamination shower?

275
00:16:20,727 --> 00:16:23,408
What exactly do you think's
going on between us?

276
00:16:24,780 --> 00:16:28,302
I'm not sure, but I think I'm about
to discover how the banana felt.

277
00:16:32,766 --> 00:16:35,024
Listen,
neither of us are neuroscientists,

278
00:16:35,274 --> 00:16:37,177
but we both understand
the biochemistry of sex.

279
00:16:37,331 --> 00:16:38,766
I mean, dopamine in our brains

280
00:16:38,901 --> 00:16:40,859
is released across synapses,
causing pleasure.

281
00:16:40,983 --> 00:16:43,574
You stick electrodes in a rat's brain,
give him an orgasm button,

282
00:16:43,694 --> 00:16:45,733
he'll push that thing
until he starves to death.

283
00:16:46,992 --> 00:16:48,354
Well, who wouldn't?

284
00:16:48,845 --> 00:16:50,870
Well, the only difference
between us and the rat

285
00:16:51,001 --> 00:16:53,278
is that you can't stick an
electrode in our hypothalamus.

286
00:16:53,433 --> 00:16:54,882
That's where you come in.

287
00:16:57,100 --> 00:16:59,743
Yeah, well, I'm just glad
to be a part of it.

288
00:17:01,227 --> 00:17:02,404
So what happens now?

289
00:17:02,616 --> 00:17:06,173
I don't know about your sex drive,
but I'm probably good till New Year's.

290
00:17:11,740 --> 00:17:13,302
- Thank you.
- Thank you.

291
00:17:16,825 --> 00:17:18,465
You want to make plans for New Year's?

292
00:17:18,667 --> 00:17:20,037
Please.
You're smothering me.

293
00:17:23,253 --> 00:17:24,873
Look. It's Dr. Stud!

294
00:17:26,930 --> 00:17:27,952
Dr. What?

295
00:17:28,130 --> 00:17:30,775
The blogosphere is a-buzzin'
with news of you and Leslie Winkle

296
00:17:30,968 --> 00:17:33,130
making <i>eine kleine
bang-bang musik</i>.

297
00:17:35,031 --> 00:17:36,420
How did it get on the Internet?

298
00:17:36,570 --> 00:17:37,792
I put it there.

299
00:17:38,988 --> 00:17:40,453
How did you know about it?

300
00:17:40,700 --> 00:17:42,166
A little bird told us.

301
00:17:43,461 --> 00:17:46,011
Apparently,
you are a magnificent beast.

302
00:17:49,075 --> 00:17:50,425
That part's true.

303
00:17:55,678 --> 00:17:57,878
I think I may have
misjudged this restaurant.

304
00:17:58,124 --> 00:18:00,103
- No kidding.
- I don't want to go out on a limb,

305
00:18:00,296 --> 00:18:03,205
but I think we may be looking
at my new Tuesday hamburger.

306
00:18:04,688 --> 00:18:07,542
Your old Tuesday hamburger
will be so brokenhearted.

307
00:18:09,140 --> 00:18:11,894
Way ahead of you. I was thinking
of moving Big Boy to Thursdays,

308
00:18:12,048 --> 00:18:13,897
and just dropping Souplantation.

309
00:18:15,345 --> 00:18:16,345
Really?

310
00:18:16,836 --> 00:18:19,862
The name always confused me anyway.
Souplantation.

311
00:18:20,211 --> 00:18:21,661
You can't grow soup.

312
00:18:25,137 --> 00:18:26,880
So, how's everything?

313
00:18:27,256 --> 00:18:30,019
Terrific. You'll be happy to know
that I plan to come here

314
00:18:30,199 --> 00:18:32,144
every Tuesday night
for the foreseeable future.

315
00:18:32,272 --> 00:18:33,293
Really?

316
00:18:35,956 --> 00:18:38,695
Who do I speak to about
permanently reserving this table?

317
00:18:39,760 --> 00:18:41,824
I don't know...
A psychiatrist?

318
00:18:43,988 --> 00:18:44,988
So, hey,

319
00:18:45,770 --> 00:18:47,815
how are things with you and Leslie?

320
00:18:48,886 --> 00:18:51,414
To be honest, I don't think
it's going to work out.

321
00:18:53,021 --> 00:18:54,313
Oh, that's too bad.

322
00:18:54,778 --> 00:18:55,955
Hey, don't worry.

323
00:18:56,129 --> 00:18:59,332
I'm sure there's someone out there
who's just right for you.

324
00:19:02,434 --> 00:19:04,035
What did she mean by that?!

325
00:19:05,268 --> 00:19:07,185
Was that just a generic platitude,

326
00:19:07,339 --> 00:19:09,287
or was that a subtle bid
for attention?

327
00:19:11,478 --> 00:19:14,369
You know why this hamburger
surpasses the Big Boy?

328
00:19:15,208 --> 00:19:18,231
This is a single-decker hamburger,
whereas the Big Boy is a double-decker.

329
00:19:18,405 --> 00:19:21,665
This has a much more satisfying
meat-to-bun-to-condiment ratio.

330
00:19:23,202 --> 00:19:24,777
Are you even listening to me?

331
00:19:24,989 --> 00:19:28,604
Of course, I'm listening.
Blah, blah, hopeless Penny delusion...

332
00:19:31,400 --> 00:19:32,459
Okay, then.

333
00:19:34,967 --> 00:19:32,459
You know, you can grow
the ingredients for soup.

