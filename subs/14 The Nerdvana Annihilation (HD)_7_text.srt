1
00:00:01,882 --> 00:00:04,777
This sandwich is
an unmitigated disaster.

2
00:00:05,530 --> 00:00:09,167
I asked for turkey and roast beef
with lettuce and Swiss on whole wheat.

3
00:00:09,327 --> 00:00:11,464
- What did they give you?
- Turkey and roast beef

4
00:00:11,584 --> 00:00:13,683
with Swiss and lettuce on whole wheat.

5
00:00:19,469 --> 00:00:22,054
It's the right ingredients,
but in the wrong order.

6
00:00:23,142 --> 00:00:24,389
In a proper sandwich,

7
00:00:24,551 --> 00:00:26,312
the cheese is adjacent to the bread

8
00:00:26,432 --> 00:00:29,112
to create a moisture barrier
against the lettuce.

9
00:00:30,031 --> 00:00:33,149
They might as well have dragged
this thing through a car wash.

10
00:00:33,845 --> 00:00:37,027
- I don't believe it.
- I know. It's basic culinary science.

11
00:00:37,653 --> 00:00:40,155
Some guy is auctioning off
a miniature Time Machine prop

12
00:00:40,316 --> 00:00:42,741
from the original film,
and no one is bidding on it.

13
00:00:42,902 --> 00:00:45,661
A time machine
from the movie <i>The Time Machine</i>?

14
00:00:46,144 --> 00:00:48,582
No. A time machine
from <i>Sophie's Choice</i>.

15
00:00:49,306 --> 00:00:51,972
Boy, Sophie could have used
a time machine in that movie.

16
00:00:52,092 --> 00:00:53,858
Did you see it? It's rough.

17
00:00:55,546 --> 00:00:58,060
- That's cool.
- It's only $800?

18
00:00:58,180 --> 00:00:59,748
Yeah. And that's my bid.

19
00:00:59,868 --> 00:01:01,302
You bid $800?

20
00:01:01,463 --> 00:01:04,368
It was a spur-of-the-moment thing.
I figured it would go for thousands,

21
00:01:04,488 --> 00:01:06,389
and I just wanted to be a part of it.

22
00:01:06,550 --> 00:01:08,429
There's only 30 sec left in the auction.

23
00:01:08,549 --> 00:01:09,685
Do you have $800?

24
00:01:09,846 --> 00:01:12,062
Not to blow on a miniature time machine.

25
00:01:12,228 --> 00:01:14,521
Don't worry. People wait
until the last second to bid,

26
00:01:14,641 --> 00:01:16,608
and they swoop in and get it.
It's called sniping.

27
00:01:16,769 --> 00:01:17,819
15 seconds.

28
00:01:17,979 --> 00:01:19,582
Come on, snipers.

29
00:01:21,167 --> 00:01:22,672
10, 9,

30
00:01:23,276 --> 00:01:25,205
- 8...
- Where are your snipers?

31
00:01:25,572 --> 00:01:26,594
- 5...
- Snipe.

32
00:01:26,714 --> 00:01:27,737
- 4...
- Snipe.

33
00:01:27,857 --> 00:01:28,880
- 3...
- Snipe!

34
00:01:29,000 --> 00:01:30,137
- 2...
- Snipe!

35
00:01:30,257 --> 00:01:31,070
1...

36
00:01:32,225 --> 00:01:33,251
Congratulations!

37
00:01:33,411 --> 00:01:35,878
You are the proud owner
of a miniature time machine!

38
00:01:37,331 --> 00:01:38,696
You lucky duck.

39
00:01:39,125 --> 00:01:40,709
I wonder why no one else bid.

40
00:01:40,829 --> 00:01:43,299
This is a classic piece
of sci-fi movie memorabilia.

41
00:01:43,419 --> 00:01:46,763
- Yeah, I know! I still can't afford it.
- Why don't we share it?

42
00:01:46,929 --> 00:01:49,933
We'll each put in 200 bucks,
we'll take turns having it in our house.

43
00:01:50,094 --> 00:01:51,760
A time-share time machine.

44
00:01:53,351 --> 00:01:54,665
I'm in. Sheldon?

45
00:01:54,785 --> 00:01:55,941
Need you ask?

46
00:01:56,551 --> 00:01:59,084
I still don't understand
why no one else bid.

47
00:02:08,153 --> 00:02:10,609
I understand
why no one else bid.

48
00:02:12,491 --> 00:02:14,970
The Big Bang Theory
Season 1 Episode 14 - The Nerdvana Annihilation

49
00:02:16,417 --> 00:02:19,172
Transcript: swsub.com
Sync: SerJo

50
00:02:39,120 --> 00:02:41,546
Did the listing actually
say "miniature"?

51
00:02:43,759 --> 00:02:45,187
I just assumed.

52
00:02:46,670 --> 00:02:49,835
Who sells a full-size
time machine for $800?

53
00:02:50,757 --> 00:02:53,625
In a Venn diagram,
that would be an individual located

54
00:02:53,745 --> 00:02:56,821
within the intersection of the sets
"No longer want my time machine"

55
00:02:56,941 --> 00:02:58,259
and "Need $800."

56
00:02:59,849 --> 00:03:01,677
It's actually a tremendous bargain.

57
00:03:01,797 --> 00:03:05,054
Even with shipping, it works out
to less than four dollars a pound.

58
00:03:06,225 --> 00:03:07,958
Cocktail shrimp are $12.50.

59
00:03:10,737 --> 00:03:12,642
How are we going to get it upstairs?

60
00:03:14,072 --> 00:03:16,830
If we take the dish off,
it might fit in the elevator.

61
00:03:16,991 --> 00:03:19,371
Yes, but the elevator
has been broken for 2 years.

62
00:03:19,491 --> 00:03:20,941
I've been meaning to ask you.

63
00:03:21,061 --> 00:03:23,338
Do you think we should
make a call about that?

64
00:03:24,958 --> 00:03:27,974
Not necessary.
I have a Master's in engineering.

65
00:03:28,094 --> 00:03:30,845
I remotely repair satellites
on a regular basis.

66
00:03:31,224 --> 00:03:33,853
I troubleshoot space shuttle payloads.

67
00:03:34,627 --> 00:03:37,059
When the Mars Rover started
pulling to the left,

68
00:03:37,179 --> 00:03:39,360
I performed a front-end alignment

69
00:03:39,480 --> 00:03:41,744
from 62 million miles away.

70
00:03:51,857 --> 00:03:53,533
No. That baby's broken.

71
00:03:56,492 --> 00:03:57,857
Come on, guys! Push!

72
00:03:57,977 --> 00:04:01,311
If I push any harder,
I'm going to give birth to my colon.

73
00:04:02,360 --> 00:04:04,274
I can't feel my fingers. Hurry up!

74
00:04:04,394 --> 00:04:06,749
It's the same amount of work
no matter how fast you go.

75
00:04:06,869 --> 00:04:08,501
- Basic physics.
- Sheldon?

76
00:04:09,127 --> 00:04:12,491
If my fingers ever work again,
I've got a job for the middle one.

77
00:04:14,180 --> 00:04:16,351
- Hey, guys.
- Hi, Penny.

78
00:04:19,311 --> 00:04:20,707
Take a break, guys.

79
00:04:22,193 --> 00:04:25,483
- What are you doing?
- We're just moving something upstairs.

80
00:04:25,808 --> 00:04:27,236
- What is it?
- Just...

81
00:04:27,935 --> 00:04:29,497
You know. Time machine.

82
00:04:30,862 --> 00:04:32,741
Neat. But I really got to get to work.

83
00:04:32,901 --> 00:04:35,827
- Give us a few minutes.
- I don't have them. I'm running late.

84
00:04:35,987 --> 00:04:37,643
Then, I have a simple solution.

85
00:04:37,763 --> 00:04:39,891
Go up to the roof.
Hop over to the next building.

86
00:04:40,011 --> 00:04:42,443
There's a small gap, don't look down
if you're subject to vertigo,

87
00:04:42,563 --> 00:04:44,126
and use their stairwell.

88
00:04:44,888 --> 00:04:46,087
You're joking, right?

89
00:04:46,247 --> 00:04:48,480
I never joke when it comes to vertigo.

90
00:04:50,593 --> 00:04:53,169
Damn. Okay, I'll just take the roof.

91
00:04:53,922 --> 00:04:55,942
If you wait for us
to set up the time machine,

92
00:04:56,062 --> 00:04:58,076
I can drop you off at work yesterday.

93
00:04:59,970 --> 00:05:01,978
Time travel joke.
It's not... never mind.

94
00:05:04,231 --> 00:05:06,555
For what it's worth,
I thought it was humorous.

95
00:05:10,496 --> 00:05:11,863
Let's just do this.

96
00:05:12,201 --> 00:05:14,582
- You guys ready to push?
- In a minute.

97
00:05:14,702 --> 00:05:16,616
Howard stepped outside to throw up.

98
00:05:22,325 --> 00:05:24,081
I don't know
what you were worried about.

99
00:05:24,201 --> 00:05:26,043
I think it really works in the room.

100
00:05:28,373 --> 00:05:31,228
It is by far the coolest thing
I have ever owned.

101
00:05:31,348 --> 00:05:34,260
The exact time machine
that carried actor Rod Taylor

102
00:05:34,420 --> 00:05:37,308
from Victorian England
into the post-apocalyptic future,

103
00:05:37,428 --> 00:05:39,584
where society had splintered
into two factions:

104
00:05:39,704 --> 00:05:42,374
the subterranean Morlocks,
who survived by feasting

105
00:05:42,494 --> 00:05:44,990
on the flesh of the gentle
surface-dwelling Eloi.

106
00:05:47,253 --> 00:05:49,520
Talk about your chick magnets.

107
00:05:52,966 --> 00:05:54,934
The guy who lives next to me
is always like,

108
00:05:55,054 --> 00:05:58,401
"I have a Jacuzzi on my balcony.
I have a Jacuzzi on my balcony."

109
00:05:58,521 --> 00:05:59,892
But wait until I tell him,

110
00:06:00,012 --> 00:06:02,112
"I've got a time machine
on my balcony.

111
00:06:03,783 --> 00:06:06,196
Stuff that in your Speedoes,
Jacuzzi Bob."

112
00:06:07,537 --> 00:06:10,268
Gentlemen, we said we'd take turns,
but I think you'll agree

113
00:06:10,388 --> 00:06:13,007
that practicality
dictates it remain here.

114
00:06:13,336 --> 00:06:15,621
You can't keep it here.
What if I meet a girl and say,

115
00:06:15,741 --> 00:06:17,507
"You wanna come up and see
my time machine?

116
00:06:17,627 --> 00:06:19,945
It's at my friends' house."
How lame is that?

117
00:06:20,622 --> 00:06:21,764
He's got a point.

118
00:06:21,884 --> 00:06:25,055
I think we're going to need some ground
rules; in addition to the expected

119
00:06:25,175 --> 00:06:27,933
"no shoes in the time machine,"
and "no eating in the time machine,"

120
00:06:28,053 --> 00:06:31,928
I propose we add "pants must be worn
at all times in the time machine."

121
00:06:33,025 --> 00:06:34,015
Seconded.

122
00:06:35,262 --> 00:06:37,033
I was going to put down a towel.

123
00:06:40,787 --> 00:06:42,298
I still want it on my balcony.

124
00:06:42,418 --> 00:06:44,748
I say we move it on a bi-monthly basis.

125
00:06:44,908 --> 00:06:47,876
- That sounds fair.
- Bi-monthly is an ambiguous term.

126
00:06:48,036 --> 00:06:49,877
It's every other month or twice a month?

127
00:06:50,038 --> 00:06:51,703
- Twice a month.
- Then no.

128
00:06:51,823 --> 00:06:53,620
- Okay, every other month.
- No.

129
00:06:55,300 --> 00:06:56,759
You can't be selfish.

130
00:06:56,920 --> 00:06:59,262
We all paid for it,
so it belongs to all of us.

131
00:06:59,423 --> 00:07:01,972
Now get out of the way
so I can sit in my time machine.

132
00:07:12,019 --> 00:07:15,924
I am setting the dials
for March 10, 1876.

133
00:07:16,272 --> 00:07:18,630
Good choice, Alexander Graham Bell
invents the telephone

134
00:07:18,750 --> 00:07:20,840
- and calls out for Dr. Watson.
- Wait a minute.

135
00:07:20,960 --> 00:07:23,536
- I'd want to see that, too.
- So when it's your turn, you can.

136
00:07:23,696 --> 00:07:25,500
But if we all go back to the same point,

137
00:07:25,620 --> 00:07:28,500
Bell's lab is going to get very crowded.
He'll know something's up.

138
00:07:29,651 --> 00:07:31,668
Also since the time machine
doesn't move in space,

139
00:07:31,829 --> 00:07:33,953
you'll end up in 1876 Pasadena.

140
00:07:34,073 --> 00:07:36,606
And even if you can make it to Boston,
are you going knock on the door

141
00:07:36,726 --> 00:07:38,803
and say to Mrs. Bell,
"Big fan of your husband.

142
00:07:38,923 --> 00:07:41,146
"Can I come in and watch him
invent the telephone?"

143
00:07:41,839 --> 00:07:44,474
Mrs. Bell was deaf.
She's not even going to hear you knock.

144
00:07:45,819 --> 00:07:48,384
I have a solution.
First, go into the future

145
00:07:48,504 --> 00:07:51,313
- and obtain a cloaking device.
- How far into the future?

146
00:07:51,474 --> 00:07:54,227
If I remember correctly, Captain Kirk
will steal a cloaking device

147
00:07:54,347 --> 00:07:57,328
from the Romulans on Stardate 5027.3,

148
00:07:57,448 --> 00:08:00,689
which would be January 10, 2328
by pre-Federation reckoning.

149
00:08:03,324 --> 00:08:07,019
I am setting the dials
for January 10, 2328.

150
00:08:07,628 --> 00:08:10,009
Here we go into the future.

151
00:08:31,636 --> 00:08:32,684
That was fun.

152
00:08:34,042 --> 00:08:34,941
My turn!

153
00:08:36,496 --> 00:08:37,966
Okay, first of all,

154
00:08:38,086 --> 00:08:40,588
what you call a gap
was nearly three feet wide.

155
00:08:40,708 --> 00:08:42,802
- I slipped and skinned my knee.
- Are you okay?

156
00:08:43,758 --> 00:08:46,594
Second of all, the door to the stairwell
of the other building was locked,

157
00:08:46,714 --> 00:08:49,329
so I had to go down the fire escape,
which ends on the 3d floor,

158
00:08:49,490 --> 00:08:52,465
forcing me to crawl through the window
of a lovely Armenian family

159
00:08:52,585 --> 00:08:54,452
who insisted I stay for lunch.

160
00:08:55,804 --> 00:08:58,465
- That doesn't sound too bad.
- It was eight courses of lamb,

161
00:08:58,585 --> 00:09:00,680
and they tried to fix me up
with their son.

162
00:09:02,894 --> 00:09:04,094
- Sorry.
- Not done.

163
00:09:04,255 --> 00:09:06,930
By the time I finally got to work,
they'd given my shift away.

164
00:09:07,091 --> 00:09:10,435
That's right. I lost an entire day's pay
thanks to this.

165
00:09:10,595 --> 00:09:12,270
- This...
- Time machine.

166
00:09:15,879 --> 00:09:18,355
The lights flash and the dish spins.
You want to try it?

167
00:09:19,688 --> 00:09:20,987
I don't want to try it!

168
00:09:21,148 --> 00:09:23,207
My God, you are grown men!

169
00:09:23,512 --> 00:09:27,265
How can you waste your lives
with these stupid toys and costumes

170
00:09:27,385 --> 00:09:30,121
and comic books
and now that?! That...

171
00:09:30,282 --> 00:09:31,779
Again. Time machine.

172
00:09:34,202 --> 00:09:36,979
Please, it's not a time machine.
If anything, it looks like something

173
00:09:37,099 --> 00:09:39,631
Elton John would drive
through the Everglades.

174
00:09:45,486 --> 00:09:47,467
It only moves in time.

175
00:09:47,587 --> 00:09:50,092
It would be worse than useless
in a swamp.

176
00:09:55,667 --> 00:09:57,363
Pathetic! All of you!

177
00:09:57,483 --> 00:09:59,168
Completely pathetic!

178
00:10:04,568 --> 00:10:05,539
My turn!

179
00:10:23,236 --> 00:10:25,217
Leonard, it's 2:00 in the morning.

180
00:10:26,376 --> 00:10:27,194
So?

181
00:10:27,355 --> 00:10:28,991
So it's my turn.

182
00:10:31,956 --> 00:10:34,443
Why did you set it
for the day before yesterday?

183
00:10:34,836 --> 00:10:36,722
Because I want to go back
and keep myself

184
00:10:36,842 --> 00:10:38,330
from getting a time machine.

185
00:10:38,490 --> 00:10:39,398
You can't.

186
00:10:39,518 --> 00:10:41,760
If you were to prevent yourself
from buying it in the past,

187
00:10:41,880 --> 00:10:44,141
you wouldn't have it available
in the present to travel back

188
00:10:44,261 --> 00:10:46,629
and stop yourself from buying it.
Ergo, you would still have it.

189
00:10:46,791 --> 00:10:49,099
This is a classic rookie
time-travel mistake.

190
00:10:52,510 --> 00:10:55,556
Can I go back and prevent you
from explaing that to me?

191
00:10:56,294 --> 00:10:57,320
Same paradox.

192
00:10:57,440 --> 00:11:00,143
If you were to travel back in time
and, say, knock me unconscious,

193
00:11:00,304 --> 00:11:02,574
you would not then have the conversation
that irritated you,

194
00:11:02,694 --> 00:11:05,399
motivating you to go back
and knock me unconscious.

195
00:11:06,662 --> 00:11:09,069
What if I knocked you
unconscious right now?

196
00:11:10,544 --> 00:11:12,240
It won't change the past.

197
00:11:14,082 --> 00:11:16,368
But it'd make the present so much nicer.

198
00:11:17,745 --> 00:11:19,603
Are you upset about something?

199
00:11:20,286 --> 00:11:21,925
What was your first clue?

200
00:11:22,812 --> 00:11:24,552
Well, it was a number of things.

201
00:11:24,672 --> 00:11:25,938
First, the late hour.

202
00:11:26,058 --> 00:11:28,057
Then your demeanor
seems very low-energy,

203
00:11:28,177 --> 00:11:30,310
- plus your irritability.
- Yes, I'm upset!

204
00:11:31,162 --> 00:11:33,448
I don't usually pick
up on those things.

205
00:11:34,113 --> 00:11:35,158
Good for me.

206
00:11:38,436 --> 00:11:39,562
Good for you.

207
00:11:40,534 --> 00:11:41,391
Wait.

208
00:11:43,514 --> 00:11:45,814
Did you want to talk about
what's bothering you?

209
00:11:47,130 --> 00:11:49,859
- I don't know. Maybe.
- I'm on fire tonight.

210
00:11:55,383 --> 00:11:56,632
Here's the thing.

211
00:11:57,195 --> 00:12:01,045
Girls like Penny never end up
with guys who own time machines.

212
00:12:01,674 --> 00:12:02,763
I disagree.

213
00:12:03,039 --> 00:12:05,414
Your inability to
successfully woo Penny

214
00:12:05,534 --> 00:12:08,053
long predates your acquisition
of the time machine.

215
00:12:10,750 --> 00:12:12,859
That failure clearly
stands on its own.

216
00:12:14,509 --> 00:12:17,660
- Thanks for pointing it out.
- In addition, your premise is flawed.

217
00:12:17,782 --> 00:12:20,359
In the original film,
Rod Taylor got Yvette Mimieux

218
00:12:20,479 --> 00:12:22,142
with that very time machine.

219
00:12:22,302 --> 00:12:24,806
In <i>Back to the Future</i>,
Marty McFly got the opportunity

220
00:12:24,926 --> 00:12:27,530
to hook up with his extremely
attractive young mother.

221
00:12:30,496 --> 00:12:33,188
- Those are movies.
- Of course they're movies.

222
00:12:33,308 --> 00:12:35,319
Were you expecting me to come up
with an example

223
00:12:35,439 --> 00:12:37,515
involving a real-life time machine?

224
00:12:39,788 --> 00:12:40,945
That's absurd.

225
00:12:43,186 --> 00:12:44,482
Come on, guys, push!

226
00:12:44,750 --> 00:12:47,916
If I push any harder, I'm going
to give birth to my colon.

227
00:12:50,142 --> 00:12:51,852
- Hey, guys.
- Hi, Penny.

228
00:12:55,207 --> 00:12:56,809
Take a break, guys!

229
00:12:57,913 --> 00:13:01,913
- What are you doing?
- You know, just moving a time machine.

230
00:13:03,301 --> 00:13:05,394
Neat, but I really gotta get to work.

231
00:13:05,991 --> 00:13:07,198
No problem.

232
00:13:13,750 --> 00:13:15,077
Hang on.

233
00:13:16,338 --> 00:13:18,230
But... what about your time machine?

234
00:13:18,350 --> 00:13:20,951
Some things are more
important than toys.

235
00:13:29,606 --> 00:13:30,770
I'm scared.

236
00:13:31,277 --> 00:13:33,108
Don't worry, baby. I've got you.

237
00:13:55,083 --> 00:13:56,854
It's still my turn.

238
00:14:02,434 --> 00:14:03,737
What are you doing?

239
00:14:04,997 --> 00:14:06,741
I'm packing up all my collectibles

240
00:14:06,861 --> 00:14:09,434
and taking them down
to the comic book store to sell.

241
00:14:09,775 --> 00:14:11,408
Was that really necessary?

242
00:14:11,669 --> 00:14:14,321
If you need money,
you can always sell blood.

243
00:14:15,789 --> 00:14:16,913
And semen.

244
00:14:18,751 --> 00:14:20,207
This is not about money.

245
00:14:20,441 --> 00:14:21,795
We brought food!

246
00:14:22,005 --> 00:14:24,821
Lox and bagels, the
breakfast of time-travelers.

247
00:14:25,398 --> 00:14:27,893
Terrific. Does anyone want to buy
my share of the time machine?

248
00:14:28,054 --> 00:14:30,281
- Why?
- I don't want it anymore.

249
00:14:30,401 --> 00:14:32,606
- Why?
- Just... personal reasons.

250
00:14:32,767 --> 00:14:35,818
My Spidey sense tells me this
has something to do with Penny.

251
00:14:36,237 --> 00:14:38,584
Look, do you want to
buy me out or not?

252
00:14:38,704 --> 00:14:40,985
I'll give you $100,
which will make me half-owner

253
00:14:41,105 --> 00:14:42,623
and we'll put it on my balcony.

254
00:14:42,743 --> 00:14:45,535
Screw his balcony. I'll give you
$120 and we'll put it in my garage.

255
00:14:45,834 --> 00:14:47,538
I paid $200 for my share.

256
00:14:47,698 --> 00:14:50,275
Dude, everybody knows a time
machine loses half its value

257
00:14:50,395 --> 00:14:52,385
the minute you drive
it off the lot.

258
00:14:52,763 --> 00:14:55,503
I'll go for $200. That time
machine stays right where it is.

259
00:14:55,664 --> 00:14:59,464
$300 and I'll throw in my original
1979 Mattel Millennium Falcon

260
00:14:59,584 --> 00:15:01,301
with real light speed sound effects.

261
00:15:01,658 --> 00:15:05,028
No. No more toys or action figures

262
00:15:05,281 --> 00:15:08,360
or props or replicas
or costumes or robots

263
00:15:08,480 --> 00:15:10,165
or Darth Vader voice changers.

264
00:15:10,285 --> 00:15:11,811
I'm getting rid of all of it.

265
00:15:11,972 --> 00:15:14,282
You can't do that. Look
what you've created here.

266
00:15:14,402 --> 00:15:16,106
It's like nerdvana.

267
00:15:19,431 --> 00:15:22,489
More importantly,
you have a Darth Vader voice changer?

268
00:15:24,211 --> 00:15:25,503
Not for long.

269
00:15:25,623 --> 00:15:27,311
I call dibs on <i>The Golden Age Flash</i>.

270
00:15:27,431 --> 00:15:30,072
I need that to complete my
<i>Justice Society of America</i> collection.

271
00:15:30,192 --> 00:15:32,525
- Too bad. I called dibs.
- You can't just call dibs.

272
00:15:32,645 --> 00:15:35,126
I can and I did.
Look up "Dibs" in Wikipedia.

273
00:15:35,805 --> 00:15:37,728
Dibs doesn't apply
in a bidding war.

274
00:15:37,848 --> 00:15:39,255
It's not a bidding war.

275
00:15:39,416 --> 00:15:41,842
I'm selling it all to Larry
down at the comic book store.

276
00:15:42,002 --> 00:15:44,512
- Did Larry call dibs?
- Will you forget dibs?!

277
00:15:44,632 --> 00:15:46,957
He offered me a fair price
for the whole collection.

278
00:15:47,077 --> 00:15:50,906
- What's the number? I'll match it.
- I'll match it plus a thousand rupees.

279
00:15:51,333 --> 00:15:53,467
- What's the exchange rate?
- None of your business.

280
00:15:53,587 --> 00:15:54,980
Take it or leave it?

281
00:15:55,390 --> 00:15:57,671
Mom? My bar mitzvah bonds.
How much do I got?

282
00:15:57,896 --> 00:15:58,937
Thanks.

283
00:15:59,125 --> 00:16:01,500
I can go $2,600 and two trees in Israel.

284
00:16:03,835 --> 00:16:04,857
Forget it, guys.

285
00:16:04,977 --> 00:16:07,845
If I sell to one of you, the other two
are going to be really mad at me.

286
00:16:07,965 --> 00:16:10,255
Who cares? As long as you pick me!

287
00:16:11,205 --> 00:16:13,282
Okay, Leonard, put down the box.
Let's talk.

288
00:16:13,402 --> 00:16:15,327
Sorry, Raj. My mind is made up.

289
00:16:15,618 --> 00:16:16,435
No!

290
00:16:16,768 --> 00:16:18,796
I can't let you do this.

291
00:16:19,361 --> 00:16:20,839
Get out of my way.

292
00:16:24,477 --> 00:16:26,638
None shall pass!

293
00:16:31,759 --> 00:16:33,352
I did not want to do this,

294
00:16:33,689 --> 00:16:36,898
but I have here the rare mint
condition production error

295
00:16:37,018 --> 00:16:39,282
<i>Star Trek: The Next Generation</i>
Geordi La Forge

296
00:16:39,402 --> 00:16:41,860
without his visor
in the original packaging.

297
00:16:42,586 --> 00:16:44,519
If you do not get out of my way,

298
00:16:45,130 --> 00:16:46,392
I will open it.

299
00:16:47,729 --> 00:16:49,532
Okay, man. Be cool.

300
00:16:50,588 --> 00:16:52,079
We're all friends here.

301
00:16:53,457 --> 00:16:54,957
What the hell's going on?

302
00:16:55,167 --> 00:16:56,617
You hypocrite!

303
00:16:57,168 --> 00:16:57,983
What?

304
00:16:58,148 --> 00:17:00,458
Little Miss "grown-ups
don't play with toys."

305
00:17:00,578 --> 00:17:04,132
If I were to go into that apartment now,
would I not find Beanie Babies?

306
00:17:04,293 --> 00:17:07,122
Are you not an accumulator
of Care Bears and My Little Ponies?

307
00:17:07,242 --> 00:17:10,263
And who is that Japanese feline
I see frolicking on your shorts?

308
00:17:10,685 --> 00:17:12,588
Hello, Hello Kitty!

309
00:17:16,937 --> 00:17:19,228
Okay, look, if this is about yesterday,
Leonard,

310
00:17:19,348 --> 00:17:21,941
I am really sorry about what I said.
I was just upset.

311
00:17:22,103 --> 00:17:25,134
- I need to hear it.
- No, you didn't. You are a great guy

312
00:17:25,254 --> 00:17:28,148
and it is the things you love
that make you who you are.

313
00:17:28,822 --> 00:17:31,379
I guess that makes
me "large breasts."

314
00:17:35,784 --> 00:17:38,819
Still, I think it's time for
me to get rid of this stuff

315
00:17:38,939 --> 00:17:41,010
and, you know,
move on with my life.

316
00:17:41,515 --> 00:17:43,356
- Really?
- Yeah.

317
00:17:45,143 --> 00:17:46,929
Well, good for you.

318
00:17:48,063 --> 00:17:49,020
Thanks.

319
00:17:51,167 --> 00:17:53,683
Do you want to...
I don't know, later...

320
00:17:53,862 --> 00:17:54,869
Excuse me.

321
00:17:55,564 --> 00:17:57,029
- Hey, Penny.
- Hi, Mike.

322
00:17:57,149 --> 00:17:59,090
- Ready to go?
- I just have to change.

323
00:17:59,210 --> 00:18:01,509
- I'll give you a hand.
- Stop it!

324
00:18:01,852 --> 00:18:02,773
Bye, guys.

325
00:18:08,116 --> 00:18:09,716
My turn on the time machine!

326
00:18:34,141 --> 00:18:35,264
It worked.

327
00:18:37,114 --> 00:18:38,651
It really worked.

328
00:18:39,819 --> 00:18:42,600
They said I was mad, but it worked!

329
00:18:51,044 --> 00:18:52,373
Not Morlocks!

330
00:18:52,777 --> 00:18:54,976
Not flesh-eating Morlocks!

331
00:18:55,446 --> 00:18:57,305
Help!

332
00:19:05,694 --> 00:19:07,746
Sheldon, are you okay?

333
00:19:08,421 --> 00:19:10,664
We have to get rid
of the time machine.

334
00:19:11,018 --> 00:19:13,227
It is a little big
for the living room, isn't it?

335
00:19:14,279 --> 00:19:16,520
Yeah, that's the problem. It's too big.

336
00:19:17,325 --> 00:19:20,023
I'm glad you agree.
I hired some guys to help us move it.

337
00:19:20,143 --> 00:19:21,457
Come on in, fellas.

338
00:19:25,931 --> 00:19:26,985
Morlocks!

339
00:19:27,221 --> 00:19:26,985
Eat him. Eat him.

