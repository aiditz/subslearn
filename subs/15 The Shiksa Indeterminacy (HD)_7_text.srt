1
00:00:01,480 --> 00:00:03,616
On the other hand,
some physicists are concerned

2
00:00:03,736 --> 00:00:05,710
that if this supercollider
actually works,

3
00:00:05,830 --> 00:00:07,923
it'll create a black hole
and swallow up the Earth,

4
00:00:08,043 --> 00:00:09,736
ending life as we know it.

5
00:00:09,856 --> 00:00:11,637
What a bunch of crybabies.

6
00:00:12,132 --> 00:00:13,832
No guts, no glory, man.

7
00:00:14,898 --> 00:00:16,263
Hey, check it out.

8
00:00:16,562 --> 00:00:19,543
The School of Pharmacology
is looking for volunteers.

9
00:00:19,663 --> 00:00:22,132
"We are testing a new medication
for social anxiety,

10
00:00:22,292 --> 00:00:26,176
"panic attacks, agoraphobia
and obsessive compulsive disorder."

11
00:00:27,945 --> 00:00:30,182
Why would they be looking
for test subjects here?

12
00:00:31,534 --> 00:00:32,861
I don't know, Raj.

13
00:00:32,981 --> 00:00:35,735
Maybe the comic book store
doesn't have a bulletin board.

14
00:00:39,373 --> 00:00:41,775
- What's going on?
- Hot girl in Sheldon's office.

15
00:00:42,938 --> 00:00:44,029
Sheldon's office?

16
00:00:46,289 --> 00:00:47,448
Is she lost?

17
00:00:48,700 --> 00:00:49,765
I don't think so.

18
00:00:49,885 --> 00:00:51,995
I followed her here
from the parking lot.

19
00:00:53,990 --> 00:00:55,523
Maybe she's his lawyer.

20
00:00:55,831 --> 00:00:58,018
Well, she's free to examine my briefs.

21
00:00:58,710 --> 00:01:01,588
I know, I'm disgusting.
I should be punished... by her.

22
00:01:01,749 --> 00:01:03,127
Look, I did it again.

23
00:01:04,117 --> 00:01:05,332
That should do it.

24
00:01:05,452 --> 00:01:06,999
Thank you for coming by.

25
00:01:13,587 --> 00:01:14,485
Hello.

26
00:01:16,091 --> 00:01:17,456
Hey, buddy...

27
00:01:19,715 --> 00:01:21,079
"Buddy..."?

28
00:01:21,977 --> 00:01:24,736
Sorry I'm late.
I'm working on a project

29
00:01:24,897 --> 00:01:27,472
that may take me up
on the next space shuttle.

30
00:01:28,051 --> 00:01:30,742
How can you be late?
I wasn't expecting you at all.

31
00:01:31,595 --> 00:01:34,329
Nobody ever expects me.
Sometimes you just look and...

32
00:01:34,490 --> 00:01:36,007
Bam! Howard Wolowitz.

33
00:01:38,201 --> 00:01:39,584
Are you gonna introduce us?

34
00:01:40,137 --> 00:01:41,753
All right. This is Missy.

35
00:01:42,076 --> 00:01:45,590
Missy, this is Leonard and Rajesh,
and you've already met Howard.

36
00:01:45,751 --> 00:01:48,576
- It's nice to meet you.
- You, too, as well, also.

37
00:01:51,446 --> 00:01:53,098
So, how do you two know each other?

38
00:01:53,943 --> 00:01:57,326
He once spent nine months
with my legs wrapped around his head.

39
00:02:00,837 --> 00:02:01,827
Excuse me?

40
00:02:02,957 --> 00:02:05,396
She's my twin sister.
She thinks she's funny,

41
00:02:05,516 --> 00:02:07,684
but frankly,
I've never been able to see it.

42
00:02:07,804 --> 00:02:10,490
That's because you have
no measurable sense of humor, Shelly.

43
00:02:10,817 --> 00:02:13,415
How exactly would one
measure a sense of humor?

44
00:02:13,742 --> 00:02:15,200
A humormometer?

45
00:02:18,241 --> 00:02:20,361
I think you're delightfully droll.

46
00:02:20,481 --> 00:02:22,959
Or, as the French say,
<i>trиs drфle</i>.

47
00:02:23,632 --> 00:02:25,130
So let me see if I got this.

48
00:02:25,644 --> 00:02:28,133
Leonard, Howard and...

49
00:02:28,294 --> 00:02:30,335
I'm sorry, what was your name again?

50
00:02:57,250 --> 00:02:58,129
Rajesh.

51
00:02:59,568 --> 00:03:02,035
The Big Bang Theory
Season 1 Episode 15 - The Shiksa Indeterminacy

52
00:03:02,702 --> 00:03:05,942
Transcript: swsub.com
Synch: SerJo

53
00:03:23,607 --> 00:03:25,794
So Missy, what brings you
all the way from Texas?

54
00:03:25,914 --> 00:03:28,157
Was it perhaps destiny?
I think it was destiny.

55
00:03:28,576 --> 00:03:30,892
My friend's getting married
at Disneyland tomorrow night.

56
00:03:31,053 --> 00:03:33,327
Destiny, thy name is Anaheim.

57
00:03:35,936 --> 00:03:39,192
And I had to drop off some papers
for Shelly to sign for my Dad's estate.

58
00:03:39,353 --> 00:03:42,862
The papers could've been mailed.
Mom sent you to spy on me, didn't she?

59
00:03:43,023 --> 00:03:45,323
I guess that's why
they call you a genius.

60
00:03:45,484 --> 00:03:47,876
They call me a genius
because I'm a genius.

61
00:03:49,054 --> 00:03:51,704
{\pos(192,200)}Tell Mom
that I currently weigh 165 pounds

62
00:03:51,865 --> 00:03:54,078
and that I'm having
regular bowel movements.

63
00:03:55,359 --> 00:03:56,876
Enjoy the wedding. Good-bye.

64
00:04:00,013 --> 00:04:03,508
If the wedding's not until tomorrow,
why don't you stay with us tonight?

65
00:04:03,669 --> 00:04:06,219
I don't think so.
Shelly doesn't like company.

66
00:04:06,379 --> 00:04:08,803
Even as a little boy,
he'd send his imaginary friends home

67
00:04:08,923 --> 00:04:10,181
at the end of the day.

68
00:04:10,655 --> 00:04:13,768
They were not friends.
They were imaginary colleagues.

69
00:04:14,688 --> 00:04:17,157
Look, you're here.
We have plenty of room.

70
00:04:17,277 --> 00:04:18,272
No, we don't.

71
00:04:18,433 --> 00:04:20,762
Come on, Shelly.
She's family.

72
00:04:21,306 --> 00:04:24,054
So what, I don't issue invitations
to your mother.

73
00:04:25,170 --> 00:04:27,907
It would be nice not to have
to drive to Anaheim in rush hour.

74
00:04:28,068 --> 00:04:29,838
And don't ever call me Shelly.

75
00:04:30,736 --> 00:04:32,537
So it's settled: you'll stay with us.

76
00:04:32,698 --> 00:04:35,859
Yeah, I'll walk you to your car.
You're in structure 3, level C, right?

77
00:04:40,011 --> 00:04:41,562
What just happened?

78
00:04:43,749 --> 00:04:45,618
So anyway, we're eight years old

79
00:04:45,738 --> 00:04:47,824
and Sheldon converts
my Easy Bake Oven

80
00:04:47,944 --> 00:04:49,929
to some kind
of high-powered furnace.

81
00:04:50,805 --> 00:04:52,076
Just classic.

82
00:04:53,608 --> 00:04:56,442
I needed a place to fire
ceramic semiconductor substrates

83
00:04:56,562 --> 00:04:58,354
for homemade integrated circuits.

84
00:04:58,582 --> 00:05:00,657
He was trying to build
some sort of armed robot

85
00:05:00,777 --> 00:05:02,190
to keep me out of his room.

86
00:05:02,507 --> 00:05:05,540
Made necessary by her insistence
on going into my room.

87
00:05:06,230 --> 00:05:08,762
Anyway, I go to make
those little corn muffins they give you.

88
00:05:08,882 --> 00:05:12,196
There's a big flash. Next thing you know
my eyebrows are gone.

89
00:05:13,617 --> 00:05:15,056
Not your eyebrows!

90
00:05:16,368 --> 00:05:18,780
Yep, I had to go through
the entire second grade

91
00:05:18,900 --> 00:05:21,294
with crooked eyebrows
my mom drew on.

92
00:05:21,658 --> 00:05:23,060
Is that what that was?

93
00:05:23,180 --> 00:05:27,175
I just assumed that the second grade
curriculum had rendered you quizzical.

94
00:05:30,463 --> 00:05:33,261
You left your underwear
in the dryer downstairs.

95
00:05:39,486 --> 00:05:40,813
Those are not mine.

96
00:05:42,402 --> 00:05:45,509
Really? They have
your little name label in them.

97
00:05:46,705 --> 00:05:48,981
Yeah, no, I do... I use those...

98
00:05:49,358 --> 00:05:52,835
just to polish up
my spearfishing equipment.

99
00:05:54,970 --> 00:05:56,147
I spearfish.

100
00:06:00,279 --> 00:06:02,835
When I'm not crossbow hunting,
I spearfish.

101
00:06:06,133 --> 00:06:08,216
This is Sheldon's twin sister, Missy.

102
00:06:08,376 --> 00:06:09,926
This is our neighbor, Penny.

103
00:06:10,087 --> 00:06:13,332
- You don't look that much alike.
- Can I get a hallelujah?

104
00:06:14,681 --> 00:06:16,768
Fraternal twins come
from two separate eggs.

105
00:06:16,888 --> 00:06:18,975
They are no more alike
than any other siblings.

106
00:06:21,806 --> 00:06:23,647
Hey, guess what.
I've been accepted

107
00:06:23,767 --> 00:06:27,567
as a test subject for a new miracle drug
to overcome pathological shyness.

108
00:06:28,354 --> 00:06:31,698
- Good for you, Raj.
- Yes, I'm very hopeful. Hello, Missy.

109
00:06:42,318 --> 00:06:44,711
They mentioned
there may be side effects.

110
00:06:46,835 --> 00:06:47,912
So, Missy,

111
00:06:48,701 --> 00:06:52,143
have you ever met a man
from the exotic subcontinent of India?

112
00:06:53,631 --> 00:06:55,972
Well, there's Dr. Patel at our church.

113
00:06:56,133 --> 00:06:57,890
Yes, Patel... good man.

114
00:06:58,343 --> 00:07:01,144
Do you like motorcycles?
'Cause I ride a hog.

115
00:07:01,985 --> 00:07:05,231
A hog? You have a two-cylinder scooter
with a basket in the front.

116
00:07:07,073 --> 00:07:09,359
You still have to wear a helmet.

117
00:07:11,073 --> 00:07:13,209
Have you ever heard
of the Kama Sutra?

118
00:07:13,329 --> 00:07:15,844
- The sex book.
- The Indian sex book.

119
00:07:16,320 --> 00:07:18,146
In other words,
if you "wonder, wonder

120
00:07:18,266 --> 00:07:20,931
who wrote the book of love,"
it was us.

121
00:07:23,169 --> 00:07:26,372
- Sheldon's sister is pretty cute, huh?
- I wasn't staring.

122
00:07:28,419 --> 00:07:31,187
I didn't say you were.
I just said she was cute.

123
00:07:33,258 --> 00:07:34,219
Maybe,

124
00:07:34,757 --> 00:07:36,971
if you like women who are tall...

125
00:07:38,190 --> 00:07:39,391
and perfect.

126
00:07:42,415 --> 00:07:44,549
Sheldon, why are you
ignoring your sister?

127
00:07:44,669 --> 00:07:47,724
I'm not ignoring my sister.
I'm ignoring all of you.

128
00:07:49,105 --> 00:07:50,360
I brought snacks!

129
00:07:50,765 --> 00:07:53,756
Oh, my, gherkins and...

130
00:07:54,637 --> 00:07:56,518
Onion dip. It's onion dip.

131
00:07:59,252 --> 00:08:00,823
We don't entertain much.

132
00:08:03,139 --> 00:08:05,433
Missy.
Do you enjoy pajamas?

133
00:08:06,667 --> 00:08:09,071
- I guess.
- We Indians invented them.

134
00:08:10,284 --> 00:08:11,522
You're welcome.

135
00:08:13,157 --> 00:08:15,684
Yeah, well, my people
invented circumcision.

136
00:08:20,604 --> 00:08:21,966
You're welcome.

137
00:08:23,697 --> 00:08:26,311
Missy, I'm gonna go get my nails done.
Do you want to come?

138
00:08:26,473 --> 00:08:28,681
God, yes.
Thanks.

139
00:08:29,162 --> 00:08:30,353
You're welcome.

140
00:08:31,552 --> 00:08:34,036
- Bye, guys.
- Bye, Missy, see you.

141
00:08:34,528 --> 00:08:37,195
- Goodbye, Leonard.
- Oh, yeah, no, bye, Penny.

142
00:08:39,201 --> 00:08:40,722
You two have to back off.

143
00:08:40,842 --> 00:08:42,745
Why should I back off?
You back off, dude.

144
00:08:42,905 --> 00:08:45,456
Excuse me, this is my apartment,
and she's my roommate's sister.

145
00:08:45,617 --> 00:08:48,303
- So what? You've already got Penny!
- How do I have Penny?

146
00:08:48,423 --> 00:08:50,294
In what universe do I have Penny?

147
00:08:51,691 --> 00:08:53,744
- So I can have Penny?
- Hell, no!

148
00:08:55,126 --> 00:08:57,335
Excuse me.
Can I interject something?

149
00:08:58,979 --> 00:09:02,181
I'm ordering pizza online.
Is everyone okay with pepperoni?

150
00:09:04,413 --> 00:09:07,470
- Sheldon, can I talk to you in private?
- I guess.

151
00:09:08,111 --> 00:09:10,357
Don't worry. I was going
to order you cheese-less.

152
00:09:10,477 --> 00:09:11,823
- Thank you.
- It's okay.

153
00:09:11,943 --> 00:09:14,949
Lactose intolerance
is nothing to be embarrassed about.

154
00:09:16,853 --> 00:09:19,694
I'm a fancy Indian man.
We invented pajamas.

155
00:09:20,622 --> 00:09:22,996
Hey, look at me.
I don't have a foreskin.

156
00:09:34,123 --> 00:09:37,383
Are you aware that your sister
is an incredibly attractive woman?

157
00:09:40,182 --> 00:09:42,425
She certainly has the symmetry
and low body fat

158
00:09:42,545 --> 00:09:44,760
that Western culture deems desirable.

159
00:09:44,880 --> 00:09:47,034
It's noteworthy,
at other points in history,

160
00:09:47,154 --> 00:09:49,154
heavier women
were the standard for beauty

161
00:09:49,274 --> 00:09:51,563
because their girth
suggested affluence.

162
00:09:52,521 --> 00:09:53,830
That's fascinating...

163
00:09:53,950 --> 00:09:56,688
I didn't say it was fascinating.
I said it was noteworthy.

164
00:09:57,183 --> 00:09:58,921
All right... noted.

165
00:09:59,973 --> 00:10:02,897
But my point is
that Koothrappali and Wolowitz,

166
00:10:03,778 --> 00:10:05,763
they're hitting on your sister.

167
00:10:09,750 --> 00:10:10,583
Okay.

168
00:10:10,914 --> 00:10:13,439
You know, I don't want to criticize
your rhetorical style,

169
00:10:13,559 --> 00:10:15,805
but we'd be a lot further along
in this conversation

170
00:10:15,925 --> 00:10:17,714
if you'd begun with that thought.

171
00:10:18,158 --> 00:10:19,466
- Great...
- I'm saying

172
00:10:19,627 --> 00:10:22,720
we took quite an unnecessary detour from
what I now understand to be your thesis.

173
00:10:22,881 --> 00:10:23,853
Whatever.

174
00:10:25,452 --> 00:10:27,265
You have to do something about it.

175
00:10:27,790 --> 00:10:30,228
- Why?
- Because she's your sister.

176
00:10:31,479 --> 00:10:32,852
I don't understand.

177
00:10:33,401 --> 00:10:35,795
Yes, we shared a uterus
for nine months,

178
00:10:35,915 --> 00:10:39,049
but since then we've pretty much
gone our own separate ways.

179
00:10:39,972 --> 00:10:42,280
Okay, consider this.

180
00:10:42,647 --> 00:10:44,226
With your father gone,

181
00:10:44,346 --> 00:10:46,034
it is your responsibility

182
00:10:46,376 --> 00:10:49,230
to make sure that Missy
chooses a suitable mate.

183
00:10:51,376 --> 00:10:52,867
I hadn't considered that.

184
00:10:53,914 --> 00:10:55,633
We do share DNA.

185
00:10:55,982 --> 00:10:58,381
So there is the possibility,
however remote,

186
00:10:58,541 --> 00:11:00,188
that resting in her loins

187
00:11:00,333 --> 00:11:03,785
is the potential for another individual
as remarkable as myself.

188
00:11:04,618 --> 00:11:06,222
Exactly.

189
00:11:06,842 --> 00:11:09,792
And you owe it to yourself
and to posterity

190
00:11:09,912 --> 00:11:13,646
to protect the genetic integrity
of your sister's future offspring.

191
00:11:15,776 --> 00:11:17,022
You're right.

192
00:11:17,321 --> 00:11:19,574
If someone wants to get
at Missy's Fallopian tubes,

193
00:11:19,694 --> 00:11:21,515
they'll have to go through me.

194
00:11:31,157 --> 00:11:33,030
I am Shiva the Destroyer!

195
00:11:33,150 --> 00:11:34,831
I will have the woman.

196
00:11:36,248 --> 00:11:39,657
I'm warning you,
I was judo champion at math camp.

197
00:11:40,431 --> 00:11:42,921
All right, now that's enough
juvenile squabbling.

198
00:11:43,041 --> 00:11:44,760
You stop it. Stop it, I say!

199
00:11:46,104 --> 00:11:48,095
I'm going to settle this right now.

200
00:11:48,443 --> 00:11:50,717
Neither of you are good enough
for my sister.

201
00:11:50,927 --> 00:11:52,518
Who are you to decide that?

202
00:11:52,679 --> 00:11:55,644
He's the man of his family.
You have to respect his wishes.

203
00:11:55,764 --> 00:11:57,862
- You're out, too, by the way.
- Say what?

204
00:11:58,137 --> 00:11:59,876
It's nothing personal. I'd just prefer

205
00:11:59,996 --> 00:12:02,212
if my future niece or nephew
didn't become flatulent

206
00:12:02,332 --> 00:12:04,381
every time they ate an eskimo pie.

207
00:12:10,790 --> 00:12:12,412
What are you so happy about?

208
00:12:12,730 --> 00:12:13,723
I'm not happy.

209
00:12:13,975 --> 00:12:16,397
It's the medication.
I can't stop smiling.

210
00:12:23,853 --> 00:12:27,055
Now that Leonard's made me aware
how high the genetic stakes are...

211
00:12:27,175 --> 00:12:30,415
we have to face the fact that none of
you are suitable mates for my sister.

212
00:12:30,885 --> 00:12:33,395
Wait a minute.
Leonard made you aware of that?

213
00:12:35,189 --> 00:12:37,243
We all make mistakes.
Let's move on.

214
00:12:38,039 --> 00:12:40,520
Excuse me, I think you're missing
a big opportunity here.

215
00:12:40,640 --> 00:12:41,426
How so?

216
00:12:41,587 --> 00:12:44,922
Everybody knows genetic diversity
produces the strongest offspring.

217
00:12:45,272 --> 00:12:48,427
Why not put a little mocha
in the family latte?

218
00:12:51,580 --> 00:12:53,099
In principle, you have a point.

219
00:12:53,273 --> 00:12:55,236
But as a practical matter,
need I remind you

220
00:12:55,404 --> 00:12:57,359
that it takes experimental
pharmaceuticals

221
00:12:57,479 --> 00:13:00,019
to simply enable you
to speak to the opposite sex.

222
00:13:01,150 --> 00:13:04,144
I think you're focusing
entirely too much on the drugs.

223
00:13:10,315 --> 00:13:11,700
Is it 'cause I'm Jewish?

224
00:13:11,820 --> 00:13:14,788
'Cause I'd kill my rabbi
with a pork chop to be with your sister.

225
00:13:21,557 --> 00:13:23,430
This has nothing to do with religion.

226
00:13:23,550 --> 00:13:26,087
This has to do with the fact
that you're a tiny, tiny man

227
00:13:26,207 --> 00:13:28,025
who still lives with his mother.

228
00:13:28,986 --> 00:13:31,183
Sheldon, you are really
being unreasonable.

229
00:13:31,478 --> 00:13:32,310
Am I?

230
00:13:32,860 --> 00:13:36,442
Here, eat this cheese without farting
and you can sleep with my sister.

231
00:13:38,214 --> 00:13:39,380
Oh, really?

232
00:13:46,541 --> 00:13:48,558
Shelly, can I speak to you for a minute?

233
00:13:48,678 --> 00:13:49,513
Alone.

234
00:13:50,402 --> 00:13:52,976
Why does everyone suddenly
want to talk to me alone?

235
00:13:53,096 --> 00:13:55,192
Usually nobody wants
to be alone with me.

236
00:14:01,705 --> 00:14:03,940
We all make mistakes.
Let's move on.

237
00:14:07,429 --> 00:14:10,056
I'm not even going to ask
why you're pimping me out for cheese.

238
00:14:11,885 --> 00:14:15,145
But since when do you care at all
about who I sleep with?

239
00:14:15,652 --> 00:14:17,608
Truthfully, I've never given it
any thought,

240
00:14:17,728 --> 00:14:21,192
but it has been pointed out to me
that you carry DNA of great potential.

241
00:14:21,504 --> 00:14:24,216
- What on earth are you talking about?
- Let me explain.

242
00:14:24,336 --> 00:14:27,427
You see,
I'm a superior genetic mutation,

243
00:14:27,547 --> 00:14:30,702
an improvement
on the existing mediocre stock.

244
00:14:32,578 --> 00:14:35,165
And what do you mean, "mediocre stock"?

245
00:14:35,326 --> 00:14:36,416
That would be you.

246
00:14:38,011 --> 00:14:41,222
But residing within you,
is the potential for another me.

247
00:14:41,550 --> 00:14:45,199
Perhaps even taller, smarter,
and less prone to freckling,

248
00:14:45,541 --> 00:14:47,429
a Sheldon 2.0, if you will.

249
00:14:49,094 --> 00:14:50,338
"Sheldon 2.0"?

250
00:14:50,548 --> 00:14:51,587
Exactly.

251
00:14:51,831 --> 00:14:55,563
Now, I am not saying that I should be
the sole decider of who you mate with.

252
00:14:55,683 --> 00:14:57,436
If you're not attracted to the suitor,

253
00:14:57,598 --> 00:14:59,688
the likelihood of conception
will be reduced.

254
00:14:59,850 --> 00:15:01,933
- You have got to be kidding me.
- Not at all.

255
00:15:02,224 --> 00:15:05,270
Frequent coitus dramatically
increases the odds of fertilization.

256
00:15:06,574 --> 00:15:08,546
Okay, Shelly, sit down.

257
00:15:12,044 --> 00:15:15,486
Now I've lived my whole life dealing
with the fact that my twin brother is,

258
00:15:15,739 --> 00:15:17,063
as mom puts it,

259
00:15:17,370 --> 00:15:19,708
"one of God's special, little people".

260
00:15:20,621 --> 00:15:23,026
I always thought I was more
like a cuckoo bird.

261
00:15:23,146 --> 00:15:27,113
You know, a superior creature whose egg
is placed in the nest of ordinary birds.

262
00:15:27,393 --> 00:15:29,722
Of course, the newly hatched cuckoo
eats all the food,

263
00:15:29,842 --> 00:15:32,172
leaving the ordinary siblings
to starve to death.

264
00:15:33,246 --> 00:15:35,842
Luckily for you,
that's where the metaphor ended.

265
00:15:37,548 --> 00:15:39,507
I thought it ended at "cuckoo".

266
00:15:42,289 --> 00:15:43,589
You listen to me.

267
00:15:43,752 --> 00:15:46,149
If you want to start acting
like a brother who cares about me,

268
00:15:46,269 --> 00:15:48,438
then terrific. Bring it on.

269
00:15:48,926 --> 00:15:51,715
But you try one time to tell me
who I should be sleeping with,

270
00:15:51,835 --> 00:15:53,690
and you and I are going
to go round and round

271
00:15:53,810 --> 00:15:56,250
the way we did when we were little.
Remember?

272
00:16:00,268 --> 00:16:02,585
- I have an alternate proposal.
- Go on.

273
00:16:03,320 --> 00:16:04,484
You donate eggs.

274
00:16:04,691 --> 00:16:06,517
We will place them in cryogenic storage.

275
00:16:06,658 --> 00:16:09,549
I will find an appropriate
sperm donor for your eggs,

276
00:16:09,669 --> 00:16:12,053
have them fertilized
and implanted in you.

277
00:16:12,214 --> 00:16:14,160
That way, everybody wins.

278
00:16:22,998 --> 00:16:24,149
Correction.

279
00:16:24,310 --> 00:16:26,613
Missy can date whoever she wants.

280
00:16:28,530 --> 00:16:30,042
Look, we have to settle this.

281
00:16:30,162 --> 00:16:31,182
I agree.

282
00:16:31,302 --> 00:16:34,293
Sheldon's sister is hiding at Penny's
because we've all been hitting on her

283
00:16:34,413 --> 00:16:36,443
- at the same time.
- She's not hiding.

284
00:16:36,579 --> 00:16:39,617
She needed privacy to call her
grandmother who's apparently very sick.

285
00:16:39,737 --> 00:16:42,018
And then I believe
she has to wash her hair.

286
00:16:43,099 --> 00:16:45,228
You poor, deluded bastard.

287
00:16:45,705 --> 00:16:48,079
- Don't start with me.
- You want to go again? Let's go.

288
00:16:48,199 --> 00:16:49,145
Sit down.

289
00:16:51,062 --> 00:16:53,681
If we're going to fight over Missy,
let's do it the right way.

290
00:16:53,888 --> 00:16:55,269
The honorable way.

291
00:16:56,934 --> 00:16:58,033
Take that!

292
00:16:58,920 --> 00:17:00,178
You want some more?

293
00:17:03,786 --> 00:17:04,953
And he's down!

294
00:17:05,085 --> 00:17:07,761
One, two, three...

295
00:17:08,532 --> 00:17:09,903
Come on! Get up!

296
00:17:10,127 --> 00:17:11,378
Stay down, bitch!

297
00:17:13,641 --> 00:17:15,415
Nine, ten.

298
00:17:17,435 --> 00:17:19,029
Natural selection at work.

299
00:17:19,337 --> 00:17:21,055
I weep for humanity.

300
00:17:22,117 --> 00:17:24,683
Excuse me while I go tell
Missy the good news.

301
00:17:31,495 --> 00:17:33,349
- Leonard.
- Hi, Penny. How's it going?

302
00:17:33,469 --> 00:17:35,928
That guy Mike that you were dating,
is that still going on?

303
00:17:36,128 --> 00:17:37,341
Pretty much. Why?

304
00:17:37,461 --> 00:17:40,302
Nothing, just catching up. By the way,
may I speak to Missy, please?

305
00:17:41,650 --> 00:17:42,995
Of course.

306
00:17:44,519 --> 00:17:47,164
- Hi, Leonard. What's up?
- Since you're leaving tomorrow,

307
00:17:47,284 --> 00:17:49,609
I was wondering
if you'd like to go to dinner with me.

308
00:17:50,345 --> 00:17:52,404
That's so sweet. But no thanks.

309
00:17:55,665 --> 00:17:57,773
- Do you have other plans or...
- No.

310
00:18:01,902 --> 00:18:03,009
All right.

311
00:18:04,707 --> 00:18:07,982
- Enjoy the rest of your evening.
- Thanks. See ya.

312
00:18:15,534 --> 00:18:17,864
Here's something we didn't anticipate.

313
00:18:23,785 --> 00:18:25,144
What do you want, Howard?

314
00:18:25,584 --> 00:18:27,882
I'm fine. Thanks for asking.

315
00:18:29,271 --> 00:18:30,942
I've come to call on Missy.

316
00:18:35,331 --> 00:18:37,762
- Hi, Howard.
- The Amazing Howard.

317
00:18:39,480 --> 00:18:42,119
- Do you like magic?
- Not really. No.

318
00:18:43,481 --> 00:18:45,415
Then you are in for a treat.

319
00:18:47,575 --> 00:18:49,732
Behold, an ordinary cane.

320
00:19:22,779 --> 00:19:25,117
Thank you. I apprec...

321
00:19:26,996 --> 00:19:28,197
Apprec...

322
00:19:29,576 --> 00:19:30,799
Appree...

323
00:19:32,998 --> 00:19:35,673
Oh, honey.
Is your medication wearing off?

324
00:19:39,389 --> 00:19:40,788
Well, hi, cutie pie.

325
00:19:41,070 --> 00:19:42,788
I was hoping you'd show up.

326
00:20:11,141 --> 00:20:13,299
We had a dog who made
a noise like that.

327
00:20:13,767 --> 00:20:15,524
Had to put him down.

328
00:20:19,654 --> 00:20:21,880
Any news you want me
to pass on to Mom?

329
00:20:22,528 --> 00:20:25,850
Well, she might be interested to know
that I have refocused my research

330
00:20:25,970 --> 00:20:28,958
from bosonic string theory
to heterotic string theory.

331
00:20:30,069 --> 00:20:31,901
I'll just tell her you say "hey".

332
00:20:34,797 --> 00:20:36,750
Well, it was pleasant seeing you,

333
00:20:36,870 --> 00:20:39,316
other than that business
with my testicles.

334
00:20:41,350 --> 00:20:42,638
Come on, Shelly.

335
00:20:48,646 --> 00:20:50,687
I want you to know
I'm very proud of you.

336
00:20:51,132 --> 00:20:51,937
Really?

337
00:20:52,057 --> 00:20:55,810
Yep, I'm always bragging to my friends
about my brother, the rocket scientist.

338
00:20:57,177 --> 00:20:59,779
You tell people I'm a rocket scientist?

339
00:21:00,980 --> 00:21:01,906
Well, yeah.

340
00:21:02,067 --> 00:21:04,034
I'm a theoretical physicist.

341
00:21:04,367 --> 00:21:07,339
- What's the difference?
- What's the difference?

342
00:21:08,575 --> 00:21:09,777
Good bye, Shelly.

343
00:21:10,040 --> 00:21:11,789
My God!

344
00:21:12,687 --> 00:21:16,580
Why don't you just tell them that I'm a
toll-taker at the Golden Gate Bridge?

345
00:21:19,290 --> 00:21:16,580
Rocket scientist. How humiliating.

