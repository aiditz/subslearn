1
00:00:28,416 --> 00:00:29,469
Checkmate.

2
00:00:31,060 --> 00:00:32,325
- Again?
- Obviously,

3
00:00:32,445 --> 00:00:35,206
you're not well suited
for three-dimensional chess.

4
00:00:35,326 --> 00:00:38,365
Perhaps three-dimensional Candy Land
would be more your speed.

5
00:00:38,982 --> 00:00:40,200
Just reset the board.

6
00:00:40,493 --> 00:00:43,684
It must be humbling to suck
on so many different levels.

7
00:00:50,548 --> 00:00:51,461
Hey, guys.

8
00:00:51,672 --> 00:00:53,922
- Did you get my mail?
- Yeah, right here.

9
00:00:54,357 --> 00:00:57,502
- How was Nebraska?
- Well, better than North Dakota.

10
00:01:03,424 --> 00:01:05,906
I guess that joke's
only funny in Nebraska.

11
00:01:06,429 --> 00:01:08,914
From the data at hand,
you really can't draw that conclusion.

12
00:01:09,034 --> 00:01:12,691
All you can say with absolute certainty
is that that joke is not funny here.

13
00:01:18,996 --> 00:01:20,490
Boy, it's good to be back.

14
00:01:21,833 --> 00:01:23,902
- How's your family?
- It was the worst trip.

15
00:01:24,022 --> 00:01:25,704
Everyone got sick over the weekend.

16
00:01:25,824 --> 00:01:27,313
- Sick?
- Here we go.

17
00:01:30,360 --> 00:01:31,668
What kind of sick?

18
00:01:31,911 --> 00:01:33,045
The flu, I guess.

19
00:01:33,259 --> 00:01:35,503
I don't need you to guess.
I need you to know.

20
00:01:35,623 --> 00:01:38,152
Now, when did the symptoms
first appear?

21
00:01:38,272 --> 00:01:39,343
Maybe Friday.

22
00:01:39,554 --> 00:01:41,637
Friday.
Was that morning or afternoon?

23
00:01:42,628 --> 00:01:43,722
I- I don't...

24
00:01:43,933 --> 00:01:46,308
Think, woman.
Who blew their nose and when?

25
00:01:47,136 --> 00:01:48,176
Sheldon, relax.

26
00:01:48,296 --> 00:01:50,854
She doesn't have any symptoms.
I'm sure she's not contagious.

27
00:01:51,065 --> 00:01:52,286
Oh, please.

28
00:01:54,310 --> 00:01:56,490
If influenza was only contagious
after symptoms appear,

29
00:01:56,610 --> 00:01:58,528
it would have died out
thousands of years ago.

30
00:01:58,707 --> 00:02:00,919
Somewhere between tool using
and cave painting,

31
00:02:01,039 --> 00:02:04,326
homo habilis would have figured out
how to kill the guy with the runny nose.

32
00:02:05,561 --> 00:02:08,452
Penny, you'll have to excuse Sheldon.
He's a bit of a germaphobe.

33
00:02:08,572 --> 00:02:10,010
It's okay. I understand.

34
00:02:10,130 --> 00:02:12,827
Thanks for your consideration.
Now please leave.

35
00:02:13,740 --> 00:02:16,296
You'd better go before he starts
spraying you with Lysol.

36
00:02:17,579 --> 00:02:19,707
Okay. Well, thank you
for getting my mail.

37
00:02:19,827 --> 00:02:21,492
No problem. Welcome home.

38
00:02:28,697 --> 00:02:29,609
What?!

39
00:02:31,342 --> 00:02:33,572
The Big Bang Theory
Season 1 Episode 11 - The Pancake Batter Anomaly

40
00:02:34,172 --> 00:02:36,945
Transcript : swsub.com
Synchro : Jesslataree, Sixe, SerJo

41
00:03:02,122 --> 00:03:03,908
What the hell are you doing?

42
00:03:04,068 --> 00:03:06,934
I'm making petri dishes
to grow throat cultures.

43
00:03:08,431 --> 00:03:09,485
With lime Jell-O?

44
00:03:09,798 --> 00:03:11,130
I need a growth medium,

45
00:03:11,250 --> 00:03:13,775
and someone polished off
the apricot yogurt.

46
00:03:15,490 --> 00:03:17,016
Here,
swab my throat.

47
00:03:19,790 --> 00:03:20,860
I don't think so.

48
00:03:20,980 --> 00:03:23,036
If I'm going to get ahead of this thing,

49
00:03:23,156 --> 00:03:25,824
I need to find out what's
growing in my throat.

50
00:03:27,641 --> 00:03:29,336
Sheldon, you are not sick.

51
00:03:30,027 --> 00:03:32,402
This is, but you are not.

52
00:03:33,685 --> 00:03:37,430
We have no idea what pathogen Typhoid
she has introduced into our environment.

53
00:03:38,040 --> 00:03:39,682
For having never been to Nebraska,

54
00:03:39,802 --> 00:03:42,910
I'm fairly certain that
I have no corn-husking antibodies.

55
00:03:46,150 --> 00:03:48,533
Sheldon, don't you think
you're overreacting?

56
00:03:48,653 --> 00:03:52,236
When I'm lying comatose in a hospital
relying on inferior minds to cure me,

57
00:03:52,471 --> 00:03:54,572
these Jell-O cultures
and my accompanying notes

58
00:03:54,771 --> 00:03:56,831
will give them a fighting chance.

59
00:04:01,275 --> 00:04:03,414
- I'm going back to bed.
- Wait.

60
00:04:04,204 --> 00:04:06,042
- Put this in the bathroom.
- What for?

61
00:04:06,253 --> 00:04:08,278
I need to measure my fluid
intake and output

62
00:04:08,398 --> 00:04:10,796
to make sure my kidneys
aren't shutting down.

63
00:04:12,677 --> 00:04:14,670
I mixed pancake batter in this!

64
00:04:16,449 --> 00:04:19,158
No, that measuring cup
has always been for urine.

65
00:04:20,905 --> 00:04:23,591
You had time to make a label
for everything in this apartment

66
00:04:23,711 --> 00:04:25,061
including the label maker,

67
00:04:25,359 --> 00:04:28,814
but you didn't have ten seconds
to make one that said "urine cup"?

68
00:04:30,572 --> 00:04:32,518
It's right here on the bottom.

69
00:04:39,780 --> 00:04:43,196
I guess I owe the Betty Crocker
Company a letter of apology.

70
00:04:59,840 --> 00:05:01,145
Oh, dear God.

71
00:05:05,983 --> 00:05:07,228
Leonard, I'm sick!

72
00:05:14,353 --> 00:05:15,681
Leonard, I'm sick!

73
00:05:28,889 --> 00:05:32,883
Leonard, my comforter fell down,
and my sinuses hurt when I bend over.

74
00:05:52,894 --> 00:05:55,235
- <i>Hey!</i>
- Leonard, where are you?

75
00:05:55,518 --> 00:05:56,656
I'm at work.

76
00:05:58,177 --> 00:05:59,740
At 6:30 in the morning?

77
00:06:02,382 --> 00:06:03,368
On Sunday?

78
00:06:06,148 --> 00:06:08,248
- Why?
- They asked me to come in.

79
00:06:08,409 --> 00:06:10,083
I didn't hear the phone ring.

80
00:06:10,863 --> 00:06:12,176
They texted me.

81
00:06:13,710 --> 00:06:17,115
Well, as I predicted,
I am sick.

82
00:06:17,391 --> 00:06:20,864
My fever has been tracking up
exponentially since 2:00 a.m.,

83
00:06:20,984 --> 00:06:23,889
and I am producing sputum
at an alarming rate.

84
00:06:26,508 --> 00:06:27,821
No kidding?

85
00:06:27,941 --> 00:06:29,353
<i>No, not only that,</i>

86
00:06:29,514 --> 00:06:32,395
it has shifted from clear
to milky green.

87
00:06:33,450 --> 00:06:34,399
<i>All right,</i>

88
00:06:34,559 --> 00:06:36,735
get some rest
and drink plenty of fluids.

89
00:06:37,043 --> 00:06:41,032
What else would I drink,
gases, solids, ionized plasma?

90
00:06:42,329 --> 00:06:43,659
Drink whatever you want.

91
00:06:43,819 --> 00:06:46,119
- I want soup.
- Then make soup.

92
00:06:46,279 --> 00:06:47,579
We don't have soup.

93
00:06:47,739 --> 00:06:49,322
I'm at work, Sheldon.

94
00:06:53,091 --> 00:06:54,378
Is that a dog?

95
00:06:56,557 --> 00:06:57,673
In the lab?

96
00:07:00,127 --> 00:07:03,119
They're training dogs
to operate the centrifuge

97
00:07:03,411 --> 00:07:05,272
for when they need dogs to...

98
00:07:05,943 --> 00:07:07,550
operate the centrifuge

99
00:07:08,061 --> 00:07:10,185
for blind scientists.
I have to go.

100
00:07:13,828 --> 00:07:16,277
Howard, it's the phone!

101
00:07:18,787 --> 00:07:22,258
I know it's the phone, Ma,
I hear the phone!

102
00:07:22,378 --> 00:07:25,736
Well, who's calling
at this ungodly hour?!

103
00:07:26,049 --> 00:07:27,492
I don't know!

104
00:07:28,061 --> 00:07:32,026
Well, ask them why they're calling
at this ungodly hour!

105
00:07:32,146 --> 00:07:35,335
How can I ask them
when I'm talking to you?!

106
00:07:39,221 --> 00:07:40,048
Hello.

107
00:07:41,585 --> 00:07:43,385
It's Leonard.
Code milky green.

108
00:07:43,545 --> 00:07:45,271
Dear Lord, not milky green.

109
00:07:46,256 --> 00:07:47,848
Affirmative. With fever.

110
00:07:48,009 --> 00:07:49,959
Who's
on the phone?!

111
00:07:50,272 --> 00:07:51,407
It's Leonard!

112
00:07:51,527 --> 00:07:53,312
Why is he calling?!

113
00:07:53,473 --> 00:07:54,781
Sheldon's sick!

114
00:07:55,082 --> 00:07:56,882
Were you playing with him?!

115
00:07:59,276 --> 00:08:02,130
For God's sake, Ma,
I'm 26 years old!

116
00:08:02,250 --> 00:08:04,721
Excuse me,
Mr. Grownup!

117
00:08:06,193 --> 00:08:07,912
What do you want for breakfast?!

118
00:08:08,032 --> 00:08:10,036
Chocolate milk and Eggos,
please!

119
00:08:11,802 --> 00:08:14,017
- <i>Listen to me.</i>
- Hang on. Call waiting.

120
00:08:14,137 --> 00:08:15,240
<i>No, don't...!</i>

121
00:08:15,894 --> 00:08:16,754
Hello.

122
00:08:17,496 --> 00:08:18,879
Howard, I'm sick.

123
00:08:24,634 --> 00:08:28,055
Howard's sleeping.
This is his mother!

124
00:08:29,750 --> 00:08:32,977
Why are you calling
at this ungodly hour?

125
00:08:34,613 --> 00:08:35,910
I need soup.

126
00:08:36,625 --> 00:08:38,847
Then call your own mother!

127
00:08:40,873 --> 00:08:42,027
It was Sheldon.

128
00:08:42,188 --> 00:08:44,321
- I tried to stop you.
- <i>It's my own fault.</i>

129
00:08:44,481 --> 00:08:48,400
I forgot the protocol we put in place
after The Great Ear Infection of '06.

130
00:08:49,389 --> 00:08:50,496
Call Koothrappali.

131
00:08:50,616 --> 00:08:54,081
We need to find a place to lay low
for the next 18 to 24 hours.

132
00:08:54,242 --> 00:08:55,287
Stand by.

133
00:08:55,407 --> 00:08:57,292
Ma, can my friends come over?

134
00:08:57,705 --> 00:09:00,559
I just had
the carpet steamed!

135
00:09:02,201 --> 00:09:03,515
That's a negatory.

136
00:09:04,394 --> 00:09:06,927
But there's a <i>Planet of The Apes</i>
marathon at the NuArt today.

137
00:09:07,797 --> 00:09:10,367
Five movies, two hours apiece..

138
00:09:11,327 --> 00:09:12,690
it's a start.

139
00:09:16,019 --> 00:09:18,111
Homeless, crazy guy at table 18.

140
00:09:24,426 --> 00:09:25,821
No, just crazy.

141
00:09:27,788 --> 00:09:30,033
Sheldon, what are you doing here?

142
00:09:30,660 --> 00:09:33,449
I'm sick.
Thank you very much.

143
00:09:33,966 --> 00:09:36,123
How could you have gotten if from me?
I'm not sick.

144
00:09:36,283 --> 00:09:37,605
You're a carrier.

145
00:09:37,725 --> 00:09:39,761
All these people here are doomed.

146
00:09:39,881 --> 00:09:41,388
You're doomed!

147
00:09:43,914 --> 00:09:45,841
Sheldon, what do you want?

148
00:09:46,274 --> 00:09:47,507
I want soup.

149
00:09:52,295 --> 00:09:53,645
Why didn't you...?

150
00:09:56,041 --> 00:09:57,811
Why didn't you just have soup at home?

151
00:09:58,420 --> 00:10:00,885
Penny, I have an IQ of 187.

152
00:10:01,161 --> 00:10:03,317
Don't you imagine that
if there were a way for me

153
00:10:03,437 --> 00:10:05,902
to have had soup at home,
I would have thought of it?

154
00:10:06,881 --> 00:10:08,322
You can have soup delivered.

155
00:10:12,749 --> 00:10:14,077
I did not think of that.

156
00:10:16,285 --> 00:10:18,697
Clearly, febrile delirium
is setting in.

157
00:10:18,832 --> 00:10:20,209
Please bring me some soup

158
00:10:20,329 --> 00:10:22,903
while I still understand
what a spoon is for.

159
00:10:23,470 --> 00:10:25,130
What kind of soup do you want?

160
00:10:25,290 --> 00:10:28,149
Well, my mother used
to make me this split pea

161
00:10:28,269 --> 00:10:31,995
with little frankfurter slices
and these homemade croutons.

162
00:10:32,499 --> 00:10:34,805
We have chicken, tortilla
and potato leek.

163
00:10:35,759 --> 00:10:38,386
Could I get any of those
with little frankfurter slices

164
00:10:38,506 --> 00:10:39,853
and homemade croutons?

165
00:10:42,047 --> 00:10:43,474
Then surprise me.

166
00:10:53,610 --> 00:10:56,521
Would you call that
"moss green" or "forest green"?

167
00:11:11,174 --> 00:11:13,284
Look at this.
Everyone went chimp.

168
00:11:14,506 --> 00:11:16,807
I'd like to point out
that I voted for orangutan,

169
00:11:16,967 --> 00:11:18,547
but you shouted me down.

170
00:11:23,537 --> 00:11:25,315
- Hi, Penny.
- Hey, where are you?

171
00:11:25,816 --> 00:11:27,941
I'm... at work.

172
00:11:29,229 --> 00:11:30,404
You sound funny.

173
00:11:30,565 --> 00:11:33,669
Look. I'm...
I'm in a radiation suit.

174
00:11:34,717 --> 00:11:36,556
- What's up?
- I'm at work, too,

175
00:11:36,676 --> 00:11:39,769
and you'll never guess who's here
infecting my entire station.

176
00:11:39,889 --> 00:11:41,707
Sheldon's at the Cheesecake Factory.

177
00:11:42,722 --> 00:11:45,145
- Just tell him to go home.
- <i>He won't leave.</i>

178
00:11:45,265 --> 00:11:49,078
He's afraid he'll pass out on the bus,
and someone will harvest his organs.

179
00:11:50,404 --> 00:11:52,836
He's paranoid,
and he's established a nest.

180
00:11:53,920 --> 00:11:55,470
Can you please come get him?

181
00:11:56,791 --> 00:11:58,557
Yeah, I'd be happy to.

182
00:12:01,268 --> 00:12:04,030
Oh, my God, there's a breach
in the radiation unit!

183
00:12:04,150 --> 00:12:06,403
The whole city is in jeopardy!
Oh, my God!

184
00:12:06,523 --> 00:12:09,067
The containment vessel's melting.
Gotta go. Bye!

185
00:12:15,340 --> 00:12:16,897
I feel really guilty.

186
00:12:17,402 --> 00:12:18,859
You did what you had to do.

187
00:12:18,979 --> 00:12:21,692
Take your stinking paws off my popcorn,

188
00:12:21,812 --> 00:12:23,774
you damn dirty ape!

189
00:12:29,009 --> 00:12:30,809
Thanks for bringing me home.

190
00:12:31,098 --> 00:12:33,313
It's okay.
I didn't really need to work today.

191
00:12:33,433 --> 00:12:36,466
It's not like I have rent
or car payments or anything.

192
00:12:36,586 --> 00:12:37,682
Good, good.

193
00:12:39,754 --> 00:12:41,675
Okay, well, you feel better.

194
00:12:41,835 --> 00:12:44,220
Wait!
Where are you going?

195
00:12:45,940 --> 00:12:48,340
Home...
to write some bad checks.

196
00:12:49,842 --> 00:12:51,509
You're going to leave me?

197
00:12:51,898 --> 00:12:54,813
Sheldon, you are a grown man.
Haven't you ever been sick before?

198
00:12:55,275 --> 00:12:57,594
Of course, but not by myself.

199
00:12:58,641 --> 00:13:00,019
Really? Never?

200
00:13:00,477 --> 00:13:02,602
Well, once, when I was 15,

201
00:13:02,722 --> 00:13:05,741
spending the summer at the Heidelberg
Institute in Germany.

202
00:13:06,270 --> 00:13:07,409
Studying abroad?

203
00:13:07,737 --> 00:13:09,578
No. Visiting professor.

204
00:13:11,659 --> 00:13:14,653
Anyway, the local cuisine was
a little more sausage-based

205
00:13:14,773 --> 00:13:18,277
than I'm used to, and the result
was an internal Blitzkrieg,

206
00:13:18,397 --> 00:13:21,613
with my lower intestine playing
the part of Czechoslovakia.

207
00:13:22,747 --> 00:13:25,426
And there was no one there
to take care of you?

208
00:13:25,587 --> 00:13:28,646
My Mom had to fly
back to Texas to help my Dad,

209
00:13:28,766 --> 00:13:32,216
because the house had slipped off
the cinder blocks again.

210
00:13:33,274 --> 00:13:35,439
- Again?
- It was tornado season.

211
00:13:35,764 --> 00:13:37,564
And it was an aluminum house.

212
00:13:39,393 --> 00:13:42,975
The housekeeper in the faculty residence
didn't speak any English.

213
00:13:43,272 --> 00:13:45,578
When I finally managed
to convince her I was sick,

214
00:13:45,698 --> 00:13:48,382
she said,"Mochtest du
eine Darmspfilung?"

215
00:13:49,611 --> 00:13:50,912
What does that mean?

216
00:13:51,032 --> 00:13:53,156
Based on what happened next,
I assume it means,

217
00:13:53,276 --> 00:13:54,790
"Would you like an enema?"

218
00:13:57,257 --> 00:13:59,640
Okay, sweetie.
I'll take care of you.

219
00:13:59,760 --> 00:14:01,219
What do you need?

220
00:14:02,305 --> 00:14:05,127
My Mom used to give me sponge baths.

221
00:14:12,255 --> 00:14:14,311
Okay, ground rules: no sponge baths,

222
00:14:14,431 --> 00:14:16,455
and definitely no enemas.

223
00:14:17,394 --> 00:14:18,318
Agreed.

224
00:14:23,474 --> 00:14:24,512
Here we go.

225
00:14:24,798 --> 00:14:27,469
Ten-and-a-half hours of ape-y goodness.

226
00:14:30,478 --> 00:14:32,148
Damn it, my glasses.

227
00:14:33,489 --> 00:14:36,038
Okay, I'm blind here, guys.
Can you help me find them?

228
00:14:36,158 --> 00:14:37,508
- Sorry.
- Okay.

229
00:14:41,091 --> 00:14:42,213
Found them.

230
00:14:44,205 --> 00:14:45,155
Oh, great.

231
00:14:45,478 --> 00:14:47,110
I'm sorry.
Don't you have a spare?

232
00:14:47,230 --> 00:14:48,247
Yeah, at home.

233
00:14:48,367 --> 00:14:49,480
If you leave now,

234
00:14:49,600 --> 00:14:53,318
you can be back before the gorillas
rip the crap out of Charlton Heston.

235
00:14:53,438 --> 00:14:55,928
Unless Sheldon's there,
in which case, you'll be trapped forever

236
00:14:56,048 --> 00:14:58,478
in his whiny, hyper-neurotic snot web.

237
00:15:06,884 --> 00:15:08,084
Hi. Penny?

238
00:15:08,635 --> 00:15:11,686
I was just wondering,
is Sheldon still at the restaurant?

239
00:15:13,727 --> 00:15:16,640
Okay, that was very nice of you.
Okay, I gotta go.

240
00:15:16,760 --> 00:15:19,708
Got kind of a full-blown
Chernobyl thing here. Gotta go, bye.

241
00:15:20,921 --> 00:15:22,378
He's home.
I'm screwed.

242
00:15:25,251 --> 00:15:28,056
Ten-and-a-half hours
of ape-y blurriness.

243
00:15:29,628 --> 00:15:30,894
How about Lasik?

244
00:15:32,757 --> 00:15:34,639
You want me to get eye surgery?

245
00:15:34,800 --> 00:15:36,991
Would you rather go home
and deal with Sheldon,

246
00:15:37,111 --> 00:15:39,895
or have a stranger carve out
your corneas with a laser beam?

247
00:15:42,364 --> 00:15:44,173
- Well?
- I'm thinking.

248
00:15:46,346 --> 00:15:48,946
Okay, nice and cozy.

249
00:15:49,476 --> 00:15:51,188
Okay? I'll see you later.

250
00:15:51,308 --> 00:15:52,119
Wait.

251
00:15:53,317 --> 00:15:55,759
Will you please
rub this on my chest?

252
00:15:57,555 --> 00:15:59,456
Sheldon,
can't you do that yourself?

253
00:15:59,656 --> 00:16:02,225
VapoRub makes
my hands smell funny.

254
00:16:03,669 --> 00:16:06,488
- But, Sheldon...
- Please, please, please?

255
00:16:11,091 --> 00:16:12,887
I can't believe I'm doing this.

256
00:16:18,891 --> 00:16:22,101
No, no. Counterclockwise,
or my chest hair mats.

257
00:16:25,006 --> 00:16:26,044
Sorry.

258
00:16:31,365 --> 00:16:33,294
Can you sing "Soft Kitty"?

259
00:16:35,749 --> 00:16:36,619
What?

260
00:16:36,933 --> 00:16:39,288
My Mom used to sing it
to me when I was sick.

261
00:16:39,706 --> 00:16:42,458
- Oh, sorry, honey. I don't know it.
- I'll teach you.

262
00:16:44,158 --> 00:16:47,747
<i>Soft kitty, warm kitty,</i>

263
00:16:47,867 --> 00:16:49,840
<i>Little ball of fur</i>

264
00:16:52,577 --> 00:16:55,988
<i>Happy kitty, sleepy kitty,</i>

265
00:16:56,108 --> 00:16:58,788
<i>purr, purr, purr</i>

266
00:17:01,809 --> 00:17:02,645
Now you.

267
00:17:08,851 --> 00:17:10,939
<i>Soft kitty,</i>

268
00:17:11,059 --> 00:17:13,278
<i>Warm kitty...</i>

269
00:17:14,727 --> 00:17:17,517
<i>Little ball of fur</i>

270
00:17:18,368 --> 00:17:19,551
Keep rubbing.

271
00:17:21,635 --> 00:17:24,197
<i>Little ball of fur</i>

272
00:17:39,079 --> 00:17:40,894
What do you see? I can't.

273
00:17:41,014 --> 00:17:42,717
The living room appears to be empty.

274
00:17:42,837 --> 00:17:44,488
He must be in his bedroom.

275
00:17:44,608 --> 00:17:47,618
My spare glasses are in my bedroom,
on my dresser,

276
00:17:47,738 --> 00:17:49,223
next to my bat signal.

277
00:17:50,890 --> 00:17:52,194
I'm not going in there.

278
00:17:52,813 --> 00:17:54,362
- Raj?
- No way, Jose.

279
00:17:55,815 --> 00:17:57,915
But I can't do it.
I can't see anything.

280
00:17:58,035 --> 00:18:01,011
It's all right. Wireless minicam
and Bluetooth headset.

281
00:18:01,579 --> 00:18:03,330
We'll be your eyes.

282
00:18:04,399 --> 00:18:05,258
Fine.

283
00:18:05,761 --> 00:18:06,834
One more thing.

284
00:18:07,335 --> 00:18:10,611
This is a subsonic impact sensor.

285
00:18:11,707 --> 00:18:14,034
If Sheldon gets out of bed
and starts to walk,

286
00:18:14,154 --> 00:18:16,677
this device will register it
and send a signal to the laptop.

287
00:18:16,797 --> 00:18:19,155
At that point,
based on the geography of the apartment

288
00:18:19,275 --> 00:18:21,052
and the ambulatory speed
of a sick Sheldon,

289
00:18:21,172 --> 00:18:24,251
you'll have 7 seconds to get out,
glasses or no glasses.

290
00:18:24,553 --> 00:18:26,270
Won't my footsteps set it off?

291
00:18:26,583 --> 00:18:28,932
No. You'll be on
your hands and knees.

292
00:18:29,052 --> 00:18:31,734
You'll need to get the sensor
as close as you can to Sheldon's room.

293
00:18:32,049 --> 00:18:34,612
How do I carry it
if I'm on my hands and knees?

294
00:18:39,962 --> 00:18:40,951
Stay low.

295
00:18:42,007 --> 00:18:42,957
Bear left.

296
00:18:43,906 --> 00:18:45,580
- Now keep true.
- What?

297
00:18:45,742 --> 00:18:48,917
- It means "go straight."
- Then just say, "Go straight."

298
00:18:49,280 --> 00:18:50,785
You don't say, "Go straight."

299
00:18:50,905 --> 00:18:53,214
When you're giving bearings,
you say, "Keep true."

300
00:18:54,011 --> 00:18:55,146
All right.

301
00:18:59,180 --> 00:19:00,763
I just hit my head.

302
00:19:01,260 --> 00:19:03,307
Because you didn't keep true.

303
00:19:06,217 --> 00:19:07,547
<i>Okay, turn right.</i>

304
00:19:09,112 --> 00:19:10,750
The picture's breaking up.

305
00:19:11,188 --> 00:19:12,774
Angle your head to the right.

306
00:19:14,025 --> 00:19:15,475
<i>Now, a little more.</i>

307
00:19:16,105 --> 00:19:17,183
Little more.

308
00:19:18,107 --> 00:19:20,200
That's it.
Now, just keep true.

309
00:19:23,635 --> 00:19:27,008
You're close enough to Sheldon's room.
Deploy the sensor.

310
00:19:28,634 --> 00:19:29,996
<i>Now, turn it on.</i>

311
00:19:30,580 --> 00:19:31,942
It wasn't on?

312
00:19:33,194 --> 00:19:35,089
Then why did I have to crawl?

313
00:19:36,641 --> 00:19:38,165
No, I guess you didn't.

314
00:19:41,942 --> 00:19:43,109
<i>Okay, it's on.</i>

315
00:19:43,229 --> 00:19:46,233
Good. From this point forward,
you will have to crawl.

316
00:19:48,073 --> 00:19:49,192
I know.

317
00:19:53,084 --> 00:19:55,943
Hang on, the sensor's picking up
something. Turn your head back.

318
00:20:02,627 --> 00:20:03,973
You rat bastard.

319
00:20:06,446 --> 00:20:08,327
Told you the sensor would work.

320
00:20:11,234 --> 00:20:13,368
You deliberately stuck me
with Sheldon.

321
00:20:13,488 --> 00:20:16,129
Come on, I had to.
You see what he's like?

322
00:20:16,793 --> 00:20:18,424
Penny, I'm hungry.

323
00:20:19,025 --> 00:20:21,802
It's okay, sweetie.
Good news! Leonard's home!

324
00:20:22,868 --> 00:20:24,388
Here you go.
Good luck. Bye.

325
00:20:25,625 --> 00:20:27,641
Leonard! I'm hungry!

326
00:20:27,981 --> 00:20:29,764
Penny, take me with you!

327
00:20:40,014 --> 00:20:41,578
I want grilled cheese.

328
00:21:00,353 --> 00:21:03,334
Do you think Penny will come here
and take care of us?

329
00:21:04,816 --> 00:21:07,427
I don't think Penny's
ever coming here again.

330
00:21:10,941 --> 00:21:12,705
I'm very congested.

331
00:21:15,067 --> 00:21:16,300
Yeah? So?

332
00:21:16,703 --> 00:21:18,213
Could you go to the kitchen

333
00:21:18,333 --> 00:21:21,158
and get me the turkey baster
labeled "mucous"?

334
00:21:24,726 --> 00:21:26,720
If I stand, I'll vomit.

335
00:21:28,212 --> 00:21:26,720
Under the sink,
yellow Tupper Ware bowl.

