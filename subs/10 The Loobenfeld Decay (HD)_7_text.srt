1
00:00:02,335 --> 00:00:06,334
No, see, the liquid metal Terminators
were created in the future by Skynet,

2
00:00:06,454 --> 00:00:10,117
and Skynet was developed by Miles Dyson,
but that future no longer exists

3
00:00:10,237 --> 00:00:12,587
due to Dyson's death
in <i>Terminator 2</i>.

4
00:00:13,615 --> 00:00:15,014
Then riddle me this:

5
00:00:15,518 --> 00:00:17,213
Assuming all the good Terminators

6
00:00:17,333 --> 00:00:19,920
were originally evil
Terminators created by Skynet

7
00:00:20,080 --> 00:00:22,173
but then reprogrammed by
the future John Connor,

8
00:00:22,293 --> 00:00:25,191
why would Skynet,
an artificial computer intelligence,

9
00:00:25,311 --> 00:00:28,706
bother to create a petite, hot,
17-year-old killer robot?

10
00:00:31,474 --> 00:00:33,624
Skynet is kinky?
I don't know...

11
00:00:34,514 --> 00:00:37,466
Artificial intelligences
do not have teen fetishes.

12
00:00:38,265 --> 00:00:39,480
Wait! They use it to...

13
00:00:39,968 --> 00:00:41,316
Too late. I win.

14
00:00:56,949 --> 00:00:58,493
What the hell is that?

15
00:00:59,717 --> 00:01:02,300
I don't know,
but if cats could sing...

16
00:01:03,170 --> 00:01:04,659
they'd hate it, too.

17
00:01:14,638 --> 00:01:15,663
Hey, guys!

18
00:01:16,141 --> 00:01:18,270
- Hi! Where you going?
- What?

19
00:01:18,431 --> 00:01:21,193
We just had to mail
some letters...

20
00:01:22,013 --> 00:01:22,805
and...

21
00:01:23,475 --> 00:01:25,047
throw away some chicken.

22
00:01:42,287 --> 00:01:44,045
You'll never guess
what just happened.

23
00:01:44,206 --> 00:01:45,119
I give up.

24
00:01:45,239 --> 00:01:47,659
I don't guess.
As a scientist, I reach conclusions

25
00:01:47,779 --> 00:01:49,884
based on observation
and experimentation.

26
00:01:50,837 --> 00:01:53,620
Although as I'm saying this, it occurs
to me you may have been employing

27
00:01:53,740 --> 00:01:56,385
a rhetorical device
rendering my response moot.

28
00:01:57,409 --> 00:02:00,456
- What was that?
- Believe it or not, personal growth.

29
00:02:00,799 --> 00:02:01,813
What happened?

30
00:02:01,973 --> 00:02:04,349
Remember when I auditioned
for that workshop production of <i>Rent</i>,

31
00:02:04,469 --> 00:02:06,212
but I didn't get it
and I didn't know why.

32
00:02:06,332 --> 00:02:08,284
I have a conclusion
based on an observation.

33
00:02:08,404 --> 00:02:10,454
No, you don't.
No, he doesn't.

34
00:02:11,403 --> 00:02:13,447
The girl they picked to play Mimi
dropped out,

35
00:02:13,567 --> 00:02:15,034
and they asked me to replace her.

36
00:02:15,314 --> 00:02:17,037
Congratulations.
What a lucky break.

37
00:02:17,198 --> 00:02:19,140
It's not a big deal,
just a one-night showcase,

38
00:02:19,260 --> 00:02:22,084
but they invite a lot of casting people.
So, you never know.

39
00:02:22,244 --> 00:02:24,282
- I think I know.
- No, you don't.

40
00:02:25,248 --> 00:02:26,286
He doesn't.

41
00:02:26,582 --> 00:02:28,380
It's this Friday at 8:00.
You want to come?

42
00:02:31,417 --> 00:02:32,565
Because...

43
00:02:33,670 --> 00:02:37,883
Friday we are attending a symposium
on molecular positronium.

44
00:02:38,119 --> 00:02:39,936
I think that's a week from Tuesday,
at 6:00.

45
00:02:40,056 --> 00:02:41,310
No, it's this Friday.

46
00:02:41,731 --> 00:02:42,674
At 8:00.

47
00:02:43,934 --> 00:02:45,649
Too bad.
I gotta get to rehearsal.

48
00:02:46,000 --> 00:02:47,750
- See you guys.
- See ya.

49
00:02:52,942 --> 00:02:54,281
You just lied to Penny.

50
00:02:54,442 --> 00:02:55,991
- Yes, I did.
- And you did it

51
00:02:56,153 --> 00:02:59,524
so casually-- no rapid breathing,
no increase in perspiration...

52
00:02:59,906 --> 00:03:00,497
So?

53
00:03:00,658 --> 00:03:03,285
So, lack of a physiological
response while lying

54
00:03:03,405 --> 00:03:05,955
is characteristic
of a violent sociopath.

55
00:03:06,441 --> 00:03:08,588
Sheldon, are you worried
about your safety?

56
00:03:08,749 --> 00:03:10,729
No, I imagine if you
were going to kill me,

57
00:03:10,849 --> 00:03:12,842
you'd have done it
a long time ago.

58
00:03:13,280 --> 00:03:14,551
That's very true.

59
00:03:34,674 --> 00:03:36,708
The Big Bang Theory
Season 1 Episode 10 - The Loobenfeld Decay

60
00:03:37,392 --> 00:03:40,342
Transcript: swsub.com
Synchro: Sixe, Jesslataree, SerJo

61
00:04:05,358 --> 00:04:06,150
What?

62
00:04:08,723 --> 00:04:10,368
I need to speak to you.

63
00:04:10,774 --> 00:04:13,262
- It's 2:00 in the morning!
- It's important.

64
00:04:13,382 --> 00:04:15,107
I highly doubt that.

65
00:04:15,898 --> 00:04:16,709
Go away.

66
00:04:21,820 --> 00:04:23,596
Are you still out there?

67
00:04:27,929 --> 00:04:28,763
What?

68
00:04:30,425 --> 00:04:32,975
You're right;
it can wait until morning.

69
00:04:35,035 --> 00:04:37,290
- What, what?
- Never mind.

70
00:04:37,410 --> 00:04:39,899
I clearly woke you up
in the middle of a REM cycle.

71
00:04:40,059 --> 00:04:41,692
You're in no state of mind to talk.

72
00:04:42,035 --> 00:04:43,238
What is it?

73
00:04:45,023 --> 00:04:48,157
I'm uncomfortable having been
included in your lie to Penny.

74
00:04:48,715 --> 00:04:50,424
What was I supposed to say?

75
00:04:50,544 --> 00:04:53,788
- You could have told her the truth.
- That would have hurt her feelings.

76
00:04:54,584 --> 00:04:56,307
Is that a relevant factor?

77
00:04:58,522 --> 00:05:00,960
Then I suppose
you could've agreed to go.

78
00:05:01,314 --> 00:05:03,282
And what would I have said afterwards?

79
00:05:03,402 --> 00:05:05,341
I would suggest something
to the effect of:

80
00:05:05,501 --> 00:05:07,714
singing is
neither an appropriate vocation

81
00:05:07,834 --> 00:05:10,235
nor avocation for you,
and if you disagree,

82
00:05:10,355 --> 00:05:12,728
I'd recommend you have a CAT scan
to look for a tumor

83
00:05:12,848 --> 00:05:15,184
pressing on the cognitive
processing centers of your brain.

84
00:05:17,260 --> 00:05:19,429
I couldn't say that.
I would have to say,

85
00:05:19,549 --> 00:05:22,858
"You were terrific and I can't wait
to hear you sing again."

86
00:05:23,463 --> 00:05:24,406
Why?

87
00:05:25,559 --> 00:05:28,988
That's the social protocol.
It's what you do when you have a friend

88
00:05:29,108 --> 00:05:31,951
who's proud of something
they really suck at.

89
00:05:32,866 --> 00:05:34,370
I was not aware of that.

90
00:05:34,661 --> 00:05:36,163
Well, now you are.

91
00:05:36,323 --> 00:05:37,873
All right. Leonard?

92
00:05:38,993 --> 00:05:41,080
When we played chess earlier,
you were terrific,

93
00:05:41,200 --> 00:05:43,546
and I can't wait to play you again.
Good night.

94
00:05:53,395 --> 00:05:56,768
This would be so much easier
if I were a violent sociopath.

95
00:06:00,253 --> 00:06:01,147
What?

96
00:06:01,307 --> 00:06:02,880
I was analyzing our lie,

97
00:06:03,000 --> 00:06:05,901
and I believe we're in danger
of Penny seeing through the ruse.

98
00:06:06,330 --> 00:06:07,328
How?

99
00:06:07,730 --> 00:06:09,409
Simple:
If she were to log onto

100
00:06:09,529 --> 00:06:13,785
www.socalphysixsgroup.org
/activities/other,

101
00:06:13,905 --> 00:06:16,045
click on "Upcoming Events,"
scroll down to "Seminars,"

102
00:06:16,165 --> 00:06:17,623
download the PDF schedule,

103
00:06:17,743 --> 00:06:20,082
and look for the seminar
on molecular positronium,

104
00:06:20,242 --> 00:06:23,493
bippity, boppity, boo--
our pants are metaphorically on fire.

105
00:06:28,892 --> 00:06:31,942
Well, sir, my trousers
will not be igniting today.

106
00:06:39,788 --> 00:06:40,811
Good morning.

107
00:06:40,972 --> 00:06:42,897
Do you have any idea
what time it is?

108
00:06:43,058 --> 00:06:43,997
Of course I do.

109
00:06:44,117 --> 00:06:47,554
My watch is linked to the atomic clock
in Boulder, Colorado.

110
00:06:47,674 --> 00:06:49,486
It's accurate
to one-tenth of a second.

111
00:06:50,439 --> 00:06:52,259
But as I'm saying this, it occurs to me

112
00:06:52,379 --> 00:06:55,408
that, once again, your question
may have been rhetorical.

113
00:06:55,735 --> 00:06:56,871
What do you want?

114
00:06:57,070 --> 00:06:59,838
Remember how Leonard told you
we couldn't come to your performance

115
00:07:00,022 --> 00:07:03,117
because we were attending a symposium
on molecular positronium?

116
00:07:04,829 --> 00:07:06,942
I remember "symposium."

117
00:07:08,514 --> 00:07:10,624
Yes.
Well... he lied.

118
00:07:11,709 --> 00:07:12,852
Wait.
What?

119
00:07:13,073 --> 00:07:16,140
He lied, and I'm feeling
very uncomfortable about it.

120
00:07:16,557 --> 00:07:18,549
Well, imagine how I'm feeling.

121
00:07:19,979 --> 00:07:20,979
Hungry?

122
00:07:22,854 --> 00:07:23,854
Tired?

123
00:07:24,793 --> 00:07:27,089
I'm sorry,
this really isn't my strong suit.

124
00:07:28,376 --> 00:07:31,246
You told her I lied?
Why would you tell her I lied?

125
00:07:32,039 --> 00:07:33,130
To help you.

126
00:07:34,917 --> 00:07:36,667
I'm sorry,
I'm not seeing the help.

127
00:07:36,827 --> 00:07:39,165
She was going to see through
your lie eventually,

128
00:07:39,325 --> 00:07:41,507
so I told her that you
were lying to protect me.

129
00:07:43,262 --> 00:07:44,921
I'm getting a bad feeling.

130
00:07:45,794 --> 00:07:46,794
Hunger?

131
00:07:48,463 --> 00:07:49,371
Indigestion?

132
00:07:49,631 --> 00:07:51,609
I'm sorry, I'm really
not very good at this.

133
00:07:52,200 --> 00:07:54,859
Anyway, Penny now believes that,
on Friday night,

134
00:07:55,019 --> 00:07:58,814
we're going to participate in my cousin
Leopold's drug intervention.

135
00:08:00,620 --> 00:08:01,976
Your cousin Leopold.

136
00:08:02,136 --> 00:08:04,730
Who most people call Leo,
but he also answers to Lee.

137
00:08:04,890 --> 00:08:07,545
- Remember that. It's important.
- What's important?

138
00:08:07,705 --> 00:08:09,007
Details, Leonard--

139
00:08:09,167 --> 00:08:12,353
the success or failure of our deceitful
enterprise turns on details.

140
00:08:13,175 --> 00:08:14,897
- Do you have a cousin Leopold?
- No.

141
00:08:15,157 --> 00:08:16,549
I made him up.

142
00:08:17,418 --> 00:08:19,151
I think you'd call him Lee.

143
00:08:20,984 --> 00:08:22,791
I don't get it.
I already told her a lie.

144
00:08:22,966 --> 00:08:25,198
Why replace it with a different lie?

145
00:08:25,630 --> 00:08:27,856
First of all, your lie
was laughably transparent,

146
00:08:28,016 --> 00:08:30,246
where mine is exquisitely convoluted.

147
00:08:31,544 --> 00:08:34,917
While you were sleeping,
I was weaving an un-unravelable web.

148
00:08:36,220 --> 00:08:37,356
Un-unravelable?

149
00:08:37,730 --> 00:08:38,568
Yes.

150
00:08:38,728 --> 00:08:41,791
If she Googles "Leopold Houston,"
she'll find a Facebook page,

151
00:08:41,951 --> 00:08:44,352
an online blog depicting
his descent into drug use

152
00:08:44,512 --> 00:08:47,380
and a desperate yet hopeful
listing on eHarmony.com.

153
00:08:49,274 --> 00:08:52,865
Okay, why would I go
to a drug intervention for your cousin?

154
00:08:53,195 --> 00:08:55,797
Because it's in Long Beach,
and I don't drive.

155
00:08:57,774 --> 00:09:00,867
- We're going to Long Beach?
- Of course not. There's no cousin Leo.

156
00:09:01,027 --> 00:09:02,945
There's no intervention.
Focus, Leonard.

157
00:09:03,205 --> 00:09:04,177
Aw, come on.

158
00:09:05,667 --> 00:09:08,284
We just leave the house
on Friday night and we return

159
00:09:08,544 --> 00:09:10,429
in the wee hours,
emotionally wrung out,

160
00:09:10,589 --> 00:09:12,914
from the work of convincing Leo
to go back into rehab.

161
00:09:13,232 --> 00:09:15,249
- He goes back into rehab?
- Yes, but,

162
00:09:15,509 --> 00:09:16,882
he can relapse if Penny

163
00:09:17,135 --> 00:09:19,591
ever invites us
to hear her sing again.

164
00:09:21,544 --> 00:09:23,140
You still told her I lied.

165
00:09:23,300 --> 00:09:26,757
For a noble purpose--
to spare me the social embarrassment

166
00:09:26,917 --> 00:09:31,223
of having a drug-addled first cousin--
which I'm assuming is embarrassing, yes?

167
00:09:32,975 --> 00:09:34,884
How am I supposed
to remember all of this?

168
00:09:35,044 --> 00:09:36,756
That's the best part.
You don't have to.

169
00:09:36,916 --> 00:09:38,916
I told Penny
that you would be embarrassed

170
00:09:39,076 --> 00:09:41,062
if you knew that she found out
that you had lied,

171
00:09:41,222 --> 00:09:44,801
so she's agreed to operate as if
the original lie is still in force.

172
00:09:47,605 --> 00:09:49,710
So she's expecting me

173
00:09:49,978 --> 00:09:52,381
to lie about going
to a symposium in Pasadena,

174
00:09:52,671 --> 00:09:54,079
when, in actuality,

175
00:09:54,339 --> 00:09:57,583
we're pretending to go to a drug
intervention in Long Beach.

176
00:09:58,104 --> 00:09:59,585
Un-unravelable.

177
00:10:05,447 --> 00:10:07,349
Look at you,
all ready for your showcase.

178
00:10:07,509 --> 00:10:09,048
- You look great.
- Thanks.

179
00:10:09,208 --> 00:10:11,199
I just wanted to come by,
wish you guys luck

180
00:10:11,359 --> 00:10:13,285
with your... symposium.

181
00:10:16,348 --> 00:10:17,198
Thank you.

182
00:10:17,631 --> 00:10:20,200
I got to tell you, a lot of friends
would let their friend go alone,

183
00:10:20,360 --> 00:10:22,816
but that's not who you are.
You are the kind of guy

184
00:10:22,976 --> 00:10:25,010
who stands by a friend when...

185
00:10:25,533 --> 00:10:27,669
Wen he has a symposium to go to.

186
00:10:31,608 --> 00:10:33,333
I don't know what to say.

187
00:10:34,546 --> 00:10:35,766
It's okay, Leonard.

188
00:10:36,026 --> 00:10:37,576
Okay, all right, good.

189
00:10:38,732 --> 00:10:40,189
Oh, boy, group hug.

190
00:10:43,271 --> 00:10:44,274
So, what's up?

191
00:10:44,534 --> 00:10:46,412
Well, Penny is on her way to perform

192
00:10:46,572 --> 00:10:48,570
in a one-night
showcase production of Rent,

193
00:10:48,894 --> 00:10:50,656
which we are unable to attend,

194
00:10:50,935 --> 00:10:53,780
because we're going to a symposium
on molecular positronium

195
00:10:53,940 --> 00:10:56,008
given by Dr. Emil Farmanfarmian.

196
00:10:56,752 --> 00:11:00,165
Wait a minute. Farmanfarmian is speaking
and you're bogarting the symposium?

197
00:11:01,606 --> 00:11:04,686
- Howard, I'm sorr...
- No, no, you're quark-blocking us.

198
00:11:07,294 --> 00:11:09,019
I don't know what to say.

199
00:11:11,228 --> 00:11:13,400
It's okay.
It's your Millennium Falcon.

200
00:11:13,560 --> 00:11:16,106
You and Chewbacca
do whatever you want to do.

201
00:11:17,544 --> 00:11:21,135
Me and Princess Leia here will find
some other way to spend the evening.

202
00:11:21,295 --> 00:11:22,503
Howard, wait.

203
00:11:23,026 --> 00:11:25,435
Sheldon,
I think we should tell them.

204
00:11:27,170 --> 00:11:30,182
Okay, sure.
I don't see a problem with that.

205
00:11:31,481 --> 00:11:32,698
There's no symposium.

206
00:11:33,657 --> 00:11:35,902
Leonard lied to me.
Isn't that right, Leonard?

207
00:11:38,359 --> 00:11:40,016
I don't know what to say!

208
00:11:40,919 --> 00:11:41,919
It's okay; I do.

209
00:11:42,079 --> 00:11:44,418
Leonard is helping Sheldon
through a family crisis.

210
00:11:44,796 --> 00:11:46,855
He made up the whole story
about the symposium

211
00:11:47,015 --> 00:11:49,386
with Dr. Farman...
farmian...

212
00:11:49,546 --> 00:11:50,546
Good for you.

213
00:11:52,209 --> 00:11:54,044
...he didn't want
Sheldon to be embarrassed.

214
00:11:54,204 --> 00:11:56,229
And there's nothing
to be embarrassed about.

215
00:11:56,389 --> 00:11:58,500
Every family in America has a relative

216
00:11:58,660 --> 00:12:01,477
holed up in a garage somewhere
huffing paint thinner.

217
00:12:02,232 --> 00:12:04,817
No, I'm lost, too.
I think she skipped a step.

218
00:12:06,727 --> 00:12:10,671
Sheldon's cousin Leo escaped rehab.
He's in a Motel 8 in Long Beach.

219
00:12:10,831 --> 00:12:12,969
The whole family's going out
for an intervention.

220
00:12:13,129 --> 00:12:14,878
Leonard is driving Sheldon down there

221
00:12:15,038 --> 00:12:17,418
to help him through this,
because he's such a good man.

222
00:12:17,578 --> 00:12:19,379
Another hug?
Thank you.

223
00:12:21,174 --> 00:12:22,759
All right, you guys, good luck.

224
00:12:22,919 --> 00:12:24,545
Thanks, Penny.
Oh-- break a leg.

225
00:12:24,705 --> 00:12:25,864
Break a leg.

226
00:12:26,944 --> 00:12:28,421
So, road trip to Long Beach.

227
00:12:28,680 --> 00:12:31,134
- We're not going to Long Beach.
- Why not?

228
00:12:31,294 --> 00:12:34,113
Because Sheldon doesn't have
a drug-addicted cousin Leopold.

229
00:12:34,545 --> 00:12:35,613
Oh, too bad.

230
00:12:37,066 --> 00:12:38,999
I've always wanted
to go to Long Beach.

231
00:12:39,159 --> 00:12:40,590
It's a very nice community.

232
00:12:40,750 --> 00:12:42,195
The Queen Mary is docked there.

233
00:12:42,355 --> 00:12:45,604
Once the largest ocean liner in the
world, it's now a hotel and restaurant,

234
00:12:45,906 --> 00:12:48,857
where they host a surprisingly gripping
murder mystery dinner.

235
00:12:49,561 --> 00:12:50,856
- Sounds fun.
- I'm game.

236
00:12:51,016 --> 00:12:52,671
- Shotgun!
- No, no, no.

237
00:12:53,857 --> 00:12:57,108
Leonard gets nauseous unless he sits
in front, and even then, it's iffy.

238
00:12:59,044 --> 00:13:00,294
Wait, are we...

239
00:13:12,597 --> 00:13:14,141
Let it go, Sheldon.

240
00:13:14,302 --> 00:13:17,603
The murderer was the first mate
whether it makes sense to you or not.

241
00:13:18,882 --> 00:13:20,740
No, that's the least of our worries.

242
00:13:20,860 --> 00:13:22,751
I've been doing
some research on addiction,

243
00:13:22,871 --> 00:13:25,068
both the biochemical
and behavioral aspects--

244
00:13:25,228 --> 00:13:28,488
and I think there's a problem
with the current version of our lie.

245
00:13:28,649 --> 00:13:31,617
What are you talking about? It's fine!
She bought it; it's over.

246
00:13:35,573 --> 00:13:36,939
Sadly, it's not.

247
00:13:37,866 --> 00:13:39,891
Substance abuse
is a lifelong struggle,

248
00:13:40,011 --> 00:13:42,701
but beyond that, I have realized
that the Leo I described

249
00:13:42,821 --> 00:13:45,284
- would not have agreed to go to rehab.
- Why not?

250
00:13:46,541 --> 00:13:48,008
Because Leo's a middle child.

251
00:13:48,921 --> 00:13:51,231
There is no Leo!
How can you say that?

252
00:13:55,256 --> 00:13:57,101
You didn't read the bio, did you?

253
00:13:58,387 --> 00:14:01,153
He's not just a middle child,
he is the quintessential middle child--

254
00:14:01,273 --> 00:14:02,814
from a broken home, to boot.

255
00:14:02,974 --> 00:14:05,910
Psychologically speaking,
the attention he gets by rebelling,

256
00:14:06,030 --> 00:14:08,809
even to the point of self-destruction,
is more emotionally valuable

257
00:14:08,929 --> 00:14:10,646
than the help he would get at rehab.

258
00:14:10,766 --> 00:14:12,690
- I've got a solution.
- Great. What is it?

259
00:14:12,810 --> 00:14:13,879
Get out!

260
00:14:15,913 --> 00:14:16,794
Fine.

261
00:14:23,441 --> 00:14:25,045
I've hesitated to point this out,

262
00:14:25,205 --> 00:14:27,686
but I must now remind you
that we are in our current predicament

263
00:14:27,806 --> 00:14:30,241
because of your initial
and totally inadequate deceit.

264
00:14:30,361 --> 00:14:32,344
I'm just trying to clean up
after your mess.

265
00:14:36,584 --> 00:14:38,115
We'll talk in the morning.

266
00:14:50,984 --> 00:14:51,995
Morning.

267
00:14:53,214 --> 00:14:54,390
Who are you?

268
00:14:55,472 --> 00:14:57,619
I am Sheldon's cousin Leo.

269
00:15:02,178 --> 00:15:04,668
Sheldon does not have
a cousin Leo.

270
00:15:04,828 --> 00:15:05,978
<i>Au contraire.</i>

271
00:15:06,638 --> 00:15:08,128
I'm 26 years old.

272
00:15:08,248 --> 00:15:11,300
I'm originally
from... Denton, Texas,

273
00:15:12,268 --> 00:15:13,510
but I was a Navy brat,

274
00:15:13,671 --> 00:15:16,435
so I was brought up on a variety
of military bases around the world.

275
00:15:16,695 --> 00:15:18,984
As a result, I've often felt
like an outsider--

276
00:15:19,104 --> 00:15:20,553
never really fitting in,

277
00:15:20,673 --> 00:15:23,150
which is probably the reason
for my substance abuse problem.

278
00:15:23,270 --> 00:15:26,064
Excuse me,
we just went over this.

279
00:15:26,348 --> 00:15:28,900
As the quintessential middle child,
your addiction is rooted

280
00:15:29,061 --> 00:15:30,652
in your unmet need for attention.

281
00:15:30,812 --> 00:15:34,031
Sheldon, are we really going
to go with pop psychology?

282
00:15:34,191 --> 00:15:36,575
For your information,
this is based on solid research.

283
00:15:36,735 --> 00:15:39,328
Just stick with the character profile
I wrote for you.

284
00:15:40,505 --> 00:15:43,057
I'm sorry.
Leonard, this is Toby Loobenfeld.

285
00:15:43,177 --> 00:15:45,903
He's a research assistant
in the particle physics lab,

286
00:15:46,023 --> 00:15:48,420
but he also minored in theater at MIT.

287
00:15:49,104 --> 00:15:51,089
It was more of a double major, actually.

288
00:15:51,436 --> 00:15:52,924
Theater and physics.

289
00:15:53,084 --> 00:15:55,552
You can guess which one
my bourgeois parents pushed me toward.

290
00:15:55,713 --> 00:15:56,797
Yeah, I got it.

291
00:15:57,088 --> 00:15:59,014
Sheldon... why?

292
00:15:59,174 --> 00:16:02,023
You see, while Leo
would not have gone into rehab,

293
00:16:02,143 --> 00:16:05,126
it is completely plausible
that we would have talked him

294
00:16:05,246 --> 00:16:07,522
into leaving the motel
and coming home with us.

295
00:16:09,164 --> 00:16:12,034
How about this as my motivation--
when I was 14 years old,

296
00:16:12,154 --> 00:16:15,427
I was abused in the Philippines
by a clubfooted Navy chaplain.

297
00:16:17,664 --> 00:16:19,919
No. We're going with middle child

298
00:16:20,039 --> 00:16:23,789
and a genetic predisposition to
inadequate serotonin production.

299
00:16:23,949 --> 00:16:26,708
Swell. How do I play
genetic predisposition?

300
00:16:27,855 --> 00:16:29,741
Subtextually, of course.

301
00:16:33,067 --> 00:16:34,543
Just have fun with it.

302
00:16:40,041 --> 00:16:42,682
- Morning, Penny.
- How did the intervention go?

303
00:16:42,843 --> 00:16:46,269
Unfortunately, we weren't able
to convince him to go to rehab.

304
00:16:46,429 --> 00:16:48,909
Based on what you told me,
I am not surprised.

305
00:16:51,560 --> 00:16:54,071
But we did convince him
to leave the motel.

306
00:16:54,304 --> 00:16:55,320
Come say hello.

307
00:16:58,672 --> 00:16:59,888
This is Penny,

308
00:17:00,402 --> 00:17:01,685
our friend and neighbor.

309
00:17:01,805 --> 00:17:02,888
Hi, Leo.

310
00:17:03,396 --> 00:17:04,788
How are you feeling?

311
00:17:10,078 --> 00:17:11,628
Let me ask you something, Penny.

312
00:17:12,038 --> 00:17:14,730
Have you ever woken up
in a fleabag motel,

313
00:17:14,850 --> 00:17:16,503
covered in your own vomit,

314
00:17:16,623 --> 00:17:19,091
next to a transsexual prostitute?

315
00:17:22,871 --> 00:17:25,591
Then don't ask me
how I'm feeling.

316
00:17:28,758 --> 00:17:30,111
Well, that's Leo.

317
00:17:32,018 --> 00:17:34,590
why don't you tell me
about your showcase last night.

318
00:17:34,710 --> 00:17:36,945
It was okay, I guess.
It wasn't a big turnout,

319
00:17:37,106 --> 00:17:38,889
but they both really seemed to like it.

320
00:17:39,009 --> 00:17:40,744
There were only
two people there?

321
00:17:40,864 --> 00:17:42,339
By the end, yeah.

322
00:17:43,987 --> 00:17:45,881
Damn you, Chaplain Harrigan!

323
00:17:48,409 --> 00:17:49,750
I'm... I'm sorry?

324
00:17:49,911 --> 00:17:51,103
The Philippines.

325
00:17:53,820 --> 00:17:55,187
1992.

326
00:17:55,939 --> 00:17:58,016
The Subic Bay Naval Station.

327
00:17:58,136 --> 00:18:01,011
A young boy on the cusp of manhood.

328
00:18:01,778 --> 00:18:03,054
His only companions,

329
00:18:03,214 --> 00:18:06,135
mongrel dogs
and malarial mosquitoes.

330
00:18:06,760 --> 00:18:08,477
Desperate and alone,

331
00:18:08,637 --> 00:18:11,968
he reached out to a man
who promised to introduce him

332
00:18:12,088 --> 00:18:14,357
to a merciful, loving God,

333
00:18:14,517 --> 00:18:16,904
but who instead
introduced him

334
00:18:17,024 --> 00:18:21,281
to a gin-pickled tongue
shoved down his adolescent throat.

335
00:18:23,813 --> 00:18:26,150
What choice did he
have but to drink,

336
00:18:26,270 --> 00:18:29,346
shoot and snort his pain away?

337
00:18:31,413 --> 00:18:33,903
Don't forget his genetic predisposition
towards addiction.

338
00:18:34,023 --> 00:18:36,045
- It's never been proven.
- There have been studies.

339
00:18:36,206 --> 00:18:37,631
Not double-blind studies.

340
00:18:37,791 --> 00:18:40,217
How could there be?
Who would be the control group?

341
00:18:40,488 --> 00:18:42,600
As you can see,
detoxing can get pretty ugly.

342
00:18:42,720 --> 00:18:44,342
Let's give them some privacy.

343
00:18:44,848 --> 00:18:46,681
Do you want to come over
and have coffee?

344
00:18:46,841 --> 00:18:48,012
Sounds good.

345
00:18:48,132 --> 00:18:50,894
I have a video of me singing last night.
Do you want to see it?

346
00:18:52,099 --> 00:18:53,630
Gee, why wouldn't I?

347
00:18:54,849 --> 00:18:56,844
It's even better than you
coming to the showcase,

348
00:18:56,964 --> 00:18:58,620
now I get to watch you watch me.

349
00:19:00,582 --> 00:19:02,236
Funny how things work out.

350
00:19:03,843 --> 00:19:06,248
...but he loved the companionship
and the wisdom

351
00:19:06,368 --> 00:19:08,745
that his own father failed to provide.

352
00:19:09,785 --> 00:19:11,623
Your parents made the right decision.

353
00:19:17,542 --> 00:19:19,278
I cannot work like this.

354
00:19:29,412 --> 00:19:30,614
This is amazing.

355
00:19:31,366 --> 00:19:34,307
Just sitting on a couch
watching TV with a woman.

356
00:19:35,223 --> 00:19:37,807
Not being drunk or high

357
00:19:37,927 --> 00:19:40,259
or wondering
if you're a dude down there.

358
00:19:42,460 --> 00:19:45,768
Leo, you are a very sweet,
really funny guy.

359
00:19:46,176 --> 00:19:47,788
You're going to do okay.

360
00:19:48,670 --> 00:19:50,515
One day at a time, Penny.

361
00:19:54,379 --> 00:19:55,896
One day at a time.

362
00:20:01,137 --> 00:20:03,203
How long is he going to stay here?

363
00:20:03,962 --> 00:20:06,843
He's a homeless drug addict.
Where is he going to go?

364
00:20:07,923 --> 00:20:06,843
Boy, you have a lot
to learn about lying.

