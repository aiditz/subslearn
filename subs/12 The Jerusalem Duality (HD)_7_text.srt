1
00:00:06,237 --> 00:00:08,595
Here's the problem with teleportation.

2
00:00:12,106 --> 00:00:13,355
Lay it on me.

3
00:00:13,992 --> 00:00:16,890
Assuming a device could be invented
which would identify

4
00:00:17,010 --> 00:00:19,629
the quantum state of matter
of an individual in one location

5
00:00:19,790 --> 00:00:22,879
and transmit that pattern to a
distant location for reassembly,

6
00:00:22,999 --> 00:00:25,050
you would not have transported
the individual.

7
00:00:25,211 --> 00:00:27,475
You would have destroyed him
in one location

8
00:00:27,595 --> 00:00:29,564
and recreated him in another.

9
00:00:31,501 --> 00:00:32,753
How about that.

10
00:00:34,345 --> 00:00:36,345
Personally,
I would never use a transporter,

11
00:00:36,465 --> 00:00:39,024
because the original Sheldon
would have to be disintegrated

12
00:00:39,144 --> 00:00:41,145
in order to create a new Sheldon.

13
00:00:42,354 --> 00:00:46,055
Would the new Sheldon be in any way
an improvement on the old Sheldon?

14
00:00:47,267 --> 00:00:49,303
No, he would be exactly the same.

15
00:00:49,652 --> 00:00:51,092
That is a problem.

16
00:00:51,864 --> 00:00:52,954
So you see it too.

17
00:00:54,094 --> 00:00:56,040
Dr. Hofstadter. Dr. Cooper.

18
00:00:56,250 --> 00:00:57,416
Dr. Gablehauser.

19
00:00:57,724 --> 00:00:59,918
Gentlemen, I'd like
you to meet Dennis Kim.

20
00:01:00,314 --> 00:01:03,013
Dennis is a highly sought-after
doctoral candidate,

21
00:01:03,133 --> 00:01:05,257
and we're hoping
he'll do his graduate work here.

22
00:01:05,418 --> 00:01:08,980
- Graduate work. Very impressive.
- And he's only 15 years old.

23
00:01:09,100 --> 00:01:12,538
Not bad... I, myself,
started graduate school at 14.

24
00:01:12,943 --> 00:01:16,394
Well, I lost a year while my family
was tunneling out of North Korea.

25
00:01:18,939 --> 00:01:20,326
Advantage: Kim.

26
00:01:22,150 --> 00:01:24,440
I thought maybe you boys
could show Dennis around.

27
00:01:24,560 --> 00:01:27,776
Let him see why we're the best physics
research facility in the country.

28
00:01:27,896 --> 00:01:29,241
I already know you're not.

29
00:01:29,361 --> 00:01:32,570
You don't have an Open Science Grid
computer or a free electron laser.

30
00:01:32,690 --> 00:01:35,580
The string theory research being done
here is nothing but a dead end.

31
00:01:35,741 --> 00:01:39,144
Excuse me, that is my research,
and it is by no means a dead end.

32
00:01:39,662 --> 00:01:42,252
Obviously you don't see it yet,
but trust me, you will.

33
00:01:45,145 --> 00:01:46,879
Dennis, we've discussed this.

34
00:01:46,999 --> 00:01:49,176
We're in the process
of updating our equipment,

35
00:01:49,337 --> 00:01:51,512
and we welcome your input
on our research goals.

36
00:01:51,674 --> 00:01:53,177
We've agreed to look the other way

37
00:01:53,297 --> 00:01:55,569
if you use up to 20%
of the grant money you attract

38
00:01:55,689 --> 00:01:57,769
to smuggle your grandfather
out of Pyongyang.

39
00:01:59,523 --> 00:02:01,448
We want him here, boys.

40
00:02:01,891 --> 00:02:03,569
- Make it happen.
- Yes, sir.

41
00:02:03,689 --> 00:02:05,476
You can count on us.
We're on it.

42
00:02:05,596 --> 00:02:07,695
What the hell do you mean, "dead end"?

43
00:02:08,288 --> 00:02:11,238
I mean, the whole landscape
of false vacuums in string theory

44
00:02:11,358 --> 00:02:14,645
could be as large as 10
to the 500th power. In addition...

45
00:02:15,020 --> 00:02:16,286
Look, chocolate milk.

46
00:02:19,439 --> 00:02:21,786
I sense a disturbance in the Force.

47
00:02:22,573 --> 00:02:25,074
A bad feeling I have about this.

48
00:02:26,332 --> 00:02:28,547
The Big Bnag Theory
Season 1 Episode 12 - The Jerusalem Duality

49
00:02:30,099 --> 00:02:33,183
Transcript: swsub.com
Synchro: SerJo

50
00:02:50,841 --> 00:02:52,999
Dennis, how long
have you been in America?

51
00:02:53,119 --> 00:02:54,750
- A year and a half.
- No kidding.

52
00:02:54,870 --> 00:02:57,620
- You speak English really well.
- So do you.

53
00:02:58,419 --> 00:03:01,678
Except for your tendency
to end sentences with prepositions.

54
00:03:03,166 --> 00:03:05,016
What are you talking about?

55
00:03:06,410 --> 00:03:07,233
That.

56
00:03:08,587 --> 00:03:09,886
He's not wrong.

57
00:03:11,014 --> 00:03:13,131
All right... And this is my office.

58
00:03:13,342 --> 00:03:15,766
- Is this part of the tour?
- Nope. Goodbye.

59
00:03:16,634 --> 00:03:18,895
Sheldon,
we've hardly shown him anything.

60
00:03:19,057 --> 00:03:21,904
All right. This is my desk.

61
00:03:22,073 --> 00:03:25,651
These are my books; this is my door.
Please close it behind you. Goodbye.

62
00:03:28,016 --> 00:03:30,401
Looks like you're doing work
in quantum loop corrections.

63
00:03:30,521 --> 00:03:32,390
Keen observation. Goodbye.

64
00:03:33,196 --> 00:03:35,646
You see where you went wrong, don't you?

65
00:03:39,242 --> 00:03:40,166
Get him out.

66
00:03:40,914 --> 00:03:44,045
Come on. I'll show you the rec center.
They've got Nautilus equipment.

67
00:03:44,380 --> 00:03:46,005
Do I look like I lift weights?

68
00:03:47,186 --> 00:03:48,606
Not heavy ones.

69
00:03:49,870 --> 00:03:51,629
It's startling you haven't considered

70
00:03:51,749 --> 00:03:54,499
a Lorentz invariant
or field theory approach.

71
00:03:54,657 --> 00:03:56,279
You think I haven't considered it?

72
00:03:56,399 --> 00:03:58,435
You really think
I haven't considered it?

73
00:03:58,597 --> 00:04:00,108
Have you considered it?

74
00:04:01,369 --> 00:04:03,632
- Get him out, Leonard.
- Come on, Dennis.

75
00:04:03,752 --> 00:04:05,647
I'll show you the radiation lab.

76
00:04:06,936 --> 00:04:08,820
You won the Stevenson Award?

77
00:04:09,378 --> 00:04:12,921
Yes. In fact, I am
the youngest person ever to win it.

78
00:04:13,641 --> 00:04:15,569
Really? How old?

79
00:04:15,780 --> 00:04:17,119
Fourteen and a half.

80
00:04:17,280 --> 00:04:19,372
You were the youngest person
ever to win it.

81
00:04:23,173 --> 00:04:25,670
It's like looking into an obnoxious
little mirror, isn't it?

82
00:04:30,919 --> 00:04:32,972
This is really delicious, isn't it?

83
00:04:36,798 --> 00:04:39,394
Still can't talk to me
unless you're drunk, huh?

84
00:04:41,742 --> 00:04:44,008
Sweetie, you are so damaged.

85
00:04:46,266 --> 00:04:48,208
Hey, I'm damaged, too.

86
00:04:49,084 --> 00:04:50,767
How about a hug for Howie?

87
00:04:51,823 --> 00:04:53,580
Sure. Raj, hug Howard.

88
00:04:58,875 --> 00:05:00,413
Something you'd like to share?

89
00:05:01,636 --> 00:05:03,589
A tale of woe, perhaps?

90
00:05:04,514 --> 00:05:05,668
15 years old.

91
00:05:06,097 --> 00:05:09,623
Dennis Kim is 15 years old,
and he's already correcting my work.

92
00:05:09,868 --> 00:05:13,353
Today, I went from being
Wolfgang Amadeus Mozart, to...

93
00:05:14,009 --> 00:05:16,596
- You know, that other guy.
- Antonio Salieri?

94
00:05:17,644 --> 00:05:20,099
Oh God, now even you
are smarter than me.

95
00:05:21,412 --> 00:05:23,453
You know, you don't have so many friends

96
00:05:23,573 --> 00:05:25,689
that you can afford to
start insulting them.

97
00:05:26,808 --> 00:05:28,691
Just eat, Sheldon, you'll feel better.

98
00:05:28,902 --> 00:05:30,306
Why waste food?

99
00:05:30,426 --> 00:05:33,293
In Texas, when a cow goes dry,
they don't keep feeding it, they just...

100
00:05:33,413 --> 00:05:36,163
take her out and shoot
her between the eyes.

101
00:05:36,691 --> 00:05:39,332
I'm confused.
Did Sheldon stop giving milk?

102
00:05:41,130 --> 00:05:43,087
You can't let this kid get to you.

103
00:05:43,207 --> 00:05:46,618
You always knew that someday someone
younger and smarter would come along.

104
00:05:46,738 --> 00:05:49,585
Yes, but I assumed I would've
been dead hundreds of years

105
00:05:49,705 --> 00:05:53,008
and that there'd be an asterisk
by his name because he'd be a cyborg.

106
00:05:54,549 --> 00:05:57,924
So you've got a little competition.
I really don't see what the big deal is.

107
00:05:58,044 --> 00:06:01,072
Of course you don't.
You've never excelled at anything.

108
00:06:03,174 --> 00:06:06,730
I don't understand exactly how did
he get any friends in the first place?

109
00:06:07,661 --> 00:06:09,119
We liked Leonard.

110
00:06:11,361 --> 00:06:13,319
What are you gonna do, just give up?

111
00:06:15,179 --> 00:06:17,717
It's what a rational person does
when his entire life's work

112
00:06:17,837 --> 00:06:21,055
is invalidated
by a postpubescent Asian wunderkind.

113
00:06:22,350 --> 00:06:24,178
He ceases his fruitless efforts,

114
00:06:24,298 --> 00:06:26,743
he donates his body
to scientific research,

115
00:06:27,061 --> 00:06:28,463
and he waits to die.

116
00:06:30,540 --> 00:06:32,392
I'm confused again.
Is he waiting,

117
00:06:32,512 --> 00:06:34,991
or do we get to shoot
him between the eyes?

118
00:06:40,082 --> 00:06:43,091
I've decided you're right.
My career is not over.

119
00:06:43,301 --> 00:06:44,218
Great!

120
00:06:44,429 --> 00:06:47,106
Since the arrival of Dennis Kim
has rendered my research pointless,

121
00:06:47,226 --> 00:06:49,564
I just have to find something else
to focus on.

122
00:06:49,958 --> 00:06:50,849
Great!

123
00:06:51,060 --> 00:06:53,694
So I've decided.
I'm going to collaborate with you.

124
00:06:56,907 --> 00:06:57,767
Great.

125
00:06:58,568 --> 00:07:00,408
So what exactly is it you do?

126
00:07:01,631 --> 00:07:05,497
You chatter on about it all the time,
but I've never really paid attention.

127
00:07:06,345 --> 00:07:08,624
Right now, I'm designing an experiment

128
00:07:08,835 --> 00:07:11,205
to study the soft component
of cosmic radiation at see level,

129
00:07:11,325 --> 00:07:14,375
- but I really don't need any help.
- Sure you do.

130
00:07:15,126 --> 00:07:17,913
See, what's this here in the schematic?
Is that a laser array?

131
00:07:18,033 --> 00:07:19,503
- Yes.
- Now...

132
00:07:22,991 --> 00:07:25,933
What happens if you use argon lasers
instead of helium-neon?

133
00:07:26,455 --> 00:07:28,436
- It would blow up.
- Are you sure?

134
00:07:28,979 --> 00:07:30,021
Pretty sure.

135
00:07:30,183 --> 00:07:31,914
"Pretty sure" is not very scientific.

136
00:07:32,034 --> 00:07:35,402
Is this how you normally work?
Just hunches and guesses and stuff?

137
00:07:37,756 --> 00:07:40,615
I understand that you're going
through a bit of a career cris,

138
00:07:40,777 --> 00:07:42,867
you're searching for some other area

139
00:07:43,029 --> 00:07:44,957
where you can feel
valuable and productive,

140
00:07:45,077 --> 00:07:47,565
but I need to tell you something.
I want you to listen carefully.

141
00:07:47,685 --> 00:07:50,101
- All right.
- Go away.

142
00:07:53,010 --> 00:07:55,172
If you're concerned about
sharing credit with me,

143
00:07:55,333 --> 00:07:57,482
your name can go first...
I'm going.

144
00:08:01,876 --> 00:08:04,399
It's a small, brown paper bag, Ma!

145
00:08:04,519 --> 00:08:06,295
I'm looking in it right now.

146
00:08:07,686 --> 00:08:10,957
Why would I make that up?
There's no Ding Dong in it.

147
00:08:14,203 --> 00:08:17,332
How are two Ding Dongs tomorrow
gonna help me today?

148
00:08:19,653 --> 00:08:20,569
So...

149
00:08:21,146 --> 00:08:22,699
This is Engineering, huh?

150
00:08:23,105 --> 00:08:24,675
I'll talk to you later.

151
00:08:25,154 --> 00:08:26,342
Engineering.

152
00:08:26,462 --> 00:08:28,421
Where the noble semiskilled laborers

153
00:08:28,541 --> 00:08:30,958
execute the vision
of those who think and dream.

154
00:08:32,160 --> 00:08:34,210
Hello, Oompa-Loompas of science.

155
00:08:36,707 --> 00:08:38,589
Sheldon, what are you doing here?

156
00:08:39,140 --> 00:08:41,001
I just came by to say hello.

157
00:08:41,211 --> 00:08:43,844
I've been in this lab for 3 years.
You've never come by to say hello.

158
00:08:44,005 --> 00:08:46,465
Up until now,
I've had better things to do.

159
00:08:47,969 --> 00:08:49,828
So, what are we making today?

160
00:08:49,948 --> 00:08:51,998
A small payload support structure

161
00:08:52,118 --> 00:08:54,222
for a European science
experimental package

162
00:08:54,342 --> 00:08:57,925
- that's going up on a space shuttle.
- Really? How does it work?

163
00:08:58,045 --> 00:09:00,319
When this is done, it will be
attached to the payload bay,

164
00:09:00,481 --> 00:09:03,031
and the sensor apparatus
will rest on it.

165
00:09:05,940 --> 00:09:07,398
So, it's a shelf.

166
00:09:11,503 --> 00:09:13,583
No, you don't understand.
During acceleration,

167
00:09:13,745 --> 00:09:16,111
it needs to stay perfectly level
and provide...

168
00:09:16,231 --> 00:09:17,587
Yeah, it's a shelf.

169
00:09:18,417 --> 00:09:20,144
I notice you're using titanium.

170
00:09:20,264 --> 00:09:22,665
Did you give any consideration
to carbon nanotubes?

171
00:09:22,785 --> 00:09:26,304
They're lighter, cheaper,
and have twice the tensile strength.

172
00:09:26,932 --> 00:09:28,630
There's a diploma in my office

173
00:09:28,750 --> 00:09:31,268
that says I have a
master's in engineering.

174
00:09:31,477 --> 00:09:35,229
You also have a note from your mother
that says, "I love you, Bubula."

175
00:09:37,518 --> 00:09:40,943
But neither of those is a cogent
argument for titanium over nanotubes.

176
00:09:43,115 --> 00:09:44,616
Go away!

177
00:09:45,848 --> 00:09:47,450
Did Leonard tell you to say that?

178
00:09:48,056 --> 00:09:49,785
No, I thought of it all by myself.

179
00:09:52,670 --> 00:09:54,741
That can't be a coincidence.

180
00:09:55,114 --> 00:09:57,718
There must be
some causal link I'm missing.

181
00:10:00,898 --> 00:10:02,463
Go away!

182
00:10:11,214 --> 00:10:13,120
Curiouser and curiouser.

183
00:10:18,207 --> 00:10:20,655
- Is he here?
- If he were, I wouldn't be.

184
00:10:21,765 --> 00:10:24,404
Do you know what he did?
He watched me work for 10 min,

185
00:10:24,524 --> 00:10:28,493
and then started to design a simple
piece of software that could replace me.

186
00:10:29,863 --> 00:10:31,241
Is that even possible?

187
00:10:31,530 --> 00:10:33,135
As it turns out, yes.

188
00:10:34,984 --> 00:10:36,788
Something's got to be done about him.

189
00:10:36,999 --> 00:10:39,535
Like what? He'll never be
able to cope with the fact

190
00:10:39,655 --> 00:10:42,878
that some 15-year-old kid is smarter
and more accomplished than he is.

191
00:10:43,613 --> 00:10:45,540
What if something happened to this boy

192
00:10:45,660 --> 00:10:47,633
so he was no longer a threat to Sheldon?

193
00:10:49,315 --> 00:10:51,277
Then our problem would be solved.

194
00:10:52,279 --> 00:10:55,057
Hang on, are we talking
about murdering Dennis Kim?

195
00:10:56,394 --> 00:10:57,698
I'm not saying no.

196
00:10:58,882 --> 00:11:02,056
We don't have to go that far.
There are other means available.

197
00:11:02,176 --> 00:11:04,344
We can't send him back to North Korea.

198
00:11:04,830 --> 00:11:06,480
He knows how to get out.

199
00:11:08,559 --> 00:11:11,823
The only thing we need to do
is make this Kim kid lose his focus.

200
00:11:11,985 --> 00:11:14,868
Won't happen. He is not interested
in anything but physics.

201
00:11:15,079 --> 00:11:16,758
- What about biology?
- What?

202
00:11:16,878 --> 00:11:18,543
You know, biology.

203
00:11:20,137 --> 00:11:23,312
The one thing that can completely
derail a world-class mind.

204
00:11:23,705 --> 00:11:24,586
He's 15.

205
00:11:24,835 --> 00:11:27,406
Yeah, so? When I was 15,
I met Denise Palmeri

206
00:11:27,594 --> 00:11:30,655
and my grade point average
fell from a 5.0 to a 1.8.

207
00:11:31,588 --> 00:11:33,097
She was sleeping with you?

208
00:11:33,437 --> 00:11:34,589
No, I just...

209
00:11:34,978 --> 00:11:37,976
wasted a lot of time thinking
about what it would be like if she did.

210
00:11:39,443 --> 00:11:41,417
Oh, good, you're all here.

211
00:11:41,537 --> 00:11:43,406
Look, if the three of you drop

212
00:11:43,526 --> 00:11:45,509
whatever it is you're working on
and join me,

213
00:11:45,629 --> 00:11:49,346
we could lick cold fusion in less
than a decade. 12 years, tops.

214
00:11:52,188 --> 00:11:53,115
Go away?

215
00:11:57,498 --> 00:11:58,700
Could it be me?

216
00:12:02,589 --> 00:12:03,595
What's up?

217
00:12:03,787 --> 00:12:07,254
We need a hot 15-year-old Asian girl
with a thing for smart guys.

218
00:12:07,990 --> 00:12:08,797
What?

219
00:12:08,958 --> 00:12:12,327
Howard, that's racist.
Any 15-year-old girl will do the trick.

220
00:12:19,109 --> 00:12:21,859
It's possible she may have
misunderstood us.

221
00:12:28,165 --> 00:12:30,103
Dr. Cooper.
Are we interrupting?

222
00:12:30,314 --> 00:12:32,792
No, please come in.
I think you'll appreciate this.

223
00:12:32,912 --> 00:12:34,323
This is very exciting.

224
00:12:34,485 --> 00:12:36,588
- What are you working on?
- Something remarkable.

225
00:12:36,708 --> 00:12:39,245
Since my prospects for the Nobel prize
in physics have disappeared

226
00:12:39,455 --> 00:12:41,037
thank you very much,

227
00:12:41,174 --> 00:12:43,438
I've decided to refocus my efforts

228
00:12:43,558 --> 00:12:46,044
and use my people skills
to win the Nobel peace prize.

229
00:12:46,988 --> 00:12:48,670
I will solve
the Middle East crisis

230
00:12:48,901 --> 00:12:51,077
by building an exact replica
of Jerusalem

231
00:12:51,197 --> 00:12:53,447
in the middle of the Mexican desert.

232
00:12:54,355 --> 00:12:55,344
To what end?

233
00:12:55,684 --> 00:12:57,343
It's like the baseball movie.

234
00:12:57,463 --> 00:12:59,322
"Build it and they will come."

235
00:13:00,698 --> 00:13:01,725
Who will come?

236
00:13:02,054 --> 00:13:03,381
The Jewish people.

237
00:13:04,134 --> 00:13:07,189
- What if they don't come?
- We'll make it nice, put out a spread.

238
00:13:08,847 --> 00:13:10,943
Okay, well, speaking of spreads,

239
00:13:11,063 --> 00:13:13,895
we're having a small welcoming party
this afternoon for Mr. Kim

240
00:13:14,015 --> 00:13:16,406
who's agreed to join us here
at the university.

241
00:13:16,567 --> 00:13:17,922
Of course he has.

242
00:13:18,042 --> 00:13:20,808
The Oracle told us
little Neo was the one.

243
00:13:23,297 --> 00:13:25,447
You can see the matrix, can't you?

244
00:13:26,243 --> 00:13:29,477
Well, obviously you're very busy
with your...

245
00:13:30,093 --> 00:13:31,364
Come, Dennis.

246
00:13:32,938 --> 00:13:36,289
You'll have to excuse Dr. Cooper.
He's been under a lot of...

247
00:13:36,927 --> 00:13:38,103
He's nuts.

248
00:13:45,192 --> 00:13:48,473
They'll come, they'll settle,
and I'll win the prize.

249
00:13:50,349 --> 00:13:53,111
I really don't understand
your objections, professor Goldfarb.

250
00:13:53,321 --> 00:13:56,906
Why wouldn't the Sonora desert
make a perfectly good promised land?

251
00:13:57,832 --> 00:13:58,871
Go away.

252
00:14:00,590 --> 00:14:03,003
We could call it Nuevo Jerusalem.

253
00:14:04,143 --> 00:14:05,498
Please go away.

254
00:14:06,780 --> 00:14:08,444
Said Pharaoh to Moses.

255
00:14:10,188 --> 00:14:12,005
Why are all these young women here?

256
00:14:12,165 --> 00:14:13,957
It's Take Your Daughter To Work Day.

257
00:14:14,650 --> 00:14:16,393
Really?
I was not aware of that.

258
00:14:16,805 --> 00:14:18,903
Yes. There was a very official e-mail

259
00:14:19,063 --> 00:14:21,594
sent to everyone
whose insurance files indicated

260
00:14:21,714 --> 00:14:24,350
they had daughters between
the ages of 14 and 16.

261
00:14:25,900 --> 00:14:27,335
- Smooth.
- Thank you.

262
00:14:29,463 --> 00:14:31,247
There's the man of the hour.

263
00:14:32,982 --> 00:14:35,320
Okay, so we now have
a socially-awkward genius

264
00:14:35,480 --> 00:14:37,779
in a room full of attractive,
age-appropriate women.

265
00:14:37,899 --> 00:14:40,241
All he has to do now is
hook up with one of them.

266
00:14:48,293 --> 00:14:50,519
Anyone else see the flaw in this plan?

267
00:14:52,188 --> 00:14:54,560
- We need a social catalyst.
- Like what?

268
00:14:54,680 --> 00:14:56,829
We can't get 15-year-old girls drunk.

269
00:14:57,803 --> 00:14:59,552
- Or can we?
- No, we can't.

270
00:15:00,758 --> 00:15:03,780
I don't think you mean "we can't."
I think you mean "we shouldn't."

271
00:15:04,988 --> 00:15:08,172
You're a Jew.
If there was another Wailing Wall

272
00:15:08,387 --> 00:15:10,014
exactly like the one in Jerusalem

273
00:15:10,134 --> 00:15:12,613
but close to taco stands
and cheap prescription drugs,

274
00:15:12,733 --> 00:15:14,525
would you still be able to wail at it?

275
00:15:16,889 --> 00:15:18,699
Okay, it's definitely me.

276
00:15:21,338 --> 00:15:23,792
We cannot leave this to chance.
Let's pick a girl

277
00:15:23,952 --> 00:15:26,831
and figure out how to get her
together with Dennis.

278
00:15:27,437 --> 00:15:28,708
How about that one?

279
00:15:29,522 --> 00:15:30,600
I know the type.

280
00:15:30,720 --> 00:15:32,977
Cheerleader, student council,
goes out with the jocks,

281
00:15:33,097 --> 00:15:35,254
won't even look at anybody
in the gifted program.

282
00:15:35,416 --> 00:15:37,644
If after 2 years of begging,
she agrees to go out with you,

283
00:15:37,764 --> 00:15:40,606
it turns out to be a setup and you're
in your mom's car with your pants off

284
00:15:40,726 --> 00:15:42,783
while the whole football team
laughs at y...

285
00:15:45,844 --> 00:15:48,441
- Are you crying?
- No, I have allergies.

286
00:15:50,442 --> 00:15:51,365
Okay...

287
00:15:52,004 --> 00:15:53,084
How about her?

288
00:15:54,604 --> 00:15:56,450
Sure, if he wants to spend two years

289
00:15:56,570 --> 00:15:59,108
doing her homework
while she drinks herself into a stupor

290
00:15:59,228 --> 00:16:00,703
with nonfat White Russians.

291
00:16:00,823 --> 00:16:03,328
You're holding her head
out of the toilet while she's puking

292
00:16:03,448 --> 00:16:05,494
and telling you she wishes
"more guys were like you."

293
00:16:05,614 --> 00:16:07,786
Then she gets into Cornell
because you wrote her essay

294
00:16:07,977 --> 00:16:09,937
for her and you drive up
to visit her one weekend

295
00:16:10,097 --> 00:16:12,164
and she acts
like she doesn't even know you.

296
00:16:15,132 --> 00:16:16,663
Okay, so not her, either.

297
00:16:18,406 --> 00:16:19,490
How about her?

298
00:16:20,231 --> 00:16:21,686
Interesting,
kind of pretty,

299
00:16:21,846 --> 00:16:24,095
a little chubby,
so probably low self-esteem.

300
00:16:24,215 --> 00:16:26,875
I think that's our girl.
One of us should go talk to her.

301
00:16:26,995 --> 00:16:28,791
I can't talk to her... you do it.

302
00:16:28,968 --> 00:16:32,270
I can't just go up and talk to her.
Howard, you talk to her.

303
00:16:33,354 --> 00:16:36,398
She'll never go for the kid
once she gets a peek at this.

304
00:16:38,924 --> 00:16:40,570
In India, this would be simpler.

305
00:16:40,730 --> 00:16:43,871
5 min with her dad, 20 goats
and a laptop, and we'd be done.

306
00:16:44,684 --> 00:16:47,744
- We're not in India.
- Why don't we do it your way then?

307
00:16:47,904 --> 00:16:50,537
We'll arrange for this girl
to move in across the hall from Dennis

308
00:16:50,697 --> 00:16:53,241
so he can pathetically
moon over her for months on end.

309
00:16:55,297 --> 00:16:58,039
- Okay, that was uncalled for.
- You started it, dude.

310
00:16:59,183 --> 00:17:01,256
Could I have everyone's
attention, please?

311
00:17:02,265 --> 00:17:05,141
What a wonderful occasion this is,

312
00:17:05,498 --> 00:17:07,570
and how fortunate
that it should happen to fall

313
00:17:07,730 --> 00:17:09,593
on Take Your Daughter to Work Day.

314
00:17:10,458 --> 00:17:14,222
We're here to welcome
Mr. Dennis Kim to our little family.

315
00:17:14,729 --> 00:17:16,158
Welcome, Dennis Kim.

316
00:17:17,852 --> 00:17:18,775
Mr. Kim

317
00:17:18,936 --> 00:17:21,837
was not only the valedictorian
at Stanford University,

318
00:17:21,997 --> 00:17:24,944
he is also the youngest recipient

319
00:17:25,104 --> 00:17:27,025
of the prestigious Stevenson Award.

320
00:17:27,285 --> 00:17:29,302
Youngest till the cyborgs rise up!

321
00:17:30,574 --> 00:17:32,539
And now, without any further ado,

322
00:17:32,659 --> 00:17:34,936
let me introduce
the man of the hour,

323
00:17:35,056 --> 00:17:36,717
Mr. Dennis Kim.

324
00:17:42,722 --> 00:17:43,582
What?!

325
00:17:45,336 --> 00:17:48,053
Would you like to tell us a little bit
about your upcoming research?

326
00:17:48,545 --> 00:17:50,830
No, thanks.
I'm going to the mall with Emma.

327
00:17:56,792 --> 00:17:58,187
The kid got a girl.

328
00:17:59,598 --> 00:18:00,688
Unbelievable.

329
00:18:01,497 --> 00:18:03,403
Did anyone see how he did it?

330
00:18:05,657 --> 00:18:07,031
Don't worry, I've got this.

331
00:18:07,560 --> 00:18:09,108
Ladies and gentlemen,

332
00:18:09,610 --> 00:18:11,049
honored daughters,

333
00:18:11,526 --> 00:18:14,829
while Mr. Kim,
by virtue of his youth and naivete,

334
00:18:14,949 --> 00:18:18,160
has fallen prey to the inexplicable need
for human contact,

335
00:18:18,280 --> 00:18:22,111
let me step in and assure you that
my research will go on uninterrupted,

336
00:18:22,231 --> 00:18:26,146
and that social relationships
will continue to baffle and repulse me.

337
00:18:28,035 --> 00:18:28,962
Thank you.

338
00:18:32,049 --> 00:18:34,017
- He's back.
- Mission accomplished.

339
00:18:34,353 --> 00:18:37,437
Forget the mission. How did
that little yutz get a girl on his own?

340
00:18:38,217 --> 00:18:40,586
I guess times have changed
since we were young.

341
00:18:40,706 --> 00:18:42,465
Smart is the new sexy.

342
00:18:43,558 --> 00:18:46,354
Then why do we go home
alone every night? We're still smart.

343
00:18:47,018 --> 00:18:48,510
Maybe we're too smart.

344
00:18:48,935 --> 00:18:50,400
So smart it's off-putting.

345
00:18:51,924 --> 00:18:53,571
Yeah, let's go with that.

346
00:18:58,099 --> 00:18:59,170
Unbelievable.

347
00:18:59,503 --> 00:19:02,140
Components I built
are on the International Space Station

348
00:19:02,260 --> 00:19:04,774
and I get a ticket for launching
a model rocket in the park.

349
00:19:04,935 --> 00:19:07,220
I don't know if the ticket was
so much for the launch

350
00:19:07,340 --> 00:19:09,241
as it was for you telling
the policewoman,

351
00:19:09,361 --> 00:19:12,274
"You have to frisk me.
I have another rocket in my pants."

352
00:19:12,972 --> 00:19:14,076
Look at that.

353
00:19:16,224 --> 00:19:17,439
It's Dennis Kim.

354
00:19:18,652 --> 00:19:20,503
I almost didn't recognize him.

355
00:19:21,201 --> 00:19:23,406
I kind of feel bad
about what we did to him.

356
00:19:27,773 --> 00:19:29,739
Yeah, we really ruined his life.

357
00:19:31,585 --> 00:19:29,739
Screw him...
He was weak.

