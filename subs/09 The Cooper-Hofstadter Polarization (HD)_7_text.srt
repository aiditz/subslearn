1
00:00:02,116 --> 00:00:02,866
Ok...

2
00:00:04,734 --> 00:00:06,462
The X10's are online

3
00:00:06,849 --> 00:00:12,807
Gentlemen, I am now about to send a signal
from this laptop through our local ISP

4
00:00:12,927 --> 00:00:16,960
racing down fibre optic cable at
the speed of light to San Fransisco,

5
00:00:17,080 --> 00:00:20,133
bouncing off a satellite in
geosynchronous orbit to Lisbon, Portugal,

6
00:00:20,253 --> 00:00:23,073
where the data packets will be headed off
to submerge transatlantic cables,

7
00:00:23,193 --> 00:00:24,879
terminating in Halifax, Nova Scotia

8
00:00:24,971 --> 00:00:28,284
and transfered across the continent
via microwave relays back to our ISP

9
00:00:28,404 --> 00:00:31,611
And the extend receiver attached to this...

10
00:00:32,596 --> 00:00:33,456
Lamp.

11
00:00:36,435 --> 00:00:38,482
Look at me, look at me. I've got juice bumps.

12
00:00:38,602 --> 00:00:41,387
- Are we ready for the stereo?
- Go for stereo.

13
00:01:03,146 --> 00:01:05,648
- Hey guys.
- Hello.

14
00:01:06,018 --> 00:01:07,353
It's a little loud.

15
00:01:07,473 --> 00:01:09,382
No problem...
Turning it down.

16
00:01:10,022 --> 00:01:13,882
San Francisco, Lisbon, Halifax...
...et voilа!

17
00:01:15,073 --> 00:01:18,868
- OK, thanks.
- Hang on, hang on. Do you not
realise what we just did?

18
00:01:19,125 --> 00:01:21,257
Yeah, you turned your stereo down with your laptop.

19
00:01:21,581 --> 00:01:26,147
No, we turned our stereo down, by sending
a signal around the world via the Internet.

20
00:01:27,471 --> 00:01:29,853
Oh. You know you can get one of those
universal remotes from Radioshack?

21
00:01:29,882 --> 00:01:31,334
They're really cheap.

22
00:01:32,684 --> 00:01:34,360
No, you don't get it.
Howard,

23
00:01:34,480 --> 00:01:37,618
- Enable public access.
- Public access enabled.

24
00:01:41,530 --> 00:01:44,662
- Boy, that's terrific. I'll see ya.
- No hang on, hang on...

25
00:01:47,165 --> 00:01:48,047
See?

26
00:01:49,680 --> 00:01:50,319
No.

27
00:01:51,201 --> 00:01:55,017
Someone in Sichuan province,
China, is using his computer

28
00:01:55,137 --> 00:01:56,649
to turn our lights on and off.

29
00:01:58,017 --> 00:01:59,627
Oh, that's handy.

30
00:02:00,995 --> 00:02:02,980
Here's a question...
Why?

31
00:02:04,679 --> 00:02:06,553
Because we can.

32
00:02:07,922 --> 00:02:10,238
They found our remote control cars!

33
00:02:12,752 --> 00:02:16,061
- Wait, what's on top of that?
- Wireless webcams, wave hello!

34
00:02:17,032 --> 00:02:19,789
The monster truck is out of Austin, Texas,

35
00:02:19,909 --> 00:02:23,478
and the blue Viper is being operated
from suburban Tel Aviv.

36
00:02:26,032 --> 00:02:28,657
- You may want to put on slacks.
- What?

37
00:02:31,392 --> 00:02:34,569
Ew, stop it! No!
Leave me alone!

38
00:02:38,054 --> 00:02:40,745
- Who's running the red Corvette?
- That would be me.

39
00:02:47,047 --> 00:02:51,347
The Big Bang Theory
Season 1 Episode 09 - The Cooper-Hofstadter Polarization
Synchro: SerJo

40
00:03:05,860 --> 00:03:09,257
You know in the future,
when we're disembodied brains in jars,

41
00:03:09,377 --> 00:03:12,081
we're going to look at this as eight hours well wasted.

42
00:03:14,000 --> 00:03:17,287
I don't want to be in a jar.
I want my brain in an android body.

43
00:03:18,632 --> 00:03:20,397
Eight feet tall and ribbed.

44
00:03:21,698 --> 00:03:22,801
I'm with you.

45
00:03:22,921 --> 00:03:26,331
I just have to make sure if I'm
a synthetic human I'd still be Jewish.

46
00:03:28,493 --> 00:03:29,574
I promised my mother.

47
00:03:31,846 --> 00:03:35,375
I suppose you could have
your android penis circumcised.

48
00:03:37,426 --> 00:03:40,635
But that's something your rabbi
would have to discuss with the manufacturer.

49
00:03:41,154 --> 00:03:43,933
Not to mention you'd have to
power down on saturdays.

50
00:03:45,963 --> 00:03:48,281
Sheldon, why is this letter in the trash?

51
00:03:48,401 --> 00:03:49,978
Well, there's always the possibility that

52
00:03:50,079 --> 00:03:52,979
a trash can spontaneously formed around the letter

53
00:03:53,662 --> 00:03:56,441
but Occam's rasor would suggest
that someone threw it out.

54
00:03:57,522 --> 00:04:00,632
It's from the Institute of
Experimental Physics, they want us to

55
00:04:00,691 --> 00:04:02,884
present our paper on the properties of supersolids

56
00:04:02,913 --> 00:04:05,873
at the topical conference of Bose-Einstein condensates.

57
00:04:05,993 --> 00:04:08,132
I know.
I read it before I threw it out.

58
00:04:09,831 --> 00:04:12,059
Ok, if I may drill down to the bedrock of my question:

59
00:04:12,179 --> 00:04:13,471
why did you throw it out?

60
00:04:13,591 --> 00:04:17,353
Because I have no interest in standing in
the Rose room of the Pasadena Marriot,

61
00:04:17,473 --> 00:04:19,647
in front of a group of judgmental strangers

62
00:04:19,767 --> 00:04:23,110
who wouldn't recognise true genius if
it was standing in front of them giving a speech.

63
00:04:24,169 --> 00:04:25,735
Which if I were there, would be.

64
00:04:27,049 --> 00:04:28,186
I don't know Sheldon.

65
00:04:28,187 --> 00:04:31,561
Those topical conferences on
Bose-Einstein condensates' parties are legendary

66
00:04:32,399 --> 00:04:35,818
- Forget the parties!
- Forget the parties? What a nerd.

67
00:04:37,870 --> 00:04:40,495
Are there any other honours I've got that I don't know about?

68
00:04:40,615 --> 00:04:43,429
Did the UPS dropped off a
Nobel prize with my name on it?

69
00:04:43,700 --> 00:04:46,693
Leonard, please don't take this the wrong way,
but the day you win a Nobel prize,

70
00:04:46,813 --> 00:04:50,893
is the day I begin my research on the drag coefficient of tassles on flying carpets.

71
00:04:53,210 --> 00:04:55,747
the only thing missing from that insult was: "your mama".

72
00:04:57,533 --> 00:04:58,526
I got one...
Hey Leonard,

73
00:04:58,646 --> 00:05:01,950
- Your mama's research methodology is so flawed...
- Shut up, Howard.

74
00:05:03,243 --> 00:05:05,572
- Sheldon, we have to do this.
- No, we don't.

75
00:05:05,627 --> 00:05:07,970
We have to take a nourishment, to expel waste,

76
00:05:08,039 --> 00:05:10,629
and inhale enough oxygen to keep our cells from dying.

77
00:05:10,711 --> 00:05:12,136
Everything else is optional.

78
00:05:13,181 --> 00:05:15,209
Ok, let me put it this way:
I'm doing it.

79
00:05:15,291 --> 00:05:17,539
You can't. I'm the lead author.

80
00:05:17,731 --> 00:05:21,389
Come on, the only reason you're the lead author is because we went alphabetically.

81
00:05:21,413 --> 00:05:23,939
I let you think we went alphabetically to spare you

82
00:05:23,967 --> 00:05:26,361
the humiliation with dealing with the fact that it was my idea.

83
00:05:26,391 --> 00:05:29,488
Now to put too fine a point to it, but I was throwing you a bone.

84
00:05:30,365 --> 00:05:31,365
You're welcome.

85
00:05:31,886 --> 00:05:35,339
Excuse me, I designed the experiment that proved the hypothesis.

86
00:05:35,462 --> 00:05:37,216
That doesn't mean proving.

87
00:05:37,504 --> 00:05:40,889
So the entire scientific community is just supposed to take your word?

88
00:05:40,944 --> 00:05:42,903
They're not supposed to, but they should.

89
00:05:43,986 --> 00:05:45,616
Alright, I don't care what you say.

90
00:05:45,671 --> 00:05:47,987
I'm going to the conference and
I'm presenting our findings

91
00:05:48,042 --> 00:05:49,919
And I forbid it.

92
00:05:50,673 --> 00:05:51,906
You forbid it?

93
00:05:51,961 --> 00:05:54,537
If I'm not taking credit for our work then nobody is.

94
00:05:54,592 --> 00:05:56,826
- So, you admit that it's our work.
- No.

95
00:05:56,894 --> 00:05:58,963
Once again, I'm throwing
you a bone.

96
00:05:59,772 --> 00:06:01,485
And once again, you are welcome.

97
00:06:10,638 --> 00:06:13,680
So, how's it going with Sheldon?
Are you guys still not talking to each other?

98
00:06:13,804 --> 00:06:15,448
Not only is he still not talking to me,

99
00:06:15,503 --> 00:06:18,819
but there's this thing he does where he stares
at you and  tries to get your brain to explode.

100
00:06:21,107 --> 00:06:24,287
You know, like in the classic sci-fi movie "Scanners"?

101
00:06:24,985 --> 00:06:26,506
Like.... Bzzz.

102
00:06:28,329 --> 00:06:29,535
Nevermind.

103
00:06:29,727 --> 00:06:31,234
How about this one?

104
00:06:31,700 --> 00:06:35,276
It says, I know my physics, but I'm still a fun guy.

105
00:06:35,948 --> 00:06:39,086
Oh, I didn't know they still made corduroy suits.

106
00:06:39,154 --> 00:06:41,374
They don't, that's why I saved this one.

107
00:06:42,210 --> 00:06:45,348
Ok, well, let's just see what else you have.

108
00:06:45,705 --> 00:06:47,609
Ok, here. Take this.

109
00:06:47,774 --> 00:06:51,665
and this, and this,
and this, and these...

110
00:06:51,761 --> 00:06:53,447
Is this all stuff you want me to try on?

111
00:06:53,474 --> 00:06:55,283
No, this is stuff I want you to throw out.

112
00:06:56,434 --> 00:07:00,049
Seriously, don't even give it to charity,
you won't be helping anyone.

113
00:07:00,901 --> 00:07:02,450
What's this?

114
00:07:02,792 --> 00:07:05,273
That's the bottled city of Kandor.

115
00:07:08,137 --> 00:07:12,152
You see, Kandor was the
capital city of the planet Krypton.

116
00:07:12,302 --> 00:07:14,978
It was miniaturised by Brainiac before Krypton exploded

117
00:07:15,015 --> 00:07:16,875
and then rescued by Superman.

118
00:07:18,030 --> 00:07:20,058
Oh, nice.

119
00:07:21,223 --> 00:07:23,950
It's a lot cooler when girls aren't looking at it.

120
00:07:24,891 --> 00:07:28,116
Here, why don't you put these while I find
a shirt and a sport cut match.

121
00:07:28,143 --> 00:07:29,157
Right, be right back.

122
00:07:29,198 --> 00:07:30,939
Where are you going? Just put them on.

123
00:07:30,980 --> 00:07:31,953
Here?

124
00:07:32,021 --> 00:07:33,761
Oh, are you shy?

125
00:07:33,926 --> 00:07:36,434
- No, I'm not shy.
- Don't worry I won't look.

126
00:07:36,598 --> 00:07:38,283
I know you won't look.
Why would you look?

127
00:07:38,332 --> 00:07:39,805
There's nothing to see...
Well, not 'nothing'...

128
00:07:39,847 --> 00:07:42,298
Sweetie, put the pants on.

129
00:07:44,066 --> 00:07:46,752
So, you know, isn't there maybe
some way you and Sheldon could

130
00:07:46,793 --> 00:07:50,233
- compromise on this whole presentation thing?
- No, No.

131
00:07:50,370 --> 00:07:52,055
Scientists do not compromise.

132
00:07:52,083 --> 00:07:54,172
Our minds are trained to synthetize facts

133
00:07:54,210 --> 00:07:56,097
and come to inarguable conclusions.

134
00:07:56,358 --> 00:07:59,263
Not to mention, Sheldon is back-crap crazy.

135
00:08:02,374 --> 00:08:03,949
What is this?

136
00:08:04,086 --> 00:08:06,046
Oh, careful.

137
00:08:06,498 --> 00:08:09,998
That's my original series Battlestar Galactica flight suit.

138
00:08:10,458 --> 00:08:12,692
Oh, why didn't you wear it on Halloween?

139
00:08:13,062 --> 00:08:15,063
Because it's not a costume, it's a flight suit.

140
00:08:18,379 --> 00:08:20,475
Ok, alright, moving on.

141
00:08:20,571 --> 00:08:22,805
Oh, wow. A paisley shirt.

142
00:08:22,846 --> 00:08:25,518
It goes with my corduroy suit.

143
00:08:25,861 --> 00:08:28,766
if you mean it should end up in
the same place, then I agree.

144
00:08:30,164 --> 00:08:32,315
Is this your only tie?

145
00:08:33,356 --> 00:08:36,234
Technically, yes. But, if you'll notice...

146
00:08:37,084 --> 00:08:38,934
It's reversible!

147
00:08:41,099 --> 00:08:42,784
So it works as two.

148
00:08:42,853 --> 00:08:45,758
Sweetie, I don't even think it works as one.

149
00:08:47,564 --> 00:08:49,226
- Is this all your clothes?
- Eh, yeah.

150
00:08:49,252 --> 00:08:51,143
Everything since the eighth grade.

151
00:08:51,266 --> 00:08:54,089
- The eighth grade?
- My last growth sprout

152
00:08:56,213 --> 00:08:59,283
Ok, well, let's go back to the curdoroy suit.

153
00:08:59,324 --> 00:09:01,270
- Great.
- Yeah.

154
00:09:02,941 --> 00:09:04,788
I said no. Put it down.

155
00:09:10,697 --> 00:09:12,725
Hey Sheldon.

156
00:09:12,986 --> 00:09:14,246
Hello Penny.

157
00:09:15,754 --> 00:09:17,672
Get anything good?

158
00:09:19,317 --> 00:09:22,770
Just the latest copy of
Applied Particle Physics quarterly.

159
00:09:22,838 --> 00:09:26,086
Oh, you know, that is so weird
that yours came and mine didn't.

160
00:09:31,869 --> 00:09:33,445
It was a joke.

161
00:09:37,706 --> 00:09:39,597
Yep.

162
00:09:39,803 --> 00:09:42,256
Tip you waitresses.
I'm here all week.

163
00:09:43,270 --> 00:09:45,407
Penny, just to save you from further awkwardness,

164
00:09:45,476 --> 00:09:48,929
know that I'm perfectly comfortable with
the two of us climbing the stairs in silence.

165
00:09:49,176 --> 00:09:51,711
Oh, yeah ok, me too. Zip it, lock it.

166
00:09:54,451 --> 00:09:55,644
Put it in your pocket.

167
00:10:00,042 --> 00:10:02,550
- So, you and Leonard...
- Oh dear God...

168
00:10:03,251 --> 00:10:04,851
Little misunderstanding, huh?

169
00:10:05,378 --> 00:10:06,700
A little misunderstanding...?

170
00:10:06,940 --> 00:10:10,065
Galileo and the Pope had a little misunderstanding.

171
00:10:13,729 --> 00:10:17,554
Anyway, I was talking to Leonard this morning
and I think he feels really bad about it.

172
00:10:19,926 --> 00:10:21,273
How do you feel?

173
00:10:22,638 --> 00:10:24,089
I don't understand the question.

174
00:10:25,539 --> 00:10:28,847
I'm just asking if it's difficult
to be fighting with your best friend.

175
00:10:30,286 --> 00:10:32,157
I haven't thought about it like that.

176
00:10:32,437 --> 00:10:35,327
I wonder if I've been experiencing physiological manifestations

177
00:10:35,447 --> 00:10:37,990
of some sort of unconscious emotional turmoil

178
00:10:39,386 --> 00:10:40,415
Wait, what?

179
00:10:40,630 --> 00:10:42,378
I couldn't poop this morning.

180
00:10:46,909 --> 00:10:49,839
You should just talk to him,
I'm sure you guys can work this out.

181
00:10:50,218 --> 00:10:52,996
- Certainly preferable to my plan.
- Which was?

182
00:10:53,386 --> 00:10:54,806
A powerful laxative.

183
00:10:57,025 --> 00:10:59,187
You absolutely should talk to him.

184
00:10:59,360 --> 00:11:01,778
Look, I know Leonard values you
as a friend and he told me himself

185
00:11:01,986 --> 00:11:05,458
without your little idea there's no way
he could come up with this whole experiment thing.

186
00:11:09,792 --> 00:11:11,509
Excuse me. "Little idea"?

187
00:11:11,754 --> 00:11:13,521
Yeah, I mean he tried to explain it to me,

188
00:11:13,641 --> 00:11:15,300
- I didn't really understand it but...
- Of course you didn't.

189
00:11:15,420 --> 00:11:16,970
He said: "Little idea"?

190
00:11:18,518 --> 00:11:20,224
Oh, well, no...
Not...

191
00:11:20,408 --> 00:11:22,734
- Not in those words.
- In what words then, exactly?

192
00:11:23,192 --> 00:11:26,153
Oh, you know... Gee. The exact words are...
It's more the spirit in which he said...

193
00:11:26,273 --> 00:11:28,694
- What did he say?
- You had a lucky hunch

194
00:11:30,547 --> 00:11:32,691
Hey, Sheldon, I've been thinking, instead of arguing about this...

195
00:11:32,811 --> 00:11:34,788
Don't you ever speak to me again.

196
00:11:37,360 --> 00:11:38,371
What...?

197
00:11:54,033 --> 00:11:56,061
Ok, I'm leaving for the conference.

198
00:11:56,428 --> 00:11:58,761
Have fun presenting my "lucky hunch"

199
00:11:58,881 --> 00:12:00,595
Shel, I didn't mean it like that.

200
00:12:00,715 --> 00:12:02,381
- Then why did you say it?
- I don't know, I wasn't...

201
00:12:02,501 --> 00:12:06,250
- Were you trying to impress Penny?
- No, no, not at all. A little bit.

202
00:12:07,524 --> 00:12:09,297
How did that work out for you?

203
00:12:09,585 --> 00:12:13,105
- Leonard, ready to go?
- Libido: 1 Truth: 0.

204
00:12:15,965 --> 00:12:17,238
Ok, I'm gonna ask you one more time.

205
00:12:17,358 --> 00:12:19,888
We did the work together,
let's present the paper together.

206
00:12:20,008 --> 00:12:21,820
And I'm telling you for the last time,

207
00:12:21,940 --> 00:12:24,770
it's pandering, it's undignifying, and bite me.

208
00:12:27,159 --> 00:12:28,164
Let's go.

209
00:12:28,402 --> 00:12:30,284
- Bye Sheldon.
- Goodbye Penny.

210
00:12:41,503 --> 00:12:43,062
One of these days... bshh!

211
00:12:46,623 --> 00:12:47,628
There you go.

212
00:12:47,927 --> 00:12:49,413
You're right this side does look better.

213
00:12:49,670 --> 00:12:52,436
No, no, I didn't say better, I said "less stained".

214
00:12:54,550 --> 00:12:57,425
I just checked the house.
There's probably 20-25 people in there.

215
00:12:57,545 --> 00:12:59,041
- You're kidding!
- Is that all?

216
00:12:59,231 --> 00:13:02,570
"All?"
In particle physics 25 is Woodstock!

217
00:13:05,903 --> 00:13:07,950
- Then good!
- I wasn't expecting such a crowd,

218
00:13:08,070 --> 00:13:09,072
I'm all nervous.

219
00:13:09,192 --> 00:13:11,368
It's ok, just open with a joke,
you'll be fine.

220
00:13:11,686 --> 00:13:13,587
A joke... Ok.

221
00:13:15,001 --> 00:13:16,500
How about this? Um, ok...

222
00:13:16,620 --> 00:13:18,066
There's this farmer

223
00:13:18,335 --> 00:13:22,117
and he has these chickens but they won't lay any eggs, so...

224
00:13:22,429 --> 00:13:24,238
he calls a physicist to help.

225
00:13:24,459 --> 00:13:27,181
The physicist then does some calculations,

226
00:13:27,433 --> 00:13:30,771
and he says: "I have a solution,

227
00:13:31,138 --> 00:13:35,349
but it only works for spherical chickens in a vacuum".

228
00:13:37,910 --> 00:13:38,915
Right?

229
00:13:43,761 --> 00:13:45,850
Oh, sorry, I just had heard it before.

230
00:13:46,557 --> 00:13:49,603
- Let's roll. Hey, nice suit.
- It's a classic, right?

231
00:13:51,087 --> 00:13:53,488
I really should have brought my own car.

232
00:13:56,102 --> 00:13:57,692
So, in conclusion,

233
00:13:57,812 --> 00:14:00,592
the data show that at temperatures approaching absolute zero,

234
00:14:00,712 --> 00:14:04,211
the moment of inertia changes, and a solid becomes a supersolid,

235
00:14:04,331 --> 00:14:08,352
which clearly appears to be a previously unknown state of matter.

236
00:14:11,254 --> 00:14:12,344
Thank you!

237
00:14:17,510 --> 00:14:19,557
- Are there any questions?
- Yeah.

238
00:14:19,790 --> 00:14:21,551
What the hell was that?

239
00:14:24,192 --> 00:14:25,282
Any other questions?

240
00:14:25,886 --> 00:14:30,438
Doctor Sheldon Cooper here, I am
the lead author of this particular paper.

241
00:14:32,126 --> 00:14:33,161
Thank you.

242
00:14:35,173 --> 00:14:39,566
And you sir, you have completely skipped
over the part where I was walking through the park

243
00:14:39,829 --> 00:14:42,956
and I saw these children on a
merry-go-round which started me thinking

244
00:14:43,076 --> 00:14:45,873
about the moment of inertia in gases like helium

245
00:14:45,993 --> 00:14:47,931
at temperatures approaching absolute zero.

246
00:14:48,051 --> 00:14:50,853
I didn't skip it. It's just
an anecdote, it's not science.

247
00:14:50,973 --> 00:14:53,967
Oh, I see. It was
the apple falling on Newton's head,

248
00:14:54,087 --> 00:14:56,655
- was that just an anecdote?
- You are not Isaac Newton.

249
00:14:56,775 --> 00:15:00,092
No, no, that's true. Gravity
would have been apparent to me without the apple.

250
00:15:00,884 --> 00:15:02,582
You cannot possibly be that arrogant.

251
00:15:02,702 --> 00:15:05,061
You continue to underestimate me,
my good man.

252
00:15:05,656 --> 00:15:09,530
Look, if you weren't happy with my presentation
then maybe you should have given it with me!

253
00:15:09,543 --> 00:15:11,576
As I have explained repeatedly,
unlike you,

254
00:15:11,591 --> 00:15:14,039
I don't need validation from lesser minds.

255
00:15:14,069 --> 00:15:14,887
No offense.

256
00:15:15,450 --> 00:15:16,659
Really, so why did you come?

257
00:15:16,674 --> 00:15:18,115
Because I knew you'd screw this up.

258
00:15:18,137 --> 00:15:19,470
I didn't screw it up!

259
00:15:19,483 --> 00:15:21,898
Oh, please. I admit that spherical
chicken joke,

260
00:15:21,920 --> 00:15:22,909
- that was hilarious.
- Thank you.

261
00:15:22,941 --> 00:15:24,847
But it was straight downhill from there.

262
00:15:25,052 --> 00:15:27,145
I've had enough of your condescendship.

263
00:15:27,245 --> 00:15:30,216
Maybe I didn't go to college
when I was 11 like you.

264
00:15:30,229 --> 00:15:33,714
Maybe I got my doctorate at
24 instead of 16

265
00:15:33,734 --> 00:15:35,441
but you are not the only person
who is smarter than

266
00:15:35,454 --> 00:15:36,848
everyone else in this room!

267
00:15:38,228 --> 00:15:38,838
No offense.

268
00:15:40,907 --> 00:15:42,667
And I am clearly not the only person

269
00:15:42,692 --> 00:15:44,458
who is tormented by insecurity

270
00:15:44,477 --> 00:15:46,585
and has an ego in need of constant validation!

271
00:15:46,726 --> 00:15:48,325
So you admit you're an egotist?

272
00:15:48,559 --> 00:15:49,336
Yes!

273
00:15:50,742 --> 00:15:53,730
My name is doctor Leonard Hofstadter
and I can never please my parents so

274
00:15:53,736 --> 00:15:56,109
I need all my self esteem
from strangers like you!

275
00:15:56,221 --> 00:15:57,210
But he's worse!

276
00:15:57,762 --> 00:15:59,119
Ok, that's it!

277
00:15:59,510 --> 00:16:00,063
Stop it!

278
00:16:02,764 --> 00:16:05,364
You cannot blow up my head
with your mind!

279
00:16:05,577 --> 00:16:07,297
Then I'll settle for your aneurism!

280
00:16:07,859 --> 00:16:09,483
- Stop it!
- You hit me!

281
00:16:09,588 --> 00:16:10,738
You saw that he hit me!

282
00:16:10,904 --> 00:16:12,366
You tried to blow up my head!

283
00:16:12,398 --> 00:16:13,303
So it was working!

284
00:16:13,335 --> 00:16:15,689
It wa... It was not!
You're a nutcase!

285
00:16:15,709 --> 00:16:16,665
We'll see about that!

286
00:16:16,810 --> 00:16:18,158
Heads up, you people in the front row!

287
00:16:18,162 --> 00:16:19,394
This is a splash zone!

288
00:16:20,550 --> 00:16:21,205
Stop it!

289
00:16:21,692 --> 00:16:22,180
Just quit it!

290
00:16:28,341 --> 00:16:30,581
Is this usually how these physics things go?

291
00:16:31,028 --> 00:16:32,267
More often than you think.

292
00:16:48,882 --> 00:16:50,667
You could have offered me a ride home.

293
00:16:52,599 --> 00:16:54,339
You're lucky I didn't run you over.

294
00:16:55,442 --> 00:16:57,328
I really don't understand what you're so unhappy about.

295
00:16:57,341 --> 00:16:58,991
You begged me to come, I came.

296
00:16:59,019 --> 00:17:00,194
There's just no pleasing you.

297
00:17:01,199 --> 00:17:03,753
You're right, I'm the problem,
I'm the one that needs help.

298
00:17:04,222 --> 00:17:06,430
Well, that's not much of
an apology but I'll take it.

299
00:17:07,615 --> 00:17:08,828
Excuse me,

300
00:17:09,367 --> 00:17:11,640
is there anything you would apologise for?

301
00:17:13,238 --> 00:17:13,854
Yes.

302
00:17:14,994 --> 00:17:17,151
I'm sorry I tried to blow up your head.

303
00:17:19,121 --> 00:17:20,225
It was uncalled for.

304
00:17:21,669 --> 00:17:22,645
You won't believe this.

305
00:17:22,671 --> 00:17:25,126
Somebody got the whole thing with their
cell phone and put it on YouTube!

306
00:17:26,731 --> 00:17:27,313
What?

307
00:17:29,006 --> 00:17:31,254
- Who would do that?
- That would be me.

308
00:17:33,050 --> 00:17:34,752
Hey, check it out,
it's a featured video!

309
00:17:34,854 --> 00:17:36,780
<i>He hit me!
You saw that he hit me!</i>

310
00:17:36,812 --> 00:17:37,833
[i]You tried to blow up my head![/i]

311
00:17:37,872 --> 00:17:40,843
<i>- Then it was working!
- It was not working! You're a nutcase!</i>

312
00:17:40,869 --> 00:17:41,884
<i>We will see about that!</i>

313
00:17:41,935 --> 00:17:43,216
<i>You people in the front row
heads up!</i>

314
00:17:43,229 --> 00:17:44,500
<i>This is splash zone</i>

315
00:17:46,091 --> 00:17:48,685
<i>Stop it! Leave it,
leave it!</i>

316
00:17:52,186 --> 00:17:53,721
<i>You want a volcano nerve pinch!</i>

317
00:17:55,367 --> 00:17:57,809
<i>You should clip your fingernails!
Those hurt!</i>

318
00:17:59,367 --> 00:18:02,498
Oh, Jeez. does this suit really look that bad?

319
00:18:03,625 --> 00:18:05,484
Forget your suit,
look at my arms waving.

320
00:18:05,490 --> 00:18:07,115
I'm like a flamingo on Ritalin.

321
00:18:10,240 --> 00:18:10,927
Howard,

322
00:18:11,155 --> 00:18:13,584
would you like to explain to me
why your Facebook page

323
00:18:13,600 --> 00:18:15,873
has a picture of me sleeping
on your shoulder

324
00:18:15,889 --> 00:18:17,334
captioned "me and my girlfriend"?

325
00:18:19,680 --> 00:18:21,702
Uh oh, here comes
the talk.

326
00:18:32,099 --> 00:18:33,859
- You hit me!
- You tried to blow up my head!

327
00:18:33,900 --> 00:18:36,898
- So it was working!
- It was not working! You're a nutcase!

328
00:18:36,937 --> 00:18:36,898
We will see about that!

