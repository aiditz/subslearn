1
00:00:02,057 --> 00:00:04,427
Этот сендвич - непереваримая катастрофа.

2
00:00:05,704 --> 00:00:07,576
Я просил индейку, ростбиф с латуком, швейцарским сыром и зернами сверху.

3
00:00:07,577 --> 00:00:09,119
Я просил индейку, ростбиф с латуком, швейцарским сыром и зернами сверху.

4
00:00:09,120 --> 00:00:10,176
А они что дали?

5
00:00:10,177 --> 00:00:11,179
Индейку, ростбиф с швейцарским сыром, латуком и зернами сверху.

6
00:00:11,180 --> 00:00:13,367
Индейку, ростбиф с швейцарским сыром, латуком и зернами сверху.

7
00:00:19,470 --> 00:00:23,301
Правильные ингридиенты,
но в неправильном порядке!

8
00:00:23,302 --> 00:00:24,303
В нормальном сендвиче

9
00:00:24,423 --> 00:00:26,211
сыр прилегает к булке,

10
00:00:26,232 --> 00:00:28,379
создавая влажный барьер латуку.

11
00:00:30,162 --> 00:00:33,621
А это пусть выкинут в машиномойку.

12
00:00:33,741 --> 00:00:35,025
Невероятно.

13
00:00:35,026 --> 00:00:36,619
Вот-вот, это же основы кулинарии!

14
00:00:37,656 --> 00:00:41,046
Какой-то парень продает с молотка
миниатюру Машины Времени

15
00:00:41,047 --> 00:00:42,753
из фильма, а никто не торгуется.

16
00:00:42,900 --> 00:00:45,784
Машина Времени из фильма
"Машина Времени"??

17
00:00:45,904 --> 00:00:48,726
Нет, Машина Времени из "Улыбки Мона Лизы".

18
00:00:49,343 --> 00:00:51,677
О, Мона Лиза должна была использовать Машину Времени в том фильме!

19
00:00:52,246 --> 00:00:53,288
Смотрели? Жесть!

20
00:00:55,550 --> 00:00:56,865
Ох, круто...

21
00:00:56,985 --> 00:00:58,173
Всего 800 долларов?

22
00:00:58,177 --> 00:00:59,804
Да...
Это я поставил.

23
00:01:00,020 --> 00:01:01,300
Ты поставил 800 долларов?

24
00:01:01,313 --> 00:01:03,063
Ну спонтанно захотелось...

25
00:01:03,064 --> 00:01:04,563
Я думал будут предлагать тысячи,

26
00:01:04,563 --> 00:01:06,317
и просто хотел поучаствовать.

27
00:01:06,843 --> 00:01:08,614
До конца аукциона 30 секунд!

28
00:01:08,615 --> 00:01:09,829
У тебя есть 800 долларов?

29
00:01:09,830 --> 00:01:11,833
Не хочу профукивать их на Машину Времени.

30
00:01:12,010 --> 00:01:13,174
Не бойся, знаешь же -
люди ждут последний момент, а потом делают ставку!

31
00:01:13,175 --> 00:01:15,357
Не бойся, знаешь же -
люди ждут последней секунды, а потом делают ставку!

32
00:01:15,358 --> 00:01:16,925
Это называется "снайпинг"..
Снайпинг - способ выигрывать онлайн-аукционы,
делая ставку в последний момент с помощью специального программного обеспечения.

33
00:01:16,926 --> 00:01:17,750
15 секунд.

34
00:01:17,885 --> 00:01:19,189
Ну же, снайперы.

35
00:01:21,209 --> 00:01:23,453
10, 9, 8...

36
00:01:23,460 --> 00:01:25,592
Ну где твои снайперы?

37
00:01:25,712 --> 00:01:26,870
- 5
- Снайп!

38
00:01:26,871 --> 00:01:27,967
- 4
- Снайп!

39
00:01:27,968 --> 00:01:28,902
- 3
- Снайп!

40
00:01:28,903 --> 00:01:30,122
- 2
- Снайп!

41
00:01:30,123 --> 00:01:31,742
1...

42
00:01:32,305 --> 00:01:33,341
Поздравляю!

43
00:01:33,461 --> 00:01:36,236
Вы стали счастливым обладателем
миниатюры Машины Времени!

44
00:01:37,546 --> 00:01:39,403
Лифчик-счастливчик.

45
00:01:39,403 --> 00:01:40,871
Не понимаю, почему никто не делал ставки?

46
00:01:40,886 --> 00:01:43,681
Это же коллекционный предмет
классики сай-фай кинематографа!

47
00:01:43,682 --> 00:01:45,834
Знаю! Но все равно не могу её себе позволить.

48
00:01:45,954 --> 00:01:46,921
А может поделимся?

49
00:01:47,013 --> 00:01:48,470
С каждого по 200 баксов

50
00:01:48,471 --> 00:01:49,656
и она будет нашей по очереди.

51
00:01:49,932 --> 00:01:53,590
Машина Времени, делённая по времени.

52
00:01:53,590 --> 00:01:55,797
- Я за! Шелдон?
- Еще спрашиваешь!

53
00:01:56,517 --> 00:01:59,294
Но я не понимаю, почему никто не торговался?

54
00:02:08,305 --> 00:02:09,978
Я понимаю, почему никто не торговался.

55
00:02:30,141 --> 00:02:31,141
Теория Большого Взрыва
Сезон 1 Эпизод 14 - The Nerdmabelia Scattering

56
00:02:34,698 --> 00:02:38,875
<font size="12" color=#38B0DE>Перевод by Yokky, Irka, NoboDy.
<font size="12" color=#38B0DE>Тайминг SerJo

57
00:02:39,186 --> 00:02:41,554
А в каталоге точно написано -
"миниатюра"?

58
00:02:43,910 --> 00:02:45,107
Ну я так предположил...

59
00:02:46,407 --> 00:02:49,545
Кто продает полноразмерную Машину Времени
за 800 долларов?

60
00:02:50,906 --> 00:02:53,918
Тот, кто находится в пересечении двух кругов диаграммы Эйлера-Венна:

61
00:02:53,919 --> 00:02:56,160
"Мне не нужна Машина Времени"

62
00:02:56,161 --> 00:02:56,876
и

63
00:02:56,877 --> 00:02:59,058
"Хочу 800 долларов".

64
00:02:59,918 --> 00:03:01,903
А вобще-то, классная сделка.

65
00:03:01,939 --> 00:03:03,442
Даже учитывая транспортировку она будет стоить
меньше 4х долларов за фунт.

66
00:03:03,443 --> 00:03:04,934
Даже учитывая транспортировку она будет стоить
меньше 4х долларов за фунт.

67
00:03:06,533 --> 00:03:08,006
Коктейли с креветками в 12.50.

68
00:03:10,809 --> 00:03:12,205
А как мы занесем её наверх?

69
00:03:14,126 --> 00:03:15,479
Если снять тарелку,

70
00:03:15,480 --> 00:03:16,535
поместится в лифт.

71
00:03:16,924 --> 00:03:19,828
Да, вот только лифт уже два года не работает.

72
00:03:19,948 --> 00:03:20,850
Я собирался вам сказать...

73
00:03:20,886 --> 00:03:22,686
Может позвоним лифтерам?

74
00:03:25,153 --> 00:03:26,074
Нет необходимости.

75
00:03:26,140 --> 00:03:28,170
Я же магистр в инженерном деле.

76
00:03:28,272 --> 00:03:31,318
Я дистанционно чиню спутники изо дня в день.

77
00:03:31,319 --> 00:03:34,403
Я диагностирую неисправности боевых отсеков космических шаттлов.

78
00:03:34,403 --> 00:03:36,500
Когда Марсоход накренился влево,

79
00:03:36,501 --> 00:03:42,628
я сделал ему регулировку углов установки передних колес, хотя тот находился за 62 миллиона миль от нас.

80
00:03:52,079 --> 00:03:53,186
Не, проказник поломался.

81
00:03:56,598 --> 00:03:58,009
Давайте парни, тужьтесь!

82
00:03:58,010 --> 00:03:59,796
Если я потужусь еще больше,

83
00:03:59,797 --> 00:04:01,353
я рожу свою толстую кишку.

84
00:04:02,573 --> 00:04:04,401
Пальцы уже онемели!
Быстрее там!

85
00:04:04,402 --> 00:04:05,643
От скорости это не зависит.
Нагрузка на пальцы в итоге одинаковая.

86
00:04:05,644 --> 00:04:06,742
От скорости это не зависит.
Нагрузка на пальцы в итоге одинаковая.

87
00:04:06,742 --> 00:04:07,907
Основы физики.

88
00:04:07,915 --> 00:04:08,991
- Шелдон?
- Ааа?

89
00:04:09,105 --> 00:04:10,791
Когда мои пальцы очухаются,

90
00:04:11,078 --> 00:04:12,495
я впущу в ход средний.

91
00:04:14,458 --> 00:04:15,181
О, хай, ребят.

92
00:04:15,418 --> 00:04:16,150
Привет, Пенни.

93
00:04:19,681 --> 00:04:20,464
Передохните там.

94
00:04:22,238 --> 00:04:23,059
Что у вас тут?

95
00:04:23,308 --> 00:04:25,364
Да так... просто... поднимает кое-что наверх...

96
00:04:25,852 --> 00:04:26,722
Что?

97
00:04:27,006 --> 00:04:29,795
Ну.. так... Машину Времени.

98
00:04:30,957 --> 00:04:32,658
Молодцы, но я тут на работу спешу...

99
00:04:33,115 --> 00:04:34,918
- Подожди, пару минут.
- Не могу пару минут.

100
00:04:35,004 --> 00:04:35,913
Я уже очень опаздываю.

101
00:04:36,184 --> 00:04:37,728
Вот пробное решение:

102
00:04:37,824 --> 00:04:39,921
Поднимись на крышу,
перепрыгни на соседнюю,

103
00:04:40,063 --> 00:04:41,016
там совсем небольшой зазор

104
00:04:41,036 --> 00:04:42,369
- не смотри в низ если боишься высоты -

105
00:04:42,548 --> 00:04:43,735
и спустись по лестнице в соседнем доме.

106
00:04:45,000 --> 00:04:46,224
Ты шутишь, да?

107
00:04:46,323 --> 00:04:49,093
Я никогда не шучу, когда говорю о боязни высоты.

108
00:04:50,681 --> 00:04:52,988
Черт... ладно, придется на крышу...

109
00:04:53,661 --> 00:04:55,278
Эй, если подождешь нас,

110
00:04:55,279 --> 00:04:58,957
могу перенести тебя на работу днём раньше.

111
00:04:59,977 --> 00:05:02,218
Ну... во времени... шутка...
Ничего.

112
00:05:03,907 --> 00:05:06,891
Если тебе будет приятнее,
мне эта шутка показалась забавной.

113
00:05:10,573 --> 00:05:11,665
Ладно давайте.

114
00:05:12,326 --> 00:05:13,289
Готовы там?

115
00:05:13,760 --> 00:05:14,663
Секунду.

116
00:05:14,880 --> 00:05:16,734
Говард вышел потошнить немного.

117
00:05:22,502 --> 00:05:24,054
Вот видишь, а ты боялся.

118
00:05:24,138 --> 00:05:25,646
В комнате смотрится очень неплохо.

119
00:05:27,649 --> 00:05:28,451
И точно.

120
00:05:28,452 --> 00:05:30,235
Круче у меня еще ничего не было!

121
00:05:30,236 --> 00:05:31,357
Круче у меня еще ничего не было!

122
00:05:31,358 --> 00:05:34,292
Та самая машина, что перенесла Рода Тейлора

123
00:05:34,332 --> 00:05:35,371
из викторианской Англии

124
00:05:35,636 --> 00:05:37,268
в пост-апокалиптическое будущее,

125
00:05:37,535 --> 00:05:39,659
где общество разщеплено на две фракции,

126
00:05:39,660 --> 00:05:42,506
где подземные Морлоки трапезничают плотью нежных надземных Элоев.

127
00:05:42,507 --> 00:05:44,714
где подземные Морлоки трапезничают плотью нежных надземных Элоев.

128
00:05:47,480 --> 00:05:50,364
Все цыпы будут о тебе говорить.

129
00:05:51,856 --> 00:05:53,015
Ага.

130
00:05:53,099 --> 00:05:54,887
Мой сосед вечно такой -

131
00:05:54,963 --> 00:05:56,614
"А у меня джакузи на балконе"

132
00:05:56,630 --> 00:05:58,150
"А у меня джакузи на балконе"

133
00:05:58,593 --> 00:05:59,678
А я теперьт буду такой -

134
00:06:00,126 --> 00:06:01,894
"А у меня Машина Времени на балконе".

135
00:06:04,015 --> 00:06:05,859
Забулькайся там в своем джакузи,
Джакузи-Боб.

136
00:06:07,436 --> 00:06:09,492
Господа, мы договорились что она будет нашей по очереди,

137
00:06:09,536 --> 00:06:10,444
но думаю вы согласитесь

138
00:06:10,491 --> 00:06:13,003
что более целесообразно держать её здесь.

139
00:06:13,464 --> 00:06:14,614
Нельзя здесь!

140
00:06:14,625 --> 00:06:15,894
Что, я встречу девушку и скажу -

141
00:06:15,966 --> 00:06:17,410
"Хочешь посмотреть мою Машину Времени?

142
00:06:17,521 --> 00:06:18,640
Она в квартире моего друга"

143
00:06:18,685 --> 00:06:19,856
Что за позорищще!

144
00:06:20,697 --> 00:06:22,444
- He's got a point.
- All right.

145
00:06:22,469 --> 00:06:24,536
Я думаю, нам надо установить несколько
фундаментальных правил, помимо,

146
00:06:24,592 --> 00:06:26,640
само собой разумеющихся, как:
"В Машину Времени заходить без обуви"

147
00:06:26,752 --> 00:06:27,998
и "Никакой еды в Машине Времени".

148
00:06:28,080 --> 00:06:30,338
Я предлагаю следующее:
"Во время пользования Машиной Времени

149
00:06:30,378 --> 00:06:31,886
штаны должны быть надеты все время."

150
00:06:33,089 --> 00:06:34,149
Поддерживаю.

151
00:06:35,377 --> 00:06:36,669
Я собирался надевать полотенце.

152
00:06:40,913 --> 00:06:42,243
А я все еще хочу поставить ее на балконе.

153
00:06:42,519 --> 00:06:44,635
И я предлагаю переносить ее бимесячно.

154
00:06:44,947 --> 00:06:46,174
- Да, это звучит разумно.
- Погодите.

155
00:06:46,238 --> 00:06:48,069
"бимесячно" это двусмысленный термин.
В оригинале "bimonthly" - означает и "дважды в месяц" и "раз в два месяца".

156
00:06:48,124 --> 00:06:50,036
Ты имеешь в виду, переносить ее
каждый месяц или дважды в месяц?

157
00:06:50,229 --> 00:06:51,300
- Дважды в месяц.
- Тогда нет.

158
00:06:51,903 --> 00:06:53,032
Ладно, каждый месяц.

159
00:06:53,037 --> 00:06:53,743
Нет.

160
00:06:55,419 --> 00:06:57,023
Шелдон, хватит быть таким эгоистичным.

161
00:06:57,118 --> 00:06:58,886
Мы все заплатили за нее, и она принадлежит всем нам.

162
00:06:59,502 --> 00:07:01,511
А теперь, поди прочь, я хочу
посидеть в моей Машине Времени.

163
00:07:11,045 --> 00:07:15,755
Итак. Я перевожу часы на 10 марта 1876 года.

164
00:07:16,364 --> 00:07:17,915
Отличный выбор, Александер Грэхам Белл

165
00:07:17,938 --> 00:07:19,988
как раз тогда изобрел телефон
и позвонил Доктору Ватсону.

166
00:07:20,254 --> 00:07:22,099
Погодите, я тоже хочу это увидеть.

167
00:07:22,235 --> 00:07:23,651
Ну так, дождись своей очереди.

168
00:07:23,759 --> 00:07:25,413
Но, если вы все вернетесь назад в одинаковое время

169
00:07:25,596 --> 00:07:27,114
Лаборатория Белла будет окружена большой толпой.

170
00:07:27,395 --> 00:07:28,530
Он подумает, что что-то случилось.

171
00:07:29,850 --> 00:07:31,594
Также, раньше Машины Времени не могли перемещаться в пространстве.

172
00:07:31,882 --> 00:07:34,098
Поэтому, тебе придет конец в Пасадене в 1876 году

173
00:07:34,177 --> 00:07:36,032
И даже если ты сможешь добраться до Бостона,
что ты будешь делать?

174
00:07:36,089 --> 00:07:37,458
Постучишь в двери и скажешь Миссис Белл:

175
00:07:37,458 --> 00:07:38,872
"Здравствуйте, я большой фанат вашего мужа,

176
00:07:39,033 --> 00:07:40,399
Можно мне зайти и посмотреть, как он изобретает телефон?"

177
00:07:41,797 --> 00:07:42,602
Миссис Белл была глухая.

178
00:07:42,643 --> 00:07:43,886
Она бы даже не услышала твоего стука.

179
00:07:46,081 --> 00:07:47,077
У меня есть решение.

180
00:07:47,190 --> 00:07:49,989
Во-первых, отправляйся в будущее и достань устройство невидимости.

181
00:07:50,505 --> 00:07:51,397
Как далеко в будущее?

182
00:07:51,506 --> 00:07:52,509
Если я правильно помню, то

183
00:07:52,552 --> 00:07:54,251
Капитан Кирк должен был украсть устройство невидимости

184
00:07:54,355 --> 00:07:57,021
у Ромуланцев в 5027.3 по Звездному Времени.

185
00:07:57,431 --> 00:08:00,638
Что должно быть 10 января 2328, если перевести на наше время.

186
00:08:02,545 --> 00:08:06,924
Хорошо, перевожу часы на 10 января 2328 года.

187
00:08:07,564 --> 00:08:10,124
Приветствую тебя, будущее!

188
00:08:31,666 --> 00:08:32,514
Это было весело.

189
00:08:34,269 --> 00:08:35,081
Моя очередь!

190
00:08:36,493 --> 00:08:38,103
Ладно, во-первых,

191
00:08:38,193 --> 00:08:40,695
тот "зазор" был почти в метр шириной.

192
00:08:40,724 --> 00:08:42,559
- Я ушибла и поранила свою коленку.
- Ты в порядке?

193
00:08:44,025 --> 00:08:45,589
Во-вторых, дверь на лестнице

194
00:08:45,654 --> 00:08:46,717
в другое здание была заперта.

195
00:08:46,731 --> 00:08:48,254
поэтому мне пришлось спускаться по пожарной лестнице,

196
00:08:48,268 --> 00:08:49,408
которая заканчивалась на третьем этаже.

197
00:08:49,442 --> 00:08:51,038
Мне оставалось только пробираться через окно

198
00:08:51,060 --> 00:08:52,549
милой армянской семьи,

199
00:08:52,688 --> 00:08:54,108
которые заставили меня остаться на обед.

200
00:08:55,715 --> 00:08:56,964
Звучит неплохо.

201
00:08:57,252 --> 00:08:58,483
Там было восемь блюд из баранины.

202
00:08:58,592 --> 00:09:00,507
И они пытались свести меня с их сыном.

203
00:09:02,998 --> 00:09:03,935
- Извини.
- Еще не все.

204
00:09:04,385 --> 00:09:05,319
Когда я наконец добралась до работы,

205
00:09:05,321 --> 00:09:06,980
они уже заменили меня.

206
00:09:07,022 --> 00:09:08,005
Да, вот так.

207
00:09:08,138 --> 00:09:10,500
Я потеряла весь дневной заработок, благодаря этой...

208
00:09:10,621 --> 00:09:12,245
- Этой...
- Машине Времени.

209
00:09:15,915 --> 00:09:17,629
Свет мигает, блюдце вращает,
не хочешь попробовать?

210
00:09:17,714 --> 00:09:18,757
Нет!!

211
00:09:19,862 --> 00:09:21,098
Я не хочу это пробовать!

212
00:09:21,180 --> 00:09:22,978
Боже мой, вы взрослые люди

213
00:09:23,586 --> 00:09:25,345
Как вы можете тратить всю свою жизнь

214
00:09:25,398 --> 00:09:27,229
На эти глупые игрушки и костюмы,

215
00:09:27,281 --> 00:09:29,280
И комиксы, и...
И как эту...

216
00:09:29,698 --> 00:09:31,888
- Эту...
- Напоминаю, Машина Времени.

217
00:09:33,967 --> 00:09:36,007
Да никакая это не Машина Времени!

218
00:09:36,035 --> 00:09:36,894
В ней можно только...

219
00:09:36,966 --> 00:09:38,996
быть Элтоном Джоном и рассекать по Эверглейдсу!
Эверглейдс --  Труднопроходимое болото, покрытое травой и водой. Находится в Южной Флориде.

220
00:09:45,489 --> 00:09:47,268
Нет, она перемещается только во времени.

221
00:09:47,767 --> 00:09:50,052
В болоте она более чем бесполезна.

222
00:09:55,689 --> 00:09:57,481
Жалкие! Вы все!

223
00:09:57,605 --> 00:09:59,115
Ничтожно жалкие.

224
00:10:04,668 --> 00:10:05,456
Моя очередь.

225
00:10:23,162 --> 00:10:24,862
Леонард, 2 часа ночи.

226
00:10:26,287 --> 00:10:27,198
И что?

227
00:10:27,353 --> 00:10:28,429
Моя очередь.

228
00:10:31,992 --> 00:10:33,774
Почему ты переключил ее на позавчера?

229
00:10:34,794 --> 00:10:36,504
Потому что я хочу вернуться назад и

230
00:10:36,593 --> 00:10:37,937
Не покупать Машину Времени.

231
00:10:38,581 --> 00:10:41,913
Ты не можешь. Если ты это сделаешь,

232
00:10:42,083 --> 00:10:43,480
То ты не сможешь пользоваться ей в настоящем.

233
00:10:43,534 --> 00:10:44,871
Чтобы путешествовать в прошлое и остановить  себя

234
00:10:44,911 --> 00:10:46,495
От ее покупки. Следовательно, ты все еще ее обладатель.

235
00:10:46,831 --> 00:10:48,534
Это классическая ошибка путешествий во времени.

236
00:10:52,340 --> 00:10:54,908
Могу ли я вернуться в прошлое и предотвратить эти объяснения?

237
00:10:56,320 --> 00:10:57,053
Тот же парадокс.

238
00:10:57,425 --> 00:10:58,924
Если ты вернешься назад во времени,

239
00:10:59,089 --> 00:11:00,243
И, скажем, вырубишь меня,

240
00:11:00,563 --> 00:11:01,814
Тогда и не будет разговора,

241
00:11:01,852 --> 00:11:03,236
Раздражающего тебя и мотивирующего

242
00:11:03,315 --> 00:11:04,690
на то, чтобы вырубить меня.

243
00:11:06,746 --> 00:11:08,361
А что если я вырублю тебя прямо сейчас?

244
00:11:10,649 --> 00:11:11,570
Прошлое этим не изменишь.

245
00:11:14,115 --> 00:11:15,601
Зато это сделает настоящее намного лучше.

246
00:11:17,744 --> 00:11:19,216
Ты чем-то расстроен?

247
00:11:20,167 --> 00:11:21,606
Как же ты догадался?

248
00:11:22,858 --> 00:11:24,382
По нескольким вещам.

249
00:11:24,690 --> 00:11:25,862
Во-первых, поздний час.

250
00:11:26,002 --> 00:11:28,245
Потом, твое поведение кажется очень вялым.

251
00:11:28,332 --> 00:11:29,537
- Плюс, твоя раздражительность...
- Да, я расстроен!

252
00:11:31,189 --> 00:11:32,680
Обычно я не обращаю внимание на такие вещи.

253
00:11:34,120 --> 00:11:34,892
Я молодец...

254
00:11:38,387 --> 00:11:39,298
Да... молодец...

255
00:11:40,647 --> 00:11:41,282
Секунду.

256
00:11:43,600 --> 00:11:45,593
А ты не хотел бы поговорить о том, что тебя беспокоит?

257
00:11:47,249 --> 00:11:48,280
Возможно, я не знаю.

258
00:11:48,896 --> 00:11:50,152
Класс, я жгу сегодня.

259
00:11:55,311 --> 00:11:56,310
Слушай.

260
00:11:57,194 --> 00:12:01,070
Такие девушки как Пэнни никогда не захотят встречаться с обладателями Машины Времени, как я.

261
00:12:01,713 --> 00:12:02,502
Я не согласен.

262
00:12:03,109 --> 00:12:05,764
Твои неудачи сблизиться с Пэнни

263
00:12:05,805 --> 00:12:07,676
Задолго предшествовали приобретению Машины Времени.

264
00:12:10,463 --> 00:12:12,492
Так что, во всем виноват только ты.

265
00:12:14,515 --> 00:12:15,666
Спасибо за объяснение.

266
00:12:15,975 --> 00:12:17,722
К тому же, твое начальное предположение абсолютно неверно.

267
00:12:17,766 --> 00:12:20,681
В оригинальном фильме Род Тэйлор
Встретил Иветт Мимье.

268
00:12:20,713 --> 00:12:21,929
С помощью этой Машины Времени.

269
00:12:22,329 --> 00:12:24,920
А в фильме "Назад в будущее" Марти МакФлай

270
00:12:24,977 --> 00:12:26,984
Познакомился со своей чрезвычайно привлекательной молодой мамой.

271
00:12:30,496 --> 00:12:31,335
Это только фильмы.

272
00:12:31,656 --> 00:12:32,759
Конечно, это только фильмы.

273
00:12:33,379 --> 00:12:35,142
Ты ожидал от меня примера

274
00:12:35,159 --> 00:12:36,943
С Машиной Времени из реальной жизни?

275
00:12:39,750 --> 00:12:40,605
Это же смехотворно.

276
00:12:43,189 --> 00:12:44,251
Давайте, парни, тужьтесь!

277
00:12:44,812 --> 00:12:46,148
Если я потужусь еще больше,

278
00:12:46,236 --> 00:12:47,829
Я рожу свою толстую кишку.

279
00:12:50,290 --> 00:12:51,107
О, хай, ребят.

280
00:12:51,250 --> 00:12:52,090
Привет, Пенни.

281
00:12:55,245 --> 00:12:56,109
Передохните там!

282
00:12:57,879 --> 00:12:58,796
Что у вас тут?

283
00:12:58,948 --> 00:13:01,728
Да так... поднимаем... Машину Времени.

284
00:13:03,608 --> 00:13:05,750
Молодцы,
но я тут спешу на работу...

285
00:13:06,027 --> 00:13:06,824
Нет проблем.

286
00:13:13,780 --> 00:13:14,493
Держись.

287
00:13:16,296 --> 00:13:17,797
Да, но как же Машина Времени?

288
00:13:18,403 --> 00:13:19,981
Есть вещи поважней игрушек.

289
00:13:29,630 --> 00:13:30,594
Мне страшно.

290
00:13:31,313 --> 00:13:32,784
Не бойся, детка.
Я с тобой.

291
00:13:55,075 --> 00:13:56,050
Вообще-то сейчас моя очередь!

292
00:14:02,425 --> 00:14:03,218
Чей-то ты делаешь?

293
00:14:05,206 --> 00:14:06,306
Собираю все свои вещички,

294
00:14:06,316 --> 00:14:08,620
чтобы отнести их в магазин
комиксов и продать.

295
00:14:09,676 --> 00:14:10,791
Это правда необходимо?

296
00:14:11,580 --> 00:14:13,304
Если тебе нужны деньги,
ты всегда можешь сдать кровь.

297
00:14:15,787 --> 00:14:16,589
И сперму.

298
00:14:18,822 --> 00:14:19,574
Дело не в деньгах

299
00:14:20,706 --> 00:14:21,445
Мы с едой!

300
00:14:21,817 --> 00:14:24,215
Рыбка и рогалики, завтрак
для путешественников во времени.

301
00:14:25,667 --> 00:14:25,968
Здорово

302
00:14:26,039 --> 00:14:27,908
Кто-то хочет выкупить мою долю
Машины Времени?

303
00:14:27,972 --> 00:14:28,430
Зачем?

304
00:14:28,619 --> 00:14:29,836
Я больше не хочу её.

305
00:14:30,264 --> 00:14:30,874
Почему?

306
00:14:31,014 --> 00:14:32,418
Ну... это личное.

307
00:14:32,849 --> 00:14:35,853
Паучье чутьё подсказывает мне,
что это как-то связано с Пенни.

308
00:14:36,445 --> 00:14:37,197
Так,

309
00:14:37,389 --> 00:14:39,159
- Вы будете выкупать или нет?
- Я даю тебе

310
00:14:39,244 --> 00:14:41,260
100 долларов, что делает меня
владельцем половины твоей части,

311
00:14:41,339 --> 00:14:42,595
и тогда мы ставим машину у
меня на балконе.

312
00:14:42,831 --> 00:14:43,628
Забудь про балкон,

313
00:14:43,715 --> 00:14:45,370
Я даю тебе 120, и мы ставим её в мой гараж.

314
00:14:45,751 --> 00:14:47,562
Моя доля стоит 200 долларов.

315
00:14:47,687 --> 00:14:48,707
Друг, всем известно,

316
00:14:48,784 --> 00:14:50,128
что Машина Времени теряет полцены

317
00:14:50,207 --> 00:14:51,431
после того как она уже не на аукционе.

318
00:14:53,105 --> 00:14:55,636
Повышаю до 200, и машина остаётся здесь.

319
00:14:55,773 --> 00:14:59,664
300 и плюс ещё мой Mattel Millenium Falcon 1979
из Звёздных Войн

320
00:14:59,664 --> 00:15:00,799
с настоящими светозвукоэффектами!

321
00:15:01,805 --> 00:15:04,904
Нет, больше ни игрушек, ни фигурок..

322
00:15:04,985 --> 00:15:08,355
ни реквизита, ни моделек,
ни костюмов, ни роботов,

323
00:15:08,355 --> 00:15:10,095
ни даже трансформаторов голоса под Дарта Вейдера.

324
00:15:10,095 --> 00:15:11,231
Я бросаю всё это.

325
00:15:11,886 --> 00:15:12,856
Ты не можешь!

326
00:15:12,946 --> 00:15:14,191
Посмотри, что ты сотворил!

327
00:15:14,395 --> 00:15:16,261
Это ж целый Ботанолэнд.

328
00:15:19,444 --> 00:15:20,129
И что гораздо важней,

329
00:15:20,852 --> 00:15:22,488
у тебя есть трансформатор голоса
под Дарта Вейдера?

330
00:15:24,190 --> 00:15:24,919
Считай, уже нет.

331
00:15:25,782 --> 00:15:27,406
Чур, Флэш из Золотого Века - мой!

332
00:15:27,499 --> 00:15:28,281
Постой-ка, мне он тоже нужен,

333
00:15:28,348 --> 00:15:30,317
тогда я дособираю мою коллекцию
Лиги Справедливости.

334
00:15:30,413 --> 00:15:31,119
Значит, не судьба, я уже зачуркался.

335
00:15:31,119 --> 00:15:32,671
- Ты не можешь просто так зачуркаться.
- Очень даже могу,

336
00:15:32,697 --> 00:15:33,585
почитай про "чур" на Википедии.

337
00:15:35,665 --> 00:15:37,447
Нельзя чуркаться на аукционной войне.

338
00:15:37,624 --> 00:15:38,823
Никакая это не война.

339
00:15:39,416 --> 00:15:41,471
Я продам всё это Ларри
в магазин комиксов.

340
00:15:41,735 --> 00:15:42,968
Почему Ларри? Он разве чуркался?

341
00:15:43,066 --> 00:15:43,903
Да забудь ты про чурканья!

342
00:15:44,827 --> 00:15:46,910
Он предложил мне хорошую цену
за всю коллекцию.

343
00:15:47,345 --> 00:15:48,262
Сколько? Я предложу столько же.

344
00:15:48,681 --> 00:15:50,566
Даю столько же и 1000 рупий сверху.

345
00:15:51,733 --> 00:15:53,445
- Какой обменный курс?
- Не твоё дело.

346
00:15:53,553 --> 00:15:54,445
Продавай продавай!

347
00:15:55,427 --> 00:15:56,552
Мам, мои сбережения с Бар-мицвы,

348
00:15:56,600 --> 00:15:57,604
сколько там у меня?

349
00:15:58,269 --> 00:15:58,678
Спасибо.

350
00:15:58,996 --> 00:16:01,258
Ставлю 2,600 долларов и
два дерева в Израиле.

351
00:16:03,949 --> 00:16:04,833
Забудьте.

352
00:16:04,910 --> 00:16:06,074
Стоит мне продать это одному из вас,

353
00:16:06,105 --> 00:16:07,633
двое других будут обижаться.

354
00:16:07,960 --> 00:16:08,564
Да ничего страшного,

355
00:16:08,629 --> 00:16:09,720
Если ты выберешь меня!

356
00:16:11,345 --> 00:16:12,552
Так, Леонард, поставь эту коробку.

357
00:16:12,600 --> 00:16:13,849
- Давай поговорим.
- Прости, Раж.

358
00:16:13,890 --> 00:16:15,078
Я уже сделал свой выбор.

359
00:16:15,379 --> 00:16:16,447
Нет

360
00:16:16,773 --> 00:16:17,814
Я не позволю тебе сделать это

361
00:16:19,286 --> 00:16:20,716
Шелдон, прочь с дороги!

362
00:16:24,533 --> 00:16:26,068
Никто не пройдёт.

363
00:16:30,355 --> 00:16:30,826
Хорошо.

364
00:16:32,058 --> 00:16:33,514
Видит Бог, я не хотел..

365
00:16:33,777 --> 00:16:36,888
вот, есть тут у меня раритетный,
совсем новенький, с производственным ляпом,

366
00:16:36,977 --> 00:16:39,184
Джорди Ла Форж из
Star Trek: The Next Generation

367
00:16:39,264 --> 00:16:41,680
без своего вайзера в оригинальной упаковке.

368
00:16:42,636 --> 00:16:43,856
Если не уйдёшь с дороги,

369
00:16:45,114 --> 00:16:45,958
Я открою его

370
00:16:47,941 --> 00:16:48,806
Так, друг,
спокойно..

371
00:16:50,513 --> 00:16:51,373
Мы твои друзья..

372
00:16:53,461 --> 00:16:54,621
Что за чёрт тут творится!?

373
00:16:55,172 --> 00:16:56,836
Ты!! Злобыня!!!

374
00:16:57,149 --> 00:16:58,123
Чего?

375
00:16:58,161 --> 00:17:00,380
Посмотрите, мисс "Взрослым-
нельзя-играть-в-игрушки".

376
00:17:00,649 --> 00:17:02,275
Да если б я зашёл
сейчас в твою квартиру,

377
00:17:02,348 --> 00:17:04,002
разве я не нашёл бы куколок пупсов?

378
00:17:04,227 --> 00:17:05,370
Не ты ли переносчик

379
00:17:05,484 --> 00:17:07,146
Плюшевых Мишек и Моих Маленьких Пони?

380
00:17:07,436 --> 00:17:10,490
А что это за японскую кошатень
я вижу на твоих шортах?

381
00:17:10,755 --> 00:17:11,867
Хелло, "Hello Kitty"!

382
00:17:15,926 --> 00:17:16,763
Так.

383
00:17:16,830 --> 00:17:19,224
Так, насчёт вчерашнего, Леонард..

384
00:17:19,280 --> 00:17:21,256
Я правда сожалею о том, что сказала

385
00:17:21,316 --> 00:17:22,983
- Я просто была на взводе.
- Нет-нет, я должен был знать..

386
00:17:23,072 --> 00:17:25,111
Нет, не должен.
Послушай, ты отличный парень

387
00:17:25,284 --> 00:17:28,066
Это вещи, которые ты любишь, именно
они делают тебя таким, какой ты есть

388
00:17:28,794 --> 00:17:30,737
Видать, я тогда - "Большие титьки"

389
00:17:35,697 --> 00:17:38,468
Все равно я думаю, уже настало время
вырасти из всего этого,

390
00:17:38,641 --> 00:17:40,860
и.. ну ты понимаешь.. двигаться дальше

391
00:17:41,404 --> 00:17:41,963
Правда?

392
00:17:42,623 --> 00:17:43,270
Да..

393
00:17:44,235 --> 00:17:46,531
Ооо, похвально

394
00:17:47,992 --> 00:17:48,716
Спасибо

395
00:17:51,330 --> 00:17:52,442
Послушай, а ты не хотела бы,

396
00:17:52,755 --> 00:17:53,748
ну может, чуть позже..

397
00:17:53,779 --> 00:17:54,643
Простите..

398
00:17:55,535 --> 00:17:56,987
Привет, Пенни.
Привет, Майк.

399
00:17:57,068 --> 00:17:58,859
Готова идти?
Да, только переоденусь.

400
00:17:58,954 --> 00:17:59,969
Дай-ка я тебе помогу!

401
00:18:00,250 --> 00:18:01,082
Перестань!

402
00:18:01,829 --> 00:18:02,586
Пока, ребята.

403
00:18:08,159 --> 00:18:09,184
Моя очередь на Машину Времени!

404
00:18:34,346 --> 00:18:35,244
Сработало.

405
00:18:37,116 --> 00:18:38,074
Действительно сработало.

406
00:18:39,643 --> 00:18:42,642
Они твердили, что я свихнулся,
но она сработала!

407
00:18:49,556 --> 00:18:50,727
О нет!

408
00:18:51,029 --> 00:18:52,264
Только не Морлоки!

409
00:18:52,728 --> 00:18:54,935
Только не плотоядные Морлоки!

410
00:18:55,379 --> 00:18:58,438
На помощь!

411
00:19:06,784 --> 00:19:07,611
Шелдон, ты в порядке?

412
00:19:08,611 --> 00:19:09,996
Нам надо избавляться от
Машины Времени.

413
00:19:11,129 --> 00:19:13,123
Да, чересчур она большая для
гостиной, тоже думаешь?

414
00:19:14,283 --> 00:19:15,659
Вот-вот, в том-то и дело.
Чересчур большая.

415
00:19:17,469 --> 00:19:18,384
Рад, что ты согласился

416
00:19:18,599 --> 00:19:20,151
Я нанял ребят, чтобы они помогли
нам утащить её.

417
00:19:20,171 --> 00:19:21,151
Заходите, парни!

418
00:19:24,726 --> 00:19:27,014
О нет, Морлоки!

419
00:19:27,265 --> 00:19:30,591
Съешьте его,
съешьте его!
Надпись на футболке: Голоднющие Морлоки.

420
00:19:34,187 --> 00:19:30,591
Леонард!

