1
00:00:02,592 --> 00:00:05,177
You know, I've been thinking
about timetravel again.

2
00:00:05,389 --> 00:00:07,936
Why? Did you hit a roadblock
with invisibility?

3
00:00:09,431 --> 00:00:10,791
Put it on a backburner.

4
00:00:11,804 --> 00:00:13,155
Anyway.
It occurs to me,

5
00:00:13,348 --> 00:00:15,064
If I have ever did
perfect time machine,

6
00:00:15,257 --> 00:00:17,225
I'd just go into the past
and give it to myself.

7
00:00:17,495 --> 00:00:20,794
That's eliminating the need for me
to invent it in the first place.

8
00:00:21,616 --> 00:00:22,638
Interesting.

9
00:00:22,829 --> 00:00:25,048
Yeah, it really kicks the pressure off.

10
00:00:26,011 --> 00:00:27,295
Sounds like a breakthrough.

11
00:00:27,468 --> 00:00:29,803
Should I call Science Magazine
and tell them to hold the cover?

12
00:00:30,274 --> 00:00:32,239
It's time travel,
I will have already done that.

13
00:00:34,403 --> 00:00:36,160
Then I guess congratulations
are in order.

14
00:00:36,707 --> 00:00:39,292
No, congratulations will
have been in order.

15
00:00:40,637 --> 00:00:42,620
You know, I am not going
to enjoy this party.

16
00:00:42,775 --> 00:00:44,585
I know,
I'm familiar with you.

17
00:00:45,120 --> 00:00:47,853
The last department party,
Professor Finkleday cornered me

18
00:00:48,007 --> 00:00:50,284
and talked about
spelunking for 45 minutes.

19
00:00:50,480 --> 00:00:51,543
Yes, I was there.

20
00:00:51,698 --> 00:00:53,565
You know what's interesting
about caves?

21
00:00:53,725 --> 00:00:55,108
- What?
- Nothing.

22
00:00:57,202 --> 00:00:59,545
Then we'll avoid him,
we'll meet the new department head,

23
00:00:59,680 --> 00:01:01,372
congratulate him,
shake his hand and go.

24
00:01:01,531 --> 00:01:04,219
How's this?
"Pleased to meet you, Dr. Gablehauser.

25
00:01:04,491 --> 00:01:06,304
"How fortunate for you
that the university's

26
00:01:06,441 --> 00:01:08,038
"chosen to hire you,
despite the fact

27
00:01:08,173 --> 00:01:10,401
"that you've done no original
research in 25 years,

28
00:01:10,536 --> 00:01:12,583
"and instead have written
a series of popular books

29
00:01:12,719 --> 00:01:14,455
"that reduce
the great concepts of science

30
00:01:14,592 --> 00:01:15,843
"to a series of anecdotes,

31
00:01:16,017 --> 00:01:18,230
"each one dumbed down
to accommodate the duration

32
00:01:18,423 --> 00:01:20,311
"of an average bowel movement.

33
00:01:21,748 --> 00:01:22,748
Mahalo."

34
00:01:26,510 --> 00:01:28,504
"Mahalo's" a nice touch.

35
00:01:29,623 --> 00:01:31,938
You know there only eight consonants
in Hawaiian language?

36
00:01:32,073 --> 00:01:34,176
Interesting.
You should lead with that.

37
00:01:36,758 --> 00:01:38,717
Oh, God, look at this buffet.

38
00:01:39,479 --> 00:01:40,731
I love America.

39
00:01:41,226 --> 00:01:42,656
You don't have buffets in India?

40
00:01:42,853 --> 00:01:45,128
Of course,
but it's all Indian food.

41
00:01:46,020 --> 00:01:48,547
You can't find a bagel
in Mumbai to save your life.

42
00:01:48,888 --> 00:01:49,888
Shmear me.

43
00:01:52,308 --> 00:01:55,067
- Here's an interesting turn of events.
- What?

44
00:01:57,549 --> 00:01:58,745
Howard brought a date?

45
00:01:58,873 --> 00:02:00,410
A more plausible explanation is that

46
00:02:00,584 --> 00:02:01,934
his work in robotics has made

47
00:02:02,098 --> 00:02:03,584
an amazing leap forward.

48
00:02:04,524 --> 00:02:06,574
Hey, what up,
science bitches?

49
00:02:08,893 --> 00:02:09,973
May I introduce my

50
00:02:10,127 --> 00:02:11,709
special lady friend Summer?

51
00:02:11,997 --> 00:02:14,600
- I told you touching's extra.
- Right. Sorry.

52
00:02:16,410 --> 00:02:18,847
Here comes our new boss.
Be polite.

53
00:02:19,200 --> 00:02:21,549
Hi, fellas.
Eric Gablehauser.

54
00:02:21,770 --> 00:02:22,826
Howard Wolowitz.

55
00:02:23,714 --> 00:02:25,256
Nice to meet you.
And you are?

56
00:02:26,262 --> 00:02:28,148
An actualrealscientist.

57
00:02:31,815 --> 00:02:32,913
How was that?

58
00:02:34,729 --> 00:02:36,697
I can't believe he fired me.

59
00:02:37,562 --> 00:02:40,146
Well, you did call him
a glorified high school science teacher

60
00:02:40,319 --> 00:02:43,500
whose last successful experiment
was lighting his own farts.

61
00:02:45,058 --> 00:02:46,081
In my defense,

62
00:02:46,273 --> 00:02:48,684
I prefaced that by saying,
"With all due respect."

63
00:03:09,937 --> 00:03:11,823
Synchro: Sixe

64
00:03:17,227 --> 00:03:18,227
Morning.

65
00:03:18,581 --> 00:03:19,581
Morning.

66
00:03:20,132 --> 00:03:21,928
You're making eggs for breakfast?

67
00:03:22,184 --> 00:03:24,636
This isn't breakfast,
it's an experiment.

68
00:03:26,827 --> 00:03:28,602
'Cause it looks a lot like breakfast.

69
00:03:29,125 --> 00:03:31,177
I finally have time
to test my hypothesis

70
00:03:31,374 --> 00:03:33,169
about the separation
of the water molecules

71
00:03:33,351 --> 00:03:36,264
from the egg proteins and its
impact vis-a-vis taste.

72
00:03:38,010 --> 00:03:39,010
Sounds yummy.

73
00:03:40,311 --> 00:03:42,122
I look forward
to your work with bacon.

74
00:03:42,567 --> 00:03:43,567
As do I.

75
00:03:45,449 --> 00:03:47,398
You know, I'm sure
if you just apologize

76
00:03:47,610 --> 00:03:49,600
to Gablehauser,
he would give you your job back.

77
00:03:49,754 --> 00:03:50,970
I don't want my job back.

78
00:03:51,476 --> 00:03:53,255
I've spent the past
three and a half years

79
00:03:53,413 --> 00:03:55,260
staring at grease boards
full of equations.

80
00:03:55,456 --> 00:03:57,881
Before that, I spent four years
working on my thesis.

81
00:03:58,035 --> 00:03:59,447
Before that, I was in college,

82
00:03:59,582 --> 00:04:01,609
and before that,
I was in the fifth grade.

83
00:04:03,006 --> 00:04:06,598
This is my first day off in decades
and I'm going to savor it.

84
00:04:09,038 --> 00:04:11,026
I'll let you get back
to fixing your eggs.

85
00:04:11,309 --> 00:04:13,874
Not just fixing my eggs,
I'm fixing everyone's eggs.

86
00:04:14,712 --> 00:04:16,262
And we all thank you.

87
00:04:47,834 --> 00:04:49,686
Use new eggs.

88
00:04:54,254 --> 00:04:56,299
I'm running out to the market.
You need anything?

89
00:04:57,457 --> 00:04:59,191
This would be
one of those circumstances

90
00:04:59,339 --> 00:05:02,727
that people unfamiliar with the law of
large numbers would call a coincidence.

91
00:05:03,387 --> 00:05:04,892
- I'm sorry?
- I need eggs.

92
00:05:05,044 --> 00:05:06,447
Four dozen should suffice.

93
00:05:06,700 --> 00:05:07,813
- Four dozen?
- Yes,

94
00:05:07,963 --> 00:05:09,809
and evenly distributed
amongst brown, white,

95
00:05:09,964 --> 00:05:12,357
free-range, large,
extra large and jumbo.

96
00:05:13,226 --> 00:05:14,307
Okay, one more time.

97
00:05:14,558 --> 00:05:16,378
Never mind.
You won't get it right.

98
00:05:16,901 --> 00:05:18,425
I'd better come with you.

99
00:05:25,313 --> 00:05:27,493
How come you didn't
go into work today?

100
00:05:27,619 --> 00:05:28,938
I'm taking a sabbatical

101
00:05:29,092 --> 00:05:31,563
because I won't kowtow
to mediocre minds.

102
00:05:32,224 --> 00:05:33,879
So you got canned, huh?

103
00:05:34,423 --> 00:05:36,970
Theoretical physicists
do not get canned...

104
00:05:37,976 --> 00:05:38,976
but yeah.

105
00:05:40,482 --> 00:05:41,794
Maybe it's all for the best.

106
00:05:41,934 --> 00:05:44,587
I always say when one door closes,
another one opens.

107
00:05:45,287 --> 00:05:46,559
No, it doesn't.

108
00:05:47,492 --> 00:05:49,761
Not unless the two doors
are connected by relays

109
00:05:49,915 --> 00:05:52,380
- or there are motion sensors involved.
- No, I meant...

110
00:05:52,515 --> 00:05:54,960
Or if the first door closing
creates a change of air pressure

111
00:05:55,095 --> 00:05:56,582
that acts upon the second door.

112
00:05:57,558 --> 00:05:58,558
Never mind.

113
00:06:00,277 --> 00:06:01,901
Slow down.

114
00:06:02,114 --> 00:06:04,381
- Please, slow down!
- We're fine!

115
00:06:04,527 --> 00:06:06,763
You're not leaving yourself
enough space between cars.

116
00:06:06,919 --> 00:06:09,652
- Oh, sure, I am.
- No, let me do the math for you.

117
00:06:09,772 --> 00:06:12,231
This car weighs,
let's say 4,000 pounds.

118
00:06:12,505 --> 00:06:14,376
Now add 140 for me,
120 for you...

119
00:06:14,732 --> 00:06:15,900
120?

120
00:06:16,286 --> 00:06:17,777
I'm sorry. Did I insult you?

121
00:06:17,970 --> 00:06:20,466
Is your body mass
somehow tied into your self-worth?

122
00:06:21,295 --> 00:06:22,318
Well, yeah.

123
00:06:23,145 --> 00:06:24,145
Interesting.

124
00:06:24,265 --> 00:06:26,169
Anyway, that gives us
a total weight of,

125
00:06:26,324 --> 00:06:27,901
let's say 4,400 pounds.

126
00:06:28,105 --> 00:06:29,338
Let's say 4,390.

127
00:06:30,219 --> 00:06:33,074
Fine. We're traveling forward at--

128
00:06:33,247 --> 00:06:34,964
good Lord-- 51 miles an hour.

129
00:06:35,591 --> 00:06:36,980
Let's assume that your brakes

130
00:06:37,154 --> 00:06:38,777
are new
and the calipers are aligned.

131
00:06:38,977 --> 00:06:40,600
Still, by the time we come
to a stop,

132
00:06:40,720 --> 00:06:43,290
we'll be occupying the same space
as that Buick in front of us,

133
00:06:43,483 --> 00:06:45,845
an impossibility that
nature will quickly resolve

134
00:06:46,014 --> 00:06:47,316
into death, mutilation...

135
00:06:47,471 --> 00:06:49,807
Oh, look, they built
a new putt-putt course.

136
00:06:53,491 --> 00:06:54,628
This is great.

137
00:06:55,143 --> 00:06:56,143
Look at me.

138
00:06:56,308 --> 00:06:58,128
I'm in the real world
of ordinary people

139
00:06:58,734 --> 00:07:01,640
just living their ordinary,
colorless workaday lives.

140
00:07:02,711 --> 00:07:03,714
Thank you.

141
00:07:03,907 --> 00:07:04,986
No, thankyou.

142
00:07:05,859 --> 00:07:07,752
And thank you, ordinary person.

143
00:07:10,190 --> 00:07:12,337
You want to hear
an interesting thing about tomatoes?

144
00:07:12,615 --> 00:07:13,795
No, not really.

145
00:07:13,967 --> 00:07:16,668
Listen, didn't you say
you need some eggs?

146
00:07:17,019 --> 00:07:20,196
Yes, but anyone who knows anything
about the dynamics of bacterial growth

147
00:07:20,350 --> 00:07:23,341
knows to pick up their refrigerated food
on the way out of the supermarket.

148
00:07:24,107 --> 00:07:26,324
Okay, then maybe you should start
heading on out then.

149
00:07:27,690 --> 00:07:29,309
No, this is fun.

150
00:07:29,717 --> 00:07:32,575
Oh, the thing about tomatoes--
and I think you'll really enjoy this--

151
00:07:32,854 --> 00:07:36,240
is they're shelved with the vegetables,
but they're technically a fruit.

152
00:07:36,452 --> 00:07:37,742
- Interesting.
- Isn't it?

153
00:07:37,903 --> 00:07:40,045
No, I mean
what you find enjoyable.

154
00:07:44,710 --> 00:07:45,710
Oh, boy.

155
00:07:47,518 --> 00:07:48,518
What now?

156
00:07:49,579 --> 00:07:51,978
Well, there's some value
to taking a multivitamin,

157
00:07:52,172 --> 00:07:54,255
but the human body
can only absorb so much.

158
00:07:54,436 --> 00:07:55,706
What you're buying here

159
00:07:55,940 --> 00:07:58,544
are the ingredients
for very expensive urine.

160
00:08:00,181 --> 00:08:02,731
Well, maybe that's what
I was going for.

161
00:08:03,205 --> 00:08:04,941
Then you'll want some manganese.

162
00:08:11,844 --> 00:08:13,302
That was fun.

163
00:08:13,422 --> 00:08:15,983
Maybe tomorrow we can go
to one of those big warehouse stores.

164
00:08:16,103 --> 00:08:17,279
Oh, I don't know.

165
00:08:17,480 --> 00:08:21,142
It's going to take me a while to recover
from all the fun I had today.

166
00:08:22,164 --> 00:08:23,164
Are you sure?

167
00:08:23,357 --> 00:08:25,294
There are a lot of advantages
to buying in bulk.

168
00:08:25,471 --> 00:08:27,976
For example, I noticed
that you purchase your tampons

169
00:08:28,169 --> 00:08:29,581
one-month supply at a time.

170
00:08:32,282 --> 00:08:33,266
What?

171
00:08:33,866 --> 00:08:35,891
Think about it.
It's a product that doesn't spoil

172
00:08:36,043 --> 00:08:39,311
and you're going to be needing them
for at least the next 30 years.

173
00:08:40,040 --> 00:08:42,650
You want me to buy 30 years
worth of tampons?

174
00:08:43,454 --> 00:08:46,059
Well, 30, 35.
When did your mother go into menopause?

175
00:08:48,038 --> 00:08:49,678
I'm not talking
about this with you.

176
00:08:51,139 --> 00:08:52,567
This is a natural human process,

177
00:08:52,721 --> 00:08:55,114
and we're talking about
statistically significant savings.

178
00:08:55,345 --> 00:08:59,050
Now, if you assume 15 tampons
per cycle and a 28-day cycle...

179
00:08:59,185 --> 00:09:00,709
Are you fairly regular?

180
00:09:05,879 --> 00:09:07,133
Okay, no warehouse store,

181
00:09:07,268 --> 00:09:09,391
but we're still on
for putt-putt golf, right?

182
00:09:15,606 --> 00:09:18,244
Hey, I just ran into Penny.
She seemed upset about something.

183
00:09:18,688 --> 00:09:20,752
I think it's her time of the month.

184
00:09:23,162 --> 00:09:25,593
I marked the calendar
for future reference.

185
00:09:28,972 --> 00:09:30,499
What's with the fish?

186
00:09:31,476 --> 00:09:32,926
It's an experiment.

187
00:09:34,337 --> 00:09:36,217
What happened
to your scrambled egg research?

188
00:09:36,388 --> 00:09:37,642
Oh, that was a dead end.

189
00:09:37,834 --> 00:09:40,632
Scrambled eggs are as good
as they're ever going to be.

190
00:09:41,290 --> 00:09:43,391
So... fish?

191
00:09:45,098 --> 00:09:47,026
I read an article
about Japanese scientists

192
00:09:47,188 --> 00:09:49,909
who inserted DNA from luminous
jellyfish into other animals,

193
00:09:50,176 --> 00:09:53,205
and I thought,
"Hey, fish nightlights."

194
00:09:56,759 --> 00:09:58,050
Fish nightlights?

195
00:09:58,414 --> 00:10:00,035
It's a billion-dollar idea.

196
00:10:02,568 --> 00:10:03,568
Mum's the word.

197
00:10:05,253 --> 00:10:06,738
Are you sure you don't want

198
00:10:06,873 --> 00:10:09,362
to just apologize to Gablehauser
and get your job back?

199
00:10:10,429 --> 00:10:11,896
No, I have too much to do.

200
00:10:13,279 --> 00:10:14,431
Like luminous fish.

201
00:10:15,395 --> 00:10:16,861
I'm sorry.
I didn't...

202
00:10:18,812 --> 00:10:20,085
That's just the beginning.

203
00:10:20,449 --> 00:10:21,649
I also have an idea

204
00:10:21,842 --> 00:10:24,449
for a bulk mail-order
feminine hygiene company.

205
00:10:24,654 --> 00:10:26,613
Oh, glow-in-the-dark tampons!

206
00:10:30,015 --> 00:10:31,867
Leonard, we're gonna be rich.

207
00:10:35,928 --> 00:10:37,846
Thanks for coming
on such short notice.

208
00:10:37,981 --> 00:10:40,658
- You did the right thing calling.
- I didn't know what else to do.

209
00:10:40,812 --> 00:10:43,955
He's lost all focus.
Every day he's got a new obsession.

210
00:10:50,887 --> 00:10:53,337
This is a particularly disturbing one.

211
00:10:56,350 --> 00:10:57,350
Mommy?

212
00:10:58,504 --> 00:11:00,200
Hi, baby!

213
00:11:11,269 --> 00:11:12,726
I'm very proud of you honey.

214
00:11:12,919 --> 00:11:14,738
You showed a lot of courage today.

215
00:11:15,680 --> 00:11:16,720
Thanks, Mom.

216
00:11:23,169 --> 00:11:26,005
Is Dr. Gablehauser going
to be my new daddy?

217
00:11:29,403 --> 00:11:30,443
We'll see.

218
00:11:31,101 --> 00:11:30,443
Sleep tight.

