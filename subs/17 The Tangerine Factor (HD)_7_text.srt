1
00:00:02,554 --> 00:00:04,707
<i>Wo de zing shi</i> Sheldon.

2
00:00:05,125 --> 00:00:06,111
Non, c'est...

3
00:00:06,272 --> 00:00:08,375
<i>Wo de ming zi shi</i> Sheldon.

4
00:00:09,795 --> 00:00:12,326
<i>Wo de ming zi shi</i> Sheldon.

5
00:00:13,815 --> 00:00:14,840
What's this?

6
00:00:16,618 --> 00:00:18,009
That's what you did.

7
00:00:18,619 --> 00:00:21,877
I assumed, as in a number of languages,
that the gesture was part of the phrase.

8
00:00:23,048 --> 00:00:24,046
Well, it's not.

9
00:00:24,673 --> 00:00:27,707
How am I supposed to know that?
As the teacher, it's your obligation

10
00:00:27,827 --> 00:00:30,886
to separate your personal idiosyncrasies
from the subject matter.

11
00:00:31,403 --> 00:00:33,845
You know, I'm really glad
you decided to learn Mandarin.

12
00:00:33,965 --> 00:00:35,457
- Why?
- Once you're fluent,

13
00:00:35,577 --> 00:00:38,310
you'll have a billion more people
to annoy instead of me.

14
00:00:41,129 --> 00:00:42,606
<i>Mei du lui zi.</i>

15
00:00:43,686 --> 00:00:46,178
You just called Leonard
a syphilitic donkey.

16
00:00:47,792 --> 00:00:49,112
My apologies,

17
00:00:49,272 --> 00:00:51,189
I'm only as good as my teacher.

18
00:00:52,424 --> 00:00:54,009
Why are you learning Chinese?

19
00:00:54,129 --> 00:00:56,408
I believe the Szechuan Palace
has been passing off

20
00:00:56,528 --> 00:00:59,951
orange chicken as tangerine chicken
and I intend to confront them.

21
00:01:01,418 --> 00:01:03,112
If I were you, I'd be more concerned

22
00:01:03,232 --> 00:01:05,165
about what they are passing off
as chicken.

23
00:01:08,290 --> 00:01:10,881
- I need to use your window.
- Sure, go ahead.

24
00:01:12,879 --> 00:01:15,506
Hey, jerk face, you forgot your iPod!

25
00:01:18,009 --> 00:01:20,221
- What's going on?
- I'll tell you what's going on.

26
00:01:20,341 --> 00:01:23,855
That stupid self-centered bastar
wrote about our sex life in his blog.

27
00:01:26,476 --> 00:01:29,132
Drop dead,
you stupid self-centered bastard!

28
00:01:31,526 --> 00:01:32,543
Thank you.

29
00:01:36,027 --> 00:01:37,578
Okay, where were we?

30
00:01:38,219 --> 00:01:40,392
Not now, I have a blog to find.

31
00:01:42,724 --> 00:01:45,544
The Big Bang Theory
Season 1 Episode 17 - The Tangerine Factor

32
00:01:46,292 --> 00:01:49,462
Transcript: swsub.com
Synchro: SerJo

33
00:02:11,593 --> 00:02:12,989
Penny, are you okay?

34
00:02:13,109 --> 00:02:15,062
I'm fine, Leonard. Just go away!

35
00:02:16,671 --> 00:02:19,601
I understand that breaking up
with someone can be very painful...

36
00:02:19,721 --> 00:02:21,854
- Go away!
- Okay, feel better! Bye!

37
00:02:27,869 --> 00:02:29,285
She doesn't want to talk.

38
00:02:30,859 --> 00:02:33,584
Not surprising.
Penny's emotional response is originated

39
00:02:33,704 --> 00:02:36,417
in the primitive portion of the brain,
known as the amygdala.

40
00:02:36,577 --> 00:02:39,452
While speech is centered in the much
more recently developed neo-cortex.

41
00:02:39,572 --> 00:02:41,505
The former can easily
overpower the latter,

42
00:02:41,625 --> 00:02:44,759
giving scientific credence to the notion
of being "rendered speechless."

43
00:02:50,939 --> 00:02:53,017
Or maybe she just doesn't want to talk.

44
00:02:55,883 --> 00:02:57,521
Look, I found an iPod!

45
00:03:01,494 --> 00:03:04,320
It's smashed beyond repair.
What are you gonna do with it?

46
00:03:05,002 --> 00:03:07,633
What else?
Sell it on eBay as "slightly used."

47
00:03:08,889 --> 00:03:10,803
It was Penny's boyfriend's,
they broke up.

48
00:03:10,923 --> 00:03:14,252
Apparently, he posted intimate details
of their physical relationship

49
00:03:14,372 --> 00:03:17,616
on his blog
which I cannot find anywhere.

50
00:03:19,111 --> 00:03:21,822
I'm gonna go back
and try talking to her again.

51
00:03:21,942 --> 00:03:23,651
Good idea, sit with her.

52
00:03:23,771 --> 00:03:25,132
Hold her, comfort her.

53
00:03:25,293 --> 00:03:26,940
And if the moment feels right,

54
00:03:27,060 --> 00:03:28,914
see if you can cop a feel.

55
00:03:31,447 --> 00:03:33,082
I'm not going to do that, Howard.

56
00:03:33,202 --> 00:03:36,909
I'm not aware of a new social convention
that requires you to intervene at all.

57
00:03:37,208 --> 00:03:38,854
What about damsel in distress?

58
00:03:39,301 --> 00:03:42,212
Twelfth century code of chivalry?
Not exactly current.

59
00:03:43,103 --> 00:03:45,535
You'd also have to be knighted
for that to apply.

60
00:03:46,671 --> 00:03:49,143
I don't care.
She's upset, I'm going over there.

61
00:03:49,263 --> 00:03:52,153
Remember to sit on your hands a bit,
so they're warm.

62
00:03:53,569 --> 00:03:56,831
I'm her friend. I'm not going
to take advantage of her vulnerability.

63
00:03:57,655 --> 00:03:59,928
So you're saying,
if in the depths of despair,

64
00:04:00,048 --> 00:04:02,503
she throws herself at you
and demands you take her,

65
00:04:02,663 --> 00:04:05,089
right there, right now,
you'll just walk away?

66
00:04:05,957 --> 00:04:08,070
I said I'm her friend,
not her gay friend.

67
00:04:13,507 --> 00:04:15,667
Listen, I know you said
you didn't want to talk...

68
00:04:15,787 --> 00:04:17,226
- I don't.
- Sorry.

69
00:04:17,960 --> 00:04:19,993
- Wait!
- Wait? Did you say "wait"?

70
00:04:22,320 --> 00:04:23,656
Tell me the truth.

71
00:04:24,114 --> 00:04:27,069
Am I just an idiot
who picks giant losers?

72
00:04:30,054 --> 00:04:32,449
So I pick good guys
but turn them into losers.

73
00:04:32,609 --> 00:04:34,076
- Of course not.
- Well,

74
00:04:34,237 --> 00:04:36,787
it's gotta be one or the other.
Which is it?

75
00:04:42,691 --> 00:04:45,004
I'm sorry, what were the choices again?

76
00:04:45,362 --> 00:04:47,298
I really thought Mike was different.

77
00:04:47,459 --> 00:04:48,716
I thought he was...

78
00:04:48,877 --> 00:04:50,585
sensitive and smart,

79
00:04:50,866 --> 00:04:53,839
I mean, not <i>you</i> smart,
normal non-freaky smart.

80
00:04:54,340 --> 00:04:55,598
Yeah, no, sure.

81
00:04:55,759 --> 00:04:58,127
And then he just goes
and has to humiliate me

82
00:04:58,247 --> 00:05:01,687
by writing about me on his blog
so the entire world can read it.

83
00:05:01,848 --> 00:05:04,327
I tell you, it's not all
that easy to find.

84
00:05:04,683 --> 00:05:07,812
Really? My friends at work found it,
my sister found it.

85
00:05:07,932 --> 00:05:09,854
Judging by my email,
a number of prisoners

86
00:05:09,974 --> 00:05:12,488
at the Michigan State
Penitentiary found it.

87
00:05:12,941 --> 00:05:14,909
What exactly did this guy write?

88
00:05:15,070 --> 00:05:18,578
Not that I need to know the details
of your sex life. I just thought...

89
00:05:19,667 --> 00:05:20,732
Never mind.

90
00:05:21,609 --> 00:05:22,925
No, you know what?

91
00:05:23,398 --> 00:05:26,244
You might as well read it.
Everybody else has. Go ahead.

92
00:05:26,776 --> 00:05:30,134
Oh, God, I just feel
so betrayed and embarrassed!

93
00:05:30,254 --> 00:05:33,022
I just want to crawl
into a hole and die!

94
00:05:36,846 --> 00:05:39,183
Okay. Well, you know,
this isn't that bad.

95
00:05:39,717 --> 00:05:41,143
It just paints the picture

96
00:05:41,303 --> 00:05:43,115
of a very affectionate woman,

97
00:05:43,235 --> 00:05:45,685
who's open to expressing her affection

98
00:05:46,164 --> 00:05:48,317
in non-traditional locales.

99
00:05:48,679 --> 00:05:49,918
Oh, God!

100
00:05:50,612 --> 00:05:52,411
Elevators, parks,

101
00:05:53,666 --> 00:05:54,925
movie theaters.

102
00:05:56,604 --> 00:05:57,975
Out of curiosity,

103
00:05:58,095 --> 00:06:00,204
is this "subway"
the transportation system

104
00:06:00,324 --> 00:06:02,247
or Subway the sandwich shop?

105
00:06:03,477 --> 00:06:04,638
Sandwich shop.

106
00:06:06,673 --> 00:06:08,671
Doesn't that violate the health code?

107
00:06:09,236 --> 00:06:11,215
No, at the sub shop
we were only making out.

108
00:06:13,637 --> 00:06:15,344
But my point is that

109
00:06:15,505 --> 00:06:17,930
you have absolutely no reason
to be embarrassed.

110
00:06:18,909 --> 00:06:21,048
Really?
Do you think I overreacted?

111
00:06:21,623 --> 00:06:24,334
- Maybe a little.
- Because I do that. I do overreact.

112
00:06:24,454 --> 00:06:26,350
Maybe I should call Mike and apologize.

113
00:06:27,576 --> 00:06:29,733
That would be under-reacting.

114
00:06:30,721 --> 00:06:33,790
He did break the implied
confidentiality of the bedroom.

115
00:06:33,910 --> 00:06:37,908
And in your case, the elevator,
parks and fast food franchise.

116
00:06:39,284 --> 00:06:41,870
You're right.
I should just say "I am done with him."

117
00:06:42,031 --> 00:06:43,831
Yes, you should.
Go ahead, say it.

118
00:06:44,526 --> 00:06:47,084
But I never gave the man
a chance to explain.

119
00:06:47,630 --> 00:06:50,254
What is there to explain?
It's all right here.

120
00:06:50,415 --> 00:06:51,890
The betrayal!

121
00:06:52,583 --> 00:06:54,904
No, you were right
the first time.

122
00:06:55,024 --> 00:06:58,522
This is a man who loves me,
but in his own stupid way,

123
00:06:58,642 --> 00:07:01,465
which is trying to show people
how he feels.

124
00:07:02,624 --> 00:07:04,636
I'm pretty sure I never said that.

125
00:07:06,221 --> 00:07:08,772
You did better than that.
You helped me see it on my own.

126
00:07:09,029 --> 00:07:10,315
Oh, good for me.

127
00:07:12,273 --> 00:07:13,436
Where are you going?

128
00:07:13,556 --> 00:07:16,071
I'm going over to Mike's.
Leonard, thank you so much.

129
00:07:16,232 --> 00:07:17,349
Oh, sure.

130
00:07:24,299 --> 00:07:26,008
Maybe I <i>am</i> her gay friend.

131
00:07:33,415 --> 00:07:35,535
I'm going to need
another Mandarin lesson.

132
00:07:35,655 --> 00:07:38,447
I obviously didn't make
my point with those people.

133
00:07:39,796 --> 00:07:40,830
For God's sake,

134
00:07:40,950 --> 00:07:43,931
if you don't like the tangerine chicken,
don't order the tangerine chicken.

135
00:07:44,294 --> 00:07:47,311
I like tangerine chicken,
I'm just not getting tangerine chicken.

136
00:07:49,322 --> 00:07:51,482
- Can we please change the subject?
- Sure.

137
00:07:51,902 --> 00:07:55,486
Tell us again how you screwed up and got
Penny back together with her boyfriend.

138
00:07:57,182 --> 00:07:58,674
Just roll the dice.

139
00:08:02,761 --> 00:08:06,455
"Enslaved by warlocks. Stay here
until you roll two, four or six."

140
00:08:06,616 --> 00:08:07,790
She was mad at him!

141
00:08:08,832 --> 00:08:11,919
She was done with him! The relationship
was broken beyond repair,

142
00:08:12,079 --> 00:08:14,415
and I walked over there,
and I fixed it.

143
00:08:15,875 --> 00:08:18,634
Boy, that story gets better
every time you hear it.

144
00:08:20,003 --> 00:08:22,808
Actually, I thought the first
two renditions were far more compelling.

145
00:08:22,928 --> 00:08:25,441
Previously, I felt sympathy
for the Leonard character,

146
00:08:25,561 --> 00:08:28,357
now I just find him
to be whiny and annoying.

147
00:08:30,097 --> 00:08:31,980
Eat your tangerine chicken.

148
00:08:32,342 --> 00:08:35,025
I'd love to, but I don't
have tangerine chicken.

149
00:08:37,682 --> 00:08:40,711
Thank you so much
for your stupid advice!

150
00:08:46,672 --> 00:08:47,855
Incredible.

151
00:08:48,234 --> 00:08:50,871
You managed to screw up the screw-up.

152
00:08:56,953 --> 00:08:58,173
I'm back.

153
00:08:58,931 --> 00:09:00,612
I'm sorry I yelled at you.

154
00:09:00,986 --> 00:09:02,386
It's not your fault.

155
00:09:02,912 --> 00:09:04,261
What happened?

156
00:09:05,448 --> 00:09:07,474
I went over to Mike's
to make up with him.

157
00:09:07,635 --> 00:09:09,227
Yeah. No, I know that part.

158
00:09:10,314 --> 00:09:12,104
But he had already moved on.

159
00:09:12,495 --> 00:09:14,022
Already? That was quick.

160
00:09:14,183 --> 00:09:18,144
That's what I said to the woman
who had her legs around his neck.

161
00:09:21,707 --> 00:09:25,569
- Penny, I am so sorry.
- How could he do that?!

162
00:09:25,948 --> 00:09:29,279
You know, you did
throw an 80-gig iPod...

163
00:09:29,489 --> 00:09:31,373
Yeah, no, how could he do that?

164
00:09:31,735 --> 00:09:35,283
I swear to God I am done
with guys like that.

165
00:09:35,403 --> 00:09:39,214
You know, macho with the perfect body
and the hair and the money.

166
00:09:39,375 --> 00:09:41,397
Yeah, that must get old quick.

167
00:09:42,264 --> 00:09:43,552
You know, just once

168
00:09:44,162 --> 00:09:46,153
I would like to go out with someone

169
00:09:46,273 --> 00:09:48,173
who is nice and honest

170
00:09:48,293 --> 00:09:50,434
and who actually cares about me.

171
00:09:52,489 --> 00:09:53,659
What about me?

172
00:09:54,391 --> 00:09:55,883
What about you what?

173
00:09:58,586 --> 00:10:00,895
What about if you went out with me?

174
00:10:03,811 --> 00:10:05,664
Are you asking me out?

175
00:10:06,962 --> 00:10:08,554
Yes, I am...

176
00:10:09,042 --> 00:10:10,348
asking you out.

177
00:10:14,432 --> 00:10:16,770
I was just going off
your comment about the nice guy.

178
00:10:16,890 --> 00:10:18,444
- No, I got that.
- And honest.

179
00:10:18,564 --> 00:10:20,589
- Totally.
- It's not a big deal.

180
00:10:22,906 --> 00:10:24,119
Yes what?

181
00:10:24,966 --> 00:10:26,791
Yes, I will go out with you.

182
00:10:30,897 --> 00:10:31,767
Really?

183
00:10:35,601 --> 00:10:38,357
Why not? I mean...
What do I have to lose?

184
00:10:41,922 --> 00:10:43,272
That's the spirit.

185
00:10:49,062 --> 00:10:51,056
Show me your citrus peels.

186
00:10:51,375 --> 00:10:54,326
<i>Gay wo kan ni de jud zi pee.</i>

187
00:10:56,176 --> 00:10:58,290
Show me your citrus peels.

188
00:10:58,410 --> 00:11:01,379
<i>Gay wo kan ni de jud zi pee.</i>

189
00:11:01,844 --> 00:11:02,854
Sheldon?

190
00:11:06,344 --> 00:11:07,316
I'm sorry.

191
00:11:08,295 --> 00:11:10,027
Look, do you have a second?

192
00:11:10,448 --> 00:11:12,610
A second what, pair of underwear?

193
00:11:13,459 --> 00:11:16,534
I was just wondering if I could
talk to you. It's about Leonard.

194
00:11:16,897 --> 00:11:17,826
Why me?

195
00:11:17,946 --> 00:11:19,871
Why not Koothrappali or Wolowitz?

196
00:11:20,377 --> 00:11:22,859
Well, Raj can't talk to me
unless he's drunk,

197
00:11:22,979 --> 00:11:25,398
and Wolowitz is, you know, disgusting.

198
00:11:27,109 --> 00:11:28,897
Yes, I suppose he is.

199
00:11:30,623 --> 00:11:33,049
All I'm saying is,
you know Leonard the best.

200
00:11:33,338 --> 00:11:34,426
Not necessarily.

201
00:11:34,546 --> 00:11:37,551
I'm often surprised
by my lack of familiarity with Leonard.

202
00:11:37,671 --> 00:11:40,807
Just the other day I discovered
he not only has a loofah,

203
00:11:40,927 --> 00:11:42,159
he hides it.

204
00:11:42,699 --> 00:11:45,612
Why do you suppose a man would
be ashamed of having a loofah?

205
00:11:45,732 --> 00:11:49,036
I, myself, prefer to have my excess
epithelial cells slough off naturally,

206
00:11:49,156 --> 00:11:52,459
but I don't condemn those who seek
to accelerate the process.

207
00:11:53,417 --> 00:11:54,647
And until recently,

208
00:11:54,788 --> 00:11:57,608
I had no idea that
despite his lactose intolerance,

209
00:11:57,728 --> 00:12:00,268
he can tolerate small amounts
of non-fat ice cream

210
00:12:00,388 --> 00:12:02,730
without producing a noxious
gas that I maintain,

211
00:12:02,850 --> 00:12:05,208
in the right concentration,
could be weaponized.

212
00:12:06,916 --> 00:12:09,295
Leonard might come home.
Can we talk in my apartment?

213
00:12:09,997 --> 00:12:11,153
We're not done?

214
00:12:13,214 --> 00:12:13,925
No.

215
00:12:14,360 --> 00:12:17,220
Why not? We're already through
the looking glass anyway.

216
00:12:22,094 --> 00:12:23,689
Okay, so here's the thing...

217
00:12:24,537 --> 00:12:27,148
I guess you're aware
that Leonard asked me out.

218
00:12:27,772 --> 00:12:31,132
He didn't actually say anything,
but when he came back to the apartment

219
00:12:31,252 --> 00:12:34,834
he was doing a dance that brought
to mind the happy hippos in <i>Fantasia</i>.

220
00:12:36,577 --> 00:12:37,981
That's nice.

221
00:12:38,286 --> 00:12:40,345
Anyhow, the thing I wanted
to talk to you about

222
00:12:40,465 --> 00:12:43,329
is, since Leonard and I
have become friends... I was just...

223
00:12:44,420 --> 00:12:45,576
Wanna sit down?

224
00:12:46,214 --> 00:12:48,176
I wish it were that simple.

225
00:12:49,954 --> 00:12:53,714
I don't spend much time here, and so
I've never really chosena place to sit.

226
00:12:54,350 --> 00:12:55,345
Well, choose.

227
00:12:55,960 --> 00:12:58,201
There are a number of options and...

228
00:12:59,230 --> 00:13:01,888
I'm really not familiar enough
with the cushion densities,

229
00:13:02,008 --> 00:13:04,101
air flow patterns
and dispersion of unlight

230
00:13:04,221 --> 00:13:05,763
to make an informed choice.

231
00:13:06,303 --> 00:13:08,880
Why don't you just pick one at random
and if you don't like it

232
00:13:09,000 --> 00:13:12,093
- you can sit somewhere else next time.
- No. That's crazy.

233
00:13:13,882 --> 00:13:16,455
You go ahead and talk
while I figure it out.

234
00:13:20,327 --> 00:13:21,503
Here's the thing.

235
00:13:21,802 --> 00:13:24,453
So I've known for a while now
that Leonard has had

236
00:13:24,771 --> 00:13:27,614
- a little crush on me...
- A little crush?

237
00:13:28,827 --> 00:13:32,854
I suppose so... in the same way Menelaus
had a "little crush" on Helen of Troy.

238
00:13:34,804 --> 00:13:36,726
I don't really know who they are...

239
00:13:36,846 --> 00:13:39,343
- Menelaus was the brother of Agamemnon.
- I don't care.

240
00:13:40,272 --> 00:13:41,636
The point is,

241
00:13:42,014 --> 00:13:44,682
Leonard isn't the kind of guy
I usually go out with.

242
00:13:44,877 --> 00:13:48,081
Leonard isn't the kind of guy
anyone usually goes out with.

243
00:13:49,212 --> 00:13:52,102
Would you be open to rotating
the couch clockwise 30 degrees?

244
00:13:52,316 --> 00:13:53,774
No. What I'm saying is

245
00:13:54,540 --> 00:13:56,736
Leonard might be
different in a good way.

246
00:13:57,292 --> 00:14:00,239
Obviously, my usual choices
have not worked out so well.

247
00:14:00,399 --> 00:14:02,543
Your last one worked
out well for Koothrappali.

248
00:14:02,663 --> 00:14:04,077
He got a free iPod.

249
00:14:06,852 --> 00:14:07,709
Glare.

250
00:14:08,777 --> 00:14:11,153
On the other hand,
if things don't go well with Leonard,

251
00:14:11,273 --> 00:14:13,460
I risk losing a really good friend.

252
00:14:13,621 --> 00:14:15,542
I'm guessing he's not looking
for a fling.

253
00:14:15,662 --> 00:14:18,053
He's the kind of guy that gets
into a relationship for...

254
00:14:18,173 --> 00:14:20,801
- like you would say, light years.
- I would not say that.

255
00:14:21,963 --> 00:14:25,191
No one would say that. A light year
is a unit of distance not time.

256
00:14:25,920 --> 00:14:27,850
Thank you for the clarification.

257
00:14:34,983 --> 00:14:35,844
Draft.

258
00:14:37,155 --> 00:14:40,244
People hear the word "year"
and they think duration.

259
00:14:40,364 --> 00:14:44,046
"Foot-pound" has the same problem,
that's a unit of work, not of weight.

260
00:14:44,166 --> 00:14:45,409
Right. Thanks.

261
00:14:45,619 --> 00:14:48,667
- It's a common mistake.
- Not the first one I've made today.

262
00:14:51,581 --> 00:14:53,469
I think this will be my seat.

263
00:14:54,829 --> 00:14:57,529
Do you have anything to say
that has anything to do with

264
00:14:57,649 --> 00:14:59,040
what I'm talking about?

265
00:14:59,972 --> 00:15:01,440
Well, let's see...

266
00:15:02,129 --> 00:15:04,220
We might consider Schrodinger's cat.

267
00:15:05,026 --> 00:15:07,431
Schrodinger?
Is that the woman in 2A?

268
00:15:07,591 --> 00:15:08,962
That's Mrs. Grossinger.

269
00:15:09,082 --> 00:15:11,386
She doesn't have a cat,
she has a Mexican hairless.

270
00:15:11,531 --> 00:15:13,407
- Annoying little animal.
- Sheldon.

271
00:15:15,455 --> 00:15:16,864
Sorry, you diverted me.

272
00:15:16,984 --> 00:15:20,291
Anyway, in 1935, Erwin Schrodinger,

273
00:15:20,411 --> 00:15:23,716
in an attempt to explain the Copenhagen
interpretation of quantum physics,

274
00:15:23,836 --> 00:15:25,322
he proposed an experiment

275
00:15:25,442 --> 00:15:29,104
where a cat is placed in a box
with a sealed vial of poison

276
00:15:29,224 --> 00:15:30,997
that will break open at a random time.

277
00:15:31,158 --> 00:15:32,665
Now, since no one knows

278
00:15:32,825 --> 00:15:36,711
when or if the poison has been released,
until the box is opened,

279
00:15:36,831 --> 00:15:40,292
the cat can be thought of
as both alive and dead.

280
00:15:45,878 --> 00:15:47,433
Sorry. I don't get the point.

281
00:15:47,553 --> 00:15:50,024
Of course you don't get it.
I haven't made it yet.

282
00:15:51,542 --> 00:15:54,276
You'd have to be psychic to get it,
and there's no such thing as "psychic."

283
00:15:54,396 --> 00:15:56,008
Sheldon, what's the point?!

284
00:15:56,242 --> 00:15:57,819
Just like Schrodinger's cat,

285
00:15:57,950 --> 00:16:00,495
your potential relationship
with Leonard right now

286
00:16:00,636 --> 00:16:02,611
can be thought of as both good and bad.

287
00:16:02,919 --> 00:16:06,273
It is only by opening the box
that you'll find out which it is.

288
00:16:07,473 --> 00:16:10,411
Okay, so you're saying I should
go out with Leonard.

289
00:16:13,889 --> 00:16:17,168
Let me start again.
In 1935, Erwin Schrodinger...

290
00:16:19,876 --> 00:16:21,380
Two seats right there.

291
00:16:33,305 --> 00:16:36,306
- Sheldon, I think I've made a mistake.
- I can see that.

292
00:16:36,446 --> 00:16:39,239
Unless you're planning on running
a marathon, choosing both stuffing

293
00:16:39,380 --> 00:16:41,810
and mashed potatoes
is a starch-filled redundancy.

294
00:16:42,647 --> 00:16:43,861
No, it's about Penny.

295
00:16:44,205 --> 00:16:46,225
A mistake involving Penny.

296
00:16:47,114 --> 00:16:48,983
You'll have to narrow it down.

297
00:16:50,208 --> 00:16:52,243
I don't think
I can go out with her tonight.

298
00:16:52,404 --> 00:16:53,530
Then don't.

299
00:16:54,380 --> 00:16:56,248
Other people would say "Why not?"

300
00:16:56,408 --> 00:16:58,443
Other people might be interested.

301
00:17:00,221 --> 00:17:02,616
- I'm going to talk anyway.
- I assumed you would.

302
00:17:04,111 --> 00:17:06,323
Now that I'm actually about
to go out with Penny,

303
00:17:06,443 --> 00:17:08,594
I'm not excited, I'm nauseous.

304
00:17:08,754 --> 00:17:10,731
Then your meal choice is appropriate.

305
00:17:10,851 --> 00:17:13,572
Starch absorbs fluid
which reduces the amount of vomit

306
00:17:13,692 --> 00:17:15,601
available for violent expulsion.

307
00:17:16,073 --> 00:17:16,802
Right.

308
00:17:16,922 --> 00:17:18,938
You also made a common
grammatical mistake.

309
00:17:19,087 --> 00:17:21,232
You said "nauseous,"
when you meant "nauseated."

310
00:17:22,597 --> 00:17:23,639
But go on.

311
00:17:24,649 --> 00:17:27,310
Sheldon, this date is probably
my one chance with Penny.

312
00:17:27,430 --> 00:17:29,065
What happens if I blow it?

313
00:17:29,642 --> 00:17:31,526
Well, if we accept your premise

314
00:17:31,646 --> 00:17:34,028
and also accept the highly
improbable assumption

315
00:17:34,148 --> 00:17:36,701
that Penny is the only woman
in the world for you,

316
00:17:36,821 --> 00:17:39,421
then we can logically conclude
that the result of blowing it

317
00:17:39,541 --> 00:17:42,902
would be that you end up a lonely,
bitter old man with no progeny.

318
00:17:43,258 --> 00:17:45,466
The image of any number
of evil lighthouse keepers

319
00:17:45,586 --> 00:17:47,814
from Scooby-Doo cartoons comes to mind.

320
00:17:48,750 --> 00:17:50,264
You're not helping.

321
00:17:50,384 --> 00:17:52,049
What response on my part

322
00:17:52,169 --> 00:17:54,430
would bring this conversation
to a speedy conclusion?

323
00:17:54,641 --> 00:17:56,999
Tell me whether or not
to go through with the date.

324
00:17:57,119 --> 00:17:58,769
Schrodinger's cat.

325
00:17:59,811 --> 00:18:01,426
That's brilliant.

326
00:18:02,990 --> 00:18:04,554
You sound surprised.

327
00:18:28,750 --> 00:18:30,618
- Come on in.
- Thank you.

328
00:18:32,854 --> 00:18:34,093
You look very nice.

329
00:18:34,213 --> 00:18:35,472
Thank you. So do you.

330
00:18:40,255 --> 00:18:41,812
I made an 8:00 reservation.

331
00:18:41,973 --> 00:18:43,963
Okay, yeah, great. Listen...

332
00:18:44,887 --> 00:18:46,358
Maybe we should talk first.

333
00:18:51,231 --> 00:18:54,652
But before you say anything, have you
ever heard of Schrodinger's cat?

334
00:18:57,254 --> 00:19:00,399
Actually, I've heard far too much
about Schrodinger's cat.

335
00:19:02,164 --> 00:19:03,030
Good.

336
00:19:09,612 --> 00:19:11,962
All right, the cat's alive.
Let's go to dinner.

337
00:19:25,584 --> 00:19:11,962
Crazy man. Call the police!

