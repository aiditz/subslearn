1
00:00:01,601 --> 00:00:04,362
So if a photon is directed
through a plane with two slits in it

2
00:00:04,485 --> 00:00:07,056
and either slit is observed,
it will not go through both slits.

3
00:00:07,178 --> 00:00:08,354
If it's unobserved, it will.

4
00:00:08,476 --> 00:00:10,635
However, if it's observed
after it's left the plane

5
00:00:10,758 --> 00:00:13,589
but before it hits its target,
it won't have gone through both slits.

6
00:00:13,753 --> 00:00:15,762
Agreed.
What's your point?

7
00:00:15,950 --> 00:00:19,140
There's no point, I just think
it's a good idea for a T-shirt.

8
00:00:24,579 --> 00:00:26,246
- Excuse me.
- Hang on.

9
00:00:30,629 --> 00:00:32,242
One across is "Aegean."

10
00:00:32,364 --> 00:00:33,759
Eight down is "Nabokov."

11
00:00:33,881 --> 00:00:36,232
26 across is "MCM."

12
00:00:36,504 --> 00:00:38,800
14 down is...
Move your finger...

13
00:00:39,687 --> 00:00:42,311
"Phylum" which makes 14 across
"Port-au-Prince"

14
00:00:43,951 --> 00:00:47,176
See, "Papa Doc's capitol idea,"
that's "Port-au-Prince."

15
00:00:48,677 --> 00:00:49,677
Haiti.

16
00:00:51,185 --> 00:00:52,771
- Can I help you?
- Yes.

17
00:00:55,673 --> 00:00:58,626
Is this the high-IQ sperm bank?

18
00:01:00,495 --> 00:01:03,188
If you have to ask,
maybe you shouldn't be here.

19
00:01:04,367 --> 00:01:05,939
I think this is the place.

20
00:01:07,149 --> 00:01:08,666
- Fill these out.
- Thank you.

21
00:01:09,001 --> 00:01:10,827
- We'll be right back.
- Take your time.

22
00:01:11,019 --> 00:01:13,178
I'll just finish my crossword puzzle.

23
00:01:14,479 --> 00:01:15,750
Oh, wait.

24
00:01:26,928 --> 00:01:28,965
Leonard, I don't think I can do this.

25
00:01:29,183 --> 00:01:31,499
What, are you kidding?
You're a semi-pro.

26
00:01:32,672 --> 00:01:35,009
No. We are committing genetic fraud.

27
00:01:35,186 --> 00:01:38,338
There's no guarantee that our sperm
is going to generate high-IQ offspring.

28
00:01:38,460 --> 00:01:41,362
Think about that. I have a sister with
the same basic DNA mix

29
00:01:41,484 --> 00:01:42,949
who hostesses at Fuddruckers.

30
00:01:44,382 --> 00:01:46,226
Sheldon, this was your idea.

31
00:01:46,473 --> 00:01:49,577
A little extra money to get fractional
T-1 bandwidth in the apartment.

32
00:01:49,699 --> 00:01:52,555
I know, and I do yearn
for faster downloads.

33
00:01:53,743 --> 00:01:56,755
But there's some poor woman
who's gonna pin her hopes on my sperm.

34
00:01:56,891 --> 00:01:59,709
What if she winds up with a toddler
who doesn't know if he should use

35
00:01:59,831 --> 00:02:03,276
an integral or a differential
to solve the area under a curve?

36
00:02:03,969 --> 00:02:06,567
- I'm sure she'll still love him.
- I wouldn't.

37
00:02:08,744 --> 00:02:10,235
Well, what do you want to do?

38
00:02:10,357 --> 00:02:11,357
I want to leave.

39
00:02:13,155 --> 00:02:14,494
What's the protocol for leaving?

40
00:02:14,847 --> 00:02:18,087
I don't know... I've never reneged
on a proffer of sperm before.

41
00:02:19,627 --> 00:02:21,076
Let's try just walking out.

42
00:02:33,238 --> 00:02:35,455
- Bye.
- Bye. Nice meeting you.

43
00:02:38,429 --> 00:02:40,151
Are you still mad about the sperm bank?

44
00:02:42,547 --> 00:02:45,459
You want to hear an interesting
thing about stairs?

45
00:02:45,927 --> 00:02:46,979
Not really.

46
00:02:47,691 --> 00:02:50,555
If the height of a single step
is off by as little as two millimeters,

47
00:02:50,692 --> 00:02:53,014
- most people will trip.
- I don't care.

48
00:02:54,900 --> 00:02:56,692
Two milli... That doesn't seem right.

49
00:02:56,814 --> 00:02:59,230
No, it's true, I did a series of
experiments when I was 12.

50
00:02:59,353 --> 00:03:00,881
My father broke his clavicle.

51
00:03:02,289 --> 00:03:04,159
Is that why they sent
you to boarding school?

52
00:03:04,281 --> 00:03:06,953
No. That was a result
of my work with lasers.

53
00:03:12,966 --> 00:03:13,991
New neighbor?

54
00:03:14,210 --> 00:03:15,210
Evidently.

55
00:03:15,661 --> 00:03:18,286
Significant improvement
over the old neighbor.

56
00:03:19,365 --> 00:03:21,470
200-pound transvestite
with a skin condition?

57
00:03:21,599 --> 00:03:22,599
Yes, she is.

58
00:03:25,078 --> 00:03:26,978
- Oh, hi.
- Hi.

59
00:03:31,861 --> 00:03:34,075
We don't mean to interrupt.
We live across the hall.

60
00:03:34,317 --> 00:03:35,998
Oh, that's nice.

61
00:03:37,051 --> 00:03:38,992
We don't live together. I mean...

62
00:03:39,697 --> 00:03:40,792
We live together,

63
00:03:40,914 --> 00:03:43,892
but in separate, heterosexual bedrooms.

64
00:03:45,324 --> 00:03:46,324
Okay, well...

65
00:03:46,513 --> 00:03:48,385
Guess I'm your new neighbor. Penny.

66
00:03:48,790 --> 00:03:50,430
- Leonard. Sheldon.
- Hi.

67
00:03:54,971 --> 00:03:56,010
Well...

68
00:03:56,889 --> 00:03:58,228
Welcome to the building.

69
00:03:58,655 --> 00:04:00,824
Thank you.
Maybe we can have coffee sometime.

70
00:04:00,946 --> 00:04:02,750
Great.

71
00:04:05,796 --> 00:04:08,037
- Well... bye.
- Bye.

72
00:04:12,010 --> 00:04:13,746
Should we have invited her for lunch?

73
00:04:14,208 --> 00:04:16,818
We're gonna start season two
of Battlestar Galactica.

74
00:04:17,442 --> 00:04:19,425
We already watched the season two DVDs.

75
00:04:19,547 --> 00:04:20,720
Not with commentary.

76
00:04:23,401 --> 00:04:25,357
I think we should be good neighbors

77
00:04:25,479 --> 00:04:27,364
and invite her over,
make her feel welcome.

78
00:04:27,879 --> 00:04:30,545
We never invited
Louie-slash-Louise over.

79
00:04:31,758 --> 00:04:33,399
And that was wrong of us.

80
00:04:33,521 --> 00:04:34,969
We need to widen our circle.

81
00:04:35,374 --> 00:04:37,506
I have a very wide circle.

82
00:04:38,400 --> 00:04:40,437
I have 212 friends on MySpace.

83
00:04:42,405 --> 00:04:44,564
Yes, and you've never met one of them.

84
00:04:45,310 --> 00:04:46,663
That's the beauty of it.

85
00:04:49,078 --> 00:04:50,677
I'm gonna invite her over.

86
00:04:51,178 --> 00:04:53,748
We'll have a nice meal and... chat.

87
00:04:53,955 --> 00:04:56,705
Chat? We don't chat.
At least not offline.

88
00:04:59,085 --> 00:05:00,413
It's not difficult.

89
00:05:00,535 --> 00:05:02,053
You just listen to what she says

90
00:05:02,175 --> 00:05:04,993
and then you say something
appropriate in response.

91
00:05:06,390 --> 00:05:07,390
To what end?

92
00:05:09,372 --> 00:05:10,976
Hi... again.

93
00:05:11,098 --> 00:05:12,971
Hi.

94
00:05:14,254 --> 00:05:15,279
Anyway...

95
00:05:15,853 --> 00:05:17,370
We brought home Indian food.

96
00:05:18,390 --> 00:05:19,390
And...

97
00:05:19,913 --> 00:05:22,087
I know that moving can be stressful,

98
00:05:22,213 --> 00:05:24,389
and I find that
when I'm undergoing stress,

99
00:05:24,511 --> 00:05:27,668
that good food and company
can have a comforting effect.

100
00:05:28,885 --> 00:05:32,030
Also, curry is a natural laxative,
and I don't have to tell you

101
00:05:32,152 --> 00:05:34,784
that, you know, a clean colon is just...

102
00:05:35,107 --> 00:05:36,898
one less thing to worry about.

103
00:05:38,685 --> 00:05:41,500
I'm no expert, but I believe
in the context of a luncheon invitation,

104
00:05:41,637 --> 00:05:44,089
you might want to skip the
reference to bowel movements.

105
00:05:44,700 --> 00:05:46,450
You're inviting me over to eat?

106
00:05:48,876 --> 00:05:50,830
Oh, that's so nice.
I'd love to.

107
00:05:51,007 --> 00:05:52,007
Great.

108
00:05:52,155 --> 00:05:54,505
So, what do you guys do
for fun around here?

109
00:05:55,296 --> 00:05:57,127
Today we tried masturbating for money.

110
00:06:01,940 --> 00:06:04,578
The Big Bang Theory
Season 1 Episode 01 - Pilot

111
00:06:04,892 --> 00:06:07,694
Transcript: swsub.com
Sync by Jesslataree & SerJo

112
00:06:25,059 --> 00:06:26,890
Okay, make yourself at home.

113
00:06:27,177 --> 00:06:28,177
Thank you.

114
00:06:28,502 --> 00:06:29,636
You're very welcome.

115
00:06:33,970 --> 00:06:35,708
This looks like some serious stuff.

116
00:06:35,830 --> 00:06:38,898
- Leonard, did you do this?
- Actually, that's my work.

117
00:06:40,855 --> 00:06:43,341
Yeah. Well, it's just
some quantum mechanics

118
00:06:43,464 --> 00:06:45,679
with a little string theory
doodling around the edges.

119
00:06:45,801 --> 00:06:47,298
That part there, that's just a joke.

120
00:06:47,421 --> 00:06:50,291
It's a spoof of the
Born-Oppenheimer approximation.

121
00:06:51,716 --> 00:06:55,037
So you're like one of those
Beautiful Mind genius guys.

122
00:06:58,795 --> 00:07:01,049
- This is really impressive.
- I have a board.

123
00:07:01,171 --> 00:07:03,248
If you like boards, this is my board.

124
00:07:04,343 --> 00:07:05,601
Holy smokes.

125
00:07:05,723 --> 00:07:07,186
If by "holy smokes", you mean

126
00:07:07,311 --> 00:07:09,170
a derivative restatement
of the kind of stuff

127
00:07:09,320 --> 00:07:12,357
you can find scribbled on the wall
of any men's room at MIT, sure.

128
00:07:13,262 --> 00:07:14,571
- What?
- Come on.

129
00:07:14,693 --> 00:07:18,301
Who hasn't seen this differential
below "Here I sit, broken-hearted"?

130
00:07:18,695 --> 00:07:21,225
At least I didn't have
to invent 26 dimensions

131
00:07:21,347 --> 00:07:22,615
just to make the math come out.

132
00:07:22,738 --> 00:07:24,188
I didn't invent them. They're there.

133
00:07:24,310 --> 00:07:27,430
- In what universe?
- In all of them... that is the point.

134
00:07:28,462 --> 00:07:29,870
Do you guys mind if I start?

135
00:07:30,471 --> 00:07:31,471
Penny...

136
00:07:32,334 --> 00:07:33,605
that's where l sit.

137
00:07:35,607 --> 00:07:36,769
So, sit next to me.

138
00:07:38,592 --> 00:07:40,013
No... I sit there.

139
00:07:41,665 --> 00:07:42,676
What's the difference?

140
00:07:42,812 --> 00:07:44,877
- What's the difference?
- Here we go.

141
00:07:45,672 --> 00:07:47,982
In the winter, that seat is
close enough to the radiator

142
00:07:48,105 --> 00:07:50,951
to remain warm, and yet not so
close as to cause perspiration

143
00:07:51,101 --> 00:07:53,466
In the summer, it's directly
in the path of a cross-breeze

144
00:07:53,616 --> 00:07:55,531
created by opening
windows there and there.

145
00:07:55,695 --> 00:07:57,274
It faces the television at an angle

146
00:07:57,396 --> 00:07:59,833
that is neither direct,
thus discouraging conversation,

147
00:07:59,993 --> 00:08:02,401
nor so far wide as to create
a parallax distortion.

148
00:08:02,523 --> 00:08:05,735
I could go on, but I think
I've made my point.

149
00:08:09,004 --> 00:08:10,289
Do you want me to move?

150
00:08:10,426 --> 00:08:12,325
- Well...
- Just sit somewhere else.

151
00:08:14,601 --> 00:08:15,601
Fine.

152
00:08:33,825 --> 00:08:34,891
Sheldon, sit!

153
00:08:40,518 --> 00:08:41,843
Well, this is nice.

154
00:08:42,048 --> 00:08:43,640
We don't have a lot of company over.

155
00:08:43,763 --> 00:08:46,478
That's not true. Koothrappali
and Wolowitz come over all the time.

156
00:08:46,601 --> 00:08:47,995
- I know, but...
- Tuesday night,

157
00:08:48,118 --> 00:08:49,854
we played Klingon Boggle
till 1:00 a.m.

158
00:08:49,976 --> 00:08:50,982
Yeah, I remember.

159
00:08:51,108 --> 00:08:52,943
- Don't say we don't have company.
- Sorry.

160
00:08:53,065 --> 00:08:55,973
- That has negative social implications.
- I said I'm sorry!

161
00:08:57,120 --> 00:08:58,120
So...

162
00:08:58,405 --> 00:08:59,581
Klingon Boggle?

163
00:09:00,014 --> 00:09:02,337
Yeah. It's like regular Boggle, but...

164
00:09:02,478 --> 00:09:03,613
in Klingon.

165
00:09:07,033 --> 00:09:09,930
That's probably enough about us.
So, tell us about you.

166
00:09:10,833 --> 00:09:12,050
Me? Okay.

167
00:09:12,541 --> 00:09:13,885
I'm a Sagittarius,

168
00:09:14,008 --> 00:09:16,322
which probably tells you
way more than you need to know.

169
00:09:16,459 --> 00:09:19,616
Yes. It tells us that you participate
in the mass cultural delusion

170
00:09:19,738 --> 00:09:21,022
that the sun's apparent position

171
00:09:21,145 --> 00:09:24,084
relative to arbitrarily defined
constellations at the time of your birth

172
00:09:24,207 --> 00:09:26,027
somehow affects your personality.

173
00:09:28,768 --> 00:09:30,121
Participate in the what?

174
00:09:30,624 --> 00:09:32,514
I think
what Sheldon's trying to say

175
00:09:32,637 --> 00:09:35,535
is that Sagittarius wouldn't have been
our first guess.

176
00:09:36,000 --> 00:09:38,186
Yeah, a lot of people
think I'm a water sign.

177
00:09:38,951 --> 00:09:40,766
Okay, let's see, what else.

178
00:09:40,888 --> 00:09:42,924
I'm a vegetarian.
Except for fish.

179
00:09:43,046 --> 00:09:45,096
And the occasional steak. I love steak!

180
00:09:47,742 --> 00:09:48,821
Well, that's interesting.

181
00:09:49,149 --> 00:09:50,972
Leonard can't process corn.

182
00:09:55,256 --> 00:09:57,511
Well... do you have some sort of a job?

183
00:09:58,558 --> 00:10:00,704
I'm a waitress
at The Cheesecake Factory.

184
00:10:01,292 --> 00:10:02,565
I love cheesecake.

185
00:10:02,687 --> 00:10:03,744
You're lactose-intolerant.

186
00:10:03,867 --> 00:10:06,305
I don't eat it...
I just think it's a good idea.

187
00:10:07,460 --> 00:10:09,744
Anyways, I'm also writing a screenplay.

188
00:10:09,866 --> 00:10:12,941
It's about this sensitive girl who comes
to L.A. from Lincoln, Nebraska,

189
00:10:13,063 --> 00:10:16,587
to be an actress and winds up
a waitress at The Cheesecake Factory.

190
00:10:18,213 --> 00:10:19,976
So, it's based on your life.

191
00:10:20,158 --> 00:10:21,251
No, I'm from Omaha.

192
00:10:25,503 --> 00:10:27,560
If that was movie, I would go see it.

193
00:10:27,682 --> 00:10:28,721
I know, right?

194
00:10:29,207 --> 00:10:31,025
Okay, let's see, what else...

195
00:10:32,385 --> 00:10:34,093
Guess that's about it.

196
00:10:34,649 --> 00:10:36,098
That's the story of Penny.

197
00:10:37,277 --> 00:10:38,794
It sounds wonderful.

198
00:10:40,164 --> 00:10:41,164
It was.

199
00:10:41,787 --> 00:10:44,255
Until I fell in love with a jerk!

200
00:10:48,040 --> 00:10:49,079
What's happening?

201
00:10:52,082 --> 00:10:54,392
God, you know,
4 years I lived with him.

202
00:10:54,514 --> 00:10:57,462
4 years... that's like
as long as high school.

203
00:10:57,584 --> 00:10:59,593
It took you 4 years
to get through high school?

204
00:11:02,280 --> 00:11:05,082
It just... I can't believe
I trusted him.

205
00:11:10,032 --> 00:11:12,642
Should I say something?
I feel like I should say something.

206
00:11:12,765 --> 00:11:14,460
You? No, you'll only make it worse.

207
00:11:14,611 --> 00:11:16,736
You want to know the most pathetic part?

208
00:11:16,907 --> 00:11:19,900
Even though I hate
his lying, cheating guts...

209
00:11:20,624 --> 00:11:22,162
I still love him.

210
00:11:23,187 --> 00:11:24,187
Is that crazy?

211
00:11:27,755 --> 00:11:30,161
No, it's not crazy. It's a...

212
00:11:31,120 --> 00:11:32,610
It's a paradox.

213
00:11:32,865 --> 00:11:36,364
Paradoxes are part of nature.
Think about light.

214
00:11:36,486 --> 00:11:38,960
If you look at Huygens, light is a wave,

215
00:11:39,082 --> 00:11:40,919
as confirmed
by the double-slit experiments,

216
00:11:41,041 --> 00:11:43,339
but then along comes Albert Einstein

217
00:11:43,461 --> 00:11:46,716
and discovers that light
behaves like particles, too.

218
00:11:49,307 --> 00:11:50,961
Well, I didn't make it worse.

219
00:11:53,334 --> 00:11:55,384
I'm so sorry. I'm such a mess.

220
00:11:55,808 --> 00:11:57,940
On top of everything else,
I'm all gross from moving

221
00:11:58,063 --> 00:11:59,649
and my stupid shower doesn't even work.

222
00:11:59,771 --> 00:12:00,837
Our shower works.

223
00:12:02,680 --> 00:12:04,632
Really? Would it be totally
weird if I used it?

224
00:12:04,754 --> 00:12:05,984
- Yes.
- No.

225
00:12:08,950 --> 00:12:10,327
It's right down the hall.

226
00:12:10,607 --> 00:12:11,607
Thanks.

227
00:12:11,866 --> 00:12:13,301
You guys are really sweet.

228
00:12:20,016 --> 00:12:22,462
Well, this is an
interesting development.

229
00:12:25,499 --> 00:12:26,499
How so?

230
00:12:26,661 --> 00:12:28,808
It has been some time
since we've had a woman

231
00:12:28,930 --> 00:12:30,611
take her clothes off in our apartment.

232
00:12:31,417 --> 00:12:33,234
That's not true.
Remember at Thanksgiving,

233
00:12:33,373 --> 00:12:35,791
my grandmother with Alzheimer's
had that episode?

234
00:12:37,904 --> 00:12:39,599
Point taken.
It has been some time

235
00:12:39,722 --> 00:12:41,567
since we've had a woman
take her clothes off,

236
00:12:41,690 --> 00:12:44,016
after which we didn't
want to rip our eyes out.

237
00:12:45,407 --> 00:12:48,304
The worst part was watching
her carve that turkey.

238
00:12:49,343 --> 00:12:52,200
So what exactly are you
trying to accomplish here?

239
00:12:53,136 --> 00:12:54,204
Excuse me?

240
00:12:54,477 --> 00:12:56,514
That woman is not
going to have sex with you.

241
00:12:56,737 --> 00:12:59,006
I'm not trying to have sex with her.

242
00:12:59,138 --> 00:13:01,133
Good. Then you won't be disappointed.

243
00:13:02,434 --> 00:13:04,718
What makes you think she
wouldn't have sex with me?

244
00:13:04,840 --> 00:13:06,766
I'm a male and she's a female.

245
00:13:07,247 --> 00:13:09,406
Yes, but not of the same species.

246
00:13:10,402 --> 00:13:12,740
I'm not going to engage
in hypotheticals here.

247
00:13:12,862 --> 00:13:14,612
I'm just trying to be a good neighbor.

248
00:13:15,008 --> 00:13:16,088
Of course.

249
00:13:16,758 --> 00:13:19,684
That's not to say that if a carnal
relationship were to develop,

250
00:13:19,806 --> 00:13:21,353
that I wouldn't participate.

251
00:13:23,708 --> 00:13:24,836
However briefly.

252
00:13:26,403 --> 00:13:28,719
Do you think this possibility
will be helped or hindered

253
00:13:28,856 --> 00:13:32,272
when she discovers your Luke Skywalker
no-more-tears shampoo?

254
00:13:33,536 --> 00:13:34,958
It's Darth Vader shampoo.

255
00:13:36,924 --> 00:13:38,837
Luke Skywalker's the conditioner.

256
00:13:41,434 --> 00:13:44,154
- Wait till you see this.
- It's fantastic, unbelievable.

257
00:13:44,277 --> 00:13:45,277
See what?

258
00:13:46,514 --> 00:13:50,643
It's a Stephen Hawking lecture
from MIT in 1974.

259
00:13:50,765 --> 00:13:51,855
This isn't a good time.

260
00:13:51,977 --> 00:13:55,025
It's before he became
a creepy computer voice.

261
00:13:58,400 --> 00:14:00,342
- That's great. You guys have to go.
- Why?

262
00:14:00,464 --> 00:14:01,661
It's just not a good time.

263
00:14:01,784 --> 00:14:03,375
Leonard has a lady over.

264
00:14:03,990 --> 00:14:06,477
Yeah, right... your
grandmother back in town?

265
00:14:09,757 --> 00:14:12,286
And she's not a lady.
She's just a new neighbor.

266
00:14:12,567 --> 00:14:14,972
Hang on, there really is a lady here?

267
00:14:16,169 --> 00:14:19,189
And you want us out because
you're anticipating coitus?

268
00:14:19,693 --> 00:14:21,337
I'm not anticipating coitus.

269
00:14:21,459 --> 00:14:22,756
So she's available for coitus?

270
00:14:22,878 --> 00:14:25,250
Can we please just stop saying "coitus"?

271
00:14:25,372 --> 00:14:28,153
Technically, that would
be"coitus interruptus. "

272
00:14:28,861 --> 00:14:31,922
Hey, is there a trick to getting it
to switch from tub to shower...?

273
00:14:32,527 --> 00:14:34,538
Hi. Sorry.
Hello.

274
00:14:36,657 --> 00:14:38,229
<i>Enchantй, mademoiselle.</i>

275
00:14:40,541 --> 00:14:43,617
Howard Wolowitz,
Caltech department of applied physics.

276
00:14:43,739 --> 00:14:45,871
You may be familiar
with some of my work.

277
00:14:45,993 --> 00:14:48,500
It's currently orbiting
Jupiter's largest moon

278
00:14:48,622 --> 00:14:51,054
taking high-resolution
digital photographs.

279
00:14:52,146 --> 00:14:54,456
Penny. I work at The Cheesecake Factory.

280
00:14:55,039 --> 00:14:56,870
I'll show you the trick with the shower.

281
00:14:57,406 --> 00:14:58,406
<i>Bonne douche.</i>

282
00:14:59,154 --> 00:15:00,548
I'm sorry?

283
00:15:00,727 --> 00:15:02,532
It's French for "good shower. "

284
00:15:02,654 --> 00:15:05,333
It's a sentiment I can express
in six languages.

285
00:15:06,213 --> 00:15:07,921
Save it for your blog, Howard.

286
00:15:16,019 --> 00:15:18,332
All right, there it goes.
It sticks. I'm sorry.

287
00:15:18,454 --> 00:15:20,002
- Okay, thanks.
- You're welcome.

288
00:15:20,124 --> 00:15:22,167
You're just gonna
to step right... Okay, I'll...

289
00:15:23,155 --> 00:15:25,807
- Hey, Leonard?
- The hair products are Sheldon's.

290
00:15:28,054 --> 00:15:29,663
Can I ask you a favor?

291
00:15:29,793 --> 00:15:30,793
A favor?

292
00:15:31,419 --> 00:15:34,521
Sure, you can ask me a favor.
I would do you a favor for you.

293
00:15:35,464 --> 00:15:36,852
It's okay if you say no.

294
00:15:37,073 --> 00:15:38,594
I'll probably say yes.

295
00:15:39,858 --> 00:15:43,151
It's just not the kind of thing
you ask a guy you just met.

296
00:15:51,100 --> 00:15:54,162
I really think we should examine
the chain of causality here.

297
00:15:54,291 --> 00:15:55,668
- Must we?
- Event A...

298
00:15:55,788 --> 00:15:57,899
A beautiful woman
stands naked in our shower.

299
00:15:58,032 --> 00:16:01,431
Event B... we drive halfway across town
to retrieve a television set

300
00:16:01,568 --> 00:16:03,448
from the aforementioned
woman's ex-boyfriend.

301
00:16:03,635 --> 00:16:05,728
Query... on what plane of existence

302
00:16:05,850 --> 00:16:09,061
is there even a semi-rational
link between these events?

303
00:16:10,221 --> 00:16:12,521
She asked me to do her a favor, Sheldon.

304
00:16:12,866 --> 00:16:15,366
Yes, well, that may be
the proximal cause of our journey,

305
00:16:15,489 --> 00:16:17,880
but we both know it only exists
in contradistinction

306
00:16:18,002 --> 00:16:19,862
to the higher level distal cause.

307
00:16:19,984 --> 00:16:22,233
- Which is?
- You think with your penis.

308
00:16:23,545 --> 00:16:27,060
That's a biological impossibility.
And you didn't have to come.

309
00:16:27,182 --> 00:16:30,194
Right, I could have stayed behind
and watch Wolowitz try to hit on Penny

310
00:16:30,317 --> 00:16:31,875
in Russian, Arabic and Farsi.

311
00:16:32,832 --> 00:16:34,554
Why can't she get her own TV?

312
00:16:34,745 --> 00:16:36,614
Come on, you know
how it is with breakups.

313
00:16:36,736 --> 00:16:38,847
No, I don't... and neither do you.

314
00:16:40,056 --> 00:16:41,520
I broke up with Joyce Kim.

315
00:16:41,642 --> 00:16:44,895
You did not break up with Joyce Kim.
She defected to North Korea.

316
00:16:45,182 --> 00:16:46,857
To mend her broken heart.

317
00:16:50,334 --> 00:16:52,985
This situation is much less complicated.

318
00:16:53,151 --> 00:16:55,735
There's some kind of dispute
between Penny and her ex-boyfriend

319
00:16:55,858 --> 00:16:57,470
as to who gets custody of the TV.

320
00:16:57,716 --> 00:16:59,733
She just wanted to avoid
having a scene with him.

321
00:16:59,855 --> 00:17:03,838
- So we get to have a scene with him?
- No, there's not going to be a scene.

322
00:17:04,207 --> 00:17:05,902
There's two of us and one of him.

323
00:17:06,189 --> 00:17:08,785
Leonard, the two of us
can't even carry a TV.

324
00:17:10,934 --> 00:17:13,818
So, you guys work with Leonard
and Sheldon at the university?

325
00:17:19,605 --> 00:17:21,611
I'm sorry, do you speak English?

326
00:17:22,448 --> 00:17:25,137
He speaks English.
He just can't speak to women.

327
00:17:26,022 --> 00:17:27,293
Really? Why?

328
00:17:27,771 --> 00:17:29,097
He's kind of a nerd.

329
00:17:32,073 --> 00:17:33,073
Juice box?

330
00:17:38,362 --> 00:17:39,482
I'll do the talking.

331
00:17:39,961 --> 00:17:42,770
- <i>Yeah?</i>
- Hi, I'm Leonard, this is Sheldon.

332
00:17:42,938 --> 00:17:44,516
- Hello.
- What did I just--

333
00:17:46,816 --> 00:17:49,649
- We're here to pick up Penny's TV.
- <i>Get lost.</i>

334
00:17:49,771 --> 00:17:50,946
Okay, thanks for you time.

335
00:17:51,519 --> 00:17:53,408
We're not going
to give up just like that.

336
00:17:53,535 --> 00:17:54,937
Leonard, the TV's in the building.

337
00:17:55,059 --> 00:17:57,855
We've been denied access
to the building, ergo, we are done.

338
00:17:58,102 --> 00:18:00,982
Excuse me. If I were to give up
on the first little hitch,

339
00:18:01,105 --> 00:18:03,749
I never would have identified
the fingerprints of string theory

340
00:18:03,871 --> 00:18:05,661
in the aftermath of the Big Bang.

341
00:18:07,002 --> 00:18:09,077
My apologies. What's your plan?

342
00:18:23,280 --> 00:18:25,809
It's just a privilege to watch
your mind at work.

343
00:18:27,004 --> 00:18:29,711
Come on, we have a combined IQ of 360.

344
00:18:29,833 --> 00:18:32,721
We should be able to figure out
how to get into a stupid building.

345
00:18:36,851 --> 00:18:38,450
What do you think their combined IQ is?

346
00:18:38,573 --> 00:18:39,573
Just grab the door!

347
00:18:42,297 --> 00:18:43,297
This is it.

348
00:18:44,858 --> 00:18:45,993
I'll do the talking.

349
00:18:46,246 --> 00:18:48,420
Good thinking.
I'll just be the muscle.

350
00:18:56,075 --> 00:18:57,075
Yeah?

351
00:18:57,269 --> 00:18:58,936
I'm Leonard, this is Sheldon.

352
00:18:59,401 --> 00:19:00,413
From the intercom.

353
00:19:02,449 --> 00:19:04,157
How the hell did you get
in the building?

354
00:19:05,564 --> 00:19:06,985
We're scientists.

355
00:19:09,363 --> 00:19:10,757
Tell him about our IQ.

356
00:19:23,666 --> 00:19:25,324
- Leonard...
- What?

357
00:19:25,446 --> 00:19:27,841
- My mom bought me those pants.
- I'm sorry.

358
00:19:29,332 --> 00:19:31,164
You're going to have to call her.

359
00:19:33,934 --> 00:19:36,709
Sheldon, I am so sorry
I dragged you through this.

360
00:19:36,960 --> 00:19:40,628
It's okay. It wasn't my first pantsing
and it won't be my last.

361
00:19:41,557 --> 00:19:43,021
And you were right about my motives.

362
00:19:43,143 --> 00:19:46,506
I was hoping to establish
a relationship with Penny

363
00:19:46,628 --> 00:19:48,636
that might have someday led to sex.

364
00:19:49,924 --> 00:19:51,701
Well, you got me out of my pants.

365
00:19:53,867 --> 00:19:55,548
Anyway, I've learned my lesson.

366
00:19:55,726 --> 00:19:57,504
She's out of my league,
I'm done with her.

367
00:19:57,626 --> 00:20:00,551
Got my work, one day
I'll win the Nobel Prize

368
00:20:00,673 --> 00:20:01,827
and then I'll die alone.

369
00:20:02,080 --> 00:20:04,952
Don't think like that.
You're not going to die alone.

370
00:20:05,179 --> 00:20:07,502
Thank you, Sheldon.
You're a good friend.

371
00:20:09,299 --> 00:20:12,289
And you're certainly not
going to win a Nobel Prize.

372
00:20:12,836 --> 00:20:16,404
This is one of my favorite places
to kick back after a quest.

373
00:20:16,744 --> 00:20:18,168
They have a great house ale.

374
00:20:18,763 --> 00:20:19,891
Cool tiger.

375
00:20:20,013 --> 00:20:21,978
Yeah, I've had him since level ten.

376
00:20:23,127 --> 00:20:24,453
His name is Buttons.

377
00:20:25,468 --> 00:20:27,698
Anyway, if you had
your own game character

378
00:20:27,820 --> 00:20:30,115
we could hang out, maybe go on a quest.

379
00:20:30,924 --> 00:20:32,905
That sounds interesting.

380
00:20:33,272 --> 00:20:34,284
You'll think about it?

381
00:20:34,612 --> 00:20:37,137
I don't think I'll be able
to stop thinking about it.

382
00:20:40,101 --> 00:20:41,153
Smooth.

383
00:20:43,493 --> 00:20:44,493
We're home.

384
00:20:45,083 --> 00:20:46,696
My God, what happened?

385
00:20:46,874 --> 00:20:48,931
Well, your ex-boyfriend
sends his regards

386
00:20:49,053 --> 00:20:51,827
and I think the rest
is fairly self-explanatory.

387
00:20:52,539 --> 00:20:55,805
I'm so sorry. I really thought
if you guys went instead of me

388
00:20:55,928 --> 00:20:57,187
he wouldn't be such an ass.

389
00:20:57,309 --> 00:20:59,296
No, it was a valid hypothesis.

390
00:20:59,418 --> 00:21:02,083
That was a valid...?
What is happening to you?

391
00:21:03,220 --> 00:21:06,806
Really, thank you so much
for going and trying, you're just...

392
00:21:07,557 --> 00:21:10,332
You're so terrific. Really.

393
00:21:11,649 --> 00:21:13,099
Why don't you put some clothes on,

394
00:21:13,221 --> 00:21:15,353
I'll get my purse,
and dinner is on me, okay?

395
00:21:15,478 --> 00:21:16,687
- Really? Great.
- Thank you.

396
00:21:23,223 --> 00:21:25,163
You're not done with her, are you?

397
00:21:26,569 --> 00:21:28,838
Are babies will be smart and beautiful.

398
00:21:31,548 --> 00:21:33,051
Not to mention imaginary.

399
00:21:39,044 --> 00:21:40,916
- Is Thai food okay with you, Penny?
- Sure.

400
00:21:41,038 --> 00:21:42,044
We can't have Thai food,

401
00:21:42,170 --> 00:21:43,807
- we had Indian for lunch.
- So?

402
00:21:44,164 --> 00:21:46,513
- They're both curry-based cuisines.
- So?

403
00:21:46,650 --> 00:21:48,495
It would be gastronomically redundant.

404
00:21:48,742 --> 00:21:52,538
I can see we're going to have
to spell out everything for this girl.

405
00:21:53,043 --> 00:21:54,260
Any ideas, Raj?

406
00:21:56,729 --> 00:21:58,892
Turn left on Lake Street
and head up to Colorado.

407
00:21:59,014 --> 00:22:01,767
I know a wonderful
little sushi bar that has karaoke.

408
00:22:01,893 --> 00:22:03,095
That sounds like fun.

409
00:22:16,529 --> 00:22:18,825
I don't know what your odds
are in the world as a whole,

410
00:22:18,962 --> 00:22:20,924
but as far as the population
of this car goes,

411
00:22:21,046 --> 00:22:20,924
you're a veritable mack daddy.

