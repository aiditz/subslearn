class Subtitles {
  constructor(elIndex) {
    this.elIndex = elIndex;

    this.index = 0;
    this.subtitles = [];

    this.filenameEl = $('#sub-filename-' + elIndex);
    this.displayEl = $('#sub-display-' + elIndex);

    this.filenameEl.on('change', async () => {
      await this.loadSubs();
    });

    this.filenameEl.on('keydown', e => e.preventDefault());
  }

  async init() {
    await this.loadSubs();
  }

  async loadSubs() {
    const content = await api('get', '/subtitles/getFile', {fileName: this.filename});

    this.subtitles = content.replace(/\r/g, "").split(/\n{2,}/g).filter(Boolean).map(item => {
      const lines = item.split(/\n/g);

      return {
        num: lines[0],
        time: lines[1],
        phrase: lines.slice(2).join('<br>')
      };
    });

    this.gotoPhrase(this.index);
  }

  gotoPhrase(index) {
    this.index = index;

    let displayText;

    if (index >= this.length) {
      displayText = '--- Phrase index out of bounds ---';
    } else {
      displayText = this.subtitles[index].phrase;
    }

    this.displayEl.html(displayText);
  }

  get filename() {
    return this.filenameEl.val();
  }

  get currentPhraseData() {
    return this.subtitles[this.index] || {};
  }

  get length() {
    return this.subtitles.length;
  }
}