class Pair {
  constructor(sub1, sub2) {
    this.index = 0;
    this.subs = [sub1, sub2];

    this.timeEl = $('#time');
  }

  async init() {
    this.index = 0;

    for (let sub of this.subs) {
      await sub.init();

      sub.filenameEl.on('change', () => {
        api('post', '/storage/set', {key: 'subs-name-' + sub.elIndex, value: sub.filename});
      });
    }
  }

  nextPhrase() {
    if (this.length <= this.index + 1) {
      return;
    }

    this.gotoPhrase(this.index + 1);
  }

  prevPhrase() {
    if (this.index === 0) {
      return;
    }

    this.gotoPhrase(this.index - 1);
  }

  gotoPhrase(index, {byUser = true} = {}) {
    const indexChanged = Number(index) !== Number(this.index);
    this.index = Number(index);

    this.subs.forEach(sub => sub.gotoPhrase(index));

    const data = this.subs[0].currentPhraseData;
    this.timeEl.text(`${data.num}/${this.subs[0].subtitles.length}`);

    if (indexChanged && byUser) {
      api('post', '/storage/set', {key: 'current-phrase-index', value: index});
    }
  }

  get length() {
    return this.subs[0].length;
  }
}

async function api(method, url, params) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: '/api' + url,
      method: method.toUpperCase(),
      dataType: 'json',
      data: params,
      success: resolve,
      error: reject
    });
  });
}